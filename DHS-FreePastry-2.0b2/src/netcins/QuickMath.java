package netcins;

public class QuickMath {
	public static double floor(double d) {
		return (double)((long)d);
	}
	
	public static double ceil(double d) {
		double f = floor(d);
		return (d == f ? f : f + 1);
	}
	
	public static double random() {
		return Math.random();
	}
}
