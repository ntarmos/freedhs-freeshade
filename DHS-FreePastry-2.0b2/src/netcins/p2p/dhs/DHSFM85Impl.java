/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Vector;

import netcins.dbms.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import rice.environment.Environment;

final class DHSFM85Impl extends DHS {

	protected DHSFM85Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Vector<String> metrics, Environment env) {
		super(sketchAlgo, numVecs, hashAlgo, L, retries, metrics, env);
		assert(sketchAlgo.isFM85());
	}

	protected DHSFM85Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Environment env) {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, new Vector<String>(), env);
	}
	
	protected DHSFM85Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, int L, int retries, Environment env) {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, env);
	}
	
	/*
	protected DHSFM85Impl(DHSSketchAlgorithms sketchAlgo, HashSketchImpl[] centralizedHS, int retries, Environment env) {
		super(sketchAlgo, centralizedHS, retries, env);
	}
	*/
	
	protected void insertBinarySearch(DHSPastryAppl sourceNode, String metric, ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}
	
	protected void insertClassic(DHSPastryAppl sourceNode, String metric, ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	protected DistributedEstimationResult[] estimatePartial(DHSPastryAppl sourceNode, Vector<String> metrics, int numBits, BigInteger[][] vecsValue, int hopCount, HashSet<Integer> bitsProbed) {
		if (bitsProbed == null)
			bitsProbed = new HashSet<Integer>();
		
		int bit;
		for (bit = 0; bit < numBits; bit ++) {
			if (bitsProbed.contains(bit)) {
				if (noMetricVecsSetForBit(vecsValue, bit) == true) {
				//if (vecsValue[bit].bitCount() == 0) {
					// DEBUG
					//System.out.print(" ^" + bit + ":0 ");
					break;
				} else {
					// DEBUG
					//System.out.print(" ~" + bit + ":0:" + vecsValue[bit].bitCount() + " ");
					continue;
				}
			}
			
			DHSBitProbeResult response = probe(sourceNode, metrics, bit);
			bitsProbed.add(bit);
			hopCount += response.hopCount;
		 
			if (response.resultData != null && response.resultData.length > 0) {
				for (int item = 0; item < response.resultData.length; item ++) {
					DHSTuple dhsTuple = response.resultData[item];
					int metricIndex = dhsMetrics.indexOf(dhsTuple.getMetric());
					distributedHS.get(metricIndex).setBit(dhsTuple.getVec(), dhsTuple.getBit());
					vecsValue[metricIndex][dhsTuple.getBit()] = vecsValue[metricIndex][dhsTuple.getBit()].setBit(dhsTuple.getVec());
				}
				if (noMetricVecsSetForBit(vecsValue, bit) == true) {
					//if (vecsValue[bit].bitCount() == 0) {
					// DEBUG
					//System.out.print("*");
					break;
				}
			} else {
				// DEBUG
				//System.out.print("@");
				break;
			}
			
			// DEBUG
			//System.out.print(" " + bit + ":" + vecsValue[bit].bitCount() + ":" + ((response.resultData != null) ? response.resultData.length : -1) + " ");
		}
		// DEBUG
		//System.out.println(" " + bit + ":" + vecsValue[(bit == numBits ? numBits - 1 : bit)].bitCount() + " done.");
		DistributedEstimationResult[] ret = new DistributedEstimationResult[metrics.size()];
		for (int i = 0; i < ret.length; i ++) {
			int metricIndex = dhsMetrics.indexOf(metrics.get(i));
			if ( metricIndex >= 0)
				ret[i] = new DistributedEstimationResult(distributedHS.get(metricIndex).estimate(SketchAlgorithms.FM85), hopCount, bitsProbed.size());
		}
		
		//return new DistributedEstimationResult(baseHashSketchImpl.estimate(SketchAlgorithms.FM85), hopCount, bitsProbed.size());
		return ret;
	}
}
