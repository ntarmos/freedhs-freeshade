/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.math.BigInteger;
import java.util.Hashtable;
import java.util.Vector;

import netcins.p2p.dhs.messaging.DHSBitProbeRequestMessage;
import netcins.p2p.dhs.messaging.DHSBitProbeResponseMessage;
import netcins.p2p.dhs.messaging.DHSInsertionRequestMessage;
import netcins.p2p.dhs.messaging.DHSInsertionResponseMessage;
import netcins.p2p.dhs.messaging.DHSMessage;
import netcins.p2p.dhs.messaging.DHSRequestMessage;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.IdRange;
import rice.pastry.NodeHandle;
import rice.pastry.PastryNode;
import rice.pastry.client.CommonAPIAppl;
import rice.pastry.messaging.Message;
import rice.pastry.routing.RouteMessage;

public class DHSPastryAppl extends CommonAPIAppl {
	private static final int myAddress = DHSMessage.getMyAddress();
	
	private PastryNode pastryNode = null;
	private DHS dhsImpl = null;
	
	private Vector<DHSTuple> bitData = null;
	private Vector<DHSTuple> localData = null;
	private int insertionHits = 0, queryHits = 0;
	
	private Hashtable<Long, DHSInsertionRequestMessage> pendingInsertions = null;
	private Hashtable<Long, DHSInsertionResponseMessage> insertionResponses = null;
	private Hashtable<Long, DHSBitProbeRequestMessage> pendingQueries = null;
	private Hashtable<Long, DHSBitProbeResponseMessage> queryResponses = null;
	
	public DHSPastryAppl(PastryNode pn, DHS dhsImpl) {
		super(pn);
		this.pastryNode = pn;
		this.dhsImpl = dhsImpl;
		this.address = myAddress;
		clearData();
	}
	
	public int getAddress() { return myAddress; }
	public PastryNode getPastryNode() { return pastryNode; }
	
	public void clearData() {
		this.bitData = new Vector<DHSTuple>();
		this.localData = new Vector<DHSTuple>();
		this.pendingInsertions = new Hashtable<Long, DHSInsertionRequestMessage>(1000);
		this.insertionResponses = new Hashtable<Long, DHSInsertionResponseMessage>(1000);
		this.pendingQueries = new Hashtable<Long, DHSBitProbeRequestMessage>(1000);
		this.queryResponses = new Hashtable<Long, DHSBitProbeResponseMessage>(1000);
		if (this.dhsImpl != null) {
			this.dhsImpl.clearHS();
		}
		this.insertionHits = this.queryHits = 0;
	}
	
	public boolean isNodeReady() { return pastryNode.isReady(); }
	
	public void waitForPastryNode() {
		synchronized (pastryNode) {
			while (pastryNode.isReady() == false) {
				try {
					pastryNode.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void addTuple(DHSTuple tuple) {
		localData.add(tuple);
	}
	
	public long insert(Id targetId, DHSTuple data) {
		DHSInsertionRequestMessage omsg = null;
		if (logger.level <= Logger.FINE)
			omsg = new DHSInsertionRequestMessage(data, pastryNode.getLocalHandle(), true);
		else
			omsg = new DHSInsertionRequestMessage(data, pastryNode.getLocalHandle(), false);
		omsg.setTargetId(targetId);
		pendingInsertions.put(omsg.getMsgId(), omsg);
		route(targetId, omsg);
		return omsg.getMsgId();
	}
	
	public Vector<DHSTuple> getLocalData() { return localData; }
	
	public long query(Id targetId, DHSTuple[] data) {
		DHSBitProbeRequestMessage omsg = null;
		
		if (logger.level <= Logger.FINE)
			omsg = new DHSBitProbeRequestMessage(data, pastryNode.getLocalHandle(), true);
		else
			omsg = new DHSBitProbeRequestMessage(data, pastryNode.getLocalHandle(), false);
		omsg.setTargetId(targetId);
		pendingQueries.put(omsg.getMsgId(), omsg);
		
		boolean overlap = false;
		for (int i = 0; overlap == false && i < data.length; i ++) {
			if (dhsImpl.idRangeOverlapsWithArc(new IdRange(pastryNode.getLeafSet().get(-1).getNodeId(), pastryNode.getLeafSet().get(1).getNodeId()), data[i].getBit()) == true) {
				overlap = true;
				if (logger.level <= Logger.FINER)
					logger.log("Client request remote query to id " + targetId + " for bit #" + data[i].getBit() + 
							" but I (" + pastryNode.getNodeId() + ") am in that arc too. Silently forcing target to local node...");
			}
		}
		if (overlap == true) {
			deliver(targetId, omsg);
		} else {
			route(targetId, omsg);
		}
		return omsg.getMsgId();
	}
	
	private void processDHSBitProbeRequestMessage(DHSBitProbeRequestMessage imsg) {
		DHSTuple[] dhsTuples = imsg.getData();
		Vector<String> metrics = new Vector<String>();
		
		for (DHSTuple tuple : dhsTuples)
			metrics.add(tuple.getMetric());
		
		Vector<DHSTuple> response = new Vector<DHSTuple>();
		int relevantResults[] = new int[metrics.size()];
		boolean resultVecs[][] = new boolean[metrics.size()][dhsImpl.getNumVecs()];
		
		for (DHSTuple tuple : bitData) {
			int metricIndex = metrics.indexOf(tuple.getMetric());
			if (metricIndex >= 0) {
				response.add(tuple);
				if (tuple.getBit() == dhsTuples[metricIndex].getBit()) {
					if (resultVecs[metricIndex][tuple.getVec()] == false) {
						resultVecs[metricIndex][tuple.getVec()] = true;
						relevantResults[metricIndex]++;
					}
				}
			}
		}
		
		BigInteger needMore = BigInteger.ZERO;
		for (int i = 0; i < relevantResults.length; i ++) {
			if (relevantResults[i] < dhsImpl.getNumVecs()) {
				needMore = needMore.setBit(i);
			}
		}
		
		DHSTuple[] ret = null;
		DHSBitProbeResponseMessage omsg = null;
		
		if (response.size() > 0) {
			ret = new DHSTuple[response.size()];
			response.copyInto(ret);
		}
		
		if (ret != null && needMore.equals(BigInteger.ZERO)) {
			omsg = new DHSBitProbeResponseMessage(ret, pastryNode.getLocalHandle(), imsg);
		} else {
			NodeHandle neighborCW = pastryNode.getLeafSet().get(1);
			NodeHandle neighborCWNext = pastryNode.getLeafSet().get(2);
			NodeHandle neighborCCW = pastryNode.getLeafSet().get(-1);
			NodeHandle neighborCCWPrev = pastryNode.getLeafSet().get(-2);
			
			assert(neighborCWNext == neighborCW.getLocalNode().getLeafSet().get(1));
			assert(neighborCCWPrev == neighborCCW.getLocalNode().getLeafSet().get(-1));
			
			IdRange rangeCCW = new IdRange(neighborCCWPrev.getNodeId(), pastryNode.getNodeId());
			IdRange rangeCW = new IdRange(pastryNode.getNodeId(), neighborCWNext.getNodeId());
			
			int numFalse = 0;
			for (int i = 0; i < metrics.size(); i ++)
				if (needMore.testBit(i) == true  && dhsImpl.idRangeOverlapsWithArc(rangeCCW, dhsTuples[i].getBit()) == false)
					numFalse ++;
			if (numFalse == needMore.bitCount())
				neighborCCW = null;
			
			numFalse = 0;
			for (int i = 0; i < metrics.size(); i ++)
				if (needMore.testBit(i) == true  && dhsImpl.idRangeOverlapsWithArc(rangeCW, dhsTuples[i].getBit()) == false)
					numFalse ++;
			if (numFalse == needMore.bitCount())
				neighborCW = null;
				
			omsg = new DHSBitProbeResponseMessage(ret, pastryNode.getLocalHandle(), imsg, neighborCW, neighborCCW);
		}
		omsg.setTargetId(imsg.getSenderId());
		
		route(imsg.getSenderId(), omsg, imsg.getSender());
	}
	
	synchronized private void processDHSBitProbeResponseMessage(DHSBitProbeResponseMessage imsg) {
		assert(pendingQueries.containsKey(imsg.getReqMsgId()) == true);
		DHSBitProbeRequestMessage reqMsg = pendingQueries.remove(imsg.getReqMsgId());
		DHSBitProbeResponseMessage resp = null;
		
		if (queryResponses.containsKey(imsg.getReqMsgId())) {
			resp = queryResponses.remove(imsg.getReqMsgId());
			resp.mergeRetryNodes(imsg);
			resp.mergeData(imsg);
			resp.addHops(imsg.getHops());
			if (resp.getData() != null && resp.getData().length == dhsImpl.getNumVecs()) {
				resp.setMustRetry(false);
				// DEBUG
				//System.out.print("*");
			}
		} else {
			resp = imsg;
		}
		
		if (resp.mustRetry() == true && reqMsg.incRetries() == dhsImpl.getRetries()) { //Bailing out...
			// DEBUG
			//System.out.print("#");
			resp.setMustRetry(false);
		}
		
		if (resp.mustRetry() == true && resp.hasRetryNode() == false) { // Also bailing out...
			// DEBUG
			//System.out.print("!");
			resp.setMustRetry(false);
		}
		
		queryResponses.put(resp.getReqMsgId(), resp);
		
		if (resp.mustRetry() == true) { //Retrying through successor probing...
			// DEBUG
			//System.out.print("_");
			NodeHandle targetHandle = resp.getRetryNode();
			reqMsg.setTargetId(targetHandle.getNodeId());
			reqMsg.clearHops();
			pendingQueries.put(reqMsg.getMsgId(), reqMsg);
			route((rice.pastry.Id)targetHandle.getNodeId(), reqMsg, targetHandle);
		} else {
			notify();
		}
	}
	
	private void processDHSInsertionRequestMessage(DHSInsertionRequestMessage imsg) {
		DHSTuple[] data = imsg.getData();
		assert(data.length == 1);
		int index = bitData.indexOf(data[0]);
		if (index < 0) {
			bitData.add(data[0]);
		} else if (bitData.get(index).getTimestamp() < data[0].getTimestamp()) {
			bitData.set(index, data[0]);
		}
		DHSInsertionResponseMessage omsg = new DHSInsertionResponseMessage(imsg.getData(), pastryNode.getLocalHandle(), imsg);
		omsg.setTargetId(imsg.getSenderId());
		route(imsg.getSenderId(), omsg, imsg.getSender());
	}
	
	synchronized private void processDHSInsertionResponseMessage(DHSInsertionResponseMessage imsg) {
		assert(pendingInsertions.containsKey(imsg.getReqMsgId()));
		DHSInsertionRequestMessage rmsg = pendingInsertions.remove(imsg.getReqMsgId());
		notify();
		//XXX: Commented out for memory space conservation reasons. However, a full-fledged implementation might need this.
		//insertionResponses.put(rmsg.getMsgId(), imsg);
	}
	
	public void deliver(rice.pastry.Id key, Message msg) {
		if (msg == null) {
			System.err.println("Null message ?!");
			return;
		}
		
		// DEBUG: Print extra information...
		if (logger.level <= Logger.FINE) {
			if (msg instanceof DHSMessage)
				logger.log("AP::  " + (DHSMessage)msg + " => " + getNodeId() + ".");
		}
		
		if (msg instanceof DHSBitProbeRequestMessage) {
			processDHSBitProbeRequestMessage((DHSBitProbeRequestMessage)msg);
			queryHits ++;
		} else if (msg instanceof DHSBitProbeResponseMessage) {
			processDHSBitProbeResponseMessage((DHSBitProbeResponseMessage)msg);
		} else if (msg instanceof DHSInsertionRequestMessage) {
			processDHSInsertionRequestMessage((DHSInsertionRequestMessage)msg);
			insertionHits ++;
		} else if (msg instanceof DHSInsertionResponseMessage) {
			processDHSInsertionResponseMessage((DHSInsertionResponseMessage)msg);
		} else {
			System.err.println("The thing that shouldn't be!");
		}
	}
	
	public int getInsertionHits() { return insertionHits; }
	public int getQueryHits() { return queryHits; }
	public boolean hasPendingInsertions() { return (pendingInsertions.size() > 0); }
	public int getNumPendingInsertions() { return pendingInsertions.size(); }
	public boolean hasResponseToInsertion(long msgId) { return insertionResponses.containsKey(msgId); }
	public void waitForInsertions() {
		synchronized (this) {
			while (hasPendingInsertions() == true) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void waitForInsertion(long queryId) {
		synchronized (this) {
			while (pendingInsertions.containsKey(queryId)) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public int getInsertionHops(long msgId) {
		DHSInsertionResponseMessage rmsg = insertionResponses.get(msgId);
		if (rmsg == null) return -1;
		return rmsg.getHopCount();
	}
	public DHSTuple[] getResponseToInsertion(long msgId) {
		DHSInsertionResponseMessage rmsg = insertionResponses.get(msgId);
		return (rmsg != null ? rmsg.getData() : null);
	}
	public DHSTuple[] removeResponseToInsertion(long msgId) {
		DHSInsertionResponseMessage rmsg = insertionResponses.remove(msgId);
		return (rmsg != null ? rmsg.getData() : null);
	}
	public int clearInsertionResponses() {
		int ret = insertionResponses.size();
		insertionResponses.clear();
		return ret;
	}
	public void clearHits() { this.insertionHits = this.queryHits = 0; }
	
	public boolean hasPendingQueries() { return (pendingQueries.size() > 0); }
	public int getNumPendingQueries() { return pendingQueries.size(); }
	public boolean hasResponseToQuery(long msgId) { return (queryResponses.containsKey(msgId) && !queryResponses.get(msgId).mustRetry()); }
	public int getQueryHopCount(long msgId) {
		DHSBitProbeResponseMessage rmsg = queryResponses.get(msgId);
		if (rmsg == null) return -1;
		return rmsg.getHopCount();
	}
	public Vector<Id> getQueryHops(long msgId) {
		DHSBitProbeResponseMessage rmsg = queryResponses.get(msgId);
		if (rmsg == null) return null;
		return rmsg.getHops();
	}
	public DHSTuple[] getResponseToQuery(long msgId) {
		DHSBitProbeResponseMessage rmsg = queryResponses.get(msgId);
		return (rmsg != null ? rmsg.getData() : null);
	}
	public void waitForQueries() {
		synchronized (this) {
			while (hasPendingQueries() == true) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void waitForQuery(long queryId) {
		synchronized (this) {
			while (hasResponseToQuery(queryId) == false) {
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public DHSTuple[] removeResponseToQuery(long msgId) {
		DHSBitProbeResponseMessage rmsg = queryResponses.remove(msgId);
		return (rmsg != null ? rmsg.getData() : null);
	}
	
	public Logger getLogger() { return logger; }
	
	public void setDHSImpl(DHS dhsImpl) {
		this.dhsImpl = dhsImpl;
	}
	
	public boolean isLeafSetComplete() {
		return pastryNode.getLeafSet().isComplete();
	}
	
	public void route(rice.pastry.Id key, Message msg) {
		route(key, msg, null);
	}
	
	@SuppressWarnings("deprecation")
	public void forward(RouteMessage rm) {
		Object o = rm.unwrap();
		if (o instanceof DHSRequestMessage && rm.nextHop != null && pastryNode.getNodeId().equals(rm.nextHop.getNodeId()) == false) {
			// DHSResponseMessage's are single-hop and thus ignored.
			NodeHandle sender = rm.getSender();
			
			((DHSMessage)o).addHop(rm.nextHop.getNodeId());
				
			// DEBUG: Print extra information...
			if (logger.level <= Logger.FINER)
				logger.log("AP::  " + o + " => " + rm.getNextHop().getNodeId() + ".");
				
			rm = new RouteMessage(rm.getTarget(), (DHSMessage)o, rm.nextHop, rm.getOptions());
			rm.setSender(sender);
		}
	}
	
	
	public void update(NodeHandle nh, boolean joined) {
	}
	
	public String toString() {
		return "[ DHSPastryAppl " + pastryNode + " :: " + dhsImpl + " ]";
	}
}
