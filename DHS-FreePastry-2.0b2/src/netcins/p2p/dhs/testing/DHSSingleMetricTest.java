/**
 * $Id$
 */

package netcins.p2p.dhs.testing;

import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.QuickMath;
import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.p2p.dhs.DHS;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithms;
import rice.environment.Environment;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.direct.BasicNetworkSimulator;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.standard.RandomNodeIdFactory;

public class DHSSingleMetricTest {
	private DHS dhsImpl = null;
	private PastryIdFactory idFactory = null;
	private PastryNodeFactory factory = null;
	private Environment env = null;
	private Vector<DHSPastryAppl> pastryAppls = null;
	private DHSSketchAlgorithms sketchAlgo = DHS.defaultSketchAlgo;
	private HashSketchImpl centralizedHashSketch = null;
	
	private int numItems = -1;
	private int numNodes = -1;
	private int numVecs = -1;
	private int L = -1;
	private int retries = -1;
	private int qNode = -1;
	
	private boolean isInited = false;
	private final Logger logger = Logger.getLogger("netcins.p2p.dhs.testing.DHSSingleMetricTest");
	private BasicNetworkSimulator simulator = null;
	
	public DHSSingleMetricTest(DHSSketchAlgorithms sketchAlgo, int numVecs, int L, int retries, Environment env) {
		init(sketchAlgo, numVecs, L, retries, env);
	}
	public DHSSingleMetricTest(int numVecs, int L, int retries, Environment env) {
		init(DHS.defaultSketchAlgo, numVecs, L, retries, env);
	}
	
	public DHSSingleMetricTest() {
		this(-1, -1, -1, null);
	}

	public void init(DHSSketchAlgorithms algorithm, int numVecs, int L, int retries, Environment env) {
		if (isInited) destroy();
		this.env = env;
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null) {
			destroy();
			return;
		}
		this.pastryAppls = new Vector<DHSPastryAppl>();
		this.idFactory = new rice.pastry.commonapi.PastryIdFactory(env);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(new RandomNodeIdFactory(env), simulator, env);
		this.centralizedHashSketch = new HashSketchImpl(numVecs, L);
		
		initDHSImpl(algorithm, numVecs, L, retries);
		Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting").setLevel(Level.INFO);
		isInited = true;
	}
	
	public void initDHSImpl(DHSSketchAlgorithms algorithm, int numVecs, int L, int retries) {
		Vector<String> metrics = new Vector<String>();
		metrics.add(DHS.defaultMetric);
		this.sketchAlgo = algorithm;
		this.numVecs = numVecs;
		this.L = L;
		this.retries = retries;
		this.dhsImpl = DHS.newInstance(algorithm, numVecs, L, retries, metrics, env);
		System.out.println("Initializing DHSSingleMetricTest with " + this.dhsImpl);
		for (DHSPastryAppl app : pastryAppls)
			app.setDHSImpl(this.dhsImpl);
	}
	
	/*
	public void initDHSImpl(DHSSketchAlgorithms algorithm, HashSketchImpl centralizedHS, int retries) {
		initDHSImpl(algorithm, centralizedHS.getNumVecs(), centralizedHS.getNumBits(), retries);
	}
	*/
	
	private void clearHits() {
		for (DHSPastryAppl app : pastryAppls)
			app.clearHits();
	}
	
	
	public boolean isInited() { return isInited; }
	
	public void destroy() {
		if (isInited == false) return;
		isInited = false;
		env.destroy();
		pastryAppls = null;
		env = null;
	}
	
	public void clearData() {
		if (isInited == false) return;
		for (DHSPastryAppl appl : pastryAppls) {
			appl.clearData();
		}
		clearHits();
		numItems = 0;
	}

	public void addNodes(int numNodes) {
		
		System.runFinalization();
		System.gc();
		long startTime = Calendar.getInstance().getTimeInMillis(), endTime = 0;
		
		if (isInited == false) throw new RuntimeException("Uninitialized test instance!");
		this.numNodes = numNodes;
		System.out.println("Populating the overlay with new nodes...");
		
		PastryNode bootstrapNode = null;
		for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0) { // Bootstrap node
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle) null);
				synchronized (node) { // Wait for the bootstrap to get ready...
					while (!node.isReady()) {
						try {
							node.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
			else { // Normal node
				node = factory.newNode(bootstrapNode.getLocalHandle());
			}
			
			logger.log(Level.FINER, "Finished creating new node " + node);
			
			DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);
			pastryAppls.add(dhsAppl);
			
			if ((curNodeIndex % 100) == 0)
				System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
			else if ((curNodeIndex % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + numNodes + "/" + numNodes + " done.");

		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
		for (int i = 0; i < pastryAppls.size(); i ++) {
			pastryAppls.get(i).waitForPastryNode();
			
			if ((i % 100) == 0)
				System.out.print(" " + i + "/" + numNodes + " ");
			else if ((i % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + pastryAppls.size() + "/" + numNodes + " done.");
		
		System.runFinalization();
		System.gc();
		endTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("DHS node generation took " + (endTime - startTime)/1000 + "\".");
		qNode = (int)QuickMath.floor(QuickMath.random() * pastryAppls.size());
	}
	
	public void addData(int[] tuples) {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = Calendar.getInstance().getTimeInMillis(), endTime = 0;
		
		if (isInited == false) throw new RuntimeException("Uninitialized test instance!");
		this.numItems = tuples.length;
		
		System.out.print("Injecting " + numItems + " new data tuples...");
		for (int data = 0; data < numItems; data ++) {
			dhsImpl.insert(pastryAppls.get(data % pastryAppls.size()), DHS.defaultMetric, new BasicItem(tuples[data]));
			centralizedHashSketch.insert(new BasicItem(tuples[data]));
		}
		System.out.println(" done.");
		System.out.print("Waiting for the data items to reach their destinations...");
		for (DHSPastryAppl curAppl : pastryAppls) {
			curAppl.waitForInsertions();
			curAppl.clearInsertionResponses();
		}
		
		System.out.println(" done.");

		endTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("DHS data generation took " + (endTime - startTime)/1000 + "\".");
		System.runFinalization();
		System.gc();
		long numHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls)
			numHits += curAppl.getInsertionHits();
		System.out.println("Node insertion hits: " + numHits);
	}
	
	public void query() {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = Calendar.getInstance().getTimeInMillis(), endTime;
		System.out.println("Querying node: " + pastryAppls.get(qNode).getNodeId());
		Vector<String> metrics = new Vector<String>();
		metrics.add(DHS.defaultMetric);
		System.out.println(sketchAlgo + " centralized estimation: " + centralizedHashSketch.estimate(sketchAlgo.isDF03() ? SketchAlgorithms.DF03 : SketchAlgorithms.FM85));
		System.out.println(sketchAlgo + " distributed estimation: " + dhsImpl.estimate(pastryAppls.get(qNode), metrics)[0]);
		//Vector<HashSketchImpl> hashSketches = dhsImpl.getHashSketches();
		//for (HashSketchImpl hs : hashSketches)
		//	System.out.println(sketchAlgo + " HS Differences :: \n" + hs.diff(centralizedHashSketch));
		System.runFinalization();
		System.gc();
		endTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("DHS query took " + (endTime - startTime)/1000 + "\".");
		long numHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls)
			numHits += curAppl.getQueryHits();
		System.out.println("Node query hits: " + numHits);
	}
	
	public static void main(String[] args) {
		try {
			
			int numNodes = Integer.parseInt(args[0]);
			int numTuples = Integer.parseInt(args[1]);
			int numVecs = Integer.parseInt(args[2]);
			int L = Integer.parseInt(args[3]);
			int retries = Integer.parseInt(args[4]);
			long startTime = Calendar.getInstance().getTimeInMillis(), endTime = 0;
			
			DHSSingleMetricTest test = new DHSSingleMetricTest();
			int seed = (int)QuickMath.floor(Math.random() * (double)Integer.MAX_VALUE);
				
			int[] data = new int[numTuples];
			for (int i = 0; i < numTuples; i ++)
				data[i] = i; //(int)QuickMath.floor(QuickMath.random() * (double)Integer.MAX_VALUE);
			
			System.out.println(" --- Begin Simulation --- ");
			System.out.println(" --- Seed: " + seed + " ---");
			try {
				test.init(null, numVecs, L, retries, Environment.directEnvironment(seed));
				test.addNodes(numNodes);
				System.out.println("\n ---\n");
				boolean firstRun = true;
				for (DHSSketchAlgorithms algo : DHSSketchAlgorithms.values()) {
					System.out.println("Testing " + algo + "...");
					test.initDHSImpl(algo, numVecs, L, retries);
					if (firstRun) {
						test.clearData();
						test.addData(data);
						firstRun = false;
					}
					test.query();
					System.out.println("\n ---\n");
				}
				test.destroy();
				endTime = Calendar.getInstance().getTimeInMillis();
				System.out.println(" Total Simulation Time: " + (endTime - startTime)/1000.0 + "\".");
				System.out.println(" --- End Simulation --- \n");
			} catch (Exception e) {
				e.printStackTrace();
				if (test != null) test.destroy();
				System.exit(-1);
			}
		} catch (Exception e) {
			System.out.println("Usage: java [-cp FreePastry-<version>.jar]");
			System.out.println("\tnetcins.p2p.dhs.testing.DHSSingleMetricTest <numNodes> <numItems> <numVecs> <L> <retries>");
			System.exit(0);
		}
	}
}
