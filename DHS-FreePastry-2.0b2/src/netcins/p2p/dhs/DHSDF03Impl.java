/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Vector;

import netcins.QuickMath;
import netcins.dbms.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import rice.environment.Environment;
import rice.pastry.Id;

final class DHSDF03Impl extends DHS {
	
	int vecsSetUpperLimit = 0;
	
	protected DHSDF03Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Vector<String> dhsMetrics, Environment env) {
		super(sketchAlgo, numVecs, hashAlgo, L, retries, dhsMetrics, env);
		assert(sketchAlgo.isDF03());
		this.vecsSetUpperLimit = (int)QuickMath.ceil((double)this.L * HashSketchImpl.df03VecsSetULPercentage);
	}
	
	protected DHSDF03Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Environment env) {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, new Vector<String>(), env);
	}
	
	protected DHSDF03Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, int L, int retries, Environment env) {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, new Vector<String>(), env);
	}
	
	/*
	protected DHSDF03Impl(DHSSketchAlgorithms sketchAlgo, HashSketchImpl[] centralizedHS, int retries, Environment env) {
		super(sketchAlgo, centralizedHS, retries, env);
		this.bitsUpperLimit = (int)QuickMath.ceil((double)this.numVecs * HashSketchImpl.df03VecsSetULPercentage);
	}
	*/
	
	protected DistributedEstimationResult[] estimatePartial(DHSPastryAppl sourceNode, Vector<String> metrics, int numBits, BigInteger[][] vecsValue, int hopCount, HashSet<Integer> bitsProbed) {
		if (sketchAlgo.usesBinarySearch() == false)
			return estimateClassicPartial(sourceNode, metrics, numBits, 0, vecsValue, hopCount, bitsProbed);
		return estimateBinarySearchPartial(sourceNode, metrics, numBits, 0, vecsValue, hopCount, bitsProbed);
	}

	private DistributedEstimationResult[] estimateClassicPartial(DHSPastryAppl sourceNode, Vector<String> metrics, int numBits, int endBit, BigInteger[][] vecsValue, int hopCount, HashSet<Integer> bitsProbed) {
		if (bitsProbed == null) {
			bitsProbed = new HashSet<Integer>();
		}
		
		int bit;
		//for (bit = numBits - 1; bit >= endBit && vecsValue[bit].bitCount() < vecsSetUpperLimit; bit --) {
		for (bit = numBits - 1; bit >= endBit && allMetricVecsSetToMoreThanForBit(vecsValue, vecsSetUpperLimit, bit) == false; bit --) {
			if (bitsProbed.contains(bit))
				continue;
			
			DHSBitProbeResult response = probe(sourceNode, metrics, bit);
			bitsProbed.add(bit);
			
			hopCount += response.hopCount;
			
			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item ++) {
					DHSTuple dhsTuple = response.resultData[item];
					int metricIndex = dhsMetrics.indexOf(dhsTuple.getMetric());
					distributedHS.get(metricIndex).setBit(dhsTuple.getVec(), dhsTuple.getBit());
					vecsValue[metricIndex][dhsTuple.getBit()] = vecsValue[metricIndex][dhsTuple.getBit()].setBit(dhsTuple.getVec());
				}
			}
			if (allMetricVecsSetToMoreThanForBit(vecsValue, vecsSetUpperLimit, bit) == true) {
			//if (vecsValue[bit].bitCount() >= vecsSetUpperLimit) {
				//System.out.print("*");
				break;
			}
			// DEBUG
			//if (bit == (L - 1))
			//	System.out.print(" " + L + ":" + vecsValue[bit].bitCount() + " ");
			//else 
			//	System.out.print(" " + bit + ":" + vecsValue[bit].bitCount() + " ");
		}
		//System.out.println(" " + (bit + 1) + ":" + vecsValue[bit + 1].bitCount() + " done.");
		DistributedEstimationResult[] ret = new DistributedEstimationResult[metrics.size()];
		for (int i = 0; i < ret.length; i ++) {
			int metricIndex = dhsMetrics.indexOf(metrics.get(i));
			if (metricIndex >= 0)
				ret[i] = new DistributedEstimationResult(distributedHS.get(metricIndex).estimate(SketchAlgorithms.DF03), hopCount, bitsProbed.size());
		}
		
		//return new DistributedEstimationResult(baseHashSketchImpl.estimate(SketchAlgorithms.DF03), hopCount, bitsProbed.size());
		return ret;
	}
	
	private DistributedEstimationResult[] estimateBinarySearchPartial(DHSPastryAppl sourceNode, Vector<String> metrics, int numBits, int endBit, BigInteger[][] vecsValue, int hopCount, HashSet<Integer> bitsProbed) {
		int bitLow = endBit, bitHigh = numBits;
		for (int curBit = (bitLow + (int)QuickMath.floor(QuickMath.random() * (bitHigh - bitLow))); bitLow != bitHigh - 1 && bitLow != bitHigh; curBit = sketchAlgo.nextBit(curBit, bitLow, bitHigh)) {
			
			DHSBitProbeResult response = probe(sourceNode, metrics, curBit);
			bitsProbed.add(curBit);
			hopCount += response.hopCount;
			
			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item ++) {
					DHSTuple dhsTuple = response.resultData[item];
					int metricIndex = dhsMetrics.indexOf(dhsTuple.getMetric());
					distributedHS.get(metricIndex).setBit(dhsTuple.getVec(), dhsTuple.getBit());
					//if (dhsTuple.getBit() == curBit) {
					//	if (curVecsValue[dhsTuple.getVec()] == false)
					//		curVecsSet ++;
					//	curVecsValue[dhsTuple.getVec()] = true;
					//}
					vecsValue[metricIndex][dhsTuple.getBit()] = vecsValue[metricIndex][dhsTuple.getBit()].setBit(dhsTuple.getVec());
				}
				if (allMetricVecsSetToMoreThanForBit(vecsValue, vecsSetUpperLimit, curBit) == true)
				//if (vecsValue[curBit].bitCount() >= vecsSetUpperLimit)
					bitLow = curBit;
				else
					bitHigh = curBit;
			} else {
				bitHigh = curBit;
			}
			//if (vecsValue[curBit].bitCount() >= vecsSetUpperLimit) System.out.print("@");
			//if (curBit == (L - 1))
			//	System.out.print(" " + L + ":" + vecsValue[curBit].bitCount() + " ");
			//else 
			//	System.out.print(" " + curBit + ":" + vecsValue[curBit].bitCount() + " ");
		}
		return estimateClassicPartial(sourceNode, metrics, numBits, bitLow, vecsValue, hopCount, bitsProbed);
	}

	protected void insertBinarySearch(DHSPastryAppl sourceNode, String metric, ItemSerializable object) {
		int[] bits = baseHashSketchImpl.tryAddObject(object);
		for (int bit = bits[1]; bit >= 0; bit --) {
			Id targetId = mapToNode(bit);
			DHSTuple tupleData = new DHSTuple(metric, bits[0], bit);
			Long msgId = sourceNode.insert(targetId, tupleData);
		}
		
		//centralizedHS.get(dhsMetrics.indexOf(metric)).insert(object);
		numItems ++;
	}
	
	protected void insertClassic(DHSPastryAppl sourceNode, String metric, ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}
}
