/**
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

public class DHSInsertionResponseMessage extends DHSResponseMessage {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1714809481776585036L;

	public DHSInsertionResponseMessage(DHSTuple[] data, NodeHandle sourceNode, DHSInsertionRequestMessage rmsg) {
		super(data, sourceNode, rmsg);
	}
}