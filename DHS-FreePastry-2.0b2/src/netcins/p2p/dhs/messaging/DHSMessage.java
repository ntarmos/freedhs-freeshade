/**
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import java.util.Vector;

import netcins.QuickMath;
import netcins.p2p.dhs.DHSTuple;
import rice.pastry.Id;
import rice.pastry.NodeHandle;
import rice.pastry.messaging.Message;

public abstract class DHSMessage extends Message {
	public static final boolean defaultStoreHops = false;
	private static final int myAddress = 0x09630234;
	
	private boolean storeHops = DHSMessage.defaultStoreHops;
	protected DHSTuple[] data = null;
	protected long msgId = -1;
	protected Vector<Id> hops = null;
	protected int hopCount = 0;
	protected Id targetId = null;

	protected DHSMessage(DHSTuple[] data, NodeHandle sourceNode, boolean storeHops) {
		super(myAddress);
		this.setSender(sourceNode);
		this.data = data;
		this.msgId = (long)QuickMath.floor(Math.random() * (double)Long.MAX_VALUE);
		this.storeHops = storeHops;
	}
	
	protected DHSMessage(DHSTuple data, NodeHandle sourceNode, boolean storeHops) {
		super(myAddress);
		this.setSender(sourceNode);
		this.data = new DHSTuple[1];
		this.data[0] = data;
		this.msgId = (long)QuickMath.floor(Math.random() * (double)Long.MAX_VALUE);
		this.storeHops = storeHops;
	}
	
	/*
	protected DHSMessage(DHSTuple data, NodeHandle sourceNode) {
		this(data, sourceNode, DHSMessage.defaultStoreHops);
	}
	*/
	
	public static int getMyAddress() { return myAddress; }
	
	public int getHopCount() { return (storeHops ? (hops != null ? hops.size() : 0) : hopCount); }
	public void addHop(Id id) {
		if (storeHops) {
			if (hops == null)
				hops = new Vector<Id>();
			hops.add(id);
		}
		hopCount ++;
	}
	public void addHops(Vector<Id> hops) {
		if (storeHops) {
			if (hops == null || hops.size() == 0) return;
			if (this.hops == null) this.hops = new Vector<Id>();
			this.hops.addAll(hops);
		}
	}
	public void addHops(int hops) {
		this.hopCount += hops;
	}
	public void clearHops() { hops = null; hopCount = 0; }
	public boolean getStoreHops() { return storeHops; }
	
	public Vector<Id> getHops() { return hops; }
	
	public DHSTuple[] getData() { return data; }
	public void setData(DHSTuple[] data) { this.data = data; }
	
	public Long getMsgId() { return msgId; }
	protected void setMsgId(Long msgId) { this.msgId = msgId; }
	
	public Id getTargetId() { return targetId; }
	public void setTargetId(Id targetId) { this.targetId = targetId; }
	
	public String toString() {
		return "[ " + this.getClass().getSimpleName() + "." + msgId + " :: " + getSenderId() + " => " + getTargetId() + " ]";
	}
}