/**
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.p2p.commonapi.Id;
import rice.pastry.NodeHandle;

public abstract class DHSResponseMessage extends DHSMessage {
	private Long reqMsgId = null;
	
	protected DHSResponseMessage(DHSTuple[] data, NodeHandle sourceNode, DHSRequestMessage reqMsg) {
		super(data, sourceNode, reqMsg.getStoreHops());
		this.reqMsgId = reqMsg.getMsgId();
		this.hopCount = reqMsg.hopCount;
		super.addHops(reqMsg.getHops());
	}
	
	public Long getReqMsgId() { return reqMsgId; }
	public void incHopCount() { }
	public void addHop(Id hop) {}
	public String toString() {
		return "[ " + this.getClass().getSimpleName() + "." + /*msgId + "/" +*/ reqMsgId + " :: " + getSenderId() + " => " + getTargetId() + " ]";
	}
}