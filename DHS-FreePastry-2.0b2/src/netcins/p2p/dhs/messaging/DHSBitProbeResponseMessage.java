/**
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import java.util.Arrays;
import java.util.HashSet;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

public class DHSBitProbeResponseMessage extends DHSResponseMessage {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4592031860756380066L;
	NodeHandle retryNodeCW = null;
	NodeHandle retryNodeCCW = null;
	private boolean goingCW = true;
	private boolean mustRetry = false;
	
	public DHSBitProbeResponseMessage(DHSTuple[] data, NodeHandle sourceNode, DHSBitProbeRequestMessage reqMsg) {
		this(data, sourceNode, reqMsg, null, null);
	}
	
	public DHSBitProbeResponseMessage(DHSTuple[] data, NodeHandle sourceNode, DHSBitProbeRequestMessage reqMsg, NodeHandle retryNodeCW, NodeHandle retryNodeCCW) {
		super(data, sourceNode, reqMsg);
		this.retryNodeCW = retryNodeCW;
		this.retryNodeCCW = retryNodeCCW;
		this.goingCW = (retryNodeCW != null);
		this.mustRetry = (this.retryNodeCW != null || this.retryNodeCCW != null);
	}
	public void mergeRetryNodes(DHSBitProbeResponseMessage msg) {
		if (goingCW == true && this.retryNodeCW == null)
			this.retryNodeCW = msg.retryNodeCW;
		if (goingCW == false && this.retryNodeCCW == null)
			this.retryNodeCCW = msg.retryNodeCCW;
		this.goingCW = (this.goingCW ? (this.retryNodeCW != null) : (this.retryNodeCCW == null ? this.goingCW : false));
	}
	public boolean hasRetryNode() { return (retryNodeCW != null || retryNodeCCW != null); }
	public NodeHandle getRetryNode() {
		NodeHandle ret = (goingCW ? retryNodeCW : retryNodeCCW);
		
		if (ret == null) {
			goingCW = !goingCW;
			ret = (goingCW ? retryNodeCW : retryNodeCCW);
		}
		
		assert(hasRetryNode() == true && ret != null);
		
		if (ret != null) {
			if (goingCW) retryNodeCW = null;
			else retryNodeCCW = null;
		}
		
		return ret;
	}
	NodeHandle[] getRetryNodes() {
		NodeHandle[] ret = new NodeHandle[2];
		ret[0] = retryNodeCW;
		ret[1] = retryNodeCCW;
		return ret;
	}
	public boolean mustRetry() { return mustRetry; }
	public void setMustRetry(boolean mustRetry) { this.mustRetry = mustRetry; }
	
	public void mergeData(DHSBitProbeResponseMessage imsg) {
		DHSTuple[] iData = imsg.getData();
		HashSet<DHSTuple> merged = new HashSet<DHSTuple>();
		if (data != null)
			merged.addAll(Arrays.asList(data));
		if (iData != null)
			merged.addAll(Arrays.asList(iData));
		
		if (merged.size() > 0) {
			data = new DHSTuple[merged.size()];
			merged.toArray(data);
		} else {
			data = null;
		}
	}
	
	/*
	public void mergeData(DHSBitProbeResponseMessage imsg) {
		DHSTuple[] iData = imsg.getData();
		Vector<DHSTuple> merged = new Vector<DHSTuple>();
		for (int i = 0; data != null && i < data.length; i ++)
			if (merged.contains(data[i]) == false)
				merged.add(data[i]);
		for (int i = 0; iData != null && i < iData.length; i ++)
			if (merged.contains(iData[i]) == false)
				merged.add(iData[i]);
		if (merged.size() > 0) {
			data = new DHSTuple[merged.size()];
			merged.copyInto(data);
		}
		else
			data = null;
	}
	*/
	
	public String toString() {
		return super.toString() + "[ GCW: " + goingCW + " , retry: " + mustRetry +
		" , CCW: " +  (retryNodeCCW != null ? retryNodeCCW.getNodeId() : "<null>") +
		" , CW: " +   (retryNodeCW != null ? retryNodeCW.getNodeId() : "<null>") + " ]";
	}
}
