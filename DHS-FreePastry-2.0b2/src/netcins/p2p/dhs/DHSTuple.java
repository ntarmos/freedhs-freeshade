/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.io.IOException;
import java.util.Calendar;

import netcins.dbms.ItemSerializable;

import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;
import rice.p2p.util.rawserialization.SimpleOutputBuffer;

public class DHSTuple implements ItemSerializable {
	private String metric = null;
	private int bit = 0;
	private int vec = 0;
	private long timestamp = 0;
		
	public DHSTuple(String metric, int vec, int bit, long timestamp) {
		this.metric = metric;
		this.bit = bit;
		this.vec = vec;
		this.timestamp = timestamp;
	}
	
	public DHSTuple(String metric, int vec, int bit) {
		this(metric, vec, bit, Calendar.getInstance().getTimeInMillis());
	}
	
	public DHSTuple(InputBuffer buf) throws IOException {
		this.deserialize(buf);
	}
	
	public void serialize(OutputBuffer buf) throws IOException {
		buf.writeUTF(metric);
		buf.writeInt(vec);
		buf.writeInt(bit);
		buf.writeLong(timestamp);
	}
	
	public void deserialize(InputBuffer buf) throws IOException {
		metric = buf.readUTF();
		vec = buf.readInt();
		bit = buf.readInt();
		timestamp = buf.readLong();
	}
	
	public byte[] toByteArray() {
		SimpleOutputBuffer ob = new SimpleOutputBuffer();
		try {
			this.serialize(ob);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return ob.getBytes();
	}
	
	public boolean equals(Object o) {
		if (o instanceof DHSTuple) {
			DHSTuple t = (DHSTuple)o;
			return (((metric == null && t.metric == null) || metric.equals(t.metric)) && bit == t.bit && vec == t.vec);
		}
		return false;
	}
	
	public String getMetric() { return metric; }
	public int getBit() { return bit; }
	public int getVec() { return vec; }
	public long getTimestamp() { return timestamp; }
	public void setTimestamp(long timestamp) { this.timestamp = timestamp; }
	public String toString() { return "[ " + metric + " :: " + vec + " : " + bit + " ]"; }
}
