package netcins.p2p.dhs;

import netcins.QuickMath;

/**
 * @author ntarmos
 *
 */
public enum DHSSketchAlgorithms {
	FM85Classic,
	DF03Classic,
	FM85BinarySearch,
	DF03BinarySearch;
	
	public boolean isDF03() {
		return this.equals(DHSSketchAlgorithms.DF03Classic) ||
		this.equals(DHSSketchAlgorithms.DF03BinarySearch);
	}
	
	public boolean isFM85() {
		return this.equals(DHSSketchAlgorithms.FM85Classic) ||
		this.equals(DHSSketchAlgorithms.FM85BinarySearch);
	}
	
	public boolean usesBinarySearch() {
		return  this.equals(DHSSketchAlgorithms.FM85BinarySearch) ||
		this.equals(DHSSketchAlgorithms.DF03BinarySearch);
	
	}
	
	public String toString() {
		String ret = null;
		switch (this) {
		case FM85Classic: ret = "FM85/Classic"; break;
		case FM85BinarySearch: ret = "FM85/Binary Search"; break;
		case DF03Classic: ret = "DF03/Classic"; break;
		case DF03BinarySearch: ret = "DF03/Binary Search"; break;
		}
		return ret;
	}
	
	public int nextBit(int curBit, int low, int high) {
		int ret = 0;
		switch (this) {
		case FM85Classic: ret = curBit + 1; break;
		case DF03Classic: ret = curBit - 1; break;
		case FM85BinarySearch:
		case DF03BinarySearch: ret = low + (int)QuickMath.floor((double)(high - low)/2.0); break;
		}
		return ret;
	}
}