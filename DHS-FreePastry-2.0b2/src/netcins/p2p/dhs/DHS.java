/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Random;
import java.util.Vector;

import netcins.QuickMath;
import netcins.dbms.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.DistributedSketch;
import rice.environment.Environment;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.IdRange;
import rice.pastry.commonapi.PastryIdFactory;

public abstract class DHS implements DistributedSketch {
	
	public static final int defaultNumVecs = 256;
	public static final int defaultRetries = 5;
	public static final String defaultMetric = "Default Metric";
	public static final DHSSketchAlgorithms defaultSketchAlgo = DHSSketchAlgorithms.FM85BinarySearch;
	
	protected static final int intsPerId = Id.IdBitLength / 32;
	
	protected int L = -1;
	protected int numVecs = -1;
	protected String hashAlgo = null;
	protected DHSSketchAlgorithms sketchAlgo;
	
	protected HashSketchImpl baseHashSketchImpl = null;
	protected Vector<HashSketchImpl> distributedHS = null;
	protected Vector<String> dhsMetrics = null;
	protected PastryIdFactory idFactory = null;
	protected Environment env = null;
	protected Random rng = null;
	
	protected int retries = 0, numItems = 0;
	
	
	/////////////////////
	// Private methods //
	/////////////////////
	
	private final DistributedEstimationResult[] estimateBinarySearch(DHSPastryAppl sourceNode, Vector<String> metrics) {
		BigInteger[][] vecsValue = new BigInteger[metrics.size()][L];
		for (int metric = 0; metric < metrics.size(); metric ++)
			for (int bit = 0; bit < L; bit ++)
				vecsValue[metric][bit] = BigInteger.ZERO;
		int hopCount = 0;
		HashSet<Integer> bitsProbed = new HashSet<Integer>();
		
		for (HashSketchImpl h : distributedHS)
			h.clear();
		
		// Binary search for the highest bit position for which all vectors have a 0-bit...
		int bitLow = 0, bitHigh = L;
		for (int curBit = (bitLow + (int)QuickMath.floor(QuickMath.random() * (bitHigh - bitLow))); bitLow != bitHigh - 1 && bitLow != bitHigh; curBit = sketchAlgo.nextBit(curBit, bitLow, bitHigh)) {
			assert(bitsProbed.contains(curBit) == false);
			
			if (allMetricVecsSetForBit(vecsValue, curBit) == true) {
				bitLow = curBit;
				continue;
			}
			
			DHSBitProbeResult response = probe(sourceNode, metrics, curBit);
			bitsProbed.add(curBit);
			hopCount += response.hopCount;
			
			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item ++) {
					DHSTuple dhsTuple = response.resultData[item];
					int metricIndex = dhsMetrics.indexOf(dhsTuple.getMetric());
					distributedHS.get(metricIndex).setBit(dhsTuple.getVec(), dhsTuple.getBit());
					vecsValue[metricIndex][dhsTuple.getBit()] = vecsValue[metricIndex][dhsTuple.getBit()].setBit(dhsTuple.getVec());
				}
				if (noMetricVecsSetForBit(vecsValue, curBit) == false)
					bitLow = curBit;
				else
					bitHigh = curBit;
			} else {
				bitHigh = curBit;
			}
			// DEBUG
			//if (vecsValue[curBit].bitCount() == numVecs) System.out.print("@");
			//if (curBit == (L - 1))
			//	System.out.print(" " + L + ":" + vecsValue[curBit].bitCount() + " ");
			//else 
			//	System.out.print(" " + curBit + ":" + vecsValue[curBit].bitCount() + " ");
		}
		
		// ... then revert to the standard probing procedure, with the above position as the starting point.
		return estimatePartial(sourceNode, metrics, bitLow, vecsValue, hopCount, bitsProbed);
	}
	
	private final DistributedEstimationResult[] estimateClassic(DHSPastryAppl sourceNode, Vector<String> metrics) {
		for (HashSketchImpl hs : distributedHS)
			hs.clear();
		BigInteger[][] vecsValue = new BigInteger[metrics.size()][this.L];
		for (int metric = 0; metric < vecsValue.length; metric ++)
			for (int bit = 0; bit < L; bit ++)
				vecsValue[metric][bit] = BigInteger.ZERO;
		return estimatePartial(sourceNode, metrics, L, vecsValue, 0, null);
	}
	
	
	///////////////////////
	// Protected methods //
	///////////////////////
	
	protected boolean allMetricVecsSetForBit(BigInteger[][] vecsValue, int bit) {
		return allMetricVecsSetToForBit(vecsValue, numVecs, bit);
	}
	
	protected boolean noMetricVecsSetForBit(BigInteger[][] vecsValue, int bit) {
		return allMetricVecsSetToForBit(vecsValue, 0, bit);
	}
	
	protected boolean allMetricVecsSetToMoreThanForBit(BigInteger[][] vecsValue, int threshold, int bit) {
		BigInteger tmpVecsSet = BigInteger.ZERO;
		for (int metric = 0; metric < vecsValue.length; metric ++) {
			if (vecsValue[metric][bit].bitCount() >= threshold)
				tmpVecsSet = tmpVecsSet.setBit(metric);
		}
		return (tmpVecsSet.bitCount() == vecsValue.length);
	}
	
	protected boolean allMetricVecsSetToForBit(BigInteger[][] vecsValue, int value, int bit) {
		BigInteger tmpVecsSet = BigInteger.ZERO;
		for (int metric = 0; metric < vecsValue.length; metric ++) {
			if (vecsValue[metric][bit].bitCount() == value)
				tmpVecsSet = tmpVecsSet.setBit(metric);
		}
		return (tmpVecsSet.bitCount() == vecsValue.length);
	}
	
	private IdRange getIdRangeForBit(int r) {
		return new IdRange(Id.build(thr(r)), Id.build(thr(r - 1)));
	}
	
	private Id getRandomIdForBit(int r) {
		int index = Id.IdBitLength - r - 1;
		
		assert(r >= -1 && r < L && index < Id.IdBitLength);
		
		byte rnd[] = new byte[(int)QuickMath.ceil((double)index / 8.0)];
		byte id[] = new byte[Id.IdBitLength / 8];
		rng.nextBytes(rnd);
		byte mask = 0x00;
		for (int bit = 0; bit <= index % 8; bit ++) {
			mask |= (0x01 << bit);
		}
		rnd[rnd.length - 1] &= mask;
		for (int i = 0; i < rnd.length ; i ++)
			id[i] = rnd[i];
		return Id.build(id);
	}
	
	private int[] thr(int r) {
		assert(r >= -1 && r < L);
		int id[] = new int[intsPerId];
		int index = Id.IdBitLength - r - 1;
		if (index < Id.IdBitLength)
			id[(int)QuickMath.floor(index / 32)] = (1 << (index % 32));
		else
			for (int i = 0; i < intsPerId; i ++)
				id[i] = 0xffffffff;
		return id;
	}
	
	protected DHSBitProbeResult probe(DHSPastryAppl sourceNode, Vector<String> metrics, int bit) {
		DHSTuple[] queryTuples = new DHSTuple[metrics.size()];
		for (int i = 0; i < queryTuples.length; i ++)
			queryTuples[i] = new DHSTuple(metrics.get(i), -1, bit);
		
		long msgId = sourceNode.query(mapToNode(bit), queryTuples);
		sourceNode.waitForQuery(msgId);

		int numHops = sourceNode.getQueryHopCount(msgId);
		assert(numHops >= 0);
		
		// DEBUG: print intermediate hops, as stored in query packet...
		Logger logger = sourceNode.getLogger();
		if (logger.level <= Logger.FINE) {
			Vector<Id> hops = sourceNode.getQueryHops(msgId);
			StringBuilder sb = new StringBuilder();
			if (hops != null && hops.size() > 0) {
				sb.append("\t" + hops.get(0));
				for (int hop = 1; hop < hops.size(); hop ++)
					sb.append(" => " + hops.get(hop));
				sb.append("\n");
			}
			logger.log("Query " + msgId + ": " + numHops + " hops :: " + sb.toString());
		}
			
		return new DHSBitProbeResult(sourceNode.removeResponseToQuery(msgId), numHops);
	}
	
	
	//////////////////////////////
	// Final non-public methods //
	//////////////////////////////
	
	protected final void basicInsert(DHSPastryAppl sourceNode, String metric, ItemSerializable object) {
		int[] bits = baseHashSketchImpl.tryAddObject(object);
		Id targetId = mapToNode(bits[1]);
		DHSTuple tupleData = new DHSTuple(metric, bits[0], bits[1]);
		Long msgId = sourceNode.insert(targetId, tupleData);
		
		numItems ++;
	}
	
	/*
	protected final void insertClassic(DHSPastryAppl sourceNode, String metric, DHSTuple dhsTuple) {
		basicInsert(sourceNode, metric, dhsTuple);
	}
	
	protected final void insertBinarySearch(DHSPastryAppl sourceNode, String metric, DHSTuple dhsTuple) {
		insertBinarySearch(sourceNode, metric, dhsTuple);
	}
	*/
	
	
	/////////////////////////////
	// Package-access methods //
	/////////////////////////////
	
	boolean idRangeOverlapsWithArc(IdRange range, int l) {
		assert(l >= 0 && l < L);
		return (!range.intersect(getIdRangeForBit(l)).isEmpty());
		/*
		Id idRangeHigh = range.getCW(), idRangeLow = range.getCCW();
		Id idLow = thr(l), idHigh = thr(l - 1);
		
		if (idLow.equals(idHigh))
			return idLow.isBetween(idRangeLow, idRangeHigh);

		boolean iccwIn = idLow.isBetween(idRangeLow, idRangeHigh) || idLow.equals(idRangeLow);
		boolean icwIn = idLow.isBetween(idRangeLow, idRangeHigh);
		boolean rccwIn = idRangeLow.isBetween(idLow, idHigh) || idRangeLow.equals(idLow);
		boolean rcwIn = idRangeHigh.isBetween(idLow, idHigh);
		
		return ((iccwIn && icwIn && rccwIn && rcwIn) || (icwIn || iccwIn || rcwIn || rccwIn));
		*/
	}
	
	Id mapToNode(int l) {
		assert(l >= 0 && l < L);
		return getRandomIdForBit(l);
		/*
		BigInteger intLow = new BigInteger(thr(l).toStringFull(), 16);
		BigInteger intHigh = new BigInteger(thr(l - 1).toStringFull(), 16);
		BigInteger rnd = (new BigInteger(Id.IdBitLength, rng)).mod(intHigh.subtract(intLow)).add(intLow);
		assert(intLow.compareTo(rnd) <= 0 && intHigh.compareTo(rnd) >= 0);
		
		String material = rnd.toString(16);
		if (material.length() < (Id.IdBitLength / 4)) {
			String padding = new String();
			for (int i = material.length(); i < (Id.IdBitLength / 4); i ++)
				padding = padding + "0";
			material = padding + material;
		}
		return Id.build(material);
		*/
	}
	
	
	////////////////////
	// Public methods //
	////////////////////
	
	public static DHS newInstance(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Environment env) {
		if (sketchAlgo == null) return null;
		if (sketchAlgo.isDF03()) return new DHSDF03Impl(sketchAlgo, numVecs, hashAlgo, L, retries, env);
		else return new DHSFM85Impl(sketchAlgo, numVecs, hashAlgo, L, retries, env);
	}
	
	public static DHS newInstance(DHSSketchAlgorithms sketchAlgo, int numVecs, int L, int retries, Environment env) {
		if (sketchAlgo == null) return null;
		if (sketchAlgo.isDF03()) return new DHSDF03Impl(sketchAlgo, numVecs, L, retries, env);
		else return new DHSFM85Impl(sketchAlgo, numVecs, L, retries, env);
	}
	
	public static DHS newInstance(DHSSketchAlgorithms sketchAlgo, int numVecs, int L, int retries, Vector<String> metrics, Environment env) {
		if (sketchAlgo == null) return null;
		if (sketchAlgo.isDF03()) return new DHSDF03Impl(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, metrics, env);
		else return new DHSFM85Impl(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, metrics, env);
	}
	
	public static DHS newInstance(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Vector<String> dhsMetrics, Environment env) {
		if (sketchAlgo == null) return null;
		if (sketchAlgo.isDF03()) return new DHSDF03Impl(sketchAlgo, numVecs, hashAlgo, L, retries, dhsMetrics, env);
		else return new DHSFM85Impl(sketchAlgo, numVecs, hashAlgo, L, retries, dhsMetrics, env);
	}
	/*
	public static DHS newInstance(DHSSketchAlgorithms sketchAlgo, HashSketchImpl[] centralizedHS, int retries, Environment env) {
		if (sketchAlgo == null || centralizedHS == null) return null;
		if (sketchAlgo.isDF03()) return new DHSDF03Impl(sketchAlgo, centralizedHS, retries, env);
		else return new DHSFM85Impl(sketchAlgo, centralizedHS, retries, env);
	}
	*/
	
	protected DHS(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Vector<String> dhsMetrics, Environment env) {
		this.L = L;
		this.hashAlgo = hashAlgo;
		this.sketchAlgo = sketchAlgo;
		this.numVecs = numVecs;
		this.retries = retries;
		this.baseHashSketchImpl = new HashSketchImpl(numVecs, L, hashAlgo);
		this.dhsMetrics = dhsMetrics;
		this.distributedHS = new Vector<HashSketchImpl>();
		for (int i = 0; i < dhsMetrics.size(); i ++)
			this.distributedHS.add(new HashSketchImpl(baseHashSketchImpl));
		this.env = env;
		this.idFactory = new PastryIdFactory(env);
		this.rng = new Random();
		if (sketchAlgo.isDF03()) assert(this instanceof DHSDF03Impl);
		else assert(this instanceof DHSFM85Impl);
	}
	
	protected DHS(DHSSketchAlgorithms sketchAlgo, int numVecs, String hashAlgo, int L, int retries, Environment env) {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, new Vector<String>(), env);
	}
	/*
	protected DHS(DHSSketchAlgorithms sketchAlgo, HashSketchImpl[] centralizedHS, int retries, Environment env) {
		this.sketchAlgo = sketchAlgo;
		this.retries = retries;
		this.env = env;
		this.idFactory = new PastryIdFactory(env);
		this.rng = new Random();
		
		this.centralizedHS = centralizedHS;
		this.L = centralizedHS[0].getNumBits();
		this.hashAlgo = centralizedHS[0].getHashAlgo();
		this.numVecs = centralizedHS[0].getNumVecs();
		this.baseHashSketchImpl = new HashSketchImpl(numVecs, L, hashAlgo);
		if (sketchAlgo.isDF03())
			assert(this instanceof DHSDF03Impl);
		else
			assert(this instanceof DHSFM85Impl);
	}
	*/
	
	protected DHS(DHSSketchAlgorithms sketchAlgo, int numVecs, int L, int retries, Environment env) {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, env);
	}
	
	public void clearHS() { if (distributedHS != null) for (HashSketchImpl hs : distributedHS) hs.clear(); }
	public void clear() { clearHS(); /*if (centralizedHS != null) for (HashSketchImpl hs : centralizedHS) hs.clear();*/ }
	public int getNumPendingInsertions(DHSPastryAppl sourceNode) { return sourceNode.getNumPendingInsertions(); }
	public int getNumVecs() { return numVecs; }
	public int getRetries() { return retries; }
	public int getNumItems() { return numItems; }
	public String toString() { return "[ DHS :: algo: " + sketchAlgo + " , HS: " + baseHashSketchImpl + " , retries: " + retries + " ]"; }
	public String getHashAlgo() { return hashAlgo; }
	public int getNumBits() { return L; }
	public Vector<String> getDHSMetrics() { return dhsMetrics; }
	public Vector<HashSketchImpl> getHashSketches() { return distributedHS; }
	
	public void addTuple(DHSPastryAppl sourceNode, String metric, ItemSerializable tuple) {
		int[] bits = baseHashSketchImpl.tryAddObject(tuple);
		Id targetId = mapToNode(bits[1]);
		DHSTuple tupleData = new DHSTuple(metric, bits[0], bits[1]);
		sourceNode.addTuple(tupleData);
	}
	
	/*
	public void batchInsert(PastryAppl sourceNode, String metric) {
		HashSketchImpl tmpHs = new HashSketchImpl(numVecs, L, hashAlgo);
		for (DHSTuple tuple : ((DHSPastryAppl)sourceNode).getLocalData())
			if (tuple.getMetric().equals(metric))
				tmpHs.setBit(tuple.getVec(), tuple.getBit());
		for (int i = 0; i < baseHashSketchImpl.getNumBits(); i ++) {
			for (int j = 0; j < baseHashSketchImpl.getNumVecs(); j ++) {
				if (tmpHs.getBit(j, i)) {
					insert(sourceNode, metric, new DHSTuple(metric, j, i));
				}
			}
		}
	}
	*/
	
	public final void insert(DHSPastryAppl sourceNode, String metric, ItemSerializable object) {
		if (dhsMetrics.indexOf(metric) < 0) {
			dhsMetrics.add(metric);
			distributedHS.add(new HashSketchImpl(baseHashSketchImpl));
		}
		
		if (sketchAlgo.usesBinarySearch())
			insertBinarySearch(sourceNode, metric, object);
		else
			insertClassic(sourceNode, metric, object);
	}
	
	/*
	public final void insert(DHSPastryAppl sourceNode, String metric, DHSTuple dhsTuple) {
		if (sketchAlgo.usesBinarySearch() == true)
			insertBinarySearch(sourceNode, metric, dhsTuple);
		else
			insertClassic(sourceNode, metric, dhsTuple);
	}
	*/
	
	public final DistributedEstimationResult[] estimate(DHSPastryAppl sourceNode, Vector<String> metrics) {
		DistributedEstimationResult[] ret = null;
		Logger logger = sourceNode.getLogger();
		
		if (sketchAlgo.usesBinarySearch() == true)
			ret = estimateBinarySearch(sourceNode, metrics);
		else
			ret = estimateClassic(sourceNode, metrics);
		
		if (logger.level <= Logger.FINE) {
			for (HashSketchImpl hs : distributedHS)
				logger.log(sketchAlgo + " distributed HS :: \n" + hs + "\n" + hs.toStringFull());
		}
		
		return ret;
	}
	

	public final String getSketchAlgoName() { return sketchAlgo.toString(); }
	
	//////////////////////
	// Abstract methods //
	//////////////////////
	
	protected abstract void insertClassic(DHSPastryAppl sourceNode, String metric, ItemSerializable item);
	protected abstract void insertBinarySearch(DHSPastryAppl sourceNode, String metric, ItemSerializable item);
	protected abstract DistributedEstimationResult[] estimatePartial(DHSPastryAppl sourceNode, Vector<String> metrics, int numBits, BigInteger[][] vecsValue, int hopCount, HashSet<Integer> bitsProbed);
	
}

class DHSBitProbeResult {
	DHSTuple[] resultData = null;
	int hopCount = 0;

	public DHSBitProbeResult(DHSTuple[] resultData, int hopCount) {
		this.resultData = resultData;
		this.hopCount = hopCount;
		assert(hopCount >= 0);
	}
}
