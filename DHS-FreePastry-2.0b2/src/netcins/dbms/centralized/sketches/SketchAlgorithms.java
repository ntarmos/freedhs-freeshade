/**
 * 
 */
package netcins.dbms.centralized.sketches;

public enum SketchAlgorithms {
	FM85,
	DF03,
	TugOfWar;
}

