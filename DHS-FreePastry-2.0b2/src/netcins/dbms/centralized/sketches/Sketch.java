package netcins.dbms.centralized.sketches;

import netcins.dbms.ItemSerializable;

public interface Sketch {
	public void insert(ItemSerializable object);
	public void clear();
	public double estimate(SketchAlgorithms algo);
}
