/**
 * $Id: HashSketchImpl.java 78 2006-11-03 15:07:18Z ntarmos $
 */
package netcins.dbms.centralized.sketches;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.QuickMath;
import netcins.dbms.BasicItem;
import netcins.dbms.ItemSerializable;

/**
 * @author ntarmos
 *
 */
public class HashSketchImpl implements Sketch {
	
	public static final String defaultHashAlgo = "SHA-1";
	public static final double df03VecsSetULPercentage = 0.8;
	private MessageDigest hash = null;
	private BigInteger[] sketch = null;
	private String hashAlgo = null;
	private int numBits = 0;
	
	public HashSketchImpl(HashSketchImpl other) {
		this(other.getNumVecs(), other.getNumBits(), other.getHashAlgo());
	}
	
	public HashSketchImpl(int numVecs, String hashAlgo) {
		try {
			hash = MessageDigest.getInstance(hashAlgo);
		} catch (NoSuchAlgorithmException e) {
			Logger log = Logger.getLogger(this.getClass().getName());
			log.log(Level.SEVERE, "Can't find an implementation for " + hashAlgo + ". Bailing out...");
			System.exit(0);
		}
		numBits = hash.getDigestLength() * 8;
		sketch = new BigInteger[numVecs];
		this.hashAlgo = hashAlgo;
		for (int i = 0; i < numVecs; i ++)
			sketch[i] = BigInteger.ZERO;
	}
	
	public HashSketchImpl(int numVecs) {
		this(numVecs, defaultHashAlgo);
	}
	
	public HashSketchImpl(int numVecs, int numBits, String hashAlgo) {
		this(numVecs, hashAlgo);
		assert(numBits <= (hash.getDigestLength() * 8));
		this.numBits = numBits;
	}
	
	public HashSketchImpl(int numVecs, int numBits) {
		this(numVecs, numBits, defaultHashAlgo);
	}
	
	public int getNumVecs() {
		return (sketch != null ? sketch.length : 0);
	}
	
	public int getNumBits() {
		return numBits;
	}
	
	public String getHashAlgo() {
		return hashAlgo;
	}
	
	/**
	 * Adds an object to the hash sketch.
	 * @param data Input object
	 * @return An int[] containing: (i) in position 0, the index of the vector to be updated
	 * by the input object, and (ii) in position 1, the index of the bit to be set in the selected
	 * bitmap vector.
	 */
	public int[] tryAddObject(ItemSerializable data) {
		if (sketch == null || sketch.length == 0)
			throw new RuntimeException("HashSketchImpl: Uninitialized instance.");
		
		int[] ret = new int[2];
		
		hash.reset();
		hash.update(data.toByteArray());
		byte[] digest = hash.digest();
		
		int numVecBits = Integer.SIZE - Integer.numberOfLeadingZeros(sketch.length) - 1;
		int numVecBytes = numVecBits / 8;	
		
		int vec = 0;
		for (int i = 0; i < numVecBytes; i ++)
			vec |= (((int)(digest[i] & 0xff)) << (i * 8));
		if ((numVecBits % 8) != 0) {
			byte lastVecByte = digest[numVecBytes];
		
			byte mask = 0x00;
			for (int i = 0; i < (numVecBits % 8); i ++)
				mask |= (0x01 << i);
			vec |= ((int)((lastVecByte & mask) & 0xff)) << (numVecBytes * 8);
		}
		
		int rho = 0;
		for (int i = numVecBits; i < digest.length * 8; i ++) {
			if ((digest[i / 8] & (1 << (i % 8))) != 0) rho ++;
			else break;
		}

		ret[0] = vec;
		ret[1] = rho;
		
		return ret;
	}
	
	public void insert(ItemSerializable data) {
		int[] bits = tryAddObject(data);
		setBit(bits[0], bits[1]);
	}
	
	public double estimate(SketchAlgorithms algo) {
		switch (algo) {
			case DF03: { return df03(); }
			case FM85: { return fm85(); }
			default: { return -1; }
		}
	}
	
	private double df03() {
		double meanR = 0;
		int[] allR = new int[sketch.length];
		for (int i = 0; i < sketch.length; i ++) {
			allR[i] = sketch[i].bitLength();
		}
		Arrays.sort(allR);
		int low = (int)QuickMath.floor((double)sketch.length * (1.0 - HashSketchImpl.df03VecsSetULPercentage));
		
		for (int i = low; i < sketch.length; i ++)
			meanR += allR[i];
		
		meanR /= (sketch.length - low); 
		return 0.39701 * Math.pow(2, meanR) * (sketch.length - low);
	}
	
	private double fm85() {
		double meanR = 0;
		for (int i = 0; i < sketch.length; i ++) {
			for (int j = 0; j <= sketch[i].bitLength(); j ++) {
				if (sketch[i].testBit(j) == false) {
					meanR += j;
					break;
				}
			}
		}
		meanR /= sketch.length;
		return 1.29281 * Math.pow(2.0, meanR) * sketch.length;
	}
	
 	public String diff(HashSketchImpl hs) {
 		if (this.sketch == null || hs.sketch == null || this.sketch.length != hs.sketch.length || hs.numBits != this.numBits)
 			return "Uninitialized or different type sketches";
 		BigInteger[] ret = new BigInteger[this.sketch.length];
 		for (int i = 0; i < this.sketch.length; i ++)
 			ret[i] = this.sketch[i].xor(hs.sketch[i]);
 		return toStringFull(ret, this.numBits);
 	}
	
	public boolean setBit(int nVec, int nBit) {
		assert(nVec >= 0 && nVec < sketch.length);
		assert(nBit >= 0 && nBit < numBits);
		boolean oldValue = sketch[nVec].testBit(nBit);
		sketch[nVec] = sketch[nVec].setBit(nBit);
		return oldValue;
	}
	
	public boolean getBit(int nVec, int nBit) {
		assert(nVec >= 0 && nVec < sketch.length);
		assert(nBit >= 0 && nBit < numBits);
		return sketch[nVec].testBit(nBit);
	}
	
	public void clear() {
		for (int i = 0; i < sketch.length; i ++)
			sketch[i] = BigInteger.ZERO;
	}
	
	private int rho(BigInteger input) {
		return (input.equals(BigInteger.ZERO) ? 0 /*numBits*/ : input.getLowestSetBit());
	}
	
	public static void main(String[] args) {
		HashSketchImpl hs = new HashSketchImpl(256, "SHA-1");
		Random prng = new Random(System.currentTimeMillis());
		for (int i = 0; i < 50000; i ++) {
			int data = (int)QuickMath.floor(prng.nextDouble() * 2000.0);
			hs.insert(new BasicItem(data));
		}
		System.out.println("DF03: " + hs.df03());
		System.out.println("FM85: " + hs.fm85());
	}

	public String toString() {
		return "[ hash: " + hashAlgo + " , numVecs: " + sketch.length + " , numBits: " + numBits + " ]";
	}
	
	public String toStringFull() {
		return HashSketchImpl.toStringFull(this);
	}
	
	public static String toStringFull(HashSketchImpl hs) {
		return HashSketchImpl.toStringFull(hs.sketch, hs.numBits);
	}
	
	private static String toStringFull(BigInteger[] hs, int nBits) {
		if (hs == null)
			return new String("[HS :: Uninitialized instance]");
		StringBuilder ret = new StringBuilder();
		int numVecDigits = (hs.length > 1000 ? 4 : (hs.length > 100 ? 3 : 2));
		for (int i = 0; i < hs.length; i ++) {
			String sString = hs[i].toString(2);
			StringBuilder pad = new StringBuilder();
			for (int j = sString.length(); j < nBits; j ++)
				pad.append("0");
			ret.append("[ ");
			for (int p = 0; p < (numVecDigits - (i < 10 ? 1 : (i < 100 ? 2 : 3))); p ++)
				ret.append(" ");
			
			ret.append(i + " : " + pad.toString() + sString + " ]\n");
		}
		return ret.toString();
	}
}

