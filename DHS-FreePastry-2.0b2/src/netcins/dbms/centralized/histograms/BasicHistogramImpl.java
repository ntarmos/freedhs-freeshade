/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import netcins.dbms.BasicItem;

public abstract class BasicHistogramImpl implements Histogram {
	
	protected BasicItem lowest = null;
	protected BasicItem highest = null;
	protected Bucket[] buckets = null;
	
	protected BasicHistogramImpl(BasicItem lowest, BasicItem highest, int numBuckets) {
		assert (lowest != null && highest != null && numBuckets > 0);
		this.lowest = (lowest.compareTo(highest) <= 0 ? lowest : highest);
		this.highest = (highest.compareTo(lowest) > 0 ? highest : lowest);
		this.buckets = new Bucket[numBuckets];
	}
	
	protected BasicHistogramImpl(BasicItem lowest, BasicItem highest, Bucket[] buckets) {
		assert (lowest != null && highest != null);
		this.lowest = (lowest.compareTo(highest) <= 0 ? lowest : highest);
		this.highest = (highest.compareTo(lowest) > 0 ? highest : lowest);
		this.buckets = buckets;
	}
	
	public void insert(BasicItem item) {
		buckets[getBucketForItem(item)].addItem(item);
	}
	
	protected final void setLowest(BasicItem lowest) { this.lowest = lowest; }
	public final BasicItem getLowest() { return lowest; }
	protected final void setHighest(BasicItem highest) { this.highest = highest; }
	public final BasicItem getHighest() { return highest; }
	public int getNumBuckets() { return buckets.length; }
	public String toString() { return "[ " + buckets.length + " buckets, values in [" + lowest + " , " + highest + ") ]"; }
}
