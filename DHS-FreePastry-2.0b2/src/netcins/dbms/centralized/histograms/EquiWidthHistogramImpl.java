/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import netcins.QuickMath;
import netcins.dbms.BasicItem;

public class EquiWidthHistogramImpl extends BasicHistogramImpl {
	private double bucketSpread = 0;

	public EquiWidthHistogramImpl(BasicItem lowest, BasicItem highest, int numBuckets) {
		super(lowest, highest, numBuckets);
		assert (lowest != null && highest != null && numBuckets > 0);
		bucketSpread = highest.subtract(lowest).doubleValue() / (double)(numBuckets);
		initBuckets();
	}
	
	public EquiWidthHistogramImpl(BasicItem lowest, BasicItem highest, Bucket[] buckets) {
		super(lowest, highest, buckets);
		bucketSpread = highest.subtract(lowest).doubleValue() / (double)(buckets.length);
	}
	
	public double getItemsInRange(BasicItem low, BasicItem high) {
		int startBucket = getBucketForItem(low);
		int endBucket = getBucketForItem(high);
		double ret = 0;
		for (int i = startBucket; i <= endBucket; i ++)
			ret += buckets[i].getItemCountInRange_USA(low, high);
		return ret;
	}

	public int getBucketForItem(BasicItem item) {
		int ret = (int)QuickMath.floor(item.subtract(lowest).doubleValue() / bucketSpread);
		return (ret < buckets.length ? ret : ret - 1);
	}

	public void initBuckets() {
		for (int i = 0; i < buckets.length; i ++)
			buckets[i] = new Bucket(lowest.add(new BasicItem(i * bucketSpread)), lowest.add(new BasicItem((i + 1) * bucketSpread)));
	}
	
	public String toString() { return "[ " + super.toString() + " , spread: " + bucketSpread + " ]"; }
}
