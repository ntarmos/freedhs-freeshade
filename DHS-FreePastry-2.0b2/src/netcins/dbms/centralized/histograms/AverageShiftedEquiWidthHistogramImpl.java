/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import netcins.dbms.BasicItem;

public class AverageShiftedEquiWidthHistogramImpl implements Histogram {
	private BasicItem lowest = null, highest = null, shift = null;
	private int numShifts;
	private EquiWidthHistogramImpl[] histograms = null;

	public AverageShiftedEquiWidthHistogramImpl(BasicItem lowest, BasicItem highest, BasicItem shift, int numShifts, int internalBuckets) {
		this.lowest = lowest;
		this.highest = highest;
		this.shift = shift;
		this.numShifts = numShifts;
		histograms = new EquiWidthHistogramImpl[numShifts];
		for (int i = 0; i < numShifts; i ++) {
			histograms[i] = new EquiWidthHistogramImpl(lowest.add(shift.multiply(i)), highest.add(shift.multiply(i)), internalBuckets);
		}
	}

	public void insert(BasicItem item) {
		for (int i = 0; i < histograms.length; i ++)
			histograms[i].insert(item);
	}

	public double getItemsInRange(BasicItem low, BasicItem high) {
		double ret = 0;
		for (EquiWidthHistogramImpl h : histograms)
			ret += h.getItemsInRange(low, high);
		return (ret / (double)histograms.length);
	}

	public int getBucketForItem(BasicItem item) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void initBuckets() {
		// TODO Auto-generated method stub
	}

	public int getNumBuckets() {
		return histograms.length * histograms[0].getNumBuckets();
	}

	public BasicItem getLowest() {
		return lowest;
	}

	public BasicItem getHighest() {
		return highest;
	}
}
