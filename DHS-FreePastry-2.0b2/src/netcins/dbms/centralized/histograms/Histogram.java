/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import netcins.dbms.BasicItem;

public interface Histogram {
	public void insert(BasicItem item);
	public abstract double getItemsInRange(BasicItem low, BasicItem high);
	public int getBucketForItem(BasicItem item);
	public void initBuckets();
	public int getNumBuckets();
	public BasicItem getLowest();
	public BasicItem getHighest();
}
