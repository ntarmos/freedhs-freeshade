package netcins.dbms.centralized.histograms;

import netcins.dbms.BasicItem;

public class Bucket {
	private BasicItem lowest = null;
	private BasicItem highest = null;
	private double itemCount = 0;
	
	public Bucket(BasicItem lowest, BasicItem highest, double itemCount) {
		this.lowest = (lowest.compareTo(highest) <= 0 ? lowest : highest);
		this.highest = (highest.compareTo(lowest) > 0 ? highest : lowest);
		this.itemCount = itemCount;
	}
	
	public Bucket(BasicItem lowest, BasicItem highest) {
		this(lowest, highest, 0);
	}
	
	public void addItem(BasicItem item) {
		if (item.compareTo(lowest) >= 0 && item.compareTo(highest) < 0)
			itemCount ++;
		else {
			int i = 0;
			i ++;
		}
	}
	
	public double getItemCountInRange_USA(BasicItem low, BasicItem high) {
		if (high.compareTo(lowest) <= 0 || low.compareTo(highest) > 0)
			return 0;
		BasicItem l = (low.compareTo(this.lowest) <= 0 ? this.lowest : low);
		BasicItem h = (high.compareTo(this.highest) > 0 ? this.highest : high);
		return (itemCount * (h.subtract(l).divide(highest.subtract(lowest))).doubleValue());
	}
	
	public double getItemCount() { return itemCount; }
	public void setItemCount(double itemCount) { this.itemCount = itemCount; }
	public BasicItem getLowest() { return lowest; }
	public BasicItem getHighest() { return highest; }
}
