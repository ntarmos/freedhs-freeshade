package netcins.dbms;

import java.io.IOException;
import java.io.Serializable;

import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;

public interface ItemSerializable extends Serializable {
	public byte[] toByteArray();
	public void serialize(OutputBuffer buf) throws IOException;
	public void deserialize(InputBuffer buf) throws IOException;
}
