/**
 * $Id$
 */
package netcins.dbms.distributed;

import java.io.IOException;

import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;

/**
 * @author ntarmos
 *
 */
public class StringTupleData extends TupleData {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6638072779823612522L;

	public StringTupleData(String primaryKey, String data, long timestamp) {
		super(primaryKey, data, timestamp);
	}

	public StringTupleData(String primaryKey, String data) {
		this(primaryKey, data, System.currentTimeMillis());
	}
	
	public String toString() {
		return "[ " + primaryKey + " : " + data /*+ " , " + timestamp*/ + " ]";
	}
	
	public String getPrimaryKey() {
		return (String)primaryKey;
	}
	
	public String getData() {
		return (String)data;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public void serialize(OutputBuffer buf) throws IOException {
		super.serialize(buf);
		buf.writeUTF((String)data);
	}
	
	public static StringTupleData deserialize(InputBuffer buf) throws IOException {
		StringTupleData ret = (StringTupleData)TupleData.deserialize(buf);
		ret.setData(buf.readUTF());
		return ret;
	}
	
	/*
	private void writeObject(ObjectOutputStream oos) throws IOException {
		SimpleOutputBuffer ob = new SimpleOutputBuffer();
		this.serialize(ob);
		oos.write(ob.getBytes());
	}
	
	private void readObject(ObjectInputStream ois) throws IOException {
		primaryKey = ois.readUTF();
		data = ois.readUTF();
		timestamp = ois.readLong();
	}
	*/
}
