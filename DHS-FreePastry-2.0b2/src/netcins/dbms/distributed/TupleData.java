/**
 * $Id$
 */
package netcins.dbms.distributed;

import java.io.IOException;
import java.io.Serializable;

import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;

/**
 * @author ntarmos
 *
 */
public class TupleData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3766381650772885531L;
	protected String primaryKey;
	protected Object data;
	protected long timestamp;
	
	public TupleData(String primaryKey, Object data, long timestamp) {
		this.primaryKey = primaryKey;
		this.data = data;
		this.timestamp = timestamp;
	}

	public TupleData(String primaryKey, Object data) {
		this(primaryKey, data, System.currentTimeMillis());
	}
	
	public String toString() {
		return "[ " + primaryKey + " : " + data /*+ " , " + timestamp*/ + " ]";
	}
	
	public void serialize(OutputBuffer buf) throws IOException {
		buf.writeLong(timestamp);
		buf.writeUTF((String)primaryKey);
	}
	
	public static TupleData deserialize(InputBuffer buf) throws IOException {
		TupleData data = new TupleData(null, null);
		data.timestamp = buf.readLong();
		data.primaryKey = buf.readUTF();
		return data;
	}
	
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	public Object getData() {
		return data;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public long getTimestamp() {
		return timestamp;
	}
	
	/*
	private void writeObject(ObjectOutputStream oos) throws IOException {
		SimpleOutputBuffer ob = new SimpleOutputBuffer();
		this.serialize(ob);
		oos.write(ob.getBytes());
	}
	
	private void readObject(ObjectInputStream ois) throws IOException {
		primaryKey = ois.readUTF();
		data = ois.readUTF();
		timestamp = ois.readLong();
	}
	*/
}
