package netcins.dbms.distributed.sketches;

public class DistributedEstimationResult {
	double distributedEstimation = 0;
	int hopCount = 0;
	int numBitsProbed = 0;

	public DistributedEstimationResult(double distributedEstimation, int hopCount, int numBitsProbed) {
		this.distributedEstimation = distributedEstimation;
		this.hopCount = hopCount;
		this.numBitsProbed = numBitsProbed;
	}
	
	public String toString() { return distributedEstimation + " (bits probed: " + numBitsProbed + " , hop count: " + hopCount + " , avg hops per probe: " + (double)hopCount/(double)numBitsProbed + ")"; }
	public double getDistributedEstimation() { return distributedEstimation; }
	public void setDistributedEstimation(double distributedEstimation) { this.distributedEstimation = distributedEstimation; }
	public int getHopCount() { return hopCount; }
	public int getNumBitsProbed() { return numBitsProbed; }
}
