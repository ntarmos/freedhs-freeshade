package netcins.dbms.distributed.sketches;

import java.util.Vector;

import netcins.dbms.ItemSerializable;
import netcins.p2p.dhs.DHSPastryAppl;

public interface DistributedSketch {
	public void insert(DHSPastryAppl sourceNode, String metric, ItemSerializable object);
	public void clear();
	public DistributedEstimationResult[] estimate(DHSPastryAppl sourceNode, Vector<String> metrics);
}
