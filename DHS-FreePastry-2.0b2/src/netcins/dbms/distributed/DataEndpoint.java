/**
 * $Id$
 */
package netcins.dbms.distributed;

import java.io.IOException;

import netcins.p2p.dhs.DHSTuple;
import rice.p2p.commonapi.rawserialization.InputBuffer;

/**
 * @author ntarmos
 *
 */
public interface DataEndpoint {
	int readInt(InputBuffer buf) throws IOException;
	long readLong(InputBuffer buf) throws IOException;
	double readDouble(InputBuffer buf) throws IOException;
	String readString(InputBuffer buf) throws IOException;
	TupleData readTupleData(InputBuffer buf) throws IOException;
	TupleData readStringTupleData(InputBuffer buf) throws IOException;
	DHSTuple readDHSTuple(InputBuffer buf) throws IOException;
}
