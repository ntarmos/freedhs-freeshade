/**
 * $Id$
 */
package netcins.dbms.distributed.testing;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.dbms.distributed.PersistentMultiStorage;
import netcins.dbms.distributed.StringTupleData;
import netcins.dbms.distributed.TupleData;
import netcins.dbms.distributed.TuplePastContent;
import rice.Continuation;
import rice.environment.Environment;
import rice.p2p.commonapi.Id;
import rice.p2p.past.PastContent;
import rice.p2p.past.PastImpl;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.SphereNetwork;
import rice.pastry.standard.RandomNodeIdFactory;
import rice.persistence.LRUCache;
import rice.persistence.MemoryStorage;
import rice.persistence.Storage;
import rice.persistence.StorageManagerImpl;

/**
 * @author ntarmos
 *
 */
public class TupleInsertionTest {
	private static int insertions = 0;
	private static int insertionsFailed = 0;
	private static int lookups = 0;
	private PastryIdFactory idFactory = null;
	private PastryNodeFactory factory = null;
	private Environment env = null;
	private Vector<PastImpl> nodes = null;
	private Vector<PastryNode> pastryNodes = null;
	private final Logger logger = Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting");
	/**
	 * 
	 */
	public TupleInsertionTest(Environment env) {
		init(env);
	}
	
	public void init(Environment env) {
		this.nodes = new Vector<PastImpl>();
		this.pastryNodes = new Vector<PastryNode>();
		this.env = env;
		this.idFactory = new rice.pastry.commonapi.PastryIdFactory(env);
		this.factory = new DirectPastryNodeFactory(new RandomNodeIdFactory(env), new SphereNetwork(env), env);
		Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting").setLevel(Level.INFO);
	}
	
	public void destroy() {
		env.destroy();
		nodes = null;
		pastryNodes = null;
		env = null;
	}
	
	public void populate(int numNodes, int numTuples) {
		System.out.println("Populating the overlay with new nodes...");
		
		PastryNode bootstrapNode = null;
		int curNodeIndex = 0;
		for (curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0)
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle) null);
			else
				node = factory.newNode(bootstrapNode.getLocalHandle());
			
			// the node may require sending several messages to fully boot into the ring
			while (!node.isReady()) {
				synchronized (node) {
					try {
						node.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
			logger.log(Level.FINER, "Finished creating new node " + node);
			
			Storage storage = null;
			try {
				storage = new PersistentMultiStorage(idFactory, "default", ".", 4 * 1024 * 1024, true, node.getEnvironment());
			} catch (IOException e) {
				e.printStackTrace();
			}
			PastImpl past = new PastImpl(node, new StorageManagerImpl(idFactory, storage, new LRUCache(new MemoryStorage(idFactory), /*512 * 1024*/ 0, node.getEnvironment())), 0, "");
			
			nodes.add(past);
			pastryNodes.add(node);
			
			if ((curNodeIndex % 100) == 0)
				logger.log(Level.INFO, curNodeIndex + "/" + numNodes + " nodes added...");
		}
		logger.log(Level.INFO, curNodeIndex + " nodes added...");
		
		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
		for (int i = 0; i < pastryNodes.size(); i ++) {
			if (pastryNodes.get(i).getLeafSet().isComplete() == false) {
				System.err.println("Node #" + i + " is not ready. Sleeping for 10\" and retrying...");
				i = -1;
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else
				System.out.print(".");
		}
		System.out.println();
		
		System.out.println("Populating the overlay with new data tuples...");
		String key = null;
		int data;
		for (data = 0; data < numTuples; data ++) {
			key =  Integer.toString(data % (numTuples / 4)); ///*"test"; //*/ Integer.toString(env.getRandomSource().nextInt() % (numTuples / 4));
			StringTupleData tupleData = new StringTupleData(Integer.toString(data), Integer.toString(data));
			PastContent dptc = new TuplePastContent(key, tupleData, env);
			assert(dptc != null);
			logger.log(Level.FINER, "Inserting " + dptc);
			
			nodes.get(data % numNodes).insert(dptc,
					new Continuation() {
				public void receiveResult(Object result) {
					Boolean[] results = ((Boolean[]) result);
					TupleInsertionTest.insertions ++;
					for (int ctr = 0; ctr < results.length; ctr++) {
						if (results[ctr].booleanValue() == false)
							System.err.println("got " + results[ctr].booleanValue());
					}
				}
				
				public void receiveException(Exception result) {
					TupleInsertionTest.insertionsFailed ++;
					if (result instanceof rice.p2p.past.PastException) {
						System.err.println("Error inserting. Retrying... (in a real-world scenario...)");
					} else {
						result.printStackTrace();
					}
				}
			});
			/*
			 try {
			 Thread.sleep(100);
			 } catch (InterruptedException e) {
			 e.printStackTrace();
			 }
			 */
			if ((data % 1000) == 0) {
				logger.log(Level.INFO, data + " items injected and " + (TupleInsertionTest.insertions + TupleInsertionTest.insertionsFailed) + " items added...");
				while ((TupleInsertionTest.insertions + TupleInsertionTest.insertionsFailed) < data) {
					System.out.println((TupleInsertionTest.insertions + TupleInsertionTest.insertionsFailed) + "/" + data + " items inserted so far. Sleeping for 10\" to allow the DHT to stabilize.");
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(nodes.get(0).getOutstandingMessages().length + " outstanding messages at node 0...");
				}
			}
		}
		logger.log(Level.INFO, data + " items added...");
		
		while ((TupleInsertionTest.insertions + TupleInsertionTest.insertionsFailed) < data) {
			System.out.println((TupleInsertionTest.insertions + TupleInsertionTest.insertionsFailed) + "/" + data + " items inserted so far. Sleeping for 10\" to allow the DHT to stabilize.");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(nodes.get(0).getOutstandingMessages().length + " outstanding messages at node 0...");
		}
		System.out.println("Done populating the DHT with new data tuples." + (TupleInsertionTest.insertions +  TupleInsertionTest.insertionsFailed) + " items inserted.");
		
		for (int i = 0; i < nodes.size(); i ++)
			logger.log(Level.FINE, "Node " + nodes.get(i).getLocalNodeHandle().getId() + " stores " + nodes.get(i).scan().numElements() + " items.");
	}
	
	public void query() {
		//for (int i = 0; i < nodes.size(); i ++)
		//	nodes.get(i).hopCount = 0;
		StringTupleData tupleData = new StringTupleData(Integer.toString(0), Integer.toString(0));
		PastContent dptc = new TuplePastContent(Integer.toString(0), tupleData, env);
		System.out.println("Searching " + dptc);
		int lookups = 0;
		for (int i = 0; i < nodes.size(); i ++) {
			lookups ++;
			final Id curId = nodes.get(i).getLocalNodeHandle().getId();
			nodes.get(i).lookup(dptc.getId(), false,
					new Continuation() {
				public void receiveResult(Object result) {
					TuplePastContent tpc = (TuplePastContent)result;
					TupleInsertionTest.lookups ++;
					if (result != null) {
						Id nodeId = tpc.getNodeId();
						if (nodeId != null)
							System.out.println("Node " + curId + " :: lookup: Found " + tpc.getData().size() + " result(s) on node " + nodeId);
						else
							System.out.println("Node " + curId + " :: lookup: Found " + tpc.getData().size() + " result(s) on unknown node ");
						logger.log(Level.FINE, "Node " + curId + " :: lookup: Results: " + result);
					} else {
						System.err.println("Node " + curId + " :: lookup: Empty result set!");
					}
				}
				
				public void receiveException(Exception result) {
					TupleInsertionTest.lookups ++;
					result.printStackTrace();
				}
			});
			
			/*
			 nodes.get(i).lookupHandles(dptc.getId(), 1,
			 new Continuation() {
			 public void receiveResult(Object result) {
			 if (result instanceof PastContentHandle[]) {
			 PastContentHandle pch[] = (PastContentHandle[])result;
			 if (result != null && pch.length > 0) {
			 for (int i = 0; i < pch.length; i ++) {
			 TuplePastContentHandle tpc = (TuplePastContentHandle)pch[i];
			 if (tpc != null) {
			 System.out.println("Node " + curId + " :: lookupHandle: Found " + tpc.getData().size() + " result(s) on node " + tpc.getNodeHandle().toString());
			 logger.log(Level.FINE, "Node " + curId + " :: lookupHandle: Results: " + tpc);
			 } else {
			 System.err.println("Node " + curId + " :: lookupHandle: Empty result set!");
			 }
			 }
			 } else {
			 System.err.println("Node " + curId + " :: lookupHandle: Empty result set!");
			 }
			 } else {
			 System.err.println("Node " + curId + " :: Got a " + result.getClass().getSimpleName());
			 }
			 }
			 public void receiveException(Exception result) {
			 result.printStackTrace();
			 }
			 }
			 );
			 */
		}
		
		while (TupleInsertionTest.lookups < lookups) {
			System.out.println(TupleInsertionTest.lookups + "/" + lookups + " lookups returned so far. Sleeping for 10' to allow for the rest to complete.");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Done executing all lookups." );

		logger.log(Level.FINER, "Complete node data sets following... ");
		int hopCount = 0;
		for (int i = 0; i < nodes.size(); i ++) {
			//hopCount += nodes.get(i).hopCount;
			//System.err.println("Node " + nodes.get(i).getLocalNodeHandle().getId() + " :: " + nodes.get(i).hopCount + " hops");
			//System.err.println("Leaf-set size:" + pastryNodes.get(i).getLeafSet().getUniqueCount());
			//hopCount += nodes.get(i).getHopCount();
			if (logger.getLevel().intValue() > Level.FINER.intValue()) {
				final PastImpl curNode = nodes.get(i);
				Id[] idSet = nodes.get(i).getStorageManager().scan().asArray();
				for (int j = 0; j < idSet.length; j ++) {
					final Id curId = idSet[j];
					nodes.get(i).getStorageManager().getObject(idSet[j],
							new Continuation() {
						public void receiveResult(Object result) {
							TuplePastContent tpc = (TuplePastContent)result;
							StringBuilder msg = new StringBuilder();
							msg.append("  Node " + curNode.getLocalNodeHandle().getId() + ": ");
							msg.append("   [ " + curId + " -- ");
							if (tpc != null) {
								for (Enumeration<TupleData> i = tpc.getData().elements(); i.hasMoreElements(); ) {
									msg.append(i.nextElement() + " ");
								}
								msg.append("]\n");
							} else {
								msg.append("null ]\n");
							}
							logger.log(Level.FINER, msg.toString());
						}
						public void receiveException(Exception result) {
							result.printStackTrace();
						}
					}
					);
				}
			}
		}
		
		System.out.println(nodes.size() + " nodes in the overlay, " + (TupleInsertionTest.insertions + TupleInsertionTest.insertionsFailed) + " items inserted.");
		System.out.println("Total lookups: " + TupleInsertionTest.lookups + ", total hops: " + hopCount + ", average hops per lookup: " + ((double)hopCount / (double)TupleInsertionTest.lookups));
	}
	
	/**
	 * Usage: java [-cp FreePastry-<version>.jar]
	 * netcins.dbms.distributed.testing.TupleInseritonTest numNodes numItems
	 *
	 * @param args The command line arguments
	 */
	public static void main(String[] args) {
		try {
			// Loads pastry settings
			
			Environment env = Environment.directEnvironment();
			//Environment env = new Environment();
			
			int numNodes = Integer.parseInt(args[0]);
			int numTuples = Integer.parseInt(args[1]);
			
			// launch our node!
			try {
				TupleInsertionTest test = new TupleInsertionTest(env);
				test.populate(numNodes, numTuples);
				test.query();
				test.destroy();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			// remind user how to use
			System.out.println("Usage:");
			System.out.println("java [-cp FreePastry-<version>.jar] netcins.dbms.distributed.testing.InsertionTest numNodes numItems");
			System.exit(0);
		}
	}
	
	
}
