/**
 * $Id$
 */
package netcins.dbms.distributed;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import rice.p2p.commonapi.Endpoint;
import rice.p2p.commonapi.Id;
import rice.p2p.commonapi.NodeHandle;
import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;
import rice.p2p.past.rawserialization.RawPastContentHandle;

/**
 * @author ntarmos
 *
 */
public class TuplePastContentHandle implements RawPastContentHandle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1830269055910655439L;
	private NodeHandle storageNode;
	private Id id;
	private Hashtable<String, TupleData> data;
	public static final short TYPE = -13;
	/**
	 * 
	 */
	public TuplePastContentHandle(NodeHandle nodeHandle, Id id, Hashtable<String, TupleData> data) {
		this.storageNode = nodeHandle;
		this.id = id;
		this.data = data;
	}

	public TuplePastContentHandle(InputBuffer buf, Endpoint endpoint) throws IOException {
		this.id = endpoint.readId(buf, buf.readShort());
		this.storageNode = endpoint.readNodeHandle(buf);
		int size = ((DataEndpoint)endpoint).readInt(buf);
		if (size == 0)
			data = null;
		else {
			data = new Hashtable<String, TupleData>();
			for (int i = 0; i < size; i ++) {
				TupleData tuple = ((DataEndpoint)endpoint).readTupleData(buf); 
				data.put(tuple.getPrimaryKey(), tuple);
			}
		}
	}

	public short getType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see rice.p2p.past.rawserialization.RawPastContentHandle#serialize(rice.p2p.commonapi.rawserialization.OutputBuffer)
	 */
	public void serialize(OutputBuffer buf) throws IOException {
		buf.writeShort(id.getType());
		id.serialize(buf);
		storageNode.serialize(buf);		
		if (data == null || data.size() == 0)
			buf.writeInt(0);
		else {
			Vector<TupleData> vec = new Vector<TupleData>();
			for (Enumeration<TupleData> values = data.elements(); values.hasMoreElements(); ) {
				vec.add(values.nextElement());
			}
			
			buf.writeInt(vec.size());
			for (int i = 0; i < vec.size(); i ++)
				vec.get(i).serialize(buf);
		}
	}

	/* (non-Javadoc)
	 * @see rice.p2p.past.PastContentHandle#getId()
	 */
	public Id getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see rice.p2p.past.PastContentHandle#getNodeHandle()
	 */
	public NodeHandle getNodeHandle() {
		return storageNode;
	}
	
	public Hashtable<String, TupleData> getData() {
		return data;
	}

}
