package netcins.dbms.distributed.histograms;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.Bucket;
import netcins.dbms.centralized.histograms.EquiWidthHistogramImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.p2p.dhs.DHS;
import netcins.p2p.dhs.DHSPastryAppl;

public class DHSEquiWidthHistogramImpl extends EquiWidthHistogramImpl implements DistributedHistogram {
	private Vector<String> bucketMetrics = null;
	private DHS dhsImpl = null;
	private String instance = null;

	public DHSEquiWidthHistogramImpl(String instance, BasicItem lowest, BasicItem highest, int numBuckets, DHS dhsImpl) {
		super(lowest, highest, numBuckets);
		assert(dhsImpl != null && dhsImpl.getDHSMetrics().size() == numBuckets);
		this.instance = instance;
		this.dhsImpl = dhsImpl;
		this.bucketMetrics = dhsImpl.getDHSMetrics();
	}
	
	public static Vector<String> generateBucketMetrics(String instance, int numBuckets) {
		Vector<String> ret = new Vector<String>();
		for (int i = 0; i < numBuckets; i ++)
			ret.add(new String(instance + " Bucket " + i));
		return ret;
	}

	public void insert(DHSPastryAppl sourceNode, BasicItem item) {
		int targetBucket = getBucketForItem(item);
		dhsImpl.insert(sourceNode, bucketMetrics.get(targetBucket), item);
		super.insert(item);
	}

	public DistributedEstimationResult getItemsInRange(DHSPastryAppl sourceNode, BasicItem low, BasicItem high) {
		DistributedHistogramReconstructionResult recon = reconstruct(sourceNode, low, high);
		return new DistributedEstimationResult(((EquiWidthHistogramImpl)recon.getHistogram()).getItemsInRange(low, high), recon.getHopCount(), recon.getNumBitsProbed());
	}
	
	public String toString() { return "[ " + instance + " :: " + super.toString() + " " + dhsImpl.toString() + " ]"; }
	
	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode) {
		return reconstruct(sourceNode, lowest, highest);
	}

	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode, BasicItem low, BasicItem high) {
		double estimation = 0;
		int hopCount = 0;
		int numBitsProbed = 0;
		int bucketLow = getBucketForItem(low);
		int bucketHigh = getBucketForItem(high);
		
		Vector<String> queryMetrics = new Vector<String>();
		for (int i = bucketLow; i <= bucketHigh; i ++)
			queryMetrics.add(bucketMetrics.get(bucketLow + i));
		Bucket[] tmpBuckets = new Bucket[queryMetrics.size()];
		
		
		DistributedEstimationResult[] res = dhsImpl.estimate(sourceNode, queryMetrics);
		for (int i = 0; i < tmpBuckets.length; i ++) {
			tmpBuckets[i] = new Bucket(buckets[i + bucketLow].getLowest(), buckets[i + bucketLow].getHighest(), res[i].getDistributedEstimation());
		}
		if (res.length > 0) {
			hopCount = res[0].getHopCount();
			numBitsProbed = res[0].getNumBitsProbed();
		}

		return new DistributedHistogramReconstructionResult(new EquiWidthHistogramImpl(lowest, highest, tmpBuckets), hopCount, numBitsProbed);
	}
}
