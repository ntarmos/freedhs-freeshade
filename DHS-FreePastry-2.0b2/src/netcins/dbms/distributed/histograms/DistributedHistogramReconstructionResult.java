package netcins.dbms.distributed.histograms;

import netcins.dbms.centralized.histograms.Histogram;

public class DistributedHistogramReconstructionResult {
	private Histogram histogram = null;
	private int hopCount = 0;
	private int numBitsProbed = 0;

	public DistributedHistogramReconstructionResult(Histogram histogram, int hopCount, int numBitsProbed) {
		this.histogram = histogram;
		this.hopCount = hopCount;
		this.numBitsProbed = numBitsProbed;
	}
	
	public int getHopCount() { return hopCount; }
	public int getNumBitsProbed() { return numBitsProbed; }
	public Histogram getHistogram() { return histogram; }
}
