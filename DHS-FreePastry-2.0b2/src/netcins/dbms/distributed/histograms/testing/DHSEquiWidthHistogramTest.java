/**
 * $Id$
 */

package netcins.dbms.distributed.histograms.testing;

import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.QuickMath;
import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.dbms.distributed.histograms.DHSEquiWidthHistogramImpl;
import netcins.p2p.dhs.DHS;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithms;
import rice.environment.Environment;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.direct.BasicNetworkSimulator;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.standard.RandomNodeIdFactory;

public class DHSEquiWidthHistogramTest {
	private DHS dhsImpl = null;
	private PastryIdFactory idFactory = null;
	private PastryNodeFactory factory = null;
	private Environment env = null;
	private Vector<DHSPastryAppl> pastryAppls = null;
	private DHSSketchAlgorithms sketchAlgo = DHS.defaultSketchAlgo;
	private DHSEquiWidthHistogramImpl dhsHisto = null;
	private HashSketchImpl centralizedHashSketch = null;
	
	private int numItems = -1;
	private int numNodes = -1;
	private int numVecs = -1;
	private int L = -1;
	private int retries = -1;
	private int qNode = -1;
	
	private boolean isInited = false;
	private final Logger logger = Logger.getLogger("netcins.p2p.dhs.testing.DHSSingleMetricTest");
	private BasicNetworkSimulator simulator = null;
	
	public DHSEquiWidthHistogramTest(BasicItem lowest, BasicItem highest, int numBuckets, DHSSketchAlgorithms sketchAlgo, int numVecs, int L, int retries, Environment env) {
		init(lowest, highest, numBuckets, sketchAlgo, numVecs, L, retries, env);
	}
	public DHSEquiWidthHistogramTest(BasicItem lowest, BasicItem highest, int numBuckets, int numVecs, int L, int retries, Environment env) {
		init(lowest, highest, numBuckets, DHS.defaultSketchAlgo, numVecs, L, retries, env);
	}
	
	public void init(BasicItem lowest, BasicItem highest, int numBuckets, DHSSketchAlgorithms algorithm, int numVecs, int L, int retries, Environment env) {
		if (isInited) destroy();
		this.env = env;
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null) {
			destroy();
			return;
		}
		this.pastryAppls = new Vector<DHSPastryAppl>();
		this.idFactory = new rice.pastry.commonapi.PastryIdFactory(env);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(new RandomNodeIdFactory(env), simulator, env);
		this.centralizedHashSketch = new HashSketchImpl(numVecs, L);
		
		initDHSHisto(algorithm, numVecs, L, retries, lowest, highest, numBuckets);
		Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting").setLevel(Level.INFO);
		isInited = true;
	}
	
	public void initDHSHisto(DHSSketchAlgorithms algorithm, int numVecs, int L, int retries, BasicItem lowest, BasicItem highest, int numBuckets) {
		this.sketchAlgo = algorithm;
		this.numVecs = numVecs;
		this.L = L;
		this.retries = retries;
		if (algorithm != null) {
			Vector<String> dhsMetrics = DHSEquiWidthHistogramImpl.generateBucketMetrics("Basic instance", numBuckets);
			this.dhsImpl = DHS.newInstance(algorithm, numVecs, L, retries, dhsMetrics, env);
			this.dhsHisto = new DHSEquiWidthHistogramImpl("Basic instance", lowest, highest, numBuckets, dhsImpl);
			System.out.println("Initializing DHSEquiWidthHistogramTest with " + this.dhsHisto);
			for (DHSPastryAppl app : pastryAppls)
				app.setDHSImpl(this.dhsImpl);
		}
	}
	
	private void clearHits() {
		for (DHSPastryAppl app : pastryAppls)
			app.clearHits();
	}
	
	
	public boolean isInited() { return isInited; }
	
	public void destroy() {
		if (isInited == false) return;
		isInited = false;
		env.destroy();
		pastryAppls = null;
		env = null;
	}
	
	public void clearData() {
		if (isInited == false) return;
		for (DHSPastryAppl appl : pastryAppls) {
			appl.clearData();
		}
		clearHits();
		numItems = 0;
	}

	public void addNodes(int numNodes) {
		
		System.runFinalization();
		System.gc();
		long startTime = Calendar.getInstance().getTimeInMillis(), endTime = 0;
		
		if (isInited == false) throw new RuntimeException("Uninitialized test instance!");
		this.numNodes = numNodes;
		System.out.println("Populating the overlay with new nodes...");
		
		PastryNode bootstrapNode = null;
		for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0) { // Bootstrap node
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle) null);
				synchronized (node) { // Wait for the bootstrap to get ready...
					while (node.isReady() == false) {
						try {
							node.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			} else { // Normal node
				node = factory.newNode(bootstrapNode.getLocalHandle());
			}
			
			logger.log(Level.FINER, "Finished creating new node " + node);
			
			DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);
			
			pastryAppls.add(dhsAppl);
			
			if ((curNodeIndex % 100) == 0)
				System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
			else if ((curNodeIndex % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + numNodes + "/" + numNodes + " done.");

		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
		for (int i = 0; i < pastryAppls.size(); i ++) {
			PastryNode curNode = pastryAppls.get(i).getPastryNode();
			synchronized (curNode) {
				while (curNode.isReady() == false) {
					try {
						curNode.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			if ((i % 100) == 0)
				System.out.print(" " + i + "/" + numNodes + " ");
			else if ((i % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + pastryAppls.size() + "/" + numNodes + " done.");
		
		System.runFinalization();
		System.gc();
		endTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("DHS node generation took " + (endTime - startTime)/1000 + "\".");
		qNode = (int)QuickMath.floor(QuickMath.random() * pastryAppls.size());
	}
	
	public void addData(int[] tuples) {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = Calendar.getInstance().getTimeInMillis(), endTime = 0;
		
		if (isInited == false) throw new RuntimeException("Uninitialized test instance!");
		this.numItems = tuples.length;
		
		System.out.print("Injecting " + numItems + " new data tuples...");
		for (int data = 0; data < numItems; data ++) {
			dhsHisto.insert(pastryAppls.get(data % pastryAppls.size()), new BasicItem(tuples[data]));
			centralizedHashSketch.insert(new BasicItem(tuples[data]));
			if ((data % 100000) == 0)
				waitForInsertionsToComplete();
		}
		System.out.println(" done.");
		System.out.print("Waiting for the data items to reach their destinations...");
		waitForInsertionsToComplete();
		System.out.println(" done. ");
		
		endTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("DHS data generation took " + (endTime - startTime)/1000 + "\".");
		System.runFinalization();
		System.gc();
		long numHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls)
			numHits += curAppl.getInsertionHits();
		System.out.println("Average node insertion hits: " + ((double)numHits/(double)numNodes));
	}

	private void waitForInsertionsToComplete() {
		for (DHSPastryAppl curAppl : pastryAppls) {
			curAppl.waitForInsertions();
			curAppl.clearInsertionResponses();
		}
	}
	
	public void query(BasicItem lowest, BasicItem highest) {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = Calendar.getInstance().getTimeInMillis(), endTime;
		System.out.println("Querying node: " + pastryAppls.get(qNode).getNodeId());
		System.out.println(sketchAlgo + " centralized estimation: " + centralizedHashSketch.estimate(sketchAlgo.isDF03() ? SketchAlgorithms.DF03 : SketchAlgorithms.FM85));
		System.out.println(sketchAlgo + " distributed estimation: " + dhsHisto.getItemsInRange(pastryAppls.get(qNode), dhsHisto.getLowest(), dhsHisto.getHighest()));
		System.runFinalization();
		System.gc();
		endTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("DHS query took " + (endTime - startTime)/1000 + "\".");
		long numHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls)
			numHits += curAppl.getQueryHits();
		System.out.println("Average node query hits: " + ((double)numHits/(double)numNodes));
	}
	
	public static void main(String[] args) {
		try {
			
			int numNodes = Integer.parseInt(args[0]);
			int numTuples = Integer.parseInt(args[1]);
			int numVecs = Integer.parseInt(args[2]);
			int L = Integer.parseInt(args[3]);
			int retries = Integer.parseInt(args[4]);
			BasicItem lowest = new BasicItem(Integer.parseInt(args[5]));
			BasicItem highest = new BasicItem(Integer.parseInt(args[6]));
			int numBuckets = Integer.parseInt(args[7]);
			long startTime = Calendar.getInstance().getTimeInMillis(), endTime = 0;
			
			DHSEquiWidthHistogramTest test = new DHSEquiWidthHistogramTest(lowest, highest, numBuckets, null, 0, 0, 0, null);
				
			int[] data = new int[numTuples];
			for (int i = 0; i < numTuples; i ++)
				//data[i] =  (int)QuickMath.floor(lowest.doubleValue() + QuickMath.random() * (highest.subtract(lowest).doubleValue()));
				data[i] = lowest.intValue() + (i % (int)QuickMath.floor(highest.subtract(lowest).doubleValue()));
			
			System.out.println(" --- Begin Simulation --- ");
			try {
				test.init(lowest, highest, numBuckets, null, numVecs, L, retries, Environment.directEnvironment());
				test.addNodes(numNodes);
				System.out.println("\n ---\n");
				boolean firstRun = true;
				for (DHSSketchAlgorithms algo : DHSSketchAlgorithms.values()) {
					System.out.println("Testing " + algo + "...");
					test.initDHSHisto(algo, numVecs, L, retries, lowest, highest, numBuckets);
					if (firstRun) {
						test.clearData();
						test.addData(data);
						firstRun = false;
					}
					test.query(lowest, highest);
					System.out.println("\n ---\n");
				}
				test.destroy();
				endTime = Calendar.getInstance().getTimeInMillis();
				System.out.println(" Total Simulation Time: " + (endTime - startTime)/1000.0 + "\".");
				System.out.println(" --- End Simulation --- \n");
			} catch (Exception e) {
				e.printStackTrace();
				if (test != null) test.destroy();
				System.exit(-1);
			}
		} catch (Exception e) {
			System.out.println("Usage: java [-cp FreePastry-<version>.jar]");
			System.out.println("\tnetcins.p2p.dhs.testing. <numNodes> <numItems> <numVecs> <L> <retries>");
			System.exit(0);
		}
	}
}
