package netcins.dbms.distributed.histograms;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.Histogram;
import netcins.p2p.dhs.DHSPastryAppl;

public interface DistributedHistogram extends Histogram {
	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode);
	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode, BasicItem low, BasicItem high);
}
