/**
 * $Id$
 */
package netcins.dbms.distributed;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;

import rice.environment.Environment;
import rice.p2p.commonapi.Id;
import rice.p2p.past.Past;
import rice.p2p.past.PastContent;
import rice.p2p.past.PastContentHandle;
import rice.p2p.past.PastException;
import rice.pastry.commonapi.PastryIdFactory;

/**
 * @author ntarmos
 *
 */
public class TuplePastContent implements PastContent, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1643198120811019624L;
	private Id id = null;
	private Hashtable<String, TupleData> data = null;
	private static final short TYPE = -14;
	private Id nodeId = null;
	/**
	 * 
	 */
	
	public TuplePastContent(String id, TupleData tupleData, Environment env) {
		PastryIdFactory idFactory = new PastryIdFactory(env);
		this.id = idFactory.buildId(id);
		this.data = new Hashtable<String, TupleData>();
		this.data.put(tupleData.getPrimaryKey(), tupleData);
	}

	public TuplePastContent(Id id, TupleData tupleData) {
		this.id = id;
		this.data = new Hashtable<String, TupleData>();
		this.data.put(tupleData.getPrimaryKey(), tupleData);
	}
	
	/*
	public TuplePastContent(String id, TupleData data) {
		this.id = id;
		this.data = new Hashtable<String, TupleData>();
		this.data.put(data.getPrimaryKey(), data);
	}*/

	public TuplePastContent(Id id, Hashtable<String, TupleData> data) {
		this.id = id;
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see rice.p2p.past.PastContent#checkInsert(rice.p2p.commonapi.Id, rice.p2p.past.PastContent)
	 */
	public PastContent checkInsert(rice.p2p.commonapi.Id id, PastContent existingContent)
	throws PastException {
		// only allow correct content hash key
		if (!id.equals(getId())) {
			throw new PastException("ContentHashPastContent: can't insert, content hash incorrect");
		}
		TuplePastContent ret = (TuplePastContent)merge(this, (TuplePastContent)existingContent);
		ret.nodeId = nodeId;

		return ret;
		
		/*

		// Update the vector of tuples with the newly added one.
		 TuplePastContent eContent = ((TuplePastContent)this);
		 logger.log(Level.FINE, " Found existing data for ID " + id.toString() + ". Appending new values...");
		 for (Enumeration<TupleData> i = eContent.data.elements(); i.hasMoreElements(); ) {
				TupleData newTuple = i.nextElement(); 
				TupleData oldTuple = ret.data.get(newTuple.getPrimaryKey());
				if (oldTuple == null || newTuple.getTimestamp() > oldTuple.getTimestamp())
					ret.data.put(newTuple.getPrimaryKey(), newTuple);
			}
		
		//System.out.println(" Result set size: " + ret.data.size());

		return ret;
		*/
	}
	
	public static PastContent merge(TuplePastContent oldPC, TuplePastContent newPC) throws PastException {
		TuplePastContent oldTPC = (TuplePastContent)oldPC;
		TuplePastContent newTPC = (TuplePastContent)newPC;
		TuplePastContent ret = null;
		if (oldTPC == null)
			ret = newTPC;
		else if (newTPC == null)
			ret = oldTPC;
		else {
			if (!oldTPC.getId().equals(newTPC.getId())) {
				throw new PastException("ContentHashPastContent: can't insert, content hash incorrect");
			}
			ret = oldTPC;
			for (Enumeration<TupleData> i = newTPC.data.elements(); i.hasMoreElements(); ) {
				TupleData newTuple = i.nextElement(); 
				TupleData oldTuple = ret.data.get(newTuple.getPrimaryKey());
				if (oldTuple == null || newTuple.getTimestamp() > oldTuple.getTimestamp())
					ret.data.put(newTuple.getPrimaryKey(), newTuple);
			}
		}
		return ret;
	}
	
	public PastContent merge(rice.p2p.commonapi.Id id, PastContent existingContent) throws PastException {
		if (!id.equals(getId())) {
			throw new PastException("ContentHashPastContent: can't insert, content hash incorrect");
		}
		
		return merge(this, (TuplePastContent)existingContent);
	}

	/* (non-Javadoc)
	 * @see rice.p2p.past.PastContent#getHandle(rice.p2p.past.Past)
	 */
	public PastContentHandle getHandle(Past local) {
	    return new TuplePastContentHandle(local.getLocalNodeHandle(), getId(), getData());
	}

	/* (non-Javadoc)
	 * @see rice.p2p.past.PastContent#getId()
	 */
	public Id getId() {
		return id;
	}
	
	public Id getNodeId() {
		return nodeId;
	}

	public Hashtable<String, TupleData> getData() {
		return data;
	}

	public short getType() {
		return TYPE;
	}
	
	/* (non-Javadoc)
	 * @see rice.p2p.past.PastContent#isMutable()
	 */
	public boolean isMutable() {
		return false;
	}

	public String toString() {
		StringBuilder ret = new StringBuilder("[ " + getId().toString() + " : ");
		if (data != null && data.size() > 0) {
			Enumeration<TupleData> values = data.elements();
			ret.append(values.nextElement());
			for (; values.hasMoreElements(); ) {
				ret.append(" , " + values.nextElement());
			}
		}
		ret.append(" ]");
		return ret.toString();
	}
}
