package netcins.dbms;

import java.io.IOException;

import netcins.QuickMath;

import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;

public class BasicItem extends Number implements Comparable<Number>, ItemSerializable {
	private double value = 0;
	private transient final static int byteCount = Double.SIZE;
	
	public BasicItem(Number n) {
		this.value = n.doubleValue();
	}
	
	public BasicItem(double value) {
		this.value = value;
	}
	
	public BasicItem(byte material[]) {
		long res = 0;
		for (int i = 0; i < material.length; i ++)
			res |= (((long)(material[i] & 0xff)) << (i * 8));
		value = Double.longBitsToDouble(res);
	}
	
	public int compareTo(Number n) { return (value == n.doubleValue() ? 0 : (value < n.doubleValue() ? -1 : 1)); }
	
	public BasicItem subtract(BasicItem i) { return new BasicItem(value - i.value); }
	public BasicItem subtract(Number n) { return new BasicItem(value - n.doubleValue()); }
	
	public BasicItem add(BasicItem i) { return new BasicItem(value + i.value); }
	public BasicItem add(Number n) { return new BasicItem(value + n.doubleValue()); }
	
	public BasicItem multiply(BasicItem i) { return new BasicItem(value * i.value); }
	public BasicItem multiply(Number n) { return new BasicItem(value * n.doubleValue()); }
	
	public BasicItem divide(BasicItem i) { return new BasicItem(value / i.value); }
	public BasicItem divide(Number n) { return new BasicItem(value / n.doubleValue()); }
	
	public boolean equals(Object i) {
		if (i instanceof BasicItem)
			return (value == ((BasicItem)i).value);
		if (i instanceof Number)
			return (value == ((Number)i).doubleValue());
		return false;
	}
	
	public byte[] toByteArray() {
		long rawBits = Double.doubleToRawLongBits(value);
		byte ret[] = new byte[byteCount];
		for (int i = 0; i < byteCount; i ++) {
			ret[i] = (new Long((rawBits & (0xffL << (i * 8))) >> (i * 8))).byteValue();
		}
		return ret;
	}
	
	public String toString() { return Double.toString(value); }
	
	public void serialize(OutputBuffer buf) throws IOException { buf.writeDouble(value); }
	public void deserialize(InputBuffer buf) throws IOException { value = buf.readDouble(); }

	@Override
	public int intValue() {
		return (int)QuickMath.floor(value);
	}

	@Override
	public long longValue() {
		return (long)QuickMath.floor(value);
	}

	@Override
	public float floatValue() {
		return (float)value;
	}

	@Override
	public double doubleValue() {
		return value;
	}
}
