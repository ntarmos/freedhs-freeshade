/*************************************************************************

"FreePastry" Peer-to-Peer Application Development Substrate 

Copyright 2002, Rice University. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither  the name  of Rice  University (RICE) nor  the names  of its
contributors may be  used to endorse or promote  products derived from
this software without specific prior written permission.

This software is provided by RICE and the contributors on an "as is"
basis, without any representations or warranties of any kind, express
or implied including, but not limited to, representations or
warranties of non-infringement, merchantability or fitness for a
particular purpose. In no event shall RICE or contributors be liable
for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of
substitute goods or services; loss of use, data, or profits; or
business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even
if advised of the possibility of such damage.

********************************************************************************/
/*
 *  Created on Jan 30, 2006
 */
package rice.p2p.commonapi.appsocket;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Interface for sending bulk data from the application. Mimics java's
 * non-blocking SocketChannel interface this should make it easier to implement
 * in any Java-based p2p overlay.
 *
 * @version $Id: pretty.settings 2305 2005-03-11 20:22:33Z jeffh $
 * @author jeffh
 */
public interface AppSocket {
  /**
   * Reads a sequence of bytes from this channel into a subsequence of the given
   * buffers.
   *
   * @param dsts DESCRIBE THE PARAMETER
   * @param offset DESCRIBE THE PARAMETER
   * @param length DESCRIBE THE PARAMETER
   * @return DESCRIBE THE RETURN VALUE
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  long read(ByteBuffer[] dsts, int offset, int length) throws IOException;

  /**
   * Writes a sequence of bytes to this channel from a subsequence of the given
   * buffers.
   *
   * @param srcs DESCRIBE THE PARAMETER
   * @param offset DESCRIBE THE PARAMETER
   * @param length DESCRIBE THE PARAMETER
   * @return DESCRIBE THE RETURN VALUE
   * @throws IOException
   */
  long write(ByteBuffer[] srcs, int offset, int length) throws IOException;

  /**
   * Must be called every time a Read/Write occurs to continue operation.
   *
   * @param wantToRead DESCRIBE THE PARAMETER
   * @param wantToWrite DESCRIBE THE PARAMETER
   * @param timeout DESCRIBE THE PARAMETER
   * @param receiver DESCRIBE THE PARAMETER
   * @receiver will have receiveSelectResult() called on it note that you must
   *      call select() each time receiveSelectResult() is called. This is so
   *      your application can properly handle flow control
   */
  void register(boolean wantToRead, boolean wantToWrite, int timeout, AppSocketReceiver receiver);

  /**
   * Disables the output stream for this socket. Used to properly close down a
   * socket used for bi-directional communication that can be initated by either
   * side.
   */
  void shutdownOutput();

  /**
   * Closes this socket.
   */
  void close();

}
