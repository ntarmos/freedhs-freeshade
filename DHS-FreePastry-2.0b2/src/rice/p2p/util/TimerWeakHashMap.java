/*************************************************************************

"FreePastry" Peer-to-Peer Application Development Substrate 

Copyright 2002, Rice University. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither  the name  of Rice  University (RICE) nor  the names  of its
contributors may be  used to endorse or promote  products derived from
this software without specific prior written permission.

This software is provided by RICE and the contributors on an "as is"
basis, without any representations or warranties of any kind, express
or implied including, but not limited to, representations or
warranties of non-infringement, merchantability or fitness for a
particular purpose. In no event shall RICE or contributors be liable
for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of
substitute goods or services; loss of use, data, or profits; or
business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even
if advised of the possibility of such damage.

********************************************************************************/
/*
 *  Created on Jun 9, 2006
 */
package rice.p2p.util;

import java.util.WeakHashMap;

import rice.selector.*;

/**
 * Weak hash map that holds hard link to keys for a minimum time.
 *
 * @version $Id: pretty.settings 2305 2005-03-11 20:22:33Z jeffh $
 * @author Jeff Hoye
 */
public class TimerWeakHashMap extends WeakHashMap {

  int defaultDelay;
  Timer timer;

  /**
   * Constructor for TimerWeakHashMap.
   *
   * @param t DESCRIBE THE PARAMETER
   * @param delay DESCRIBE THE PARAMETER
   */
  public TimerWeakHashMap(Timer t, int delay) {
    this.defaultDelay = delay;
    timer = t;
  }


  /**
   * DESCRIBE THE METHOD
   *
   * @param key DESCRIBE THE PARAMETER
   * @param val DESCRIBE THE PARAMETER
   * @return DESCRIBE THE RETURN VALUE
   */
  public Object put(Object key, Object val) {
    refresh(key);
    return super.put(key, val);
  }

  /**
   * DESCRIBE THE METHOD
   *
   * @param key DESCRIBE THE PARAMETER
   */
  public void refresh(Object key) {
    refresh(key, defaultDelay);
  }

  /**
   * DESCRIBE THE METHOD
   *
   * @param key DESCRIBE THE PARAMETER
   * @param delay DESCRIBE THE PARAMETER
   */
  public void refresh(Object key, int delay) {
    timer.schedule(
      new HardLinkTimerTask(key), delay);
  }

  /**
   * DESCRIBE THE CLASS
   *
   * @version $Id: pretty.settings 2305 2005-03-11 20:22:33Z jeffh $
   * @author jeffh
   */
  static class HardLinkTimerTask extends TimerTask {
    Object hardLink;

    /**
     * Constructor for HardLinkTimerTask.
     *
     * @param hardLink DESCRIBE THE PARAMETER
     */
    public HardLinkTimerTask(Object hardLink) {
      this.hardLink = hardLink;
    }

    /**
     * Main processing method for the HardLinkTimerTask object
     */
    public void run() {
      // do nothing, just expire, taking the hard link away with you
    }
  }

}
