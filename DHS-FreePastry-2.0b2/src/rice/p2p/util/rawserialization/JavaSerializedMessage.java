/*************************************************************************

"FreePastry" Peer-to-Peer Application Development Substrate 

Copyright 2002, Rice University. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither  the name  of Rice  University (RICE) nor  the names  of its
contributors may be  used to endorse or promote  products derived from
this software without specific prior written permission.

This software is provided by RICE and the contributors on an "as is"
basis, without any representations or warranties of any kind, express
or implied including, but not limited to, representations or
warranties of non-infringement, merchantability or fitness for a
particular purpose. In no event shall RICE or contributors be liable
for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of
substitute goods or services; loss of use, data, or profits; or
business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even
if advised of the possibility of such damage.

********************************************************************************/
/*
 *  Created on Feb 21, 2006
 */
package rice.p2p.util.rawserialization;

import java.io.*;

import rice.p2p.commonapi.rawserialization.*;
import rice.pastry.messaging.*;
import rice.p2p.commonapi.Message;

/**
 * Wrapper that converts rice.pastry.messaging.Message to
 * rice.pastry.messageing.PRawMessage
 *
 * @version $Id$
 * @author Jeff Hoye
 */
public class JavaSerializedMessage implements RawMessage {

  Message msg;

//  Exception constructionStack;

  /**
   * Constructor for JavaSerializedMessage.
   *
   * @param msg DESCRIBE THE PARAMETER
   */
  public JavaSerializedMessage(Message msg) {
    this.msg = msg;
    if (msg == null) {
      throw new RuntimeException("msg cannot be null");
    }
//    constructionStack = new Exception("Stack Trace: msg:"+msg).printStackTrace();
  }

  /**
   * Gets the Type attribute of the JavaSerializedMessage object
   *
   * @return The Type value
   */
  public short getType() {
    return 0;
  }

  /**
   * Gets the Message attribute of the JavaSerializedMessage object
   *
   * @return The Message value
   */
  public Message getMessage() {
    return msg;
  }

  /**
   * Gets the Priority attribute of the JavaSerializedMessage object
   *
   * @return The Priority value
   */
  public byte getPriority() {
    return msg.getPriority();
  }

  /**
   * DESCRIBE THE METHOD
   *
   * @param buf DESCRIBE THE PARAMETER
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  public void serialize(OutputBuffer buf) throws IOException {
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);

      // write out object and find its length
      oos.writeObject(msg);
      oos.close();

      byte[] temp = baos.toByteArray();
      buf.write(temp, 0, temp.length);
      //    System.out.println("JavaSerializedMessage.serailize() "+msg+" length:"+temp.length);
      //    new Exception("Stack Trace").printStackTrace();
      //    constructionStack.printStackTrace();
    } catch (IOException ioe) {
      throw new JavaSerializationException(msg, ioe);
    }
  }

  /**
   * Converts to a String representation of the object.
   *
   * @return A string representation of the object.
   */
  public String toString() {
    return "JavaSerializedMessage[" + msg + "]";
  }
}
