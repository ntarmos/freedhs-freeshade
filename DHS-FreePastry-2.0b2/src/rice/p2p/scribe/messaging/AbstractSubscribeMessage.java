/*************************************************************************

"FreePastry" Peer-to-Peer Application Development Substrate 

Copyright 2002, Rice University. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither  the name  of Rice  University (RICE) nor  the names  of its
contributors may be  used to endorse or promote  products derived from
this software without specific prior written permission.

This software is provided by RICE and the contributors on an "as is"
basis, without any representations or warranties of any kind, express
or implied including, but not limited to, representations or
warranties of non-infringement, merchantability or fitness for a
particular purpose. In no event shall RICE or contributors be liable
for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of
substitute goods or services; loss of use, data, or profits; or
business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even
if advised of the possibility of such damage.

********************************************************************************/

package rice.p2p.scribe.messaging;

import java.io.IOException;

import rice.*;
import rice.p2p.commonapi.*;
import rice.p2p.commonapi.rawserialization.*;
import rice.p2p.scribe.*;
import rice.p2p.scribe.rawserialization.ScribeContentDeserializer;

/**
 * @(#) AbstractSubscribeMessage.java The ack for a subscribe message.
 *
 * @version $Id: AbstractSubscribeMessage.java 3274 2006-05-15 16:17:47Z jeffh $
 * @author Alan Mislove
 */
public abstract class AbstractSubscribeMessage extends ScribeMessage {

  /**
   * The id of this subscribe message
   */
  protected int id;

  /**
   * Constructor which takes a unique integer Id
   *
   * @param source The source address
   * @param topic DESCRIBE THE PARAMETER
   * @param id The unique id
   */
  public AbstractSubscribeMessage(NodeHandle source, Topic topic, int id) {
    super(source, topic);

    this.id = id;
  }

  /**
   * Protected because it should only be called from an extending class, to get
   * version numbers correct.
   *
   * @param buf DESCRIBE THE PARAMETER
   * @param endpoint DESCRIBE THE PARAMETER
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  protected AbstractSubscribeMessage(InputBuffer buf, Endpoint endpoint) throws IOException {
    super(buf, endpoint);
    id = buf.readInt();
  }

  /**
   * Returns this subscribe lost message's id
   *
   * @return The id of this subscribe lost message
   */
  public int getId() {
    return id;
  }

  /**
   * DESCRIBE THE METHOD
   *
   * @param buf DESCRIBE THE PARAMETER
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  public void serialize(OutputBuffer buf) throws IOException {
    super.serialize(buf);
    buf.writeInt(id);
  }
}

