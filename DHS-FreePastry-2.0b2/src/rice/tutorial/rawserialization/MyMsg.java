/*************************************************************************

"FreePastry" Peer-to-Peer Application Development Substrate 

Copyright 2002, Rice University. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither  the name  of Rice  University (RICE) nor  the names  of its
contributors may be  used to endorse or promote  products derived from
this software without specific prior written permission.

This software is provided by RICE and the contributors on an "as is"
basis, without any representations or warranties of any kind, express
or implied including, but not limited to, representations or
warranties of non-infringement, merchantability or fitness for a
particular purpose. In no event shall RICE or contributors be liable
for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of
substitute goods or services; loss of use, data, or profits; or
business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even
if advised of the possibility of such damage.

********************************************************************************/
/*
 *  Created on Feb 15, 2005
 */
package rice.tutorial.rawserialization;

import java.io.IOException;

import rice.p2p.commonapi.*;
import rice.p2p.commonapi.rawserialization.*;

/**
 * An example message.
 *
 * @version $Id: pretty.settings 2305 2005-03-11 20:22:33Z jeffh $
 * @author Jeff Hoye
 */
public class MyMsg implements RawMessage {

  /**
   * Where the Message came from.
   */
  Id from;
  /**
   * Where the Message is going.
   */
  Id to;
  /**
   * DESCRIBE THE FIELD
   */
  protected static final short TYPE = 1;

  /**
   * Constructor.
   *
   * @param from DESCRIBE THE PARAMETER
   * @param to DESCRIBE THE PARAMETER
   */
  public MyMsg(Id from, Id to) {
    this.from = from;
    this.to = to;
  }

  /**
   * Constructor for MyMsg.
   *
   * @param buf DESCRIBE THE PARAMETER
   * @param endpoint DESCRIBE THE PARAMETER
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  public MyMsg(InputBuffer buf, Endpoint endpoint) throws IOException {
    from = endpoint.readId(buf, buf.readShort());
    to = endpoint.readId(buf, buf.readShort());
  }

  /**
   * Use low priority to prevent interference with overlay maintenance traffic.
   *
   * @return The Priority value
   */
  public byte getPriority() {
    return Message.LOW_PRIORITY;
  }

  /**
   * Gets the Type attribute of the MyMsg object
   *
   * @return The Type value
   */
  public short getType() {
    return TYPE;
  }

  /**
   * Converts to a String representation of the object.
   *
   * @return A string representation of the object.
   */
  public String toString() {
    return "MyMsg from " + from + " to " + to;
  }

  /**
   * DESCRIBE THE METHOD
   *
   * @param buf DESCRIBE THE PARAMETER
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  public void serialize(OutputBuffer buf) throws IOException {
    buf.writeShort(from.getType());
    from.serialize(buf);
    buf.writeShort(to.getType());
    to.serialize(buf);
  }
}
