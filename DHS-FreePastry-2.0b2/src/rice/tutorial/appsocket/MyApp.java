/*************************************************************************

"FreePastry" Peer-to-Peer Application Development Substrate 

Copyright 2002, Rice University. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither  the name  of Rice  University (RICE) nor  the names  of its
contributors may be  used to endorse or promote  products derived from
this software without specific prior written permission.

This software is provided by RICE and the contributors on an "as is"
basis, without any representations or warranties of any kind, express
or implied including, but not limited to, representations or
warranties of non-infringement, merchantability or fitness for a
particular purpose. In no event shall RICE or contributors be liable
for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of
substitute goods or services; loss of use, data, or profits; or
business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even
if advised of the possibility of such damage.

********************************************************************************/
/*
 *  Created on Feb 15, 2005
 *
 *  TODO To change the template for this generated file go to
 *  Window - Preferences - Java - Code Style - Code Templates
 */
package rice.tutorial.appsocket;

import java.io.IOException;
import java.nio.ByteBuffer;

import rice.p2p.commonapi.*;
import rice.p2p.commonapi.appsocket.*;

/**
 * A very simple application.
 *
 * @version $Id: pretty.settings 2305 2005-03-11 20:22:33Z jeffh $
 * @author Jeff Hoye
 */
public class MyApp implements Application {
  /**
   * The Endpoint represents the underlieing node. By making calls on the
   * Endpoint, it assures that the message will be delivered to a MyApp on
   * whichever node the message is intended for.
   */
  protected Endpoint endpoint;

  /**
   * The node we were constructed on.
   */
  protected Node node;

  ByteBuffer[] outs;
  ByteBuffer out;

  ByteBuffer[] ins;
  ByteBuffer in;

  int MSG_LENGTH;

  /**
   * Constructor for MyApp.
   *
   * @param node DESCRIBE THE PARAMETER
   * @param factory DESCRIBE THE PARAMETER
   */
  public MyApp(Node node, final IdFactory factory) {
    // We are only going to use one instance of this application on each PastryNode
    this.endpoint = node.buildEndpoint(this, "myinstance");
    this.node = node;

    MSG_LENGTH = node.getLocalNodeHandle().getId().toByteArray().length;
    outs = new ByteBuffer[1];
    out = ByteBuffer.wrap(node.getLocalNodeHandle().getId().toByteArray());
    outs[0] = out;

    ins = new ByteBuffer[1];
    in = ByteBuffer.allocate(MSG_LENGTH);
    ins[0] = in;

    // example receiver interface
    endpoint.accept(
      new AppSocketReceiver() {

        /**
         * Called if we have a problem.
         *
         * @param socket DESCRIBE THE PARAMETER
         * @param e DESCRIBE THE PARAMETER
         */
        public void receiveException(AppSocket socket, Exception e) {
          e.printStackTrace();
        }

        public void receiveSelectResult(AppSocket socket, boolean canRead, boolean canWrite) {
          in.clear();
          try {
            long ret = socket.read(ins, 0, ins.length);

            if (ret != MSG_LENGTH) {
              // if you sent any kind of long message, you would need to handle this case better
              System.out.println("Error, we only received part of a message." + ret + " from " + socket);
              return;
            }

            System.out.println(MyApp.this.node.getLocalNodeHandle() + " Received message from " + factory.buildId(in.array()));
          } catch (IOException ioe) {
            ioe.printStackTrace();
          }
          // only need to do this if expecting more messages
//        socket.register(true, false, 3000, this);
        }

        /**
         * When we accept a new socket.
         *
         * @param socket DESCRIBE THE PARAMETER
         */
        public void receiveSocket(AppSocket socket) {
//        System.out.println("Accepted socket: "+socket);
          socket.register(true, false, 30000, this);

          // it's critical to call this to be able to accept multiple times
          endpoint.accept(this);
        }

      });

    endpoint.register();
  }

  /**
   * Getter for the node.
   *
   * @return The Node value
   */
  public Node getNode() {
    return node;
  }

  /**
   * Called to directly send a message to the nh
   *
   * @param nh DESCRIBE THE PARAMETER
   */
  public void sendMyMsgDirect(NodeHandle nh) {
    System.out.println(this + " opening to " + nh);
    endpoint.connect(nh,
      new AppSocketReceiver() {

        /**
         * Called if there is a problem.
         *
         * @param socket DESCRIBE THE PARAMETER
         * @param e DESCRIBE THE PARAMETER
         */
        public void receiveException(AppSocket socket, Exception e) {
          e.printStackTrace();
        }

        /**
         * Example of how to write some bytes
         *
         * @param socket DESCRIBE THE PARAMETER
         * @param canRead DESCRIBE THE PARAMETER
         * @param canWrite DESCRIBE THE PARAMETER
         */
        public void receiveSelectResult(AppSocket socket, boolean canRead, boolean canWrite) {
          try {
            long ret = socket.write(outs, 0, outs.length);
//          System.out.println("WROTE:"+ret+" to "+socket);
            // see if we are done
            if (!out.hasRemaining()) {
              socket.close();
              out.clear();
            } else {
              // keep writing
              socket.register(false, true, 30000, this);
            }
          } catch (IOException ioe) {
            ioe.printStackTrace();
          }
        }

        /**
         * Called when the socket comes available.
         *
         * @param socket DESCRIBE THE PARAMETER
         */
        public void receiveSocket(AppSocket socket) {
//        System.out.println("Connected socket: "+socket);
          socket.register(false, true, 30000, this);
        }
      }, 30000);
  }

  /**
   * Called when we receive a message.
   *
   * @param id DESCRIBE THE PARAMETER
   * @param message DESCRIBE THE PARAMETER
   */
  public void deliver(Id id, Message message) {
    System.out.println(this + " received " + message);
  }

  /**
   * Called when you hear about a new neighbor. Don't worry about this method
   * for now.
   *
   * @param handle DESCRIBE THE PARAMETER
   * @param joined DESCRIBE THE PARAMETER
   */
  public void update(NodeHandle handle, boolean joined) {
  }

  /**
   * Called a message travels along your path. Don't worry about this method for
   * now.
   *
   * @param message DESCRIBE THE PARAMETER
   * @return DESCRIBE THE RETURN VALUE
   */
  public boolean forward(RouteMessage message) {
    return true;
  }

  /**
   * Converts to a String representation of the object.
   *
   * @return A string representation of the object.
   */
  public String toString() {
    return "MyApp " + endpoint.getId();
  }

}
