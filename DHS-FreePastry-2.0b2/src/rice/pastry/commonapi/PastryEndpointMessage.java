/*************************************************************************

"FreePastry" Peer-to-Peer Application Development Substrate 

Copyright 2002, Rice University. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

- Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

- Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

- Neither  the name  of Rice  University (RICE) nor  the names  of its
contributors may be  used to endorse or promote  products derived from
this software without specific prior written permission.

This software is provided by RICE and the contributors on an "as is"
basis, without any representations or warranties of any kind, express
or implied including, but not limited to, representations or
warranties of non-infringement, merchantability or fitness for a
particular purpose. In no event shall RICE or contributors be liable
for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of
substitute goods or services; loss of use, data, or profits; or
business interruption) however caused and on any theory of liability,
whether in contract, strict liability, or tort (including negligence
or otherwise) arising in any way out of the use of this software, even
if advised of the possibility of such damage.

********************************************************************************/

package rice.pastry.commonapi;

import java.io.IOException;

import rice.p2p.commonapi.Message;
import rice.p2p.commonapi.rawserialization.*;
import rice.p2p.util.rawserialization.JavaSerializedMessage;
import rice.pastry.NodeHandle;
import rice.pastry.messaging.*;

/**
 * This class is an internal message to the commonapi gluecode.
 *
 * @version $Id: PastryEndpointMessage.java 3387 2006-09-12 13:09:45Z jeffh $
 * @author Alan Mislove
 * @author Peter Druschel
 */
public class PastryEndpointMessage extends PRawMessage {

  /**
   * DESCRIBE THE FIELD
   */
  protected RawMessage message;

  /**
   * DESCRIBE THE FIELD
   */
  public static final short TYPE = 2;

  private static final long serialVersionUID = 4499456388556140871L;

//  protected boolean isRaw = false;

  /**
   * Constructor.
   *
   * @param address DESCRIBE THE PARAMETER
   * @param message DESCRIBE THE PARAMETER
   * @param sender DESCRIBE THE PARAMETER
   */
  public PastryEndpointMessage(int address, Message message, NodeHandle sender) {
    this(address, message instanceof RawMessage ? (RawMessage) message : new JavaSerializedMessage(message), sender);
  }

  /**
   * Constructor for PastryEndpointMessage.
   *
   * @param address DESCRIBE THE PARAMETER
   * @param message DESCRIBE THE PARAMETER
   * @param sender DESCRIBE THE PARAMETER
   */
  public PastryEndpointMessage(int address, RawMessage message, NodeHandle sender) {
    super(address);
    setSender(sender);
    this.message = message;
//    isRaw = true;
    setPriority(message.getPriority());
  }

  /**
   * Constructor for PastryEndpointMessage.
   *
   * @param address DESCRIBE THE PARAMETER
   * @param buf DESCRIBE THE PARAMETER
   * @param md DESCRIBE THE PARAMETER
   * @param sender DESCRIBE THE PARAMETER
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  public PastryEndpointMessage(int address, InputBuffer buf, MessageDeserializer md, NodeHandle sender) throws IOException {
    super(address);

    byte version = buf.readByte();
    switch (version) {
      case 0:
        setSender(sender);
        //    isRaw = buf.readBoolean();
        byte priority = buf.readByte();
        short type = buf.readShort();
        if (type == 0) {
          message = new JavaSerializedMessage(md.deserialize(buf, type, priority, sender));
        } else {
          message = (RawMessage) md.deserialize(buf, type, priority, sender);
        }
        if (getMessage() == null) {
          throw new IOException("PEM.deserialize() message = null type:" + type + " md:" + md);
        }
//    System.out.println("PEM.deserialize() message:"+message+" type:"+type+" md:"+md);
        break;
      default:
        throw new IOException("Unknown Version: " + version);
    }

  }

  /**
   * Returns the internal message
   *
   * @return the credentials.
   */
  public Message getMessage() {
    if (message.getType() == 0) {
      return ((JavaSerializedMessage) message).getMessage();
    }
    return message;
  }

  /**
   * Raw Serialization **************************************
   *
   * @return The Type value
   */
  public short getType() {
    return TYPE;
  }


  /**
   * Returns the internal message
   *
   * @param message The new Message value
   */
  public void setMessage(Message message) {
    if (message instanceof RawMessage) {
      setMessage((RawMessage) message);
    } else {
      this.message = new JavaSerializedMessage(message);
//     isRaw = false;
    }
  }

  /**
   * Returns the internal message
   *
   * @param message The new Message value
   */
  public void setMessage(RawMessage message) {
//    isRaw = true;
    this.message = message;
  }

  /**
   * Returns the String representation of this message
   *
   * @return The string
   */
  public String toString() {
    return "[PEM " + getMessage() + "]";
  }

  /**
   * DESCRIBE THE METHOD
   *
   * @param buf DESCRIBE THE PARAMETER
   * @exception IOException DESCRIBE THE EXCEPTION
   */
  public void serialize(OutputBuffer buf) throws IOException {
//    buf.writeBoolean(isRaw);

    buf.writeByte((byte) 0);
    // version
    buf.writeByte(message.getPriority());
    buf.writeShort(message.getType());
    message.serialize(buf);
//    System.out.println("PEM.serialize() message:"+message+" type:"+message.getType());
  }

}


