/**
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import netcins.ItemSerializable;

/**
 * Basic interface to be implemented by all sketch implementations.
 * 
 * @author ntarmos
 * @version $Rev$
 */
public interface Sketch {
	/**
	 * Inserts a single item in the sketch.
	 * 
	 * @param object The item to be added.
	 */
	public void insert(ItemSerializable object);

	/**
	 * Clears the internal state of the current sketch instance.
	 */
	public void clear();

	/**
	 * Computes the estimate of the current sketch instance.
	 * 
	 * @param algo The algorithm to use. See
	 *            {@link netcins.dbms.centralized.sketches.SketchAlgorithms} for
	 *            a list of supported algorithms. Not all algorithms are
	 *            supported by all sketch implementations.
	 * @return The estimate for the current sketch instance.
	 */
	public double estimate(SketchAlgorithms algo)
			throws UnsupportedSketchAlgorithmException;

	/**
	 * Computes the changes to be made in the internal structure of the current
	 * sketch instance, in order to insert the given item to the sketch. To be
	 * used by distributed counterparts of centralized sketches and for testing
	 * purposes.
	 * 
	 * @param data The item to be inserted.
	 * @return The changes to be made to the internal sketch structure. The
	 *         actual type of the changes differs from sketch to sketch. See the
	 *         individual sketch implementations for details.
	 * @see netcins.p2p.dhs.DHSImpl
	 * @see netcins.dbms.distributed.sketches.DHSAMSSketchImpl
	 */
	public Object tryAddObject(ItemSerializable data);
}
