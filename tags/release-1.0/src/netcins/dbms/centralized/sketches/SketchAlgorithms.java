/**
 * $Id$
 */

package netcins.dbms.centralized.sketches;

/**
 * Enumeration capturing the implemented Sketch estimators.
 * 
 * @author ntarmos
 * @version $Rev$
 * @see netcins.dbms.centralized.sketches.Sketch
 * @see netcins.dbms.centralized.sketches.HashSketchImpl
 * @see netcins.dbms.centralized.sketches.AMSSketchImpl
 */
public enum SketchAlgorithms {
	/** FM85 Hash Sketch estimator */
	FM85,
	/** DF03 Hash Sketch estimator */
	DF03,
	/** AMS Tug-of-War estimator */
	TugOfWar;
}
