/**
 * $Id$
 */

package netcins.dbms.centralized.sketches;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class UnsupportedSketchAlgorithmException extends Exception {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3888787148579751554L;

	public UnsupportedSketchAlgorithmException(String msg) {
		super(msg);
	}

	public UnsupportedSketchAlgorithmException() {
		super("Unsupported sketch algorithm");
	}
}
