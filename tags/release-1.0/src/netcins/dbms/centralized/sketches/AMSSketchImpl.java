/**
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

import netcins.ItemSerializable;
import netcins.dbms.BasicItem;
import netcins.util.QuickMath;

/**
 * Implementation of Tug-of-War AMS skethces. For a list of other sketch
 * algorithms implemented see
 * {@link netcins.dbms.centralized.sketches.SketchAlgorithms}.
 * 
 * @author ntarmos
 * @version $Rev$
 * @see netcins.dbms.centralized.sketches.Sketch
 */
public class AMSSketchImpl implements Sketch {
	/** Default hash algorithm used to compute the hash value for item insertion */
	public static final String	defaultHashAlgo	= "MD5";

	private MessageDigest		hash			= null;
	private double				counters[][]	= null;
	private int					s1, s2;
	private String				hashAlgo		= null;

	/**
	 * Constructor for AMSSketchImpl.
	 * 
	 * @param s1 The S1 parameter of Tug-of-War AMS sketches.
	 * @param s2 The S2 parameter of Tug-of-War AMS sketches.
	 * @throws NoSuchAlgorithmException
	 */
	public AMSSketchImpl(int s1, int s2) throws NoSuchAlgorithmException {
		this(s1, s2, AMSSketchImpl.defaultHashAlgo);
	}

	/**
	 * Constructor for AMSSketchImpl.
	 * 
	 * @param s1 The S1 parameter of Tug-of-War AMS sketches.
	 * @param s2 The S2 parameter of Tug-of-War AMS sketches.
	 * @param hashAlgo The name of the hash algorithm to use. Must be a valid
	 *            hash, supported by the local environment/provider.
	 */
	public AMSSketchImpl(int s1, int s2, String hashAlgo)
			throws NoSuchAlgorithmException {
		this.hashAlgo = hashAlgo;
		try {
			hash = MessageDigest.getInstance(this.hashAlgo);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw e;
		}
		this.s1 = s1;
		this.s2 = s2;
		this.counters = new double[s1][s2];
	}

	/**
	 * Inserts a single item into the sketch.
	 * 
	 * @see netcins.ItemSerializable
	 * @see netcins.dbms.centralized.sketches.Sketch
	 */
	public void insert(ItemSerializable item) {
		int bits[][] = (int[][])tryAddObject(item);

		for (int indexS1 = 0; indexS1 < s1; indexS1++) {
			for (int indexS2 = 0; indexS2 < s2; indexS2++) {
				counters[indexS1][indexS2] += bits[indexS1][indexS2];
			}
		}
	}

	/**
	 * Computes the changes that have to be made to the internal counters data
	 * in order to insert the new item into the current AMSSketchImpl instance.
	 * 
	 * @param item The item to be added.
	 * @return A two-dimensional array of size [s1][s2] with the changes to be
	 *         made.
	 * @see netcins.dbms.centralized.sketches.Sketch
	 */
	public Object tryAddObject(ItemSerializable item) {
		byte[] itemBytes = item.toByteArray();
		byte[] material = new byte[itemBytes.length + 4];
		for (int i = 0; i < itemBytes.length; i++) {
			material[4 + i] = itemBytes[i];
		}
		int ret[][] = new int[s1][s2];

		for (int indexS1 = 0; indexS1 < s1; indexS1++) {
			for (int indexS2 = 0; indexS2 < s2; indexS2++) {
				int bitIndex = indexS1 * s2 + indexS2;
				material[0] = (byte)(bitIndex & 0x000000ff);
				material[1] = (byte)((bitIndex & 0x0000ff00) >> 8);
				material[2] = (byte)((bitIndex & 0x00ff0000) >> 16);
				material[3] = (byte)((bitIndex & 0xff000000) >> 24);

				hash.reset();
				hash.update(material);
				byte[] digest = hash.digest();
				ret[indexS1][indexS2] = (((digest[0] & 0x01) == 0) ? -1 : 1);
			}
		}
		return ret;
	}

	/**
	 * Clears the current AMSSketchImpl instance.
	 * 
	 * @see netcins.dbms.centralized.sketches.Sketch#clear()
	 */
	public void clear() {
		counters = new double[s1][s2];
	}

	/**
	 * Computes the estimate using the AMS Tug-of-War algorithm. Prefer this
	 * method insted of {@link #estimate(SketchAlgorithms)}.
	 * 
	 * @return The Tug-of-War estimate for the current sketch instance.
	 * @see netcins.dbms.centralized.sketches.Sketch#estimate(SketchAlgorithms)
	 * @see #estimate(SketchAlgorithms)
	 */
	public double estimate() {
		try {
			return estimate(SketchAlgorithms.TugOfWar);
		} catch (UnsupportedSketchAlgorithmException e) {
			e.printStackTrace();
		}
		throw new UnknownError();
	}

	/**
	 * Computes the estimate using the AMS Tug-of-War algorithm.
	 * 
	 * @param algo The algorithm to use (must be
	 *            {@link SketchAlgorithms#TugOfWar}).
	 * @return The Tug-of-War estimate for the current sketch instance.
	 * @throws UnsupportedSketchAlgorithmException
	 * @see netcins.dbms.centralized.sketches.Sketch#estimate(SketchAlgorithms)
	 */
	public double estimate(SketchAlgorithms algo)
			throws UnsupportedSketchAlgorithmException {
		if (SketchAlgorithms.TugOfWar.equals(algo) == false)
			throw new UnsupportedSketchAlgorithmException();

		double sValues[] = new double[s1];
		for (int indexS1 = 0; indexS1 < s1; indexS1++) {
			for (int indexS2 = 0; indexS2 < s2; indexS2++) {
				sValues[indexS1] += counters[indexS1][indexS2]
						* counters[indexS1][indexS2];
			}
			sValues[indexS1] /= s2;
		}
		Arrays.sort(sValues);
		return sValues[s1 / 2];
	}

	/**
	 * Returns the S1 parameter for the current AMSSketchImpl instance.
	 * 
	 * @return The S1 parameter for the current AMSSketchImpl instance.
	 */
	public int getS1() {
		return s1;
	}

	/**
	 * Returns the S2 parameter for the current AMSSketchImpl instance.
	 * 
	 * @return The S2 parameter for the current AMSSketchImpl instance.
	 */
	public int getS2() {
		return s2;
	}

	public void setCounters(double[][] counters) {
		if (counters == null || counters.length == 0 || counters[0].length == 0)
			return;
		this.s1 = counters.length;
		this.s2 = counters[0].length;
		this.counters = new double[s1][s2];
		for (int i = 0; i < s1; i++)
			for (int j = 0; j < s2; j++)
				this.counters[i][j] = counters[i][j];
	}

	public static void main(String args[]) {
		int s1 = Integer.parseInt(args[0]);
		int s2 = Integer.parseInt(args[1]);
		int numItems = Integer.parseInt(args[2]);
		AMSSketchImpl ams = null;
		try {
			ams = new AMSSketchImpl(s1, s2);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return;
		}
		Random prng = new Random(System.currentTimeMillis());
		long time = System.currentTimeMillis();
		for (int i = 0; i < numItems; i++) {
			int data = prng.nextInt();
			ams.insert(new BasicItem(data));
		}
		double estimate = ams.estimate();
		time = System.currentTimeMillis() - time;
		System.out.println("AMS(" + numItems + "): " + estimate + " , error: "
				+ (100 * QuickMath.abs(numItems - estimate) / (double)numItems)
				+ "%");
		System.out.println("Total time: " + (time / 1000) + "''");
	}

}
