/**
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.BitSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.ItemSerializable;
import netcins.p2p.dhs.DHSTuple;
import netcins.util.QuickMath;

/**
 * @author ntarmos
 * 
 */
public class HashSketchImpl implements Sketch {

	public static final String	defaultHashAlgo			= "MD5";
	public static final double	df03VecsSetULPercentage	= 1.0;
	private MessageDigest		hash					= null;
	private BitSet[]			sketch					= null;
	private String				hashAlgo				= null;
	private int					numBits					= 0;
	private boolean				isEmpty					= true;

	public HashSketchImpl(HashSketchImpl other) throws NoSuchAlgorithmException {
		this(other.getNumVecs(), other.getNumBits(), other.getHashAlgo());
	}

	public HashSketchImpl(int numVecs, String hashAlgo)
			throws NoSuchAlgorithmException {
		try {
			hash = MessageDigest.getInstance(hashAlgo);
		} catch (NoSuchAlgorithmException e) {
			Logger log = Logger.getLogger(this.getClass().getName());
			log.log(Level.SEVERE, "Can't find an implementation for "
					+ hashAlgo + ". Bailing out...");
			throw e;
		}
		numBits = hash.getDigestLength() * 8;
		sketch = new BitSet[numVecs];
		this.hashAlgo = hashAlgo;
		for (int i = 0; i < numVecs; i++)
			sketch[i] = new BitSet();
	}

	public HashSketchImpl(int numVecs) throws NoSuchAlgorithmException {
		this(numVecs, defaultHashAlgo);
	}

	public HashSketchImpl(int numVecs, int numBits, String hashAlgo)
			throws NoSuchAlgorithmException {
		this(numVecs, hashAlgo);
		assert (numBits <= (hash.getDigestLength() * 8));
		this.numBits = numBits;
	}

	public HashSketchImpl(int numVecs, int numBits)
			throws NoSuchAlgorithmException {
		this(numVecs, numBits, defaultHashAlgo);
	}

	public int getNumVecs() {
		return (sketch != null ? sketch.length : 0);
	}

	public int getNumBits() {
		return numBits;
	}

	public String getHashAlgo() {
		return hashAlgo;
	}

	public BitSet getVec(int vec) {
		return sketch[vec];
	}

	/**
	 * Adds an object to the hash sketch.
	 * 
	 * @param data Input object
	 * @return An int[] containing: (i) in position 0, the index of the vector
	 *         to be updated by the input object, and (ii) in position 1, the
	 *         index of the bit to be set in the selected bitmap vector.
	 */
	public Object tryAddObject(ItemSerializable data) {
		if (sketch == null || sketch.length == 0)
			throw new RuntimeException(
					"HashSketchImpl: Uninitialized instance.");

		int[] ret = new int[2];

		if (data instanceof DHSTuple) {
			DHSTuple tuple = (DHSTuple)data;
			if (tuple.getBit() < getNumBits() && tuple.getVec() < getNumVecs()) {
				ret[0] = tuple.getVec();
				ret[1] = tuple.getBit();
				return ret;
			} else
				throw new RuntimeException("Huh?!");
		}

		hash.reset();
		hash.update(data.toByteArray());
		byte[] digest = hash.digest();

		int numVecBits = Integer.SIZE
				- Integer.numberOfLeadingZeros(sketch.length) - 1;
		int numVecBytes = numVecBits / 8;

		int vec = 0;
		for (int i = 0; i < numVecBytes; i++)
			vec |= (((int)(digest[i] & 0xff)) << (i * 8));
		if ((numVecBits % 8) != 0) {
			byte lastVecByte = digest[numVecBytes];

			byte mask = 0x00;
			for (int i = 0; i < (numVecBits % 8); i++)
				mask |= (0x01 << i);
			vec |= ((int)((lastVecByte & mask) & 0xff)) << (numVecBytes * 8);
		}

		int rho = 0;
		for (int i = numVecBits; i < digest.length * 8; i++) {
			if ((digest[i / 8] & (1 << (i % 8))) != 0)
				rho++;
			else
				break;
		}

		ret[0] = vec;
		ret[1] = rho;

		return ret;
	}

	public void insert(ItemSerializable data) {
		int[] bits = (int[])tryAddObject(data);
		setBit(bits[0], bits[1]);
	}

	public double estimate(SketchAlgorithms algo)
			throws UnsupportedSketchAlgorithmException {
		switch (algo) {
			case DF03: {
				return df03();
			}
			case FM85: {
				return fm85();
			}
			default: {
				throw new UnsupportedSketchAlgorithmException();
			}
		}
	}

	private double df03() {
		double meanR = 0;
		int[] allR = new int[sketch.length];
		for (int i = 0; i < sketch.length; i++) {
			allR[i] = (sketch[i].length() > 0 ? sketch[i].length() : 0);
		}
		Arrays.sort(allR);
		int low = (int)QuickMath.floor((double)sketch.length
				* (1.0 - HashSketchImpl.df03VecsSetULPercentage));

		for (int i = low; i < sketch.length; i++)
			meanR += allR[i];

		meanR /= (sketch.length - low);
		return 0.39701 * Math.pow(2, meanR) * (sketch.length - low);
	}

	private double fm85() {
		double meanR = 0;
		for (int i = 0; i < sketch.length; i++) {
			for (int j = 0; j <= sketch[i].length(); j++) {
				if (sketch[i].get(j) == false) {
					meanR += j;
					break;
				}
			}
		}
		meanR /= sketch.length;
		return 1.29281 * Math.pow(2.0, meanR) * sketch.length;
	}

	public String diff(HashSketchImpl hs) {
		if (this.sketch == null || hs.sketch == null
				|| this.sketch.length != hs.sketch.length
				|| hs.numBits != this.numBits)
			return "Uninitialized or different type sketches";
		BitSet[] ret = new BitSet[this.sketch.length];
		for (int i = 0; i < this.sketch.length; i++) {
			ret[i] = this.sketch[i];
			ret[i].xor(hs.sketch[i]);
		}
		return toStringFull(ret, this.numBits);
	}

	public boolean setBit(int nVec, int nBit) {
		assert (nVec >= 0 && nVec < sketch.length);
		assert (nBit >= 0 && nBit < numBits);
		boolean oldValue = sketch[nVec].get(nBit);
		sketch[nVec].set(nBit);
		isEmpty = false;
		return oldValue;
	}

	public boolean getBit(int nVec, int nBit) {
		assert (nVec >= 0 && nVec < sketch.length);
		assert (nBit >= 0 && nBit < numBits);
		return sketch[nVec].get(nBit);
	}

	public void clear() {
		for (int i = 0; i < sketch.length; i++)
			sketch[i] = new BitSet();
		isEmpty = true;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	/*
	 * private int rho(BitSet input) { return (input.cardinality() == 0 ? 0 :
	 * input.nextSetBit(0)); }
	 */

	public String toString() {
		return "[ hash: " + hashAlgo + " , numVecs: " + sketch.length
				+ " , numBits: " + numBits + " ]";
	}

	public String toStringFull() {
		return HashSketchImpl.toStringFull(this);
	}

	public static String toStringFull(HashSketchImpl hs) {
		return HashSketchImpl.toStringFull(hs.sketch, hs.numBits);
	}

	private static String toStringFull(BitSet[] hs, int nBits) {
		if (hs == null)
			return "[HS :: Uninitialized instance]";
		StringBuilder ret = new StringBuilder();
		int numVecDigits = (hs.length > 1000 ? 4 : (hs.length > 100 ? 3 : 2));
		for (int i = 0; i < hs.length; i++) {
			StringBuilder builder = new StringBuilder();
			for (int j = 0; j < hs[i].length() && j < nBits; j++)
				builder.append(hs[i].get(j));
			for (int j = builder.length(); j < nBits; j++)
				builder.append("0");
			ret.append("[ ");
			for (int p = 0; p < (numVecDigits - (i < 10 ? 1 : (i < 100 ? 2 : 3))); p++)
				ret.append(" ");

			ret.append(i + " : " + builder.toString() + " ]" + "\n");
		}
		return ret.toString();
	}

	/*
	 * public static void main(String[] args) { HashSketchImpl hs = new
	 * HashSketchImpl(256, "SHA-1"); Random prng = new
	 * Random(System.currentTimeMillis()); for (int i = 0; i < 50000; i ++) {
	 * int data = (int)QuickMath.floor(prng.nextDouble() * 2000.0);
	 * hs.insert(new BasicItem(data)); } System.out.println("DF03: " +
	 * hs.df03()); System.out.println("FM85: " + hs.fm85()); }
	 */
}
