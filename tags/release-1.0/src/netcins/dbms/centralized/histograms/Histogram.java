/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import java.util.Collection;
import java.util.Vector;

import netcins.dbms.BasicItem;

/**
 * Basic interface to be implemented by all centralized histogram types.
 * 
 * @author ntarmos
 * @version $Rev$
 */
public interface Histogram {

	/**
	 * Inserts the specified item into the histogram.
	 * 
	 * @param item The item to insert.
	 * @throws BucketIndexOutOfBoundsException
	 */
	public void insert(BasicItem item) throws BucketIndexOutOfBoundsException;

	/**
	 * Inserts all data items in the specified Collection into the histogram.
	 * 
	 * @param items A java.util.Collection of BasicItem objects to insert into
	 *            the histogram.
	 * @throws BucketIndexOutOfBoundsException
	 */
	public void insertAll(Collection<? extends BasicItem> items)
			throws BucketIndexOutOfBoundsException;

	/**
	 * Inserts all data items in the specified Collection into the histogram.
	 * 
	 * @param items A java.util.Collection of BasicItem objects to insert into
	 *            the histogram.
	 * @throws BucketIndexOutOfBoundsException
	 */
	public void insertAll(Vector<ItemFreq> items)
			throws BucketIndexOutOfBoundsException;

	/**
	 * Inserts the specified item into the histogram with the given frequency.
	 * 
	 * @param item The item to insert.
	 * @param freq The item's frequency. It's up to the histogram implementation
	 *            to decide whether to copy the frequency verbatim to the target
	 *            bucket or add it to any previous frequency information for the
	 *            specified item.
	 * @throws BucketIndexOutOfBoundsException if the item inserted is less than
	 *             the histogram's lowest value or greater-or-equal to the
	 *             histogram's highest value.
	 */
	public void insert(BasicItem item, double freq)
			throws BucketIndexOutOfBoundsException;

	/**
	 * Returns the estimated number of items in the specified range.
	 * 
	 * @param low The lower bound of the range.
	 * @param high The upper bound of the range.
	 * @return The estimated number of items in the specified range.
	 * @throws BucketIndexOutOfBoundsException
	 */
	public abstract double getNumItemsInRange(BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException;

	/**
	 * Returns the estimated number of distinct items in the specified range.
	 * 
	 * @param low The lower bound of the range.
	 * @param high The upper bound of the range.
	 * @return The estimated number of distinct items in the specified range.
	 * @throws BucketIndexOutOfBoundsException
	 */
	public abstract double getNumDistinctItemsInRange(BasicItem low,
			BasicItem high) throws BucketIndexOutOfBoundsException;

	/**
	 * Returns the estimated number of items in the specified bucket.
	 * 
	 * @param bucket The index of the bucket whose item count is requested.
	 * @return The estimated number of items in the specified bucket.
	 * @throws BucketIndexOutOfBoundsException
	 */
	public abstract double getNumItemsInBucket(int bucket)
			throws BucketIndexOutOfBoundsException;

	/**
	 * Returns the estimated number of distinct items in the specified bucket.
	 * 
	 * @param bucket The index of the bucket whose item count is requested.
	 * @return The estimated number of distinct items in the specified bucket.
	 * @throws BucketIndexOutOfBoundsException
	 */
	public abstract double getNumDistinctItemsInBucket(int bucket)
			throws BucketIndexOutOfBoundsException;

	/**
	 * Computes the index of the bucket corresponding to the given item. The
	 * index returned corresponds to the current internal state of the
	 * histogram. That is, if one actually inserts the given item into the
	 * histogram, there is no guarantee it will actually be inserted in the
	 * bucket returned, since item insertions may alter the internal state of
	 * histograms.
	 * 
	 * @param item The item whose bucket's index is requested.
	 * @return The index of the bucket corresponding to the given item.
	 */
	public int getBucketForItem(BasicItem item)
			throws BucketIndexOutOfBoundsException;

	/**
	 * Initializes the internal bucket structure.
	 */
	public void initBuckets() throws UnsupportedOperationException;

	/**
	 * Returns the number of buckets of the histogram.
	 * 
	 * @return The number of buckets of the histogram.
	 */
	public int getNumBuckets();

	/**
	 * Sets the number of buckets of the histogram.
	 */
	public void setNumBuckets(int numBuckets);

	/**
	 * Returns a Bucket object with the information for the requested histogram
	 * bucket.
	 * 
	 * @param bucket The index of the requested bucket.
	 * @return A Bucket object with the requested information.
	 * @throws BucketIndexOutOfBoundsException if the supplied bucket index is
	 *             less than 0 or greater-or-equal to the number of buckets in
	 *             the histogram.
	 */
	public Bucket getBucket(int bucket) throws BucketIndexOutOfBoundsException;

	/**
	 * Clears the internal bucket structure and any other state kept by the
	 * histogram.
	 */
	public void clear();

	/**
	 * Returns the lowest value that can be inserted in the current histogram.
	 * 
	 * @return The lowest value that can be inserted in the current histogram.
	 */
	public BasicItem getLowest();

	/**
	 * Sets the lowest value that can be inserted in the current histogram.
	 */
	public void setLowest(BasicItem lowest);

	/**
	 * Returns the highest value that can be inserted in the current histogram.
	 * 
	 * @return The highest value that can be inserted in the current histogram.
	 */
	public BasicItem getHighest();

	/**
	 * Sets the highest value that can be inserted in the current histogram.
	 */
	public void setHighest(BasicItem highest);

	/**
	 * Returns a String representation of the histogram and its internals.
	 * 
	 * @return A String representation of the histogram and its internals.
	 */
	public String toStringFull();
}
