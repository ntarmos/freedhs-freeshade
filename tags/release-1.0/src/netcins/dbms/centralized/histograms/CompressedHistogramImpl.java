/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import netcins.dbms.BasicItem;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class CompressedHistogramImpl extends AdvancedHistogramImpl {

	private static class ItemFreqComparator implements Comparator<ItemFreq>, Serializable {
		/**
		 * 
		 */
		private static final long	serialVersionUID	= 2561240099026747754L;

		// Sort in reverse order.
		public int compare(ItemFreq o1, ItemFreq o2) {
			return (o1.getFreq() > o2.getFreq() ? -1
					: (o1.getFreq() < o2.getFreq() ? 1 : 0));
		}
	}

	public CompressedHistogramImpl(BasicItem lowest, BasicItem highest,
			Integer numBuckets) {
		super(lowest, highest, numBuckets);
	}

	public CompressedHistogramImpl(BasicItem lowest, BasicItem highest,
			Bucket[] buckets) {
		super(lowest, highest, buckets);
	}

	public int getBucketForItem(BasicItem item)
			throws BucketIndexOutOfBoundsException {
		initBuckets();
		for (int i = 0; i < buckets.length - 1; i++)
			if (buckets[i].getLowest().equals(item))
				return i;
		return buckets.length - 1;
	}

	public double getNumItemsInRange(BasicItem low, BasicItem high) {
		initBuckets();
		double freq = 0;
		for (int i = 0; i < buckets.length - 1; i++)
			if (buckets[i].getLowest().compareTo(low) >= 0
					&& buckets[i].getLowest().compareTo(high) < 0)
				freq += buckets[i].getItemCount();
		if (low.equals(high) == false)
			freq += buckets[buckets.length - 1].getItemCountInRange_USA(low,
					high);
		return freq;
	}

	public double getNumDistinctItemsInRange(BasicItem low, BasicItem high) {
		initBuckets();
		double freq = 0;
		for (int i = 0; i < buckets.length - 1; i++)
			if (buckets[i].getLowest().compareTo(low) >= 0
					&& buckets[i].getLowest().compareTo(high) < 0)
				freq += buckets[i].getItemCount();
		if (low.equals(high) == false)
			freq += buckets[buckets.length - 1].getDistinctItemCountInRange_USA(
					low, high);
		return freq;
	}

	public void initBuckets() {
		if (initData() == false)
			return;

		Collections.sort(data, new ItemFreqComparator());
		Iterator<ItemFreq> iterator = data.iterator();
		for (int i = 0; i < buckets.length - 1 && iterator.hasNext(); i++) {
			ItemFreq next = iterator.next();
			buckets[i] = new Bucket(next.getItem(), next.getItem(),
					next.getFreq(), 1);
		}
		double otherFreqs = 0;
		for (int i = buckets.length - 1; i < data.size() && iterator.hasNext(); i++) {
			otherFreqs += iterator.next().getFreq();
		}
		buckets[buckets.length - 1] = new Bucket(lowest, highest, otherFreqs,
				data.size() - buckets.length + 2);
		areBucketsInited = true;
	}

	public String toString() {
		return "[ CH :: " + super.toString() + " ]";
	}
}
