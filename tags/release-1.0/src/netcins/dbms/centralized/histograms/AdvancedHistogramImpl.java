/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import netcins.dbms.BasicItem;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public abstract class AdvancedHistogramImpl extends BasicHistogramImpl {

	protected Vector<ItemFreq>	data	= null;

	/**
	 * @param lowest
	 * @param highest
	 * @param numBuckets
	 */
	public AdvancedHistogramImpl(BasicItem lowest, BasicItem highest,
			Integer numBuckets) {
		super(lowest, highest, numBuckets);
		data = new Vector<ItemFreq>(100, 100);
	}

	/**
	 * @param lowest
	 * @param highest
	 * @param buckets
	 */
	public AdvancedHistogramImpl(BasicItem lowest, BasicItem highest,
			Bucket[] buckets) {
		super(lowest, highest, buckets);
		data = new Vector<ItemFreq>(100, 100);
	}

	public boolean initData() throws UnsupportedOperationException {
		if (data == null || data.size() == 0 || areBucketsInited == true)
			return false;

		Collections.sort(data);
		Vector<ItemFreq> newData = new Vector<ItemFreq>(100, 100);

		for (int item = 0; item < data.size(); item++) {
			ItemFreq next = data.get(item);
			int index = Collections.binarySearch(newData, next);
			if (index < 0)
				newData.add(-index - 1, next);
			else
				newData.get(index).addFreq(next.getFreq());
			data.set(item, null);

			/*
			 * if (next.getItem().equals(curItem.getItem())) {
			 * curItem.setFreq(curItem.getFreq() + next.getFreq());
			 * iterator.remove(); } else { curItem = next; }
			 */
		}
		data = newData;
		return true;
	}

	public static Vector<ItemFreq> initData(Vector<BasicItem> data) {
		if (data == null || data.size() == 0)
			return null;

		Collections.sort(data);
		Vector<ItemFreq> newData = new Vector<ItemFreq>(100, 100);

		for (int item = 0; item < data.size(); item++) {
			ItemFreq next = new ItemFreq(data.get(item));
			int index = Collections.binarySearch(newData, next);
			if (index < 0)
				newData.add(-index - 1, next);
			else
				newData.get(index).addFreq(next.getFreq());
		}
		return newData;
	}

	public static void initData(List<ItemFreq> data) {
		if (data == null || data.size() == 0)
			return;

		Collections.sort(data);

		Iterator<ItemFreq> iterator = data.iterator();
		ItemFreq curItem = iterator.next();
		for (; iterator.hasNext();) {
			ItemFreq next = iterator.next();
			if (next.getItem().equals(curItem.getItem())) {
				curItem.addFreq(next.getFreq());
				iterator.remove();
			} else {
				curItem = next;
			}
		}
	}

	public void insertAll(Collection<? extends BasicItem> items)
			throws BucketIndexOutOfBoundsException {
		areBucketsInited = false;
		for (BasicItem item : items)
			data.add(new ItemFreq(item));
	}

	public void insertAll(Vector<ItemFreq> items)
			throws BucketIndexOutOfBoundsException {
		areBucketsInited = false;
		data.addAll(items);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#insert(netcins.dbms.BasicItem,
	 *      double)
	 */
	public void insert(BasicItem item, double freq)
			throws BucketIndexOutOfBoundsException {
		data.add(new ItemFreq(item, freq));
		areBucketsInited = false;
	}

	public void clear() {
		super.clear();
		data.clear();
	}
}
