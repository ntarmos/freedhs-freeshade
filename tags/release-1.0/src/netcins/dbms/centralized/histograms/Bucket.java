/**
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import java.util.HashSet;

import netcins.dbms.BasicItem;
import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class Bucket {
	private BasicItem			lowest				= null;
	private BasicItem			highest				= null;
	private double				itemCount			= 0;
	private double				distinctItemCount	= 0;
	private HashSet<BasicItem>	items				= null;

	public Bucket(BasicItem lowest, BasicItem highest, double itemCount,
			double distinctItemCount) {
		this.lowest = (lowest.compareTo(highest) <= 0 ? lowest : highest);
		this.highest = (highest.compareTo(lowest) > 0 ? highest : lowest);
		this.itemCount = itemCount;
		this.distinctItemCount = distinctItemCount;
	}

	public Bucket(BasicItem lowest, BasicItem highest, double itemCount) {
		this(lowest, highest, itemCount, 0);
		this.items = new HashSet<BasicItem>(10, (float)0.9);
	}

	public Bucket(BasicItem lowest, BasicItem highest) {
		this(lowest, highest, 0);
	}

	public void addItem(BasicItem item, double freq) {
		if (item.compareTo(lowest) >= 0 && item.compareTo(highest) < 0) {
			if (items != null) {
				items.add(item);
				distinctItemCount = items.size();
			} else {
				distinctItemCount++;
			}
			itemCount += freq;
		}
	}

	public void addItem(BasicItem item) {
		addItem(item, 1);
	}

	public double getItemCountInRange_USA(BasicItem low, BasicItem high) {
		if (high.compareTo(lowest) <= 0 || low.compareTo(highest) > 0
				|| distinctItemCount == 0)
			return 0;
		if (highest.equals(lowest))
			return itemCount;
		double itemSpread = (highest.doubleValue() - lowest.doubleValue())
				/ distinctItemCount;
		double itemFreq = itemCount / distinctItemCount;

		double l = (low.compareTo(this.lowest) <= 0 ? this.lowest : low).doubleValue();
		l = QuickMath.round((l - lowest.doubleValue()) / itemSpread);
		double h = (high.compareTo(this.highest) > 0 ? this.highest : high).doubleValue();
		h = QuickMath.round((h - lowest.doubleValue()) / itemSpread);

		return itemFreq * (h - l);
	}

	public double getDistinctItemCountInRange_USA(BasicItem low, BasicItem high) {
		if (high.compareTo(lowest) <= 0 || low.compareTo(highest) > 0
				|| distinctItemCount == 0)
			return 0;
		if (highest.equals(lowest))
			return distinctItemCount;
		double itemSpread = (highest.doubleValue() - lowest.doubleValue())
				/ distinctItemCount;

		double l = (low.compareTo(this.lowest) <= 0 ? this.lowest : low).doubleValue();
		l = QuickMath.round((l - lowest.doubleValue()) / itemSpread);
		double h = (high.compareTo(this.highest) > 0 ? this.highest : high).doubleValue();
		h = QuickMath.round((h - lowest.doubleValue()) / itemSpread);

		return h - l;
	}

	public double getItemCount() {
		return itemCount;
	}

	public double getDistinctItemCount() {
		return distinctItemCount;
	}

	public void setItemCount(double itemCount) {
		this.itemCount = itemCount;
	}

	public void setDistinctItemCount(double distinctItemCount) {
		this.distinctItemCount = distinctItemCount;
	}

	public BasicItem getLowest() {
		return lowest;
	}

	public BasicItem getHighest() {
		return highest;
	}

	public String toString() {
		return ("[ " + lowest + " - " + highest + ", items: "
				+ QuickMath.doubleToString(itemCount) + ", distinct items: "
				+ QuickMath.doubleToString(distinctItemCount) + " ]");
	}
}
