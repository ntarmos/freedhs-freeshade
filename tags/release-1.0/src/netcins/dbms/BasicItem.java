/**
 * $Id$
 */

package netcins.dbms;

import java.io.IOException;

import netcins.ItemSerializable;
import netcins.util.DataSerializer;
import netcins.util.QuickMath;
import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;
import rice.pastry.Id;

/**
 * Class encapsulating the basic components of an "index tuple" in the P2P DBMS.
 * Index tuples contain the primary key of the tuple they index, and the value
 * of that tuple on the indexed attribute. BasicItem uses an Id instance to
 * represent the primary key, and a Number to represent its value.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class BasicItem extends Number implements Comparable<BasicItem>, ItemSerializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5889513213807392448L;

	/** The ID of the tuple this index tuple points to. */
	private Id					key					= null;

	/** The value of the tuple attribute represented by this BasicItem. */
	private Number				value				= null;

	/** The size in bytes of the item' key. */
	private static final int	keyByteCount		= Id.IdBitLength / 8;

	/**
	 * The size in bytes of the current BasicItem. This is large enough to hold
	 * a Double value, since Double is the wider subclass of Number.
	 */
	public static final int		SIZE				= (Double.SIZE + Id.IdBitLength) / 8;

	/**
	 * Constructs a new BasicItem instance with the given Id and value.
	 * 
	 * @param key the key
	 * @param value the value
	 */
	public BasicItem(Id key, Number value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * Constructs a new BasicItem instance with the given value and a null key.
	 * 
	 * @param value the value
	 */
	public BasicItem(Number value) {
		this.key = null;
		this.value = value; // XXX: Set to 'long' for the experimental
							// evaluation.
	}

	/**
	 * Constructs a new BasicItem from its serialized form.
	 * 
	 * @param material a byte array, filled up using {@link #toByteArray()}.
	 */
	public BasicItem(byte material[]) {
		key = Id.build(material);
		value = DataSerializer.deserializeDouble(material, keyByteCount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(BasicItem n) {
		return Double.compare(doubleValue(), n.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the difference
	 * of the current BasicItem's value, minus the value of the supplied
	 * BasicItem.
	 * 
	 * @param i the BasicItem whose value to subtract
	 * @return a new BasicItem instance whose value is equal to the difference
	 *         of the current BasicItem's value, minus the value of the supplied
	 *         BasicItem
	 */
	public BasicItem subtract(BasicItem i) {
		return new BasicItem(doubleValue() - i.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the difference
	 * of the current BasicItem's value, minus the value of the supplied Number.
	 * 
	 * @param n the Number whose value to subtract
	 * @return a new BasicItem instance whose value is equal to the difference
	 *         of the current BasicItem's value, minus the value of the supplied
	 *         Number
	 */
	public BasicItem subtract(Number n) {
		return new BasicItem(doubleValue() - n.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the sum of the
	 * current BasicItem's value plus the value of the supplied BasicItem.
	 * 
	 * @param i the BasicItem whose value to add up
	 * @return a new BasicItem instance whose value is equal to the sum of the
	 *         current BasicItem's value plus the value of the supplied
	 *         BasicItem
	 */
	public BasicItem add(BasicItem i) {
		return new BasicItem(doubleValue() + i.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the sum of the
	 * current BasicItem's value plus the value of the supplied Number.
	 * 
	 * @param n the Number whose value to add up
	 * @return a new BasicItem instance whose value is equal to the sum of the
	 *         current BasicItem's value plus the value of the supplied Number
	 */
	public BasicItem add(Number n) {
		return new BasicItem(doubleValue() + n.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the current
	 * BasicItem's value times the value of the supplied BasicItem.
	 * 
	 * @param i the BasicItem by which to multiply the curren value
	 * @return a new BasicItem instance whose value is equal to the current
	 *         BasicItem's value times the value of the supplied BasicItem
	 */
	public BasicItem multiply(BasicItem i) {
		return new BasicItem(doubleValue() * i.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the current
	 * BasicItem's value times the value of the supplied Number.
	 * 
	 * @param n the Number by which to multiply the curren value
	 * @return a new BasicItem instance whose value is equal to the current
	 *         BasicItem's value times the value of the supplied Number
	 */
	public BasicItem multiply(Number n) {
		return new BasicItem(doubleValue() * n.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the current
	 * BasicItem's value over the value of the supplied BasicItem.
	 * 
	 * @param i the BasicItem by which to divide the curren value
	 * @return a new BasicItem instance whose value is equal to the current
	 *         BasicItem's value over the value of the supplied BasicItem
	 */
	public BasicItem divide(BasicItem i) {
		return new BasicItem(doubleValue() / i.doubleValue());
	}

	/**
	 * Returns a new BasicItem instance whose value is equal to the current
	 * BasicItem's value over the value of the supplied Number.
	 * 
	 * @param n the Number by which to divide the curren value
	 * @return a new BasicItem instance whose value is equal to the current
	 *         BasicItem's value over the value of the supplied Number
	 */
	public BasicItem divide(Number n) {
		return new BasicItem(doubleValue() / n.doubleValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.ItemSerializable#toByteArray()
	 */
	public byte[] toByteArray() {
		byte ret[] = new byte[BasicItem.SIZE];

		if (key != null)
			key.toByteArray(ret, 0);
		DataSerializer.serializeNumber(value.doubleValue(), ret, keyByteCount);

		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.ItemSerializable#serialize(rice.p2p.commonapi.rawserialization.OutputBuffer)
	 */
	public void serialize(OutputBuffer buf) throws IOException {
		buf.writeDouble(doubleValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.ItemSerializable#deserialize(rice.p2p.commonapi.rawserialization.InputBuffer)
	 */
	public void deserialize(InputBuffer buf) throws IOException {
		value = buf.readDouble();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Number#intValue()
	 */
	@Override
	public int intValue() {
		return value.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Number#longValue()
	 */
	@Override
	public long longValue() {
		return value.longValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Number#floatValue()
	 */
	@Override
	public float floatValue() {
		return value.floatValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Number#doubleValue()
	 */
	@Override
	public double doubleValue() {
		return value.doubleValue();
	}

	/**
	 * @return the key
	 */
	public final Id getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public final void setKey(Id key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public final Number getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public final void setValue(Number value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result
				+ ((this.key == null) ? 0 : this.key.hashCode());
		result = PRIME * result
				+ ((this.value == null) ? 0 : this.value.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (this == o)
			return true;

		if (o instanceof BasicItem)
			return (value.equals(((BasicItem)o).value));
		if (o instanceof Number)
			return (value.equals((Number)o));
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return QuickMath.doubleToString(value.doubleValue());
	}
}
