/**
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.centralized.histograms.EquiDepthHistogramImpl;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSEquiDepthHistogramImpl extends DHSInferredHistogramImpl {

	/**
	 * @param instance
	 * @param lowest
	 * @param highest
	 * @param numBuckets
	 * @param dhsImpl
	 */
	public DHSEquiDepthHistogramImpl(String instance, BasicItem lowest,
			BasicItem highest, Integer numBuckets, DHSImpl dhsImpl) {
		super(instance, lowest, highest, numBuckets, dhsImpl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#reconstruct(netcins.p2p.dhs.DHSPastryAppl,
	 *      netcins.dbms.BasicItem, netcins.dbms.BasicItem)
	 */
	public DistributedHistogramReconstructionResult reconstruct(
			DHSPastryAppl sourceNode, BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException {
		return super.reconstruct(sourceNode, low, high,
				EquiDepthHistogramImpl.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DHSBasicHistogramImpl#toString()
	 */
	@Override
	public String toString() {
		return "[ EDH/DHS " + super.toString() + " ]";
	}
}
