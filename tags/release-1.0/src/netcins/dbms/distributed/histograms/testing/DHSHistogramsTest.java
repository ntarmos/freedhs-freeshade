/**
 * $Id$
 */

package netcins.dbms.distributed.histograms.testing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.AverageShiftedEquiWidthHistogramImpl;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.centralized.histograms.CompressedHistogramImpl;
import netcins.dbms.centralized.histograms.EquiDepthHistogramImpl;
import netcins.dbms.centralized.histograms.EquiWidthHistogramImpl;
import netcins.dbms.centralized.histograms.Histogram;
import netcins.dbms.centralized.histograms.ItemFreq;
import netcins.dbms.centralized.histograms.MaxDiffHistogramImpl;
import netcins.dbms.distributed.histograms.DHSEquiWidthHistogramImpl;
import netcins.dbms.distributed.histograms.DHSInferredHistogramImpl;
import netcins.dbms.distributed.histograms.DistributedHistogram;
import netcins.dbms.distributed.histograms.DistributedHistogramReconstructionResult;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithms;
import netcins.util.QuickMath;
import netcins.util.ZipfGenerator;
import rice.environment.Environment;
import rice.pastry.Id;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.direct.BasicNetworkSimulator;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.standard.RandomNodeIdFactory;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSHistogramsTest {
	private DHSImpl					dhsImpl				= null;
	private PastryNodeFactory		factory				= null;
	private Environment				env					= null;
	private Vector<DHSPastryAppl>	pastryAppls			= null;
	private DHSSketchAlgorithms		sketchAlgo			= DHSImpl.defaultSketchAlgo;
	private DistributedHistogram	dhsHistos[]			= new DistributedHistogram[5];
	private Histogram				centralizedHistos[]	= new Histogram[5];

	private int						numItems			= 0;
	private int						numNodes			= -1;
	private int						qNode				= -1;

	private boolean					isInited			= false;
	private final Logger			logger				= Logger.getLogger("netcins.p2p.dhs.testing.DHSSingleMetricTest");
	private BasicNetworkSimulator	simulator			= null;

	public DHSHistogramsTest(BasicItem lowest, BasicItem highest,
			int numBuckets, DHSSketchAlgorithms sketchAlgo, int numVecs, int L,
			int retries, Environment env) {
		init(lowest, highest, numBuckets, sketchAlgo, numVecs, L, retries, env);
	}

	public DHSHistogramsTest(BasicItem lowest, BasicItem highest,
			int numBuckets, int numVecs, int L, int retries, Environment env) {
		init(lowest, highest, numBuckets, DHSImpl.defaultSketchAlgo, numVecs,
				L, retries, env);
	}

	public void init(BasicItem lowest, BasicItem highest, int numBuckets,
			DHSSketchAlgorithms algorithm, int numVecs, int L, int retries,
			Environment env) {
		if (isInited)
			destroy();
		this.env = env;
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null) {
			destroy();
			return;
		}
		this.pastryAppls = new Vector<DHSPastryAppl>(10, 10);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(
				new RandomNodeIdFactory(env), simulator, env);

		this.centralizedHistos[0] = new EquiWidthHistogramImpl(lowest, highest,
				numBuckets * DHSInferredHistogramImpl.ewhNumBucketsMultiplier);
		this.centralizedHistos[1] = new EquiDepthHistogramImpl(lowest, highest,
				numBuckets);
		this.centralizedHistos[2] = new CompressedHistogramImpl(lowest,
				highest, numBuckets);
		this.centralizedHistos[3] = new MaxDiffHistogramImpl(lowest, highest,
				numBuckets);
		this.centralizedHistos[4] = new AverageShiftedEquiWidthHistogramImpl(
				lowest, highest, numBuckets);

		initDHSHisto(algorithm, numVecs, L, retries, lowest, highest,
				numBuckets);
		Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting").setLevel(
				Level.INFO);
		isInited = true;
	}

	public void initDHSHisto(DHSSketchAlgorithms algorithm, int numVecs, int L,
			int retries, BasicItem lowest, BasicItem highest, int numBuckets) {
		this.sketchAlgo = algorithm;
		if (algorithm != null) {
			try {
				this.dhsImpl = DHSImpl.newInstance(algorithm, numVecs, L,
						retries, env);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			this.dhsHistos[0] = new DHSEquiWidthHistogramImpl("BI.DIH", lowest,
					highest, numBuckets
							* DHSInferredHistogramImpl.ewhNumBucketsMultiplier,
					dhsImpl);
			System.out.println("Initializing test with " + this.dhsHistos[0]);

			for (DHSPastryAppl app : pastryAppls)
				app.setDHSImpl(this.dhsImpl);
		}
	}

	private void clearHits() {
		for (DHSPastryAppl app : pastryAppls)
			app.clearHits();
	}

	public boolean isInited() {
		return isInited;
	}

	public void destroy() {
		if (isInited == false)
			return;
		isInited = false;
		env.destroy();
		pastryAppls = null;
		env = null;
	}

	public void clearData() {
		if (isInited == false)
			return;
		for (DHSPastryAppl appl : pastryAppls) {
			appl.reinitData();
		}
		clearHits();
		numItems = 0;
	}

	public void addNodes(int numNodes) {

		System.runFinalization();
		System.gc();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numNodes = numNodes;
		System.out.println("Populating the overlay with new nodes...");

		PastryNode bootstrapNode = null;
		for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0)
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle)null);
			else
				node = factory.newNode(bootstrapNode.getLocalHandle());

			logger.log(Level.FINER, "Finished creating new node " + node);

			DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);

			pastryAppls.add(dhsAppl);

			if ((curNodeIndex % 100) == 0)
				System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
			else if ((curNodeIndex % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + numNodes + "/" + numNodes + " done.");

		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
		for (int i = 0; i < pastryAppls.size(); i++) {
			pastryAppls.get(i).waitForNodeToBeReady();

			if ((i % 100) == 0)
				System.out.print(" " + i + "/" + numNodes + " ");
			else if ((i % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + pastryAppls.size() + "/" + numNodes + " done.");

		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl node generation took "
				+ (endTime - startTime) / 1000 + "\".");
		qNode = (int)QuickMath.floor(QuickMath.random() * pastryAppls.size());
	}

	public void addData(Vector<BasicItem> tuples) {
		long startTime = System.currentTimeMillis();
		long step = 100000;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems += tuples.size();

		System.runFinalization();
		System.gc();

		System.out.println("Injecting " + tuples.size() + " new data tuples...");
		long prevTime = System.currentTimeMillis();
		for (int data = 0; data < tuples.size(); data++) {
			BasicItem item = tuples.get(data);
			dhsHistos[0].insert(pastryAppls.get(data % pastryAppls.size()),
					item);
			if (data > 0 && (data % step) == 0) {
				waitForInsertionsToComplete();
				long curTime = System.currentTimeMillis();
				System.out.print(" " + data + "/" + tuples.size() + ", " + step
						* 1000L / (curTime - prevTime) + " items/sec ");
				prevTime = curTime;
			} else if (data > 0 && (data % (step / 5)) == 0) {
				System.out.print(".");
			}
		}
		waitForInsertionsToComplete();
		System.out.println(" "
				+ (numItems % step == 0 ? step : numItems % step) + "/"
				+ numItems + ", " + step * 1000L
				/ (System.currentTimeMillis() - prevTime) + " items/sec done. ");

		System.out.println("DHSImpl data generation took "
				+ (System.currentTimeMillis() - startTime) / 1000 + "\".");

		long numHits = 0, totalHops = 0;
		for (DHSPastryAppl curAppl : pastryAppls) {
			numHits += curAppl.getInsertionHits();
			totalHops += curAppl.getInsertionsHopCount();
		}
		System.out.println("Node insertion hits: "
				+ numHits
				+ ", avg. hops per node: "
				+ QuickMath.doubleToString((double)totalHops / (double)numNodes));
	}

	private void waitForInsertionsToComplete() {
		for (DHSPastryAppl curAppl : pastryAppls) {
			curAppl.waitForInsertionsToComplete();
			curAppl.clearInsertionResponses();
		}
	}

	public void query(BasicItem lowest, BasicItem highest,
			RandomAccessFile ifData) {
		clearHits();
		System.runFinalization();
		System.gc();

		System.out.println("Querying node: "
				+ pastryAppls.get(qNode).getNodeId());
		long startTime = System.currentTimeMillis(), endTime;

		System.out.print("Reassembling input data...");
		try {
			ifData.seek(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Vector<ItemFreq> bItems = new Vector<ItemFreq>();
		boolean hasMore = true;
		int numRead = 0;
		long readStep = 100000;
		long prevTime = System.currentTimeMillis();
		while (true) {
			byte[] material = new byte[BasicItem.SIZE];
			int left = material.length;
			try {
				while (left > 0) {
					int bytesRead = ifData.read(material, material.length
							- left, left);
					if (bytesRead < 0) {
						hasMore = false;
						break;
					}
					left -= bytesRead;
				}
				if (hasMore == false)
					break;
				numRead++;
				ItemFreq newItem = new ItemFreq(new BasicItem(material));
				
				if (newItem.getItem().compareTo(lowest) >= 0 && newItem.getItem().compareTo(highest) < 0) {
					int index = Collections.binarySearch(bItems, newItem);
					if (index < 0)
						bItems.add(-index - 1, newItem);
					else
						bItems.get(index).addFreq(1);
				}

				if (numRead > 0 && (numRead % readStep) == 0) {
					long curTime = System.currentTimeMillis();
					System.out.print(" " + numRead + "/" + numItems + ", "
							+ readStep * 1000 / (curTime - prevTime)
							+ " items/sec ");
					prevTime = curTime;
				} else if (numRead > 0 && (numRead % (readStep / 5)) == 0) {
					System.out.print(".");
				}
			} catch (IOException e) {
				break;
			}
		}
		System.out.println(" done.");

		System.out.print("Inserting into centralized histogram instance...");
		try {
			centralizedHistos[0].insertAll(bItems);
		} catch (BucketIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		System.out.println(" done");

		DistributedHistogramReconstructionResult distEWHisto = testHisto(
				dhsHistos[0], centralizedHistos[0], null);
		centralizedHistos[0].clear();

		long totalHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls) {
			totalHits += curAppl.getQueryHits();
		}
		System.out.println("Average hits per node: "
				+ QuickMath.doubleToString((double)totalHits / (double)numNodes));

		/*
		 * this.dhsHistos[0] = new DHSCompressedHistogramImpl("BI", lowest,
		 * highest, 1, dhsImpl); System.out.println("Initializing test with " +
		 * this.dhsHistos[0]); this.dhsHistos[1] = new
		 * DHSEquiDepthHistogramImpl("BI", lowest, highest, 1, dhsImpl);
		 * System.out.println("Initializing test with " + this.dhsHistos[1]);
		 * this.dhsHistos[3] = new DHSMaxDiffHistogramImpl("BI", lowest,
		 * highest, 1, dhsImpl); System.out.println("Initializing test with " +
		 * this.dhsHistos[3]);
		 */

		for (int i = 1; i < centralizedHistos.length; i++) {
			System.out.print("Inserting into centralized histogram instance...");
			try {
				centralizedHistos[i].insertAll(bItems);
				centralizedHistos[i].initBuckets();
			} catch (BucketIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			System.out.println(" done.");
			testHisto(null, centralizedHistos[i], distEWHisto);
			centralizedHistos[i].clear();
		}

		/*
		 * this.dhsHistos[0] = new DHSCompressedHistogramImpl("BI", lowest,
		 * highest, numBuckets, dhsImpl); System.out.println("Initializing test
		 * with " + this.dhsHistos[0]); this.dhsHistos[1] = new
		 * DHSEquiDepthHistogramImpl("BI", lowest, highest, numBuckets,
		 * dhsImpl); System.out.println("Initializing test with " +
		 * this.dhsHistos[1]);
		 * 
		 * this.dhsHistos[3] = new DHSMaxDiffHistogramImpl("BI", lowest,
		 * highest, numBuckets, dhsImpl); System.out.println("Initializing test
		 * with " + this.dhsHistos[3]);
		 */

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl query took " + (endTime - startTime) / 1000
				+ "\".");
		long numHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls)
			numHits += curAppl.getQueryHits();
		System.out.println("Node query hits: " + numHits);
	}

	private DistributedHistogramReconstructionResult testHisto(
			DistributedHistogram dTestHisto, Histogram cTestHisto,
			DistributedHistogramReconstructionResult recHisto) {
		
		System.out.println("Testing " + cTestHisto);
		DistributedHistogramReconstructionResult histo = (recHisto == null
				? dTestHisto.reconstruct(pastryAppls.get(qNode))
				: DHSInferredHistogramImpl.reconstruct(recHisto,
						cTestHisto.getLowest(), cTestHisto.getHighest(),
						cTestHisto.getClass()));
		Histogram distHisto = histo.getHistogram();
		try {
			System.out.println(sketchAlgo
					+ " centralized estimation: "
					+ cTestHisto.getNumItemsInRange(cTestHisto.getLowest(),
							cTestHisto.getHighest()));
			System.out.println(sketchAlgo
					+ " distributed estimation: "
					+ distHisto.getNumItemsInRange(cTestHisto.getLowest(),
							cTestHisto.getHighest()));
		} catch (BucketIndexOutOfBoundsException e1) {
			e1.printStackTrace();
		}
		System.out.println(sketchAlgo + " centralized histogram: " + "\n"
				+ cTestHisto.toStringFull());
		System.out.println(sketchAlgo + " distributed histogram: " + "\n"
				+ distHisto.toStringFull());
		System.out.println(sketchAlgo + " distributed estimation: " + histo);
		double avgError = 0, maxError = Double.MIN_VALUE, minError = Double.MAX_VALUE;
		double avgNormError = 0, maxNormError = Double.MIN_VALUE, minNormError = Double.MAX_VALUE;
		int maxIndex = 0, minIndex = 0;
		int maxNormIndex = 0, minNormIndex = 0;
		for (int i = 0; i < distHisto.getNumBuckets(); i++) {
			double cItems = 0;
			double dItems = 0;
			try {
				cItems = cTestHisto.getNumItemsInBucket(i);
				dItems = distHisto.getNumItemsInBucket(i);
			} catch (BucketIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			double rawError = QuickMath.abs(cItems - dItems);
			double error = rawError / (cItems > 0 ? cItems : 1);
			double normError = error * (cItems / (double)numItems);

			avgError += error;
			avgNormError += normError;

			if (maxError < error) {
				maxError = error;
				maxIndex = i;
			}
			if (minError > error) {
				minError = error;
				minIndex = i;
			}
			if (maxNormError < normError) {
				maxNormError = normError;
				maxNormIndex = i;
			}
			if (minNormError > normError) {
				minNormError = normError;
				minNormIndex = i;
			}
		}
		avgError /= (double)distHisto.getNumBuckets();

		System.out.println(sketchAlgo + ":" + "\n" + "\tminimum cell error: "
				+ QuickMath.doubleToString(100 * minError) + "% at cell #"
				+ minIndex + "\n" + "\tmaximum cell error: "
				+ QuickMath.doubleToString(100 * maxError) + "% at cell #"
				+ maxIndex + "\n" + "\taverage cell error: "
				+ QuickMath.doubleToString(100 * avgError) + "%" + "\n"
				+ "\tminimum normalized cell error: "
				+ QuickMath.doubleToString(100 * minNormError) + "% at cell #"
				+ minNormIndex + "\n" + "\tmaximum normalized cell error: "
				+ QuickMath.doubleToString(100 * maxNormError) + "% at cell #"
				+ maxNormIndex + "\n" + "\ttotal normalized cell error: "
				+ QuickMath.doubleToString(100 * avgNormError) + "%");
		return histo;
	}

	public static void main(String[] args) {

		int numNodes = 0, numTuples = 0, numVecs = 0, L = 0, retries = 0, numBuckets = 0, iterations = 0;
		BasicItem lowest = null, highest = null;
		double theta = 0;
		DHSHistogramsTest test = null;
		File datafile = null;
		RandomAccessFile raFile = null;

		try {
			numNodes = Integer.parseInt(args[0]);
			numTuples = Integer.parseInt(args[1]);
			numVecs = Integer.parseInt(args[2]);
			L = Integer.parseInt(args[3]);
			retries = Integer.parseInt(args[4]);
			lowest = new BasicItem(Integer.parseInt(args[5]));
			highest = new BasicItem(Integer.parseInt(args[6]));
			numBuckets = Integer.parseInt(args[7]);
			theta = Double.parseDouble(args[8]);
			iterations = Integer.parseInt(args[9]);
			datafile = File.createTempFile("fp-datafile-", ".dat");
			raFile = new RandomAccessFile(datafile, "rw");
		} catch (NumberFormatException e) {
			System.out.println("Usage: java [-cp lib/<*>.jar:classes]");
			System.out.println("\tnetcins.dbms.distributed.histograms.testing.DHSEquiWidthHistogramTest "
					+ "<numNodes> <numItems> <numVecs> <L> <retries> <domain low> <domain high> <numBuckets> <zipf theta> <iterations>");
			System.exit(0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}

		try {
			long startTime = System.currentTimeMillis(), endTime = 0;
			DHSSketchAlgorithms[] testAlgorithms = new DHSSketchAlgorithms[1];
			testAlgorithms[0] = DHSSketchAlgorithms.DF03BinarySearch;
			// testAlgorithms[1] = DHSSketchAlgorithms.FM85Classic;

			test = new DHSHistogramsTest(lowest, highest, numBuckets, null, 0,
					0, 0, null);
			int seed = (int)QuickMath.floor(Math.random() * Integer.MAX_VALUE);
			System.out.println(" --- Begin Simulation --- ");
			System.out.println(" --- Seed: " + seed + ", values in ["
					+ lowest.intValue() + ", " + highest.intValue()
					+ "], Zipf theta: " + theta + " --- ");

			Environment env = Environment.directEnvironment(seed);

			// XXX: The SelectorManager goes nuts when this is set to false!
			// env.getSelectorManager().setSelect(false);

			test.init(lowest, highest, numBuckets, null, numVecs, L, retries,
					env);
			test.addNodes(numNodes);
			test.initDHSHisto(DHSSketchAlgorithms.FM85Classic, numVecs, L,
					retries, lowest, highest, numBuckets);

			ZipfGenerator zipf = new ZipfGenerator(
					(int)QuickMath.floor(highest.subtract(lowest).doubleValue()),
					theta, seed);
			System.out.println("Adding tuples...");
			Integer[] values = new Integer[(int)QuickMath.floor(highest.subtract(
					lowest).doubleValue())];
			for (int i = 0; i < values.length; i++)
				values[i] = lowest.intValue() + i;
			Collections.shuffle(Arrays.asList(values));

			int tuplesStep = 500000;
			int numSteps = (int)QuickMath.ceil((double)numTuples
					/ (double)tuplesStep);
			test.clearHits();
			for (int step = 0; step < numSteps; step++) {
				Vector<BasicItem> data = new Vector<BasicItem>(tuplesStep);
				for (int i = 0; i < (step != (numSteps - 1) ? tuplesStep
						: (numTuples - step * tuplesStep)); i++) {
					BasicItem newItem = new BasicItem(
							Id.makeRandomId(env.getRandomSource()),
							values[zipf.nextRank()]);
					data.add(newItem);
					raFile.write(newItem.toByteArray());
				}
				System.out.println("Injecting tuples "
						+ (step * tuplesStep)
						+ " through "
						+ ((step + 1) * tuplesStep > numTuples ? numTuples
								: (step + 1) * tuplesStep) + " of " + numTuples
						+ " ...");
				test.addData(data);
			}
			zipf = null;
			values = null;
			System.out.println(" done.");

			for (int i = 0; i < iterations; i++) {
				test.qNode = env.getRandomSource().nextInt(numNodes);
				for (DHSSketchAlgorithms algo : testAlgorithms) {
					System.out.println("Testing " + algo + ", iteration " + i
							+ "...");
					test.initDHSHisto(algo, numVecs, L, retries, lowest,
							highest, numBuckets);
					test.query(lowest, highest, raFile);
					System.out.println("\n" + " ---" + "\n");
				}
			}
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: "
					+ (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
			datafile.delete();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}
}
