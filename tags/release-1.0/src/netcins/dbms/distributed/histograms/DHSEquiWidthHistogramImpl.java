/**
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.Bucket;
import netcins.dbms.centralized.histograms.EquiWidthHistogramImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSEquiWidthHistogramImpl extends DHSBasicHistogramImpl {
	private double	bucketSpread	= 0;

	/**
	 * @param instance
	 * @param lowest
	 * @param highest
	 * @param numBuckets
	 * @param dhsImpl
	 */
	public DHSEquiWidthHistogramImpl(String instance, BasicItem lowest,
			BasicItem highest, Integer numBuckets, DHSImpl dhsImpl) {
		super(instance, lowest, highest, numBuckets, dhsImpl);
		assert (dhsImpl != null);
		this.bucketSpread = highest.subtract(lowest).doubleValue()
				/ (double)(numBuckets);
		initBuckets();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#insert(netcins.p2p.dhs.DHSPastryAppl,
	 *      netcins.dbms.BasicItem)
	 */
	public void insert(DHSPastryAppl sourceNode, BasicItem item) {
		int targetBucket = getBucketForItem(null, item);
		dhsImpl.insert(sourceNode, bucketMetrics.get(targetBucket), item);
		dhsImpl.insert(sourceNode,
				distinctValuesBucketMetrics.get(targetBucket), new BasicItem(
						item.longValue()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#reconstruct(netcins.p2p.dhs.DHSPastryAppl,
	 *      netcins.dbms.BasicItem, netcins.dbms.BasicItem)
	 */
	public DistributedHistogramReconstructionResult reconstruct(
			DHSPastryAppl sourceNode, BasicItem low, BasicItem high) {
		double estimation = 0;
		int hopCount = 0;
		int numBitsProbed = 0;
		int bucketLow = getBucketForItem(null, low);
		int bucketHigh = getBucketForItem(null, high);
		int queryNumBuckets = bucketHigh - bucketLow + 1;

		Vector<String> queryMetrics = new Vector<String>(queryNumBuckets * 2);
		for (int i = 0; i < queryNumBuckets; i++) {
			queryMetrics.add(bucketMetrics.get(bucketLow + i));
			queryMetrics.add(distinctValuesBucketMetrics.get(bucketLow + i));
		}

		DistributedEstimationResult[] res = null;
		try {
			res = dhsImpl.estimate(sourceNode, queryMetrics);
		} catch (NoSuchMetricException e) {
			e.printStackTrace();
			return null;
		}

		Bucket[] tmpBuckets = new Bucket[queryNumBuckets];
		for (int i = 0; i < queryNumBuckets; i++) {
			BasicItem curBucketLow = new BasicItem((bucketLow + i)
					* bucketSpread);
			BasicItem curBucketHigh = new BasicItem((bucketLow + i + 1)
					* bucketSpread);
			double freq = res[2 * i].getDistributedEstimation();
			double distFreq = res[2 * i + 1].getDistributedEstimation();
			if (distFreq > (curBucketHigh.longValue() - curBucketLow.longValue()))
				distFreq = curBucketHigh.longValue() - curBucketLow.longValue();

			tmpBuckets[i] = new Bucket(lowest.add(curBucketLow),
					lowest.add(curBucketHigh), freq, distFreq);
		}

		if (res.length > 0) {
			hopCount = res[0].getHopCount();
			numBitsProbed = res[0].getNumBitsProbed();
		}

		return new DistributedHistogramReconstructionResult(
				new EquiWidthHistogramImpl(lowest, highest, tmpBuckets),
				hopCount, numBitsProbed);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DHSBasicHistogramImpl#getBucketForItem(netcins.p2p.dhs.DHSPastryAppl,
	 *      netcins.dbms.BasicItem)
	 */
	@Override
	public int getBucketForItem(DHSPastryAppl sourceNode, BasicItem item) {
		int ret = (int)QuickMath.floor(item.subtract(lowest).doubleValue()
				/ bucketSpread);
		return (ret < getNumBuckets() ? ret : ret - 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DHSBasicHistogramImpl#toString()
	 */
	@Override
	public String toString() {
		return "[ EWH/DHS " + super.toString() + ", spread: " + bucketSpread
				+ " ]";
	}
}
