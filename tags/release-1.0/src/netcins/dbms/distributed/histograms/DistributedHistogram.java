/**
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.p2p.dhs.DHSPastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public interface DistributedHistogram {
	public DistributedHistogramReconstructionResult reconstruct(
			DHSPastryAppl sourceNode);

	public DistributedHistogramReconstructionResult reconstruct(
			DHSPastryAppl sourceNode, BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException;

	public void insert(DHSPastryAppl sourceNode, BasicItem item);

	public abstract DistributedEstimationResult getNumItemsInRange(
			DHSPastryAppl sourceNode, BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException;

	public abstract DistributedEstimationResult getNumDistinctItemsInRange(
			DHSPastryAppl sourceNode, BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException;

	public int getBucketForItem(DHSPastryAppl sourceNode, BasicItem item)
			throws BucketIndexOutOfBoundsException;

	public void initBuckets();

	public int getNumBuckets();

	public void clear();

	public BasicItem getLowest();

	public BasicItem getHighest();
}
