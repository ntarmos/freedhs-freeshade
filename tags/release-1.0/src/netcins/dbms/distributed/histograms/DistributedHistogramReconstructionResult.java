/**
 * $Id: DistributedHistogramReconstructionResult.java 126 2006-12-11 12:47:35Z
 * ntarmos $
 */

package netcins.dbms.distributed.histograms;

import netcins.dbms.centralized.histograms.Histogram;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DistributedHistogramReconstructionResult {
	private Histogram	histogram		= null;
	private int			hopCount		= 0;
	private int			numBitsProbed	= 0;

	public DistributedHistogramReconstructionResult(Histogram histogram,
			int hopCount, int numBitsProbed) {
		this.histogram = histogram;
		this.hopCount = hopCount;
		this.numBitsProbed = numBitsProbed;
	}

	public int getHopCount() {
		return hopCount;
	}

	public int getNumBitsProbed() {
		return numBitsProbed;
	}

	public Histogram getHistogram() {
		return histogram;
	}

	public String toString() {
		return "[ " + histogram + " , bits probed: " + numBitsProbed
				+ " , hop count: " + hopCount + " ]";
	}

	public String toStringFull() {
		return toString() + "\n" + histogram.toStringFull();
	}
}
