/**
 * $Id$
 */

package netcins.dbms.distributed.sketches;

import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.AMSSketchImpl;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSAMSSketchImpl implements DistributedSketch {

	private AMSSketchImpl	amsImpl		= null;
	private DHSImpl			dhsImpl		= null;
	private String			instance	= null;
	private String[][][]	amsMetrics	= null;

	public DHSAMSSketchImpl(String instance, DHSImpl dhsImpl,
			AMSSketchImpl amsImpl) {
		this.dhsImpl = dhsImpl;
		this.amsImpl = amsImpl;
		this.instance = instance;
		amsMetrics = new String[amsImpl.getS1()][amsImpl.getS2()][2];

		for (int i = 0; i < amsImpl.getS1(); i++) {
			for (int j = 0; j < amsImpl.getS2(); j++) {
				amsMetrics[i][j][0] = this.instance + "[" + i + "," + j + ",-]";
				amsMetrics[i][j][1] = this.instance + "[" + i + "," + j + ",+]";
			}
		}
	}

	public void clear() {
		amsImpl.clear();
		dhsImpl.clear();
	}

	public DistributedEstimationResult[] estimate(DHSPastryAppl sourceNode,
			Vector<String> metrics) throws NoSuchMetricException {
		Vector<String> dhsMetrics = new Vector<String>(amsImpl.getS1()
				* amsImpl.getS2() * 2, 10);
		for (String metric : metrics) {
			for (int i = 0; i < amsImpl.getS1(); i++) {
				for (int j = 0; j < amsImpl.getS2(); j++) {
					dhsMetrics.add(metric + amsMetrics[i][j][0]);
					dhsMetrics.add(metric + amsMetrics[i][j][1]);
				}
			}
		}
		dhsImpl.addDHSMetrics(dhsMetrics);
		DistributedEstimationResult[] res = dhsImpl.estimate(sourceNode,
				dhsMetrics);

		Vector<String> resultMetrics = new Vector<String>(res.length, 10);
		for (DistributedEstimationResult r : res) {
			resultMetrics.add(r.getMetric());
		}

		DistributedEstimationResult[] ret = new DistributedEstimationResult[metrics.size()];

		int s1s2 = amsImpl.getS1() * amsImpl.getS2() * 2;
		for (int i = 0; i < metrics.size(); i++) {
			double[][] counters = new double[amsImpl.getS1()][amsImpl.getS2()];
			for (int s1 = 0; s1 < amsImpl.getS1(); s1++) {
				for (int s2 = 0; s2 < amsImpl.getS2(); s2++) {
					double counterPlus = res[resultMetrics.indexOf(metrics.get(i)
							+ amsMetrics[s1][s2][0])].getDistributedEstimation();
					double counterMinus = res[resultMetrics.indexOf(metrics.get(i)
							+ amsMetrics[s1][s2][1])].getDistributedEstimation();
					counters[s1][s2] = QuickMath.floor(counterPlus
							- counterMinus);
				}
			}

			amsImpl.setCounters(counters);
			double estimate = amsImpl.estimate();
			ret[i] = new DistributedEstimationResult(metrics.get(i), estimate,
					res[0].getHopCount(), res[0].getNumBitsProbed());
		}
		return ret;
	}

	public void insert(DHSPastryAppl sourceNode, String metric,
			ItemSerializable object) {
		int[][] bits = (int[][])amsImpl.tryAddObject(object);
		for (int i = 0; i < amsImpl.getS1(); i++) {
			for (int j = 0; j < amsImpl.getS2(); j++) {
				if (bits[i][j] > 0) {
					dhsImpl.insert(sourceNode, metric + amsMetrics[i][j][0],
							object);
				} else {
					dhsImpl.insert(sourceNode, metric + amsMetrics[i][j][1],
							object);
				}
			}
		}
	}
}
