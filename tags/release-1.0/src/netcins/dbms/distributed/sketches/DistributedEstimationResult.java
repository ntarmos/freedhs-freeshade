/**
 * $Id$
 */

package netcins.dbms.distributed.sketches;

import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DistributedEstimationResult {
	private String	metric					= null;
	private double	distributedEstimation	= 0;
	private int		hopCount				= 0;
	private int		numBitsProbed			= 0;

	public DistributedEstimationResult(String metric,
			double distributedEstimation, int hopCount, int numBitsProbed) {
		this.metric = metric;
		this.distributedEstimation = distributedEstimation;
		this.hopCount = hopCount;
		this.numBitsProbed = numBitsProbed;
	}

	public String toString() {
		return (metric + ": " + QuickMath.doubleToString(distributedEstimation)
				+ " (bits probed: " + numBitsProbed + " , hop count: "
				+ hopCount + " , avg hops per probe: " + (double)hopCount
				/ (double)numBitsProbed + ")");
	}

	public double getDistributedEstimation() {
		return distributedEstimation;
	}

	public void setDistributedEstimation(double distributedEstimation) {
		this.distributedEstimation = distributedEstimation;
	}

	public int getHopCount() {
		return hopCount;
	}

	public int getNumBitsProbed() {
		return numBitsProbed;
	}

	public String getMetric() {
		return metric;
	}
}
