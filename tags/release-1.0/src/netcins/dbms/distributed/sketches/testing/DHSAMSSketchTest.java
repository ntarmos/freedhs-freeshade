/**
 * $Id$
 */

package netcins.dbms.distributed.sketches.testing;

import java.security.NoSuchAlgorithmException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.AMSSketchImpl;
import netcins.dbms.distributed.sketches.DHSAMSSketchImpl;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithms;
import netcins.util.QuickMath;
import rice.environment.Environment;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.direct.BasicNetworkSimulator;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.standard.RandomNodeIdFactory;

public class DHSAMSSketchTest {
	private DHSImpl					dhsImpl					= null;
	private PastryNodeFactory		factory					= null;
	private Environment				env						= null;
	private Vector<DHSPastryAppl>	pastryAppls				= null;
	private DHSSketchAlgorithms		sketchAlgo				= DHSImpl.defaultSketchAlgo;
	private AMSSketchImpl			centralizedAMSSketch	= null;
	private DHSAMSSketchImpl		amsImpl					= null;
	private static final String		metric					= "Single Metric";

	private int						numItems				= -1;
	private int						qNode					= -1;

	private boolean					isInited				= false;
	private final Logger			logger					= Logger.getLogger("netcins.p2p.dhs.testing.DHSSingleMetricTest");
	private BasicNetworkSimulator	simulator				= null;

	public DHSAMSSketchTest(DHSSketchAlgorithms sketchAlgo, int numVecs, int L,
			int retries, int s1, int s2, Environment env) {
		init(sketchAlgo, numVecs, L, retries, s1, s2, env);
	}

	public DHSAMSSketchTest(int numVecs, int L, int retries, int s1, int s2,
			Environment env) {
		init(DHSImpl.defaultSketchAlgo, numVecs, L, retries, s1, s2, env);
	}

	public DHSAMSSketchTest() {
	}

	public void init(DHSSketchAlgorithms algorithm, int numVecs, int L,
			int retries, int s1, int s2, Environment env) {
		if (isInited)
			destroy();
		this.env = env;
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null) {
			destroy();
			return;
		}
		this.pastryAppls = new Vector<DHSPastryAppl>(10, 10);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(
				new RandomNodeIdFactory(env), simulator, env);
		try {
			this.centralizedAMSSketch = new AMSSketchImpl(s1, s2);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		initSketchImpls(algorithm, numVecs, L, retries);
		Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting").setLevel(
				Level.INFO);
		isInited = true;
	}

	public void initSketchImpls(DHSSketchAlgorithms algorithm, int numVecs,
			int L, int retries) {
		this.sketchAlgo = algorithm;
		try {
			this.dhsImpl = DHSImpl.newInstance(algorithm, numVecs, L, retries,
					env);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			this.amsImpl = new DHSAMSSketchImpl("D", dhsImpl,
					new AMSSketchImpl(centralizedAMSSketch.getS1(),
							centralizedAMSSketch.getS2()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		System.out.println("Initializing DHSSingleMetricTest with "
				+ this.dhsImpl);
		for (DHSPastryAppl app : pastryAppls)
			app.setDHSImpl(this.dhsImpl);
	}

	private void clearHits() {
		for (DHSPastryAppl app : pastryAppls)
			app.clearHits();
	}

	public boolean isInited() {
		return isInited;
	}

	public void destroy() {
		if (isInited == false)
			return;
		isInited = false;
		env.destroy();
		pastryAppls = null;
		env = null;
	}

	public void clearData() {
		if (isInited == false)
			return;
		for (DHSPastryAppl appl : pastryAppls) {
			appl.reinitData();
		}
		clearHits();
		numItems = 0;
	}

	public void addNodes(int numNodes) {

		System.runFinalization();
		System.gc();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		System.out.println("Populating the overlay with new nodes...");

		PastryNode bootstrapNode = null;
		for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0)
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle)null);
			else
				node = factory.newNode(bootstrapNode.getLocalHandle());

			logger.log(Level.FINER, "Finished creating new node " + node);

			DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);

			pastryAppls.add(dhsAppl);

			if ((curNodeIndex % 100) == 0)
				System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
			else if ((curNodeIndex % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + numNodes + "/" + numNodes + " done.");

		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
		for (int i = 0; i < pastryAppls.size(); i++) {
			pastryAppls.get(i).waitForNodeToBeReady();
			if ((i % 100) == 0)
				System.out.print(" " + i + "/" + numNodes + " ");
			else if ((i % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + pastryAppls.size() + "/" + numNodes + " done.");

		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl node generation took "
				+ (endTime - startTime) / 1000 + "\".");
		qNode = (int)QuickMath.floor(QuickMath.random() * pastryAppls.size());
	}

	private void waitForInsertionsToComplete() {
		for (DHSPastryAppl curAppl : pastryAppls) {
			curAppl.waitForInsertionsToComplete();
			curAppl.clearInsertionResponses();
		}
	}

	public void addData(int[] tuples) {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems = tuples.length;

		System.out.println("Injecting " + numItems + " new data tuples...");
		for (int data = 0; data < numItems; data++) {
			amsImpl.insert(pastryAppls.get(data % pastryAppls.size()),
					DHSAMSSketchTest.metric, new BasicItem(tuples[data]));
			centralizedAMSSketch.insert(new BasicItem(tuples[data]));
			if ((data % 5000) == 0) {
				waitForInsertionsToComplete();
				System.out.print(" " + data + "/" + numItems + " ");
			} else if ((data % 1000) == 0) {
				System.out.print(".");
			}
		}
		waitForInsertionsToComplete();

		System.out.println(" done. ");

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl data generation took "
				+ (endTime - startTime) / 1000 + "\".");
		System.runFinalization();
		System.gc();
		long numHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls)
			numHits += curAppl.getInsertionHits();
		System.out.println("Node insertion hits: " + numHits);
	}

	public void query() {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = System.currentTimeMillis(), endTime;
		System.out.println("Querying node: "
				+ pastryAppls.get(qNode).getNodeId());
		Vector<String> metrics = new Vector<String>();
		metrics.add(DHSAMSSketchTest.metric);
		System.out.println("AMS Tug-of-War" + " centralized estimation: "
				+ centralizedAMSSketch.estimate());
		try {
			System.out.println(sketchAlgo + " distributed estimation: "
					+ amsImpl.estimate(pastryAppls.get(qNode), metrics)[0]);
		} catch (NoSuchMetricException e) {
			e.printStackTrace();
		}
		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl query took " + (endTime - startTime) / 1000
				+ "\".");
		long numHits = 0;
		for (DHSPastryAppl curAppl : pastryAppls)
			numHits += curAppl.getQueryHits();
		System.out.println("Node query hits: " + numHits);
	}

	public static void main(String[] args) {
		int numNodes = 0, numTuples = 0, numVecs = 0, L = 0, retries = 0, s1 = 0, s2 = 0;

		try {
			numNodes = Integer.parseInt(args[0]);
			numTuples = Integer.parseInt(args[1]);
			numVecs = Integer.parseInt(args[2]);
			L = Integer.parseInt(args[3]);
			retries = Integer.parseInt(args[4]);
			s1 = Integer.parseInt(args[5]);
			s2 = Integer.parseInt(args[6]);
		} catch (NumberFormatException e) {
			System.out.println("Usage: java [-cp FreePastry-<version>.jar]");
			System.out.println("\tnetcins.p2p.dhs.testing.DHSAMSSketchTest <numNodes> <numItems> <numVecs> <L> <retries> <s1> <s2>");
			System.exit(0);
		}

		long startTime = System.currentTimeMillis(), endTime = 0;

		DHSAMSSketchTest test = new DHSAMSSketchTest();
		int seed = (int)QuickMath.floor(Math.random()
				* (double)Integer.MAX_VALUE);

		int[] data = new int[numTuples];
		for (int i = 0; i < numTuples; i++)
			data[i] = (int)QuickMath.floor(QuickMath.random()
					* (double)Integer.MAX_VALUE);

		System.out.println(" --- Begin Simulation --- ");
		System.out.println(" --- Seed: " + seed + " ---");
		try {
			test.init(null, numVecs, L, retries, s1, s2,
					Environment.directEnvironment(seed));
			test.addNodes(numNodes);
			System.out.println("\n" + " ---" + "\n");
			boolean firstRun = true;
			for (DHSSketchAlgorithms algo : DHSSketchAlgorithms.values()) {
				System.out.println("Testing " + algo + "...");
				test.initSketchImpls(algo, numVecs, L, retries);
				if (firstRun) {
					test.clearData();
					test.addData(data);
					firstRun = false;
				}
				test.query();
				System.out.println("\n" + " ---" + "\n");
			}
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: "
					+ (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}
}
