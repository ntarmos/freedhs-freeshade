/**
 * $Id$
 */

package netcins.dbms.distributed.sketches;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class NoSuchMetricException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 8945210624248186057L;

	/**
	 * 
	 */
	public NoSuchMetricException() {
		this("No such metric!");
	}

	/**
	 * @param message
	 */
	public NoSuchMetricException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NoSuchMetricException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NoSuchMetricException(String message, Throwable cause) {
		super(message, cause);
	}
}
