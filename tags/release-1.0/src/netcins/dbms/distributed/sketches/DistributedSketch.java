/**
 * $Id$
 */

package netcins.dbms.distributed.sketches;

import java.util.Vector;

import netcins.ItemSerializable;
import netcins.p2p.dhs.DHSPastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public interface DistributedSketch {
	public void insert(DHSPastryAppl sourceNode, String metric,
			ItemSerializable object);

	public void clear();

	public DistributedEstimationResult[] estimate(DHSPastryAppl sourceNode,
			Vector<String> metrics) throws NoSuchMetricException;
}
