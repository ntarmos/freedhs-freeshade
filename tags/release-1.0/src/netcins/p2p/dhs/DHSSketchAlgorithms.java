/**
 * $Id$
 */

package netcins.p2p.dhs;

import netcins.util.QuickMath;

/**
 * Enumeration capturing the set of implemented DHSImpl estimators.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 * @see netcins.p2p.dhs.DHSImpl
 * @see netcins.p2p.dhs.DHSDF03Impl
 * @see netcins.p2p.dhs.DHSFM85Impl
 */
public enum DHSSketchAlgorithms {
	/** Classic FM85 estimator */
	FM85Classic,
	/** Classic DF03 estimator */
	DF03Classic,
	/** FM85 estimator with binary-search bit probing */
	FM85BinarySearch,
	/** DF03 estimator with binary-search bit probing */
	DF03BinarySearch;

	/**
	 * Tests if the current algorithm is a variation of DF03.
	 * 
	 * @return true if the current algorithm is a variation of DF03, false
	 *         otherwise.
	 */
	public boolean isDF03() {
		return this.equals(DHSSketchAlgorithms.DF03Classic)
				|| this.equals(DHSSketchAlgorithms.DF03BinarySearch);
	}

	/**
	 * Tests if the current algorithm is a variation of FM85.
	 * 
	 * @return true if the current algorithm is a variation of FM85, false
	 *         otherwise.
	 */
	public boolean isFM85() {
		return this.equals(DHSSketchAlgorithms.FM85Classic)
				|| this.equals(DHSSketchAlgorithms.FM85BinarySearch);
	}

	/**
	 * Tests if the current algorithm uses a binary-search bit probing
	 * algorithm.
	 * 
	 * @return true if the current algorithm uses binary-search bit probing,
	 *         false otherwise.
	 */
	public boolean usesBinarySearch() {
		return this.equals(DHSSketchAlgorithms.FM85BinarySearch)
				|| this.equals(DHSSketchAlgorithms.DF03BinarySearch);

	}

	/**
	 * Converts the current DHSSketchAlgorithms object to String form.
	 * 
	 * @return A String representation of the current object.
	 */
	@Override
	public String toString() {
		String ret = null;
		switch (this) {
			case FM85Classic:
				ret = "FM85/Classic";
				break;
			case FM85BinarySearch:
				ret = "FM85/Binary Search";
				break;
			case DF03Classic:
				ret = "DF03/Classic";
				break;
			case DF03BinarySearch:
				ret = "DF03/Binary Search";
				break;
		}
		return ret;
	}

	/**
	 * Computes the next bit position to probe, based on the current bit
	 * position and the upper and lower bit thresholds (in the case of an
	 * estimator using binary-search bit probing)
	 * 
	 * @param curBit The current bit position.
	 * @param low The lower bit position threshold.
	 * @param high The upper bit position threshold.
	 * @return The next bit position to probe.
	 */
	public int nextBit(int curBit, int low, int high) {
		int ret = 0;
		switch (this) {
			case FM85Classic:
				ret = curBit + 1;
				break;
			case DF03Classic:
				ret = curBit - 1;
				break;
			case FM85BinarySearch:
			case DF03BinarySearch:
				ret = low + (int)QuickMath.floor((double)(high - low) / 2.0);
				break;
		}
		return ret;
	}
}
