/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.dbms.centralized.sketches.UnsupportedSketchAlgorithmException;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.util.QuickMath;
import rice.environment.Environment;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
final class DHSDF03Impl extends DHSImpl {

	int	vecsSetUpperLimit	= 0;

	protected DHSDF03Impl(DHSSketchAlgorithms sketchAlgo, int numVecs,
			String hashAlgo, int L, int retries, Vector<String> dhsMetrics,
			Environment env) throws NoSuchAlgorithmException {
		super(sketchAlgo, numVecs, hashAlgo, L, retries, dhsMetrics, env);
		assert (sketchAlgo.isDF03());
		this.vecsSetUpperLimit = (int)QuickMath.ceil((double)getNumBits()
				* HashSketchImpl.df03VecsSetULPercentage);
	}

	protected DHSDF03Impl(DHSSketchAlgorithms sketchAlgo, int numVecs,
			String hashAlgo, int L, int retries, Environment env)
			throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, null, env);
	}

	protected DHSDF03Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, int L,
			int retries, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries,
				null, env);
	}

	@Override
	protected final DistributedEstimationResult[] estimatePartial(
			DHSPastryAppl sourceNode, Vector<String> metrics, int numBits,
			Hashtable<String, BitSet[]> vecsValue, int hopCount,
			HashSet<Integer> bitsProbed) throws NoSuchMetricException {
		if (getSketchAlgo().usesBinarySearch() == false)
			return estimateClassicPartial(sourceNode, metrics, numBits, 0,
					vecsValue, hopCount, bitsProbed);
		return estimateBinarySearchPartial(sourceNode, metrics, numBits, 0,
				vecsValue, hopCount, bitsProbed);
	}

	private DistributedEstimationResult[] estimateClassicPartial(
			DHSPastryAppl sourceNode, Vector<String> metrics, int numBits,
			int endBit, Hashtable<String, BitSet[]> vecsValue, int hopCount,
			HashSet<Integer> bitsProbed) throws NoSuchMetricException {
		if (bitsProbed == null) {
			bitsProbed = new HashSet<Integer>();
		}

		int bit;
		for (bit = numBits - 1; bit >= endBit
				&& allMetricVecsSetToMoreThanForBit(vecsValue,
						vecsSetUpperLimit, bit, metrics) == false; bit--) {
			if (bitsProbed.contains(bit))
				continue;

			DHSBitProbeResult response = probe(sourceNode, metrics, bit);
			bitsProbed.add(bit);

			hopCount += response.hopCount;

			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item++) {
					DHSTuple dhsTuple = response.resultData[item];
					setBitForMetric(dhsTuple.getMetric(), dhsTuple.getVec(),
							dhsTuple.getBit());
					vecsValue.get(dhsTuple.getMetric())[dhsTuple.getBit()].set(dhsTuple.getVec());
				}
			}
			if (allMetricVecsSetToMoreThanForBit(vecsValue, vecsSetUpperLimit,
					bit, metrics) == true) {
				break;
			}
			// DEBUG
			// if (bit == (L - 1))
			// System.out.print(" " + L + ":" + vecsValue[bit].bitCount() + "
			// ");
			// else
			// System.out.print(" " + bit + ":" + vecsValue[bit].bitCount() + "
			// ");
		}
		// System.out.println(" " + (bit + 1) + ":" + vecsValue[bit +
		// 1].bitCount() + " done.");
		DistributedEstimationResult[] ret = new DistributedEstimationResult[metrics.size()];
		for (int i = 0; i < ret.length; i++) {
			try {
				ret[i] = new DistributedEstimationResult(metrics.get(i),
						estimate(metrics.get(i), SketchAlgorithms.DF03),
						hopCount, bitsProbed.size());
			} catch (UnsupportedSketchAlgorithmException e) {
				e.printStackTrace();
			} catch (NoSuchMetricException e) {
				e.printStackTrace();
			}
		}

		return ret;
	}

	private DistributedEstimationResult[] estimateBinarySearchPartial(
			DHSPastryAppl sourceNode, Vector<String> metrics, int numBits,
			int endBit, Hashtable<String, BitSet[]> vecsValue, int hopCount,
			HashSet<Integer> bitsProbed) throws NoSuchMetricException {
		int bitLow = endBit, bitHigh = numBits;
		for (int curBit = (bitLow + (int)QuickMath.floor(QuickMath.random()
				* (bitHigh - bitLow))); bitLow <= bitHigh; curBit = getSketchAlgo().nextBit(
				curBit, bitLow, bitHigh)) {

			DHSBitProbeResult response = probe(sourceNode, metrics, curBit);
			bitsProbed.add(curBit);
			hopCount += response.hopCount;

			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item++) {
					DHSTuple dhsTuple = response.resultData[item];
					setBitForMetric(dhsTuple.getMetric(), dhsTuple.getVec(),
							dhsTuple.getBit());
					vecsValue.get(dhsTuple.getMetric())[dhsTuple.getBit()].set(dhsTuple.getVec());
				}
				if (allMetricVecsSetToMoreThanForBit(vecsValue,
						vecsSetUpperLimit, curBit, metrics) == true)
					bitLow = curBit + 1;
				else
					bitHigh = curBit - 1;
			} else {
				bitHigh = curBit - 1;
			}
			// DEBUG
			// if (vecsValue[curBit].bitCount() >= vecsSetUpperLimit)
			// System.out.print("@");
			// if (curBit == (L - 1))
			// System.out.print(" " + L + ":" + vecsValue[curBit].bitCount() + "
			// ");
			// else
			// System.out.print(" " + curBit + ":" +
			// vecsValue[curBit].bitCount() + " ");
		}
		return estimateClassicPartial(sourceNode, metrics, numBits, bitLow,
				vecsValue, hopCount, bitsProbed);
	}

	@Override
	protected final void insertBinarySearch(DHSPastryAppl sourceNode,
			String metric, ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	@Override
	protected final void insertClassic(DHSPastryAppl sourceNode, String metric,
			ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}
}
