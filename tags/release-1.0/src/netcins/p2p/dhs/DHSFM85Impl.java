/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.dbms.centralized.sketches.UnsupportedSketchAlgorithmException;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import rice.environment.Environment;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
final class DHSFM85Impl extends DHSImpl {

	protected DHSFM85Impl(DHSSketchAlgorithms sketchAlgo, int numVecs,
			String hashAlgo, int L, int retries, Vector<String> metrics,
			Environment env) throws NoSuchAlgorithmException {
		super(sketchAlgo, numVecs, hashAlgo, L, retries, metrics, env);
		assert (sketchAlgo.isFM85());
	}

	protected DHSFM85Impl(DHSSketchAlgorithms sketchAlgo, int numVecs,
			String hashAlgo, int L, int retries, Environment env)
			throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, null, env);
	}

	protected DHSFM85Impl(DHSSketchAlgorithms sketchAlgo, int numVecs, int L,
			int retries, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries,
				env);
	}

	@Override
	protected final void insertBinarySearch(DHSPastryAppl sourceNode,
			String metric, ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	@Override
	protected final void insertClassic(DHSPastryAppl sourceNode, String metric,
			ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	@Override
	protected final DistributedEstimationResult[] estimatePartial(
			DHSPastryAppl sourceNode, Vector<String> metrics, int numBits,
			Hashtable<String, BitSet[]> vecsValue, int hopCount,
			HashSet<Integer> bitsProbed) throws NoSuchMetricException {
		if (bitsProbed == null) {
			bitsProbed = new HashSet<Integer>();
		}

		int bit;
		for (bit = 0; bit < numBits; bit++) {
			if (bitsProbed.contains(bit)) {
				if (noMetricVecsSetForBit(vecsValue, bit, metrics) == true) {
					// DEBUG
					// System.out.print(" ^" + bit + ":0 ");
					break;
				} else {
					// DEBUG
					// System.out.print(" ~" + bit + ":0:" +
					// vecsValue[bit].bitCount() + " ");
					continue;
				}
			}

			DHSBitProbeResult response = probe(sourceNode, metrics, bit);
			bitsProbed.add(bit);
			hopCount += response.hopCount;

			if (response.resultData != null && response.resultData.length > 0) {
				for (int item = 0; item < response.resultData.length; item++) {
					DHSTuple dhsTuple = response.resultData[item];
					setBitForMetric(dhsTuple.getMetric(), dhsTuple.getVec(),
							dhsTuple.getBit());
					vecsValue.get(dhsTuple.getMetric())[dhsTuple.getBit()].set(dhsTuple.getVec());
				}
				if (noMetricVecsSetForBit(vecsValue, bit, metrics) == true) {
					// DEBUG
					// System.out.print("*");
					break;
				}
			} else {
				// DEBUG
				// System.out.print("@");
				break;
			}

			// DEBUG
			// System.out.print(" " + bit + ":" + vecsValue[bit].bitCount() +
			// ":" + ((response.resultData != null) ? response.resultData.length
			// : -1) + " ");
		}
		// DEBUG
		// System.out.println(" " + bit + ":" + vecsValue[(bit == numBits ?
		// numBits - 1 : bit)].bitCount() + " done.");
		DistributedEstimationResult[] ret = new DistributedEstimationResult[metrics.size()];
		for (int i = 0; i < ret.length; i++) {
			try {
				ret[i] = new DistributedEstimationResult(metrics.get(i),
						estimate(metrics.get(i), SketchAlgorithms.FM85),
						hopCount, bitsProbed.size());
			} catch (UnsupportedSketchAlgorithmException e) {
				e.printStackTrace();
			} catch (NoSuchMetricException e) {
				e.printStackTrace();
			}
		}

		return ret;
	}
}
