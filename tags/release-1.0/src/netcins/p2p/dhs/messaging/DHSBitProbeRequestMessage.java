/**
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSBitProbeRequestMessage extends DHSRequestMessage {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3031373155152500132L;
	private int					retries				= 0;

	public DHSBitProbeRequestMessage(DHSTuple data, NodeHandle sourceNode,
			boolean storeHops) {
		super(data, sourceNode, storeHops);
	}

	public DHSBitProbeRequestMessage(DHSTuple[] data, NodeHandle sourceNode,
			boolean storeHops) {
		super(data, sourceNode, storeHops);
	}

	public int incRetries() {
		retries++;
		return retries;
	}

	public int getRetries() {
		return retries;
	}
}
