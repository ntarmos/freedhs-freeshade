/**
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public abstract class DHSRequestMessage extends DHSMessage {
	protected DHSRequestMessage(DHSTuple data, NodeHandle sourceNode,
			boolean storeHops) {
		super(data, sourceNode, storeHops);
	}

	protected DHSRequestMessage(DHSTuple[] data, NodeHandle sourceNode,
			boolean storeHops) {
		super(data, sourceNode, storeHops);
	}
}
