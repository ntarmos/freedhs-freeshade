/**
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSInsertionRequestMessage extends DHSRequestMessage {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3630456659859343099L;

	public DHSInsertionRequestMessage(DHSTuple data, NodeHandle sourceNode,
			boolean storeHops) {
		super(data, sourceNode, storeHops);
	}

	@Override
	public String toString() {
		return super.toString() + "[ vec: " + ((DHSTuple)data[0]).getBit()
				+ " , bit: " + ((DHSTuple)data[0]).getBit() + " ]";
	}
}
