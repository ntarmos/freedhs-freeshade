/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.p2p.dhs.messaging.DHSBitProbeRequestMessage;
import netcins.p2p.dhs.messaging.DHSBitProbeResponseMessage;
import netcins.p2p.dhs.messaging.DHSInsertionRequestMessage;
import netcins.p2p.dhs.messaging.DHSInsertionResponseMessage;
import netcins.p2p.dhs.messaging.DHSMessage;
import netcins.p2p.dhs.messaging.DHSRequestMessage;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.IdRange;
import rice.pastry.NodeHandle;
import rice.pastry.PastryNode;
import rice.pastry.client.CommonAPIAppl;
import rice.pastry.messaging.Message;
import rice.pastry.routing.RouteMessage;

/**
 * A PastryAppl implementing the functionality required by DHSImpl.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSPastryAppl extends CommonAPIAppl {
	public static final int									batchNumReplicas			= 5;
	private static final int								myAddress					= DHSMessage.getMyAddress();
	private static final int								batchInsertionStep			= 1000;

	private PastryNode										pastryNode					= null;
	private DHSImpl											dhsImpl						= null;
	private Vector<DHSTuple>								bitData						= null;
	private Hashtable<String, HashSketchImpl>				localHS						= null;
	private Boolean											hasPendingBatchInsertions	= false;
	private Integer											insertionHits				= 0,
			queryHits = 0, insertionsHopCount = 0;
	private Hashtable<Long, DHSInsertionRequestMessage>		pendingInsertions			= null;
	private Hashtable<Long, DHSInsertionResponseMessage>	insertionResponses			= null;
	private Hashtable<Long, DHSBitProbeRequestMessage>		pendingQueries				= null;
	private Hashtable<Long, DHSBitProbeResponseMessage>		queryResponses				= null;

	/**
	 * Constructs a new DHSPastryAppl instance.
	 * 
	 * @param pn The PastryNode on which the application is to be executed.
	 * @param dhsImpl An instance of DHSImpl, to drive DHSImpl data insertion
	 *            and retrieval.
	 */
	public DHSPastryAppl(PastryNode pn, DHSImpl dhsImpl) {
		super(pn);
		this.pastryNode = pn;
		this.dhsImpl = dhsImpl;
		this.address = myAddress;
		reinitData();
	}

	/**
	 * Returns the RMI address of this application.
	 * 
	 * @return The RMI address of the DHSPastryAppl at the local node.
	 * @see rice.pastry.client.PastryAppl#getAddress()
	 */
	@Override
	public int getAddress() {
		return myAddress;
	}

	/**
	 * Called by pastry when a message is enroute and is passing through this
	 * node.
	 * 
	 * @param rm The message that is passing through.
	 * @see rice.pastry.client.CommonAPIAppl#forward(RouteMessage)
	 */
	@Override
	@SuppressWarnings("deprecation")
	public void forward(RouteMessage rm) {
		Object o = rm.unwrap();
		if (o instanceof DHSRequestMessage
				&& rm.nextHop != null
				&& pastryNode.getNodeId().equals(rm.nextHop.getNodeId()) == false) {
			// DHSResponseMessage's are single-hop and thus ignored.
			NodeHandle sender = rm.getSender();

			((DHSMessage)o).addHop(rm.nextHop.getNodeId());

			// DEBUG: Print extra information...
			if (logger.level <= Logger.FINER)
				logger.log("AP::  " + o + " => " + rm.getNextHop().getNodeId()
						+ ".");

			rm = new RouteMessage(rm.getTarget(), (DHSMessage)o, rm.nextHop,
					rm.getOptions());
			rm.setSender(sender);
		}
	}

	/**
	 * Called by pastry when the neighbor set changes.
	 * 
	 * @param nh The handle of the node that was added or removed.
	 * @param joined true if a node was added, false otherwise.
	 */
	@Override
	public void update(NodeHandle nh, boolean joined) {
	}

	/**
	 * Executed when a message is destined to the current node.
	 * 
	 * @param key The Id of the message's destination.
	 * @param msg The (unwrapped) message.
	 */
	@Override
	public void deliver(rice.pastry.Id key, Message msg) {
		if (msg == null) {
			System.err.println("Null message ?!");
			return;
		}

		// DEBUG: Print extra information...
		if (logger.level <= Logger.FINE) {
			if (msg instanceof DHSMessage)
				logger.log("AP::  " + (DHSMessage)msg + " => " + getNodeId()
						+ ".");
		}

		if (msg instanceof DHSBitProbeRequestMessage) {
			processDHSBitProbeRequestMessage((DHSBitProbeRequestMessage)msg);
		} else if (msg instanceof DHSBitProbeResponseMessage) {
			processDHSBitProbeResponseMessage((DHSBitProbeResponseMessage)msg);
		} else if (msg instanceof DHSInsertionRequestMessage) {
			processDHSInsertionRequestMessage((DHSInsertionRequestMessage)msg);
		} else if (msg instanceof DHSInsertionResponseMessage) {
			processDHSInsertionResponseMessage((DHSInsertionResponseMessage)msg);
		} else {
			System.err.println("The thing that shouldn't be!");
		}
	}

	/**
	 * Returns a String representation of the current DHSPastryAppl object.
	 * 
	 * @return A String representation of the current DHSPastryAppl object.
	 */
	@Override
	public String toString() {
		return "[ DHSPastryAppl " + pastryNode + " :: " + dhsImpl + " ]";
	}

	/**
	 * Clears all hit counters for the local node.
	 */
	public void clearHits() {
		synchronized (this) {
			this.insertionHits = this.queryHits = this.insertionsHopCount = 0;
		}
	}

	/**
	 * Returns the logger instance for the local node.
	 * 
	 * @return The logger instance for the local node.
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Initializes the local DHSImpl instance.
	 * 
	 * @param dhsImpl The DHSImpl instance.
	 */
	public void setDHSImpl(DHSImpl dhsImpl) {
		synchronized (dhsImpl) {
			this.dhsImpl = dhsImpl;
		}
	}

	/**
	 * Reinitializes the local data store and all response/query queues.
	 */
	public void reinitData() {
		synchronized (this) {
			this.bitData = new Vector<DHSTuple>(100, 2);
			this.pendingInsertions = new Hashtable<Long, DHSInsertionRequestMessage>();
			this.insertionResponses = new Hashtable<Long, DHSInsertionResponseMessage>();
			this.pendingQueries = new Hashtable<Long, DHSBitProbeRequestMessage>();
			this.queryResponses = new Hashtable<Long, DHSBitProbeResponseMessage>();
			if (this.dhsImpl != null) {
				this.dhsImpl.clearHS();
				this.localHS = new Hashtable<String, HashSketchImpl>();
			}
			this.insertionHits = this.queryHits = this.insertionsHopCount = 0;
			this.hasPendingBatchInsertions = false;
		}
	}

	/**
	 * Waits until the local PastryNode is in a ready state.
	 * 
	 * @see rice.pastry.PastryNode#isReady()
	 */
	public void waitForNodeToBeReady() {
		synchronized (this.pastryNode) {
			while (pastryNode.isReady() == false) {
				try {
					pastryNode.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/* *************************** */
	/* Insertion-related methods */
	/* *************************** */

	/**
	 * Inserts a new DHSTuple object in the overlay using the designated ID.
	 * 
	 * @param targetId The ID of the target node.
	 * @param data The DHSTuple to insert in the overlay.
	 * @return The identifier of the insertion request. This can be used later
	 *         on to query for the status of the insertion.
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToInsertion(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#getResponseToInsertion(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#getInsertionHopCount(long)
	 */
	public long insert(Id targetId, DHSTuple data) {
		DHSInsertionRequestMessage omsg = null;
		if (logger.level <= Logger.FINE)
			omsg = new DHSInsertionRequestMessage(data,
					pastryNode.getLocalHandle(), true);
		else
			omsg = new DHSInsertionRequestMessage(data,
					pastryNode.getLocalHandle(), false);
		omsg.setTargetId(targetId);

		synchronized (pendingInsertions) {
			assert (pendingInsertions.containsKey(omsg.getMsgId()) == false);
			pendingInsertions.put(omsg.getMsgId(), omsg);
		}

		route(targetId, omsg, null);
		return omsg.getMsgId();
	}

	/**
	 * Adds an item to the local batch of insertions. This method is a local
	 * operation on every node, populating a local hash sketch for the given
	 * metric. In order to actually update the data stored in the overlay one
	 * has to call {@link #doBatch()} to execute all pending batch inserts.
	 * 
	 * @param metric The metric under which to add the given item.
	 * @param data The data item to insert.
	 */
	public void batchInsert(String metric, ItemSerializable data) {
		synchronized (localHS) {
			HashSketchImpl hs = localHS.get(metric);
			if (hs == null) {
				try {
					hs = new HashSketchImpl(dhsImpl.getNumVecs(),
							dhsImpl.getNumBits(), dhsImpl.getHashAlgo());
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
					return;
				}
				localHS.put(metric, hs);
			}
			hs.insert(data);
			hasPendingBatchInsertions = true;
		}
	}

	/**
	 * Executes all pending batch inserts. New items are added to the local
	 * synopsis by calling {@link #batchInsert(String, ItemSerializable)}.
	 * 
	 * @return The number of messages sent during this batch run.
	 */
	public int doBatch() {
		int ret = 0;
		synchronized (localHS) {
			if (hasPendingBatchInsertions) {
				for (Enumeration<String> metrics = localHS.keys(); metrics.hasMoreElements();) {
					String curMetric = metrics.nextElement();
					HashSketchImpl hs = localHS.get(curMetric);
					if (hs.isEmpty())
						continue;
					for (int vec = 0; vec < hs.getNumVecs(); vec++) {
						BitSet vecBits = hs.getVec(vec);
						for (int bit = 0, i = 0; i < vecBits.length()
								&& bit <= hs.getNumBits(); bit++) {
							if (vecBits.get(bit)) {
								i++;
								for (int rep = 0; rep < batchNumReplicas; rep++) {
									ret++;
									Id targetId = dhsImpl.mapToNode(bit);
									insert(targetId, new DHSTuple(curMetric,
											vec, bit));
								}
							}
						}
					}
					hs.clear();
				}
				hasPendingBatchInsertions = false;
			}
		}
		return ret;
	}

	/**
	 * Removes all insertion responses from the response list.
	 * 
	 * @return The number of responses removed from the response list.
	 */
	public int clearInsertionResponses() {
		int ret;
		synchronized (insertionResponses) {
			ret = insertionResponses.size();
			insertionResponses.clear();
		}
		return ret;
	}

	/**
	 * Returns the number of times the current node was the target of an
	 * insertion request.
	 * 
	 * @return The number of times the current node was the target of an
	 *         insertion request.
	 */
	public int getInsertionHits() {
		synchronized (insertionHits) {
			return insertionHits;
		}
	}

	/**
	 * Gets the number of hops it took the insertion message to reach its
	 * destination. The caller must first have checked for the status of the
	 * request, using hasResponseToInsertion(msgId).
	 * 
	 * @param msgId The ID of the insertion request whose hop count is
	 *            requested.
	 * @return The number of hops it took the insertion message to reach its
	 *         destination. -1 if there is no response for the designated
	 *         insertion request ID.
	 * @see netcins.p2p.dhs.DHSPastryAppl#insert(Id, DHSTuple)
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToInsertion(long)
	 */
	public int getInsertionHopCount(long msgId) {
		synchronized (insertionResponses) {
			DHSInsertionResponseMessage rmsg = insertionResponses.get(msgId);
			if (rmsg == null)
				return -1;
			return rmsg.getHopCount();
		}
	}

	public int getInsertionsHopCount() {
		synchronized (insertionsHopCount) {
			return insertionsHopCount;
		}
	}

	/**
	 * Retruns the number of pending insertions.
	 * 
	 * @return The number of pending insertions.
	 */
	public int getNumPendingInsertions() {
		synchronized (pendingInsertions) {
			return pendingInsertions.size();
		}
	}

	/**
	 * Gets the response to the designated insertion request. The caller must
	 * first have checked for the status of the request, using
	 * hasResponseToInsertion(msgId). This method does not remove the response
	 * from the list of responses; the caller must use
	 * removeResponseToInsertion(msgId) to accomplish this.
	 * 
	 * @param msgId The ID of the insertion request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated insertion request. NULL if there is no response for
	 *         the designated request ID.
	 * @see netcins.p2p.dhs.DHSPastryAppl#insert(Id, DHSTuple)
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToInsertion(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#removeResponseToInsertion(long)
	 */
	public DHSTuple[] getResponseToInsertion(long msgId) {
		DHSInsertionResponseMessage rmsg = null;
		synchronized (insertionResponses) {
			rmsg = insertionResponses.get(msgId);
		}
		return (rmsg != null ? rmsg.getData() : new DHSTuple[0]);
	}

	/**
	 * Checks if there are any pending insertions.
	 * 
	 * @return true if there are pending insertions, false otherwise.
	 */
	public boolean hasPendingInsertions() {
		synchronized (pendingInsertions) {
			return (pendingInsertions.isEmpty() == false);
		}
	}

	/**
	 * Checks if there is a response for the designated insertion request.
	 * 
	 * @param msgId The ID of the insertion request to check
	 * @return true if there is a response for the given insertion request,
	 *         false otherwise.
	 */
	public boolean hasResponseToInsertion(long msgId) {
		synchronized (insertionResponses) {
			return (insertionResponses != null
					? insertionResponses.containsKey(msgId) : false);
		}
	}

	/**
	 * Removes and returns the response to the designated insertion request. The
	 * caller must first have checked for the status of the request, using
	 * hasResponseToInsertion(msgId). This method removes the response from the
	 * list of responses; the caller must use getResponseToInsertion(msgId) if
	 * she just wants to check the response.
	 * 
	 * @param msgId The ID of the insertion request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated insertion request. NULL if there is no response for
	 *         the designated request ID.
	 * @see netcins.p2p.dhs.DHSPastryAppl#insert(Id, DHSTuple)
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToInsertion(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#getResponseToInsertion(long)
	 */
	public DHSTuple[] removeResponseToInsertion(long msgId) {
		DHSInsertionResponseMessage rmsg = null;
		synchronized (insertionResponses) {
			rmsg = insertionResponses.remove(msgId);
		}
		return (rmsg != null ? rmsg.getData() : new DHSTuple[0]);
	}

	/**
	 * Waits until the designated insertion request has finished.
	 * 
	 * @param msgId The ID of the insertion request to wait for.
	 */
	/*
	 * public void waitForInsertionToComplete(long msgId) { synchronized
	 * (pendingInsertions) { while (pendingInsertions.containsKey(msgId) ==
	 * true) { try { pendingInsertions.wait(100)(); } catch
	 * (InterruptedException e) { e.printStackTrace(); } } } }
	 */

	/**
	 * Waits until all insertion requests have finished.
	 */
	public void waitForInsertionsToComplete() {
		synchronized (pendingInsertions) {
			while (pendingInsertions.isEmpty() == false) {
				try {
					pendingInsertions.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/* *********************** */
	/* Query-related methods */
	/* *********************** */

	/**
	 * Sends a probe to the node responsible for the designated ID, for the
	 * metrics and bits designated by the data in the second argument. Query
	 * processing will proceed from there, so for better performance tuples in
	 * data should all refer to the same bit position.
	 * 
	 * @param targetId The target ID.
	 * @param data An array of DHSTuple's denoting the metrics and bits to
	 *            probe.
	 * @return The identifier of the probe request. This can be used later on to
	 *         query for the status of the query.
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToQuery(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#getResponseToQuery(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#getQueryHops(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#getQueryHopCount(long)
	 */
	public long query(Id targetId, DHSTuple[] data) {
		DHSBitProbeRequestMessage omsg = null;

		if (logger.level <= Logger.FINE)
			omsg = new DHSBitProbeRequestMessage(data,
					pastryNode.getLocalHandle(), true);
		else
			omsg = new DHSBitProbeRequestMessage(data,
					pastryNode.getLocalHandle(), false);
		omsg.setTargetId(targetId);

		synchronized (pendingQueries) {
			assert (pendingQueries.containsKey(omsg.getMsgId()) == false);
			pendingQueries.put(omsg.getMsgId(), omsg);
		}

		boolean overlap = false;
		for (int i = 0; overlap == false && i < data.length; i++) {
			if (dhsImpl.idRangeOverlapsWithArc(new IdRange(
					pastryNode.getLeafSet().get(-1).getNodeId(),
					pastryNode.getLeafSet().get(1).getNodeId()),
					data[i].getBit()) == true) {
				overlap = true;
				if (logger.level <= Logger.FINER)
					logger.log("Client request remote query to id "
							+ targetId
							+ " for bit #"
							+ data[i].getBit()
							+ " but I ("
							+ pastryNode.getNodeId()
							+ ") am in that arc too. Silently forcing target to local node...");
			}
		}
		if (overlap == true) {
			deliver(targetId, omsg);
		} else {
			route(targetId, omsg, null);
		}
		return omsg.getMsgId();
	}

	/**
	 * Retruns the number of pending queries.
	 * 
	 * @return The number of pending queries.
	 */
	public int getNumPendingQueries() {
		synchronized (pendingQueries) {
			return pendingQueries.size();
		}
	}

	/**
	 * Gets the number of hops it took the query message to reach its
	 * destination. The caller must first have checked for the status of the
	 * request, using hasResponseToQuery(msgId).
	 * 
	 * @param msgId The ID of the query request whose hop count is requested.
	 * @return The number of hops it took the query message to reach its
	 *         destination. -1 if there is no response for the designated query
	 *         request ID.
	 * @see netcins.p2p.dhs.DHSPastryAppl#query(Id, DHSTuple[])
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToQuery(long)
	 */
	public int getQueryHopCount(long msgId) {
		DHSBitProbeResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.get(msgId);
		}
		if (rmsg == null)
			return -1;
		return rmsg.getHopCount();
	}

	/**
	 * Gets the IDs of all nodes the designated query request went through en
	 * route to its destination.
	 * 
	 * @param msgId The ID of the query request whose hops are requested.
	 * @return A Vector containing the IDs of all nodes the query message went
	 *         through, or null if there is no information for the requested
	 *         query ID.
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToQuery(long)
	 */
	public Vector<Id> getQueryHops(long msgId) {
		DHSBitProbeResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.get(msgId);
		}
		if (rmsg == null)
			return null;
		return rmsg.getHops();
	}

	/**
	 * Gets the response to the designated query request. The caller must first
	 * have checked for the status of the request, using
	 * hasResponseToQuery(msgId). This method does not remove the response from
	 * the list of responses; the caller must use removeResponseToQuery(msgId)
	 * to accomplish this.
	 * 
	 * @param msgId The ID of the query request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated query request. NULL if there is no response for the
	 *         designated request ID.
	 * @see netcins.p2p.dhs.DHSPastryAppl#query(Id, DHSTuple[])
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToQuery(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#removeResponseToQuery(long)
	 */
	public DHSTuple[] getResponseToQuery(long msgId) {
		DHSBitProbeResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.get(msgId);
		}
		return (rmsg != null ? rmsg.getData() : new DHSTuple[0]);
	}

	/**
	 * Returns the number of times the current node was the target of a query
	 * request.
	 * 
	 * @return The number of times the current node was the target of a query
	 *         request.
	 */
	public int getQueryHits() {
		synchronized (queryHits) {
			return queryHits;
		}
	}

	/**
	 * Checks if there is a response for the designated query request.
	 * 
	 * @param msgId The ID of the query request to check
	 * @return true if there is a response for the given query request, false
	 *         otherwise.
	 */
	public boolean hasResponseToQuery(long msgId) {
		synchronized (queryResponses) {
			return (queryResponses.containsKey(msgId) && !queryResponses.get(
					msgId).mustRetry());
		}
	}

	/**
	 * Checks if there are any pending queries.
	 * 
	 * @return true if there are pending queries, false otherwise.
	 */
	public boolean hasPendingQueries() {
		synchronized (pendingQueries) {
			return (pendingQueries.isEmpty() == false);
		}
	}

	/**
	 * Waits until the designated query request has finished.
	 * 
	 * @param msgId The ID of the query request to wait for.
	 */
	public void waitForQueryToComplete(long msgId) {
		synchronized (queryResponses) {
			while (queryResponses.containsKey(msgId) == false
					|| queryResponses.get(msgId).mustRetry() == true) {
				try {
					queryResponses.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Removes and returns the response to the designated query request. The
	 * caller must first have checked for the status of the request, using
	 * hasResponseToQuery(msgId). This method removes the response from the list
	 * of responses; the caller must use getResponseToQuery(msgId) if she just
	 * wants to check the response.
	 * 
	 * @param msgId The ID of the query request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated query request. NULL if there is no response for the
	 *         designated request ID.
	 * @see netcins.p2p.dhs.DHSPastryAppl#query(Id, DHSTuple[])
	 * @see netcins.p2p.dhs.DHSPastryAppl#hasResponseToQuery(long)
	 * @see netcins.p2p.dhs.DHSPastryAppl#getResponseToQuery(long)
	 */
	public DHSTuple[] removeResponseToQuery(long msgId) {
		DHSBitProbeResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.remove(msgId);
		}
		return (rmsg != null ? rmsg.getData() : new DHSTuple[0]);
	}

	// ///////////////////
	// Private methods //
	// ///////////////////

	/**
	 * Called by deliver(...) to process a DHSBitProbeRequestMessage destined to
	 * the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSBitProbeRequestMessage(DHSBitProbeRequestMessage imsg) {
		DHSTuple[] dhsTuples = imsg.getData();
		int queryBit = dhsTuples[0].getBit();
		Hashtable<String, Integer> metrics = new Hashtable<String, Integer>();
		int numMetrics = 0;

		synchronized (queryHits) {
			queryHits++;
		}

		for (int i = 0; i < dhsTuples.length; i++) {
			metrics.put(dhsTuples[i].getMetric(), i);
		}
		numMetrics = metrics.size();
		assert (numMetrics == dhsTuples.length);

		Vector<DHSTuple> response = new Vector<DHSTuple>(10, 100);
		int relevantResults[] = new int[metrics.size()];
		boolean resultVecs[][] = new boolean[metrics.size()][dhsImpl.getNumVecs()];

		synchronized (bitData) {
			for (DHSTuple tuple : bitData) {
				if (metrics.containsKey(tuple.getMetric())) {
					response.add(tuple);
					if (tuple.getBit() == queryBit) {
						int metricIndex = metrics.get(tuple.getMetric());
						if (resultVecs[metricIndex][tuple.getVec()] == false) {
							resultVecs[metricIndex][tuple.getVec()] = true;
							relevantResults[metricIndex]++;
						}
					}
				}
			}
		}

		/*
		 * for (Enumeration<String> metricEnum = metrics.keys();
		 * metricEnum.hasMoreElements(); ) { String curMetric =
		 * metricEnum.nextElement(); Vector<MinimalDHSTuple> curBSet =
		 * bitData.get(curMetric); if (curBSet == null) continue; for
		 * (MinimalDHSTuple minTuple : curBSet) { response.add(new
		 * DHSTuple(curMetric, minTuple.vec, minTuple.bit)); if (minTuple.bit ==
		 * queryBit) { int metricIndex = metrics.get(curMetric); if
		 * (resultVecs[metricIndex][minTuple.vec] == false) {
		 * resultVecs[metricIndex][minTuple.vec] = true;
		 * relevantResults[metricIndex] ++; } } } }
		 */
		metrics = null;

		/*
		 * synchronized (bitData) { for (DHSTuple tuple : bitData) { Integer
		 * metricIndex = metrics.get(tuple.getMetric()); if (metricIndex !=
		 * null) { response.add(tuple); if (tuple.getBit() ==
		 * dhsTuples[metricIndex].getBit()) { if
		 * (resultVecs[metricIndex][tuple.getVec()] == false) {
		 * resultVecs[metricIndex][tuple.getVec()] = true;
		 * relevantResults[metricIndex]++; } } } } }
		 */

		BitSet needMore = new BitSet();
		for (int i = 0; i < relevantResults.length; i++) {
			if (relevantResults[i] < dhsImpl.getNumVecs()) {
				needMore.set(i);
			}
		}

		DHSTuple[] ret = null;
		DHSBitProbeResponseMessage omsg = null;

		if (response.size() > 0) {
			ret = new DHSTuple[response.size()];
			response.copyInto(ret);
		}

		if (ret != null && needMore.isEmpty()) {
			omsg = new DHSBitProbeResponseMessage(ret,
					pastryNode.getLocalHandle(), imsg);
		} else {
			NodeHandle neighborCW = pastryNode.getLeafSet().get(1);
			NodeHandle neighborCWNext = pastryNode.getLeafSet().get(2);
			NodeHandle neighborCCW = pastryNode.getLeafSet().get(-1);
			NodeHandle neighborCCWPrev = pastryNode.getLeafSet().get(-2);
			assert (neighborCW != null && neighborCWNext != null
					&& neighborCCW != null && neighborCCWPrev != null);

			IdRange rangeCCW = new IdRange(neighborCCWPrev.getNodeId(),
					pastryNode.getNodeId());
			IdRange rangeCW = new IdRange(pastryNode.getNodeId(),
					neighborCWNext.getNodeId());

			int numFalse = 0;
			for (int i = 0; i < numMetrics; i++)
				if (needMore.get(i) == true
						&& dhsImpl.idRangeOverlapsWithArc(rangeCCW,
								dhsTuples[i].getBit()) == false)
					numFalse++;
			if (numFalse == needMore.cardinality())
				neighborCCW = null;

			numFalse = 0;
			for (int i = 0; i < numMetrics; i++)
				if (needMore.get(i) == true
						&& dhsImpl.idRangeOverlapsWithArc(rangeCW,
								dhsTuples[i].getBit()) == false)
					numFalse++;
			if (numFalse == needMore.cardinality())
				neighborCW = null;

			omsg = new DHSBitProbeResponseMessage(ret,
					pastryNode.getLocalHandle(), imsg, neighborCW, neighborCCW);
		}
		omsg.setTargetId(imsg.getSenderId());

		route(imsg.getSenderId(), omsg, imsg.getSender());
	}

	/**
	 * Called by deliver(...) to process a DHSBitProbeResponseMessage destined
	 * to the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSBitProbeResponseMessage(
			DHSBitProbeResponseMessage imsg) {
		synchronized (pendingQueries) {
			assert (pendingQueries.containsKey(imsg.getReqMsgId()) == true);
			DHSBitProbeRequestMessage reqMsg = pendingQueries.remove(imsg.getReqMsgId());
			DHSBitProbeResponseMessage resp = null;

			synchronized (queryResponses) {
				if (queryResponses.containsKey(imsg.getReqMsgId())) {
					resp = queryResponses.remove(imsg.getReqMsgId());
					resp.mergeRetryNodes(imsg);
					resp.mergeData(imsg);
					resp.addHops(imsg.getHops());
					DHSTuple[] respData = resp.getData();
					if (respData != null
							&& respData.length == dhsImpl.getNumVecs()) {
						resp.setMustRetry(false);
						// DEBUG
						// System.out.print("*");
					}
				} else {
					resp = imsg;
				}
			}

			if (resp.mustRetry() == true
					&& reqMsg.incRetries() == dhsImpl.getRetries()) { // Bailing
				// out...
				// DEBUG
				// System.out.print("#");
				resp.setMustRetry(false);
			}

			if (resp.mustRetry() == true && resp.hasRetryNode() == false) { // Also
				// bailing
				// out...
				// DEBUG
				// System.out.print("!");
				resp.setMustRetry(false);
			}

			synchronized (queryResponses) {
				assert (queryResponses.containsKey(resp.getReqMsgId()) == false);
				queryResponses.put(resp.getReqMsgId(), resp);
			}

			if (resp.mustRetry() == true) { // Retrying through successor
				// probing...
				// DEBUG
				// System.out.print("_");
				NodeHandle targetHandle = resp.getRetryNode();
				reqMsg.setTargetId(targetHandle.getNodeId());
				reqMsg.clearHops();
				assert (pendingQueries.containsKey(reqMsg.getMsgId()) == false);
				pendingQueries.put(reqMsg.getMsgId(), reqMsg);
				route((rice.pastry.Id)targetHandle.getNodeId(), reqMsg,
						targetHandle);
			} else if (pendingQueries.isEmpty() == true) {
				synchronized (queryResponses) {
					queryResponses.notifyAll();
				}
			}
		}
	}

	/**
	 * Called by deliver(...) to process a DHSInsertionRequestMessage destined
	 * to the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSInsertionRequestMessage(
			DHSInsertionRequestMessage imsg) {
		DHSTuple[] data = imsg.getData();
		assert (data.length == 1);
		synchronized (bitData) {
			int index = Collections.binarySearch(bitData, data[0]);
			if (index < 0) {
				bitData.add(-index - 1, data[0]);
			} else if (bitData.get(index).getTimestamp() < data[0].getTimestamp()) {
				bitData.set(index, data[0]);
			}
		}
		synchronized (insertionHits) {
			insertionHits++;
		}
		DHSInsertionResponseMessage omsg = new DHSInsertionResponseMessage(
				data, pastryNode.getLocalHandle(), imsg);
		omsg.setTargetId(imsg.getSenderId());
		route(imsg.getSenderId(), omsg, imsg.getSender());
	}

	/**
	 * Called by deliver(...) to process a DHSInsertionResponseMessage destined
	 * to the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSInsertionResponseMessage(
			DHSInsertionResponseMessage imsg) {
		synchronized (pendingInsertions) {
			assert (pendingInsertions.containsKey(imsg.getReqMsgId()));
			DHSInsertionRequestMessage rmsg = pendingInsertions.remove(imsg.getReqMsgId());
			if (pendingInsertions.isEmpty())
				pendingInsertions.notifyAll();
		}
		synchronized (insertionsHopCount) {
			insertionsHopCount += imsg.getHopCount();
		}
		// XXX: Commented out for memory space conservation reasons. However, a
		// full-fledged implementation might need this.
		// insertionResponses.put(rmsg.getMsgId(), imsg);
	}

}
