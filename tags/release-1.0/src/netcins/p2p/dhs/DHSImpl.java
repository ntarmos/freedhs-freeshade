/**
 * $Id$
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.dbms.centralized.sketches.UnsupportedSketchAlgorithmException;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.DistributedSketch;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.util.QuickMath;
import rice.environment.Environment;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.IdRange;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public abstract class DHSImpl implements DistributedSketch {

	/** Default number of bitmap vectors in the DHSImpl implementation */
	public static final int					defaultNumVecs		= 256;
	/** Default number of retries in the DHSImpl implementation */
	public static final int					defaultRetries		= 5;
	/** Default Hash Sketch algorithm used by the DHSImpl instance */
	public static final DHSSketchAlgorithms	defaultSketchAlgo	= DHSSketchAlgorithms.DF03BinarySearch;

	private static final int				intsPerId			= Id.IdBitLength / 32;
	private int								L					= -1;
	private int								numVecs				= -1;
	private int								retries				= 0;
	private String							hashAlgo			= null;
	private DHSSketchAlgorithms				sketchAlgo;
	private HashSketchImpl					baseHashSketchImpl	= null;
	private Vector<String>					dhsMetrics			= null;
	private Vector<HashSketchImpl>			distributedHS		= null;
	private Environment						env					= null;

	// /////////////////////
	// Protected methods //
	// /////////////////////

	protected final int[] getBitsForObject(ItemSerializable object) {
		return (int[])baseHashSketchImpl.tryAddObject(object);
	}

	protected final boolean setBitForMetric(String metric, int vec, int bit)
			throws NoSuchMetricException {
		int index = -1;
		if (distributedHS == null
				|| (index = Collections.binarySearch(dhsMetrics, metric)) < 0)
			throw new NoSuchMetricException();
		distributedHS.get(index).setBit(vec, bit);
		return true;
	}

	protected final boolean getBitForMetric(String metric, int vec, int bit)
			throws NoSuchMetricException {
		int index = -1;
		if (distributedHS == null
				|| (index = Collections.binarySearch(dhsMetrics, metric)) < 0)
			throw new NoSuchMetricException();
		return distributedHS.get(index).getBit(vec, bit);
	}

	protected final boolean allMetricVecsSetForBit(
			Hashtable<String, BitSet[]> vecsValue, int bit,
			Vector<String> queryMetrics) {
		return allMetricVecsSetToForBit(vecsValue, numVecs, bit, queryMetrics);
	}

	protected final boolean noMetricVecsSetForBit(
			Hashtable<String, BitSet[]> vecsValue, int bit,
			Vector<String> queryMetrics) {
		return allMetricVecsSetToForBit(vecsValue, 0, bit, queryMetrics);
	}

	protected final boolean allMetricVecsSetToMoreThanForBit(
			Hashtable<String, BitSet[]> vecsValue, int threshold, int bit,
			Vector<String> queryMetrics) {
		BitSet tmpVecsSet = new BitSet();

		for (int metric = 0; metric < queryMetrics.size(); metric++) {
			if (vecsValue.containsKey(queryMetrics.get(metric)) == false)
				continue;
			if (vecsValue.get(queryMetrics.get(metric))[bit].cardinality() >= threshold)
				tmpVecsSet.set(metric);
		}
		return (tmpVecsSet.cardinality() == queryMetrics.size());
	}

	protected final boolean allMetricVecsSetToForBit(
			Hashtable<String, BitSet[]> vecsValue, int value, int bit,
			Vector<String> queryMetrics) {
		BitSet tmpVecsSet = new BitSet();

		for (int metric = 0; metric < queryMetrics.size(); metric++) {
			if (vecsValue.containsKey(queryMetrics.get(metric)) == false)
				continue;
			if (vecsValue.get(queryMetrics.get(metric))[bit].cardinality() == value)
				tmpVecsSet.set(metric);
		}
		return (tmpVecsSet.cardinality() == queryMetrics.size());
	}

	protected final DHSBitProbeResult probe(DHSPastryAppl sourceNode,
			Vector<String> metrics, int bit) {
		DHSTuple[] queryTuples = new DHSTuple[metrics.size()];
		for (int i = 0; i < queryTuples.length; i++)
			queryTuples[i] = new DHSTuple(metrics.get(i), -1, bit);

		long msgId = sourceNode.query(mapToNode(bit), queryTuples);
		sourceNode.waitForQueryToComplete(msgId);

		int numHops = sourceNode.getQueryHopCount(msgId);
		assert (numHops >= 0);

		// DEBUG: print intermediate hops, as stored in query packet...
		Logger logger = sourceNode.getLogger();
		if (logger.level <= Logger.FINE) {
			Vector<Id> hops = sourceNode.getQueryHops(msgId);
			StringBuilder sb = new StringBuilder();
			if (hops != null && hops.size() > 0) {
				sb.append("\t" + hops.get(0));
				for (int hop = 1; hop < hops.size(); hop++)
					sb.append(" => " + hops.get(hop));
				sb.append("\n");
			}
			logger.log("Query " + msgId + ": " + numHops + " hops :: "
					+ sb.toString());
		}

		return new DHSBitProbeResult(sourceNode.removeResponseToQuery(msgId),
				numHops);
	}

	protected DHSImpl(DHSSketchAlgorithms sketchAlgo, int numVecs,
			String hashAlgo, int L, int retries, Vector<String> dhsMetrics,
			Environment env) throws NoSuchAlgorithmException {
		this.L = L;
		this.hashAlgo = hashAlgo;
		this.sketchAlgo = sketchAlgo;
		this.numVecs = numVecs;
		this.retries = retries;
		this.baseHashSketchImpl = new HashSketchImpl(numVecs, L, hashAlgo);
		this.dhsMetrics = new Vector<String>(10, 2);
		this.distributedHS = new Vector<HashSketchImpl>(10, 2);
		addDHSMetrics(dhsMetrics);
		this.env = env;
		if (sketchAlgo.isDF03())
			assert (this instanceof DHSDF03Impl);
		else
			assert (this instanceof DHSFM85Impl);
	}

	protected DHSImpl(DHSSketchAlgorithms sketchAlgo, int numVecs,
			String hashAlgo, int L, int retries, Environment env)
			throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, null, env);
	}

	protected DHSImpl(DHSSketchAlgorithms sketchAlgo, int numVecs, int L,
			int retries, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries,
				env);
	}

	protected final double estimate(String metric, SketchAlgorithms sketchAlgo)
			throws UnsupportedSketchAlgorithmException, NoSuchMetricException {
		int index = -1;
		if (distributedHS == null
				|| (index = Collections.binarySearch(dhsMetrics, metric)) < 0)
			throw new NoSuchMetricException();
		return distributedHS.get(index).estimate(sketchAlgo);
	}

	protected final boolean idRangeOverlapsWithArc(IdRange range, int l) {
		assert (l >= 0 && l < L);
		return (!range.intersect(getIdRangeForBit(l)).isEmpty());
	}

	protected final Id mapToNode(int l) {
		assert (l >= 0 && l < L);
		return getRandomIdForBit(l);
	}

	protected final void basicInsert(DHSPastryAppl sourceNode, String metric,
			ItemSerializable object) {
		int[] bits = (int[])baseHashSketchImpl.tryAddObject(object);
		Id targetId = mapToNode(bits[1]);
		DHSTuple tupleData = new DHSTuple(metric, bits[0], bits[1]);
		Long msgId = sourceNode.insert(targetId, tupleData);
	}

	// //////////////////
	// Public methods //
	// //////////////////

	public static final DHSImpl newInstance(DHSSketchAlgorithms sketchAlgo,
			int numVecs, String hashAlgo, int L, int retries, Environment env)
			throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs, hashAlgo, L, retries,
					env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs, hashAlgo, L, retries,
					env);
	}

	public static final DHSImpl newInstance(DHSSketchAlgorithms sketchAlgo,
			int numVecs, int L, int retries, Environment env)
			throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs, L, retries, env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs, L, retries, env);
	}

	public static final DHSImpl newInstance(DHSSketchAlgorithms sketchAlgo,
			int numVecs, int L, int retries, Vector<String> metrics,
			Environment env) throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs,
					HashSketchImpl.defaultHashAlgo, L, retries, metrics, env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs,
					HashSketchImpl.defaultHashAlgo, L, retries, metrics, env);
	}

	public static final DHSImpl newInstance(DHSSketchAlgorithms sketchAlgo,
			int numVecs, String hashAlgo, int L, int retries,
			Vector<String> dhsMetrics, Environment env)
			throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs, hashAlgo, L, retries,
					dhsMetrics, env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs, hashAlgo, L, retries,
					dhsMetrics, env);
	}

	public final void clearHS() {
		if (distributedHS != null)
			for (HashSketchImpl hs : distributedHS)
				hs.clear();
	}

	public final void clear() {
		clearHS();
	}

	public final int getNumPendingInsertions(DHSPastryAppl sourceNode) {
		return sourceNode.getNumPendingInsertions();
	}

	public final int getNumVecs() {
		return numVecs;
	}

	public final int getRetries() {
		return retries;
	}

	public String toString() {
		return "[ DHSImpl :: algo: " + sketchAlgo + " , HS: "
				+ baseHashSketchImpl + " , retries: " + retries + " ]";
	}

	public final String getHashAlgo() {
		return hashAlgo;
	}

	public final int getNumBits() {
		return L;
	}

	// public final Vector<String> getDHSMetrics() { return dhsMetrics; }
	public final void addDHSMetrics(Vector<String> dhsMetrics) {
		if (dhsMetrics == null)
			return;
		for (String s : dhsMetrics) {
			int index = -1;
			if ((index = Collections.binarySearch(this.dhsMetrics, s)) < 0) {
				this.dhsMetrics.add(-index - 1, s);
				try {
					this.distributedHS.add(-index - 1, new HashSketchImpl(
							baseHashSketchImpl));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public final void batchInsert(DHSPastryAppl sourceNode, String metric,
			ItemSerializable object) {
		int index = -1;
		if ((index = Collections.binarySearch(dhsMetrics, metric)) < 0) {
			dhsMetrics.add(-index - 1, metric);
			try {
				distributedHS.add(-index - 1, new HashSketchImpl(
						baseHashSketchImpl));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		sourceNode.batchInsert(metric, object);
	}

	public final void insert(DHSPastryAppl sourceNode, String metric,
			ItemSerializable object) {
		int index = -1;
		if ((index = Collections.binarySearch(dhsMetrics, metric)) < 0) {
			dhsMetrics.add(-index - 1, metric);
			try {
				distributedHS.add(-index - 1, new HashSketchImpl(
						baseHashSketchImpl));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}

		if (sketchAlgo.usesBinarySearch())
			insertBinarySearch(sourceNode, metric, object);
		else
			insertClassic(sourceNode, metric, object);
	}

	public final DistributedEstimationResult[] estimate(
			DHSPastryAppl sourceNode, Vector<String> metrics)
			throws NoSuchMetricException {
		DistributedEstimationResult[] ret;
		Logger logger = sourceNode.getLogger();

		if (sketchAlgo.usesBinarySearch() == true)
			ret = estimateBinarySearch(sourceNode, metrics);
		else
			ret = estimateClassic(sourceNode, metrics);

		if (logger.level <= Logger.FINE) {
			for (HashSketchImpl hs : distributedHS)
				logger.log(sketchAlgo + " distributed HS :: " + "\n" + hs
						+ "\n" + hs.toStringFull());
		}

		// DEBUG
		// System.out.println(sketchAlgo + " distributed estimation: " + ret);

		return ret;
	}

	public final String getSketchAlgoName() {
		return sketchAlgo.toString();
	}

	public final DHSSketchAlgorithms getSketchAlgo() {
		return sketchAlgo;
	}

	// ////////////////////
	// Abstract methods //
	// ////////////////////

	protected abstract void insertClassic(DHSPastryAppl sourceNode,
			String metric, ItemSerializable item);

	protected abstract void insertBinarySearch(DHSPastryAppl sourceNode,
			String metric, ItemSerializable item);

	protected abstract DistributedEstimationResult[] estimatePartial(
			DHSPastryAppl sourceNode, Vector<String> metrics, int numBits,
			Hashtable<String, BitSet[]> vecsValue, int hopCount,
			HashSet<Integer> bitsProbed) throws NoSuchMetricException;

	// ///////////////////
	// Private methods //
	// ///////////////////

	private DistributedEstimationResult[] estimateBinarySearch(
			DHSPastryAppl sourceNode, Vector<String> metrics)
			throws NoSuchMetricException {
		Hashtable<String, BitSet[]> vecsValue = new Hashtable<String, BitSet[]>();
		for (int metric = 0; metric < metrics.size(); metric++) {
			BitSet[] val = new BitSet[L];
			for (int bit = 0; bit < L; bit++)
				val[bit] = new BitSet();
			vecsValue.put(metrics.get(metric), val);
		}
		int hopCount = 0;
		HashSet<Integer> bitsProbed = new HashSet<Integer>();

		for (HashSketchImpl h : distributedHS)
			h.clear();

		// Binary search for the highest bit position for which all vectors have
		// a 0-bit...
		int bitLow = 0, bitHigh = L;
		for (int curBit = (bitLow + (int)QuickMath.floor(QuickMath.random()
				* (bitHigh - bitLow) / 2)); bitLow <= bitHigh; curBit = sketchAlgo.nextBit(
				curBit, bitLow, bitHigh)) {
			assert (bitsProbed.contains(curBit) == false);

			if (allMetricVecsSetForBit(vecsValue, curBit, metrics) == true) {
				bitLow = curBit;
				continue;
			}

			DHSBitProbeResult response = probe(sourceNode, metrics, (int)curBit);
			bitsProbed.add(curBit);
			hopCount += response.hopCount;

			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item++) {
					DHSTuple dhsTuple = response.resultData[item];
					int metricIndex = Collections.binarySearch(dhsMetrics,
							dhsTuple.getMetric());
					distributedHS.get(metricIndex).setBit(dhsTuple.getVec(),
							dhsTuple.getBit());
					vecsValue.get(dhsTuple.getMetric())[dhsTuple.getBit()].set(dhsTuple.getVec());
				}
				if (noMetricVecsSetForBit(vecsValue, curBit, metrics) == false)
					bitLow = curBit + 1;
				else
					bitHigh = curBit - 1;
			} else {
				bitHigh = curBit - 1;
			}
			// DEBUG
			// if (vecsValue[curBit].bitCount() == numVecs)
			// System.out.print("@");
			// if (curBit == (L - 1))
			// System.out.print(" " + L + ":" + vecsValue[curBit].bitCount() + "
			// ");
			// else
			// System.out.print(" " + curBit + ":" +
			// vecsValue[curBit].bitCount() + " ");
		}

		// ... then revert to the standard probing procedure, with the above
		// position as the starting point.
		return estimatePartial(sourceNode, metrics, bitLow, vecsValue,
				hopCount, bitsProbed);
	}

	private DistributedEstimationResult[] estimateClassic(
			DHSPastryAppl sourceNode, Vector<String> metrics)
			throws NoSuchMetricException {
		for (HashSketchImpl hs : distributedHS)
			hs.clear();
		Hashtable<String, BitSet[]> vecsValue = new Hashtable<String, BitSet[]>();

		for (int metric = 0; metric < metrics.size(); metric++) {
			BitSet[] val = new BitSet[L];
			for (int bit = 0; bit < L; bit++)
				val[bit] = new BitSet();
			vecsValue.put(metrics.get(metric), val);
		}
		return estimatePartial(sourceNode, metrics, L, vecsValue, 0, null);
	}

	private IdRange getIdRangeForBit(int r) {
		return new IdRange(Id.build(thr(r)), Id.build(thr(r - 1)));
	}

	private Id getRandomIdForBit(int r) {
		int index = Id.IdBitLength - r - 1;

		assert (r >= -1 && r < L && index < Id.IdBitLength);

		byte rnd[] = new byte[(int)QuickMath.ceil((double)index / 8.0)];
		byte id[] = new byte[Id.IdBitLength / 8];
		env.getRandomSource().nextBytes(rnd);
		byte mask = 0x00;
		for (int bit = 0; bit <= index % 8; bit++) {
			mask |= (0x01 << bit);
		}
		rnd[rnd.length - 1] &= mask;
		for (int i = 0; i < rnd.length; i++)
			id[i] = rnd[i];
		return Id.build(id);
	}

	private int[] thr(int r) {
		assert (r >= -1 && r < L);
		int id[] = new int[intsPerId];
		int index = Id.IdBitLength - r - 1;
		if (index < Id.IdBitLength)
			id[index / 32] = (1 << (index % 32));
		else
			for (int i = 0; i < intsPerId; i++)
				id[i] = 0xffffffff;
		return id;
	}

}

class DHSBitProbeResult {
	DHSTuple[]	resultData	= null;
	int			hopCount	= 0;

	public DHSBitProbeResult(DHSTuple[] resultData, int hopCount) {
		this.resultData = resultData;
		this.hopCount = hopCount;
		assert (hopCount >= 0);
	}
}
