/**
 * $Id$
 */

package netcins.p2p.dhs.testing;

import java.security.NoSuchAlgorithmException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithms;
import netcins.dbms.centralized.sketches.UnsupportedSketchAlgorithmException;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithms;
import netcins.util.QuickMath;
import netcins.util.ZipfGenerator;
import rice.environment.Environment;
import rice.pastry.Id;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.direct.BasicNetworkSimulator;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.standard.RandomNodeIdFactory;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 */
public class DHSSingleMetricTest {
	private DHSImpl					dhsImpl					= null;
	private PastryIdFactory			idFactory				= null;
	private PastryNodeFactory		factory					= null;
	private Environment				env						= null;
	private Vector<DHSPastryAppl>	pastryAppls				= null;
	private DHSSketchAlgorithms		sketchAlgo				= DHSImpl.defaultSketchAlgo;
	private HashSketchImpl			centralizedHashSketch	= null;
	private static final String		metric					= "Single Metric";

	private int						numItems				= 0;
	private int						qNode					= -1;

	private boolean					isInited				= false;
	private final Logger			logger					= Logger.getLogger("netcins.p2p.dhs.testing.DHSSingleMetricTest");
	private BasicNetworkSimulator	simulator				= null;

	public DHSSingleMetricTest(DHSSketchAlgorithms sketchAlgo, int numVecs,
			int L, int retries, Environment env) {
		init(sketchAlgo, numVecs, L, retries, env);
	}

	public DHSSingleMetricTest(int numVecs, int L, int retries, Environment env) {
		init(DHSImpl.defaultSketchAlgo, numVecs, L, retries, env);
	}

	public DHSSingleMetricTest() {
		this(-1, -1, -1, null);
	}

	public void init(DHSSketchAlgorithms algorithm, int numVecs, int L,
			int retries, Environment env) {
		if (isInited)
			destroy();
		this.env = env;
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null) {
			destroy();
			return;
		}
		this.pastryAppls = new Vector<DHSPastryAppl>();
		this.idFactory = new rice.pastry.commonapi.PastryIdFactory(env);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(
				new RandomNodeIdFactory(env), simulator, env);
		try {
			this.centralizedHashSketch = new HashSketchImpl(numVecs, L);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		initDHSImpl(algorithm, numVecs, L, retries);
		Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting").setLevel(
				Level.INFO);
		isInited = true;
	}

	public void initDHSImpl(DHSSketchAlgorithms algorithm, int numVecs, int L,
			int retries) {
		Vector<String> metrics = new Vector<String>();
		metrics.add(DHSSingleMetricTest.metric);
		this.sketchAlgo = algorithm;
		try {
			this.dhsImpl = DHSImpl.newInstance(algorithm, numVecs, L, retries,
					metrics, env);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		System.out.println("Initializing DHSSingleMetricTest with "
				+ this.dhsImpl);
		for (DHSPastryAppl app : pastryAppls)
			app.setDHSImpl(this.dhsImpl);
	}

	private void clearHits() {
		for (DHSPastryAppl app : pastryAppls)
			app.clearHits();
	}

	public boolean isInited() {
		return isInited;
	}

	public void destroy() {
		if (isInited == false)
			return;
		isInited = false;
		env.destroy();
		pastryAppls = null;
		env = null;
	}

	public void clearData() {
		if (isInited == false)
			return;
		for (DHSPastryAppl appl : pastryAppls) {
			appl.reinitData();
		}
		centralizedHashSketch.clear();
		clearHits();
		numItems = 0;
	}

	public void addNodes(int numNodes) {

		System.runFinalization();
		System.gc();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		// this.numNodes = numNodes;
		System.out.println("Populating the overlay with new nodes...");

		PastryNode bootstrapNode = null;
		for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0)
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle)null);
			else
				node = factory.newNode(bootstrapNode.getLocalHandle());

			logger.log(Level.FINER, "Finished creating new node " + node);

			DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);

			pastryAppls.add(dhsAppl);

			if ((curNodeIndex % 100) == 0)
				System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
			else if ((curNodeIndex % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + numNodes + "/" + numNodes + " done.");

		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
		for (int i = 0; i < pastryAppls.size(); i++) {
			pastryAppls.get(i).waitForNodeToBeReady();
			if ((i % 100) == 0)
				System.out.print(" " + i + "/" + numNodes + " ");
			else if ((i % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + pastryAppls.size() + "/" + numNodes + " done.");

		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl node generation took "
				+ (endTime - startTime) / 1000 + "\".");
		qNode = (int)QuickMath.floor(QuickMath.random() * pastryAppls.size());
	}

	private void doBatchInsertions() {
		int items = 0;
		int stopStep = 100000;
		int nextStop = stopStep;
		System.out.print(" " + 0 + "/" + numItems + " ");
		for (int i = 0; i < pastryAppls.size(); i++) {
			items += pastryAppls.get(i).doBatch();
			if (items > nextStop) {
				nextStop += stopStep;
				waitForInsertionsToComplete();
				System.out.print(" " + items + "/" + numItems + " ");
			}
		}
		waitForInsertionsToComplete();
		System.out.println(" " + numItems + "/" + numItems + " done.");
	}

	private void waitForInsertionsToComplete() {
		for (int i = 0; i < pastryAppls.size(); i++) {
			DHSPastryAppl curAppl = pastryAppls.get(i);
			curAppl.waitForInsertionsToComplete();
			curAppl.clearInsertionResponses();
		}
	}

	public void addDataBatch(int[] tuples) {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems = tuples.length;

		System.out.println("Injecting " + numItems + " new data tuples...");
		for (int data = 0; data < numItems; data++) {
			BasicItem item = new BasicItem(
					(Id)idFactory.buildRandomId(env.getRandomSource()),
					tuples[data]);
			dhsImpl.batchInsert(pastryAppls.get(data % pastryAppls.size()),
					DHSSingleMetricTest.metric, item);
			centralizedHashSketch.insert(item);
		}
		doBatchInsertions();

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl data generation took "
				+ (endTime - startTime) / 1000 + "\".");
		System.runFinalization();
		System.gc();
		long numHits = 0;
		long numInsertionsHops = 0;
		System.out.print("Insertion Hits: ");
		for (DHSPastryAppl curAppl : pastryAppls) {
			System.out.print(" " + curAppl.getInsertionHits());
			numHits += curAppl.getInsertionHits();
			numInsertionsHops += curAppl.getInsertionsHopCount();
		}
		System.out.println();
		System.out.println("Node insertion hits using batch insertions: "
				+ numHits + ", hop count: " + numInsertionsHops);
	}

	public void addData(int[] tuples) {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems = tuples.length;

		System.out.println("Injecting " + numItems + " new data tuples...");
		for (int data = 0; data < numItems; data++) {
			BasicItem item = new BasicItem(
					(Id)idFactory.buildRandomId(env.getRandomSource()),
					tuples[data]);
			dhsImpl.insert(pastryAppls.get(data % pastryAppls.size()),
					DHSSingleMetricTest.metric, item);
			centralizedHashSketch.insert(item);

			if ((data % 100000) == 0) {
				if (data > 0)
					waitForInsertionsToComplete();
				System.out.print(" " + data + "/" + numItems + " ");
			} else if ((data % 10000) == 0) {
				System.out.print(".");
			}

		}
		waitForInsertionsToComplete();

		System.out.println(" done. ");

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl data generation took "
				+ (endTime - startTime) / 1000 + "\".");
		System.runFinalization();
		System.gc();
		long numHits = 0;
		long numInsertionsHops = 0;
		System.out.print("Insertion Hits: ");
		for (DHSPastryAppl curAppl : pastryAppls) {
			System.out.print(" " + curAppl.getInsertionHits());
			numHits += curAppl.getInsertionHits();
			numInsertionsHops += curAppl.getInsertionsHopCount();
		}
		System.out.println();
		System.out.println("Node insertion hits: " + numHits + ", hop count: "
				+ numInsertionsHops);
	}

	public void query() {
		System.runFinalization();
		System.gc();
		clearHits();
		long startTime = System.currentTimeMillis(), endTime;
		System.out.println("Querying node: "
				+ pastryAppls.get(qNode).getNodeId() + ", node #" + qNode);
		Vector<String> metrics = new Vector<String>();
		metrics.add(DHSSingleMetricTest.metric);
		try {
			System.out.println(sketchAlgo
					+ " centralized estimation: "
					+ QuickMath.doubleToString(centralizedHashSketch.estimate(sketchAlgo.isDF03()
							? SketchAlgorithms.DF03 : SketchAlgorithms.FM85)));
		} catch (UnsupportedSketchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			System.out.println(dhsImpl + ", distributed estimation: "
					+ dhsImpl.estimate(pastryAppls.get(qNode), metrics)[0]);
		} catch (NoSuchMetricException e) {
			e.printStackTrace();
		}

		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl query took " + (endTime - startTime) / 1000
				+ "\".");
		long numHits = 0;
		System.out.print("Query hits: ");
		for (DHSPastryAppl curAppl : pastryAppls) {
			System.out.print(" " + curAppl.getQueryHits());
			numHits += curAppl.getQueryHits();
		}
		System.out.println();
		System.out.println("Node query hits: " + numHits);
	}

	public static void main(String[] args) {

		int numNodes = 0, numTuples = 0, numVecs = 0, L = 0, retries = 0, numValues = 0;
		double theta = 0;

		try {
			numNodes = Integer.parseInt(args[0]);
			numTuples = Integer.parseInt(args[1]);
			numVecs = Integer.parseInt(args[2]);
			L = Integer.parseInt(args[3]);
			retries = Integer.parseInt(args[4]);
			numValues = Integer.parseInt(args[5]);
			theta = Double.parseDouble(args[6]);
		} catch (NumberFormatException e) {
			System.out.println("Usage: java [-cp FreePastry-<version>.jar]");
			System.out.println("\tnetcins.p2p.dhs.testing.DHSSingleMetricTest <numNodes> <numItems> <numVecs> <L> <retries> <numValues> <zipf theta>");
			System.exit(0);
		}

		long startTime = System.currentTimeMillis(), endTime = 0;

		DHSSingleMetricTest test = null;
		int seed = (int)QuickMath.floor(Math.random()
				* (double)Integer.MAX_VALUE);

		System.out.println("\n" + " --- Begin Simulation --- ");
		ZipfGenerator zipf = new ZipfGenerator(numValues, theta);
		int[] data = new int[numTuples];
		for (int i = 0; i < numTuples; i++)
			data[i] = zipf.nextRank();

		DHSSketchAlgorithms testAlgos[] = DHSSketchAlgorithms.values();

		System.out.println(" --- Seed: " + seed + ", domain: " + numValues
				+ ", Zipf theta: " + theta + " ---");
		try {
			test = new DHSSingleMetricTest(null, numVecs, L, retries,
					Environment.directEnvironment(seed));
			test.addNodes(numNodes);
			System.out.println("\n" + " ---" + "\n");
			for (int nVecs = numVecs; nVecs >= 32; nVecs /= 2) {
				boolean firstRun = true;
				for (DHSSketchAlgorithms algo : testAlgos) {
					System.out.println("Testing " + algo + "...");
					test.initDHSImpl(algo, nVecs, L, retries);
					if (firstRun) {
						test.clearData();
						test.addData(data);
						firstRun = false;
					}
					test.query();
					System.out.println("\n" + " ---" + "\n");
				}
			}
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: "
					+ (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}
}
