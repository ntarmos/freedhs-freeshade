/**
 * $Id$
 */

package netcins.util;

import java.util.Random;

/**
 * A random number generator, following the Zipf distribution. This
 * implementation needs to keep an array of the CDF of Zipf for all possible
 * ranks, amounting to a memory requirement of 8 times the number of ranks in
 * bytes (i.e. 8Mbytes for 1M ranks). For a somewhat slower implementation for
 * theta = 1.0 but requiring O(1) space, see
 * {@link netcins.util.QuickMath#quickZipf(double, long)}.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Rev$
 * @see netcins.util.QuickMath#quickZipf(double, long)
 */
public class ZipfGenerator {
	private double	allValues[]	= null;
	private double	theta		= 0.0;
	private Random	rng			= null;

	/**
	 * Constructor for ZipfGenerator.
	 * 
	 * @param domain The number of values in the domain.
	 * @param theta The parameter of the Zipf distribution.
	 */
	public ZipfGenerator(int domain, double theta) {
		double norm = 0;
		this.theta = theta;
		this.allValues = new double[domain];

		for (int i = 0; i < allValues.length; i++) {
			allValues[i] = 1.0 / Math.pow(1 + i, theta);
			norm += allValues[i];
		}

		allValues[0] /= norm;
		for (int i = 1; i < allValues.length; i++) {
			allValues[i] = (allValues[i] / norm) + allValues[i - 1];
		}

		rng = new Random();
	}

	/**
	 * Constructor for ZipfGenerator.
	 * 
	 * @param domain The number of values in the domain.
	 * @param theta The parameter of the Zipf distribution.
	 * @param seed The seed for the internal random number generator.
	 */
	public ZipfGenerator(int domain, double theta, long seed) {
		this(domain, theta);
		rng = new Random(seed);
	}

	/**
	 * Returns the 'theta' parameter of the current Zipf distribution.
	 * 
	 * @return The 'theta' parameter of the current Zipf distribution.
	 */
	public double getTheta() {
		return theta;
	}

	/**
	 * Returns the cardinality of the domain of ranks.
	 * 
	 * @return The cardinality of the domain of ranks.
	 */
	public int getDomain() {
		return allValues.length;
	}

	/**
	 * Computes the rank corresponding to a given probability.
	 * 
	 * @param p The probability to compute the rank for. As a probability value,
	 *            p must be in the interval [0, 1].
	 * @return The rank corresponding to the given probability.
	 */
	public int getRank(double p) {
		assert (p >= 0 && p <= 1);
		int low = 0, high = allValues.length, cur = high / 2;
		while (low != high && low != high - 1) {
			if (QuickMath.abs(allValues[cur] - p) < .000000001)
				break;
			if (allValues[cur] > p) {
				high = cur;
			} else {
				low = cur;
			}
			cur = (low + high) / 2;
		}
		return cur;
	}

	/**
	 * Computes a random rank, following the Zipf distribution.
	 * 
	 * @return The next random rank, following the Zipf distribution.
	 */
	public int nextRank() {
		return getRank(rng.nextDouble());
	}

	/*
	 * public static void main(String args[]) { int numValues =
	 * Integer.parseInt(args[0]); int domain = Integer.parseInt(args[1]); double
	 * theta = Double.parseDouble(args[2]);
	 * 
	 * ZipfGenerator zipf = new ZipfGenerator(domain, theta);
	 * 
	 * PrintStream out = null; try { out = new PrintStream(new
	 * FileOutputStream("out.txt")); } catch (FileNotFoundException e) {
	 * e.printStackTrace(); }
	 * 
	 * int pop[] = new int[domain]; long time = System.currentTimeMillis();
	 * 
	 * for (int num = 0; num < numValues; num ++) pop[zipf.nextRank()] ++;
	 * 
	 * time = System.currentTimeMillis() - time; System.out.println("Total time: " +
	 * time/1000.0 + "\''");
	 * 
	 * int qpop[] = new int[domain]; time = System.currentTimeMillis();
	 * 
	 * for (int num = 0; num < numValues; num ++)
	 * qpop[(int)QuickMath.quickZipf(Math.random(), domain)] ++;
	 * 
	 * time = System.currentTimeMillis() - time; System.out.println("Total time: " +
	 * time/1000.0 + "\''");
	 * 
	 * 
	 * for (int i = 0; i < pop.length; i ++) out.println(pop[i] + " " +
	 * qpop[i]); }
	 */
}
