/**
 * $Id$
 */

package netcins.util;

import java.util.Locale;

/**
 * Fast and inexact implementation of basic math methods. All parameters are
 * supposed to be non-negative, as is the case with IDs and (without loss of
 * generality) attribute values in our experimental studies. This is to avoid
 * calling the expensive StrictMath methods.
 * 
 * @author ntarmos
 * @version $Rev$
 */
public class QuickMath {

	private QuickMath() {
	}

	/**
	 * Computes the first integer whose value is less than or equal to the
	 * supplied parameter.
	 * 
	 * @param d A non-negative double value.
	 * @return The first integer whose value is less than or equal to the
	 *         supplied parameter.
	 */
	public static final double floor(double d) {
		return (double)((long)d);
	}

	/**
	 * Computes the first integer whose value is greater than or equal to the
	 * supplied parameter.
	 * 
	 * @param d A non-negative double value.
	 * @return The first integer whose value is greater than or equal to the
	 *         supplied parameter.
	 */
	public static final double ceil(double d) {
		double f = floor(d);
		return (abs(d - f) < 0.000000001 ? f : f + 1);
	}

	/**
	 * Computes the closest integer to the given double value. That is, if the
	 * floating point part of d is &lt; 0.5 then this function returns floor(d),
	 * otherwise it returns ceil(d).
	 * 
	 * @param d A non-negative double value.
	 * @return The closest integer or equal to the supplied parameter.
	 */
	public static final double round(double d) {
		double f = floor(d);
		return (d - f < 0.5 ? f : f + 1);
	}

	/**
	 * Simple wrapper for Math.random().
	 * 
	 * @return A random double in the interval [0, 1).
	 * @see java.lang.Math#random()
	 */
	public static final double random() {
		return Math.random();
	}

	/**
	 * Computes the rank of an item in an N-item population following a
	 * Zipf-like distribution with theta = 1.0, given its probability value.
	 * This only works for theta = 1.0; for this value, Zipf's CDF can be
	 * approximated by ln(k)/ln(N). Thus, given a CDF value p, we can solve for
	 * k and get: k = ((N+1)^p) - 1.
	 * 
	 * @param p A value in the interval [0, 1) denoting the probability of the
	 *            requested item.
	 * @param N The total number of items in the population.
	 * @return A long in the interval [0, N) denoting the rank of the requested
	 *         item.
	 * @deprecated Use netcins.ZipfGenerator instead.
	 */
	public static final long quickZipf(double p, long N) {
		// final double theta = 1.0;
		return (long)(Math.pow(N + 1, p)) - 1L;
	}

	/**
	 * Computes the absolute value of v.
	 * 
	 * @param v A double whose absolute value to compute.
	 * @return v if v >= 0, -v otherwise.
	 */
	public static final double abs(double v) {
		return (v >= 0 ? v : -v);
	}

	/**
	 * Computes the maximum of two double values.
	 * 
	 * @param a The first value.
	 * @param b The second value.
	 * @return The maximum of the two input values.
	 */
	public static final double max(double a, double b) {
		return (a > b ? a : b);
	}

	/**
	 * Computes the minimum of two double values.
	 * 
	 * @param a The first value.
	 * @param b The second value.
	 * @return The minimum of the two input values.
	 */
	public static final double min(double a, double b) {
		return (a < b ? a : b);
	}

	/**
	 * Formats a double value for user-friendly output.
	 * 
	 * @param d The double value to format.
	 * @return A user-friendly String represenatation of the input value.
	 */
	public static final String doubleToString(double d) {
		return String.format((Locale)null, "%.2f", d);
	}
}
