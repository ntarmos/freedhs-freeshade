#!/bin/sh

grep 'Querying node' $1 | tail -1;
awk '/ , hop count: /{print $33"\t"$26"\t"$5}' $1 | tail -12 | sort -g
