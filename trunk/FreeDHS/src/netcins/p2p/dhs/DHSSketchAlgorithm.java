/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs;

import netcins.dbms.centralized.sketches.SketchAlgorithm;
import netcins.util.QuickMath;

/**
 * Class encapsulating the various DHS implementation choices.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSSketchAlgorithm {
	/*
	 * Default algorithm is DF03 with Binary-Search-like bit probing, recursive
	 * processing, and no result caching.
	 */
	private SketchAlgorithm	sketchAlgorithm				= new SketchAlgorithm();
	private boolean			usingSequentialBitProbing	= false;
	private boolean			usingIterativeProcessing	= false;
	private boolean			usingResultCaching			= false;
	private boolean			usingBatchInsertions		= false;
	private boolean			usingShortcuts				= false;

	/**
	 * @param usingFM85 true if the current algorithm is using a FM85 estimator,
	 *            false for a DF03 estimator
	 * @param usingSequentialBitProbing true if the current algorithm is using
	 *            sequential bit probing, false for binary-search-like bit probing
	 * @param usingIterativeProcessing true if the current algorithm is using
	 *            iterative processing, false for recursive processing 
	 * @param usingResultCaching true if the current algorithm is using result
	 *            caching, false otherwise
	 * @param usingBatchInsertions true if the current algorithm is using batched
	 * 			  insertions, false otherwise
	 * @param usingShortcuts true if the current algorithm is using routing
	 * 			  shortcuts, false otherwise
	 */
	public DHSSketchAlgorithm(boolean usingFM85, boolean usingSequentialBitProbing,
			boolean usingIterativeProcessing, boolean usingResultCaching,
			boolean usingBatchInsertions, boolean usingShortcuts) {
		if (usingFM85 == true)
			this.sketchAlgorithm.setUsingFM85();
		else
			this.sketchAlgorithm.setUsingDF03();
		this.usingSequentialBitProbing = usingSequentialBitProbing;
		this.usingIterativeProcessing = usingIterativeProcessing;
		this.usingResultCaching = usingResultCaching;
		this.usingBatchInsertions = usingBatchInsertions;
		this.usingShortcuts = usingShortcuts;
	}

	/**
	 * Default algorithm is DF03 with Binary-Search-like bit probing, recursive
	 * processing, and no result caching.
	 */
	public DHSSketchAlgorithm() {
	}

	/**
	 * @return an array with DHSSketchAlgorithm instances with all possible
	 *         parameter values. Only for testing purposes.
	 */
	public static final DHSSketchAlgorithm[] values() {
		DHSSketchAlgorithm[] ret = new DHSSketchAlgorithm[16];
		int index = 0;
		for (int fm = 0; fm < 2; fm++) {
			for (int sbp = 0; sbp < 2; sbp++) {
				for (int ir = 0; ir < 2; ir++) {
					for (int rc = 0; rc < 2; rc++) {
						for (int bi = 0; bi < 2; bi++)
							for (int us = 0; us < 2; us++)
								ret[index++] = new DHSSketchAlgorithm((fm != 0), (sbp != 0),
										(ir != 0), (rc != 0), (bi != 0), (us != 0));
					}
				}
			}
		}
		return ret;
	}

	/**
	 * @param low the lowest bit position
	 * @param high the highest bit position
	 * @return the first bit position to probe, based on the current algorithm
	 */
	public int firstBit(int low, int high) {
		return ((isUsingDF03() && isUsingIterativeProcessing() && isUsingSequentialBitProbing())
				? nextBit(high, low, high) : nextBit(-1, low, high));
	}

	/**
	 * Computes the next bit position to probe, based on the current bit
	 * position and the upper and lower bit thresholds (in the case of an
	 * estimator using binary-search bit probing)
	 * 
	 * @param curBit the current bit position.
	 * @param low the lower bit position threshold.
	 * @param high the upper bit position threshold.
	 * @return the next bit position to probe.
	 */
	public int nextBit(int curBit, int low, int high) {
		int ret = 0;

		if (isUsingDF03() && isUsingIterativeProcessing() && isUsingSequentialBitProbing())
			return curBit - 1;

		if (isUsingSequentialBitProbing())
			return curBit + 1;

		return (low + (int)QuickMath.floor((double)(high - low) / 2.0));
	}

	/**
	 * @return true if the current algorithm is using a FM85 estimator, false
	 *         otherwise
	 */
	public final boolean isUsingFM85() {
		return this.sketchAlgorithm.isUsingFM85();
	}

	/**
	 * @param usingFM85 true if the current algorithm is using a FM85 estimator,
	 *            false otherwise
	 */
	public final DHSSketchAlgorithm setUsingFM85() {
		this.sketchAlgorithm.setUsingFM85();
		return this;
	}

	/**
	 * @return true if the current algorithm is using a DF03 estimator, false
	 *         otherwise
	 */
	public final boolean isUsingDF03() {
		return !isUsingFM85();
	}

	/**
	 * Ask the algorithm to use a DF03 estimator
	 */
	public final DHSSketchAlgorithm setUsingDF03() {
		this.sketchAlgorithm.setUsingDF03();
		return this;
	}

	/**
	 * @return true if the current algorithm is using iterative processing,
	 *         false otherwise
	 */
	public final boolean isUsingIterativeProcessing() {
		return this.usingIterativeProcessing;
	}

	/**
	 * @param usingIterativeProcessing true if the current algorithm is using
	 *            iterative processing, false otherwise
	 */
	public final DHSSketchAlgorithm setUsingIterativeProcessing() {
		this.usingIterativeProcessing = true;
		return this;
	}

	/**
	 * @return true if the current algorithm is using recursive processing,
	 *         false otherwise
	 */
	public final boolean isUsingRecursiveProcessing() {
		return !isUsingIterativeProcessing();
	}

	/**
	 * @param usingRecursiveProcessing true if the current algorithm is using
	 *            recursive processing, false otherwise
	 */
	public final DHSSketchAlgorithm setUsingRecursiveProcessing() {
		this.usingIterativeProcessing = false;
		return this;
	}

	/**
	 * @return true if the current algorithm is using result caching, false
	 *         otherwise
	 */
	public final boolean isUsingResultCaching() {
		return this.usingResultCaching;
	}

	/**
	 * @param usingResultCaching true if the current algorithm is using result
	 *            caching, false otherwise
	 */
	public final DHSSketchAlgorithm setUsingResultCaching() {
		this.usingResultCaching = true;
		return this;
	}

	/**
	 * @return true if the current algorithm is using sequential bit probing,
	 *         false otherwise
	 */
	public final boolean isUsingSequentialBitProbing() {
		return this.usingSequentialBitProbing;
	}

	/**
	 * Ask the algorithm not to use result caching
	 */
	public final DHSSketchAlgorithm setNotUsingResultCaching() {
		this.usingResultCaching = false;
		return this;
	}

	/**
	 * @param usingSequentialBitProbing true if the current algorithm is using
	 *            sequential bit probing, false otherwise
	 */
	public final DHSSketchAlgorithm setUsingSequentialBitProbing() {
		this.usingSequentialBitProbing = true;
		return this;
	}

	/**
	 * @return true if the current algorithm is using binary-search-like bit
	 *         probing, false otherwise
	 */
	public final boolean isUsingBinarySearchBitProbing() {
		return !isUsingSequentialBitProbing();
	}

	/**
	 * Ask the algorithm to not use batch insertions
	 */
	public final DHSSketchAlgorithm setNotUsingBatchInsertions() {
		this.usingBatchInsertions = false;
		return this;
	}

	/**
	 * Ask the algorithm to use batch insertions
	 */
	public final DHSSketchAlgorithm setUsingBatchInsertions() {
		this.usingBatchInsertions = true;
		return this;
	}

	/**
	 * @return true if the current algorithm is using batch insertions,
	 *         false otherwise
	 */
	public final boolean isUsingBatchInsertions() {
		return this.usingBatchInsertions;
	}

	/**
	 * Ask the algorithm to use binary-search-like bit probing
	 */
	public final DHSSketchAlgorithm setUsingBinarySearchBitProbing() {
		this.usingSequentialBitProbing = false;
		return this;
	}

	/**
	 * @return true if the current algorithm is using routing shortcuts,
	 *         false otherwise
	 */
	public final boolean isUsingShortcuts() {
		return this.usingShortcuts;
	}

	/**
	 * Ask the algorithm to use routing shortcuts
	 */
	public final DHSSketchAlgorithm setUsingShortcuts() {
		this.usingShortcuts = true;
		return this;
	}

	/**
	 * Ask the algorithm to not use routing shortcuts
	 */
	public final DHSSketchAlgorithm setNotUsingShortcuts() {
		this.usingShortcuts = false;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(sketchAlgorithm.toString());
		sb.append("/" + (usingSequentialBitProbing ? "Sequential" : "BinarySearch"));
		sb.append("/" + (usingIterativeProcessing ? "Iterative" : "Recursive"));
		sb.append("/" + (usingResultCaching ? "Caching" : "NoCaching"));
		sb.append("/" + (usingBatchInsertions ? "Batching" : "NoBatching"));
		sb.append("/" + (usingShortcuts? "Shortcuts" : "NoShortcuts"));
		return sb.toString();
	}
}
