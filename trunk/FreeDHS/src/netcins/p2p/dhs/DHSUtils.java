/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs;

import java.util.BitSet;
import java.util.Hashtable;
import java.util.Vector;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public final class DHSUtils {

	private DHSUtils() {
	}

	public static final boolean allMetricVecsSetToForBit(Hashtable<String, BitSet[]> vecsValue,
		int value, int bit, Vector<String> queryMetrics) {
		BitSet tmpVecsSet = new BitSet();

		for (int metric = 0; metric < queryMetrics.size(); metric++) {
			if (vecsValue.containsKey(queryMetrics.get(metric)) == false)
				continue;
			if (vecsValue.get(queryMetrics.get(metric))[bit].cardinality() == value)
				tmpVecsSet.set(metric);
		}
		return (tmpVecsSet.cardinality() == queryMetrics.size());
	}

	public static final boolean allMetricVecsSetForBit(Hashtable<String, BitSet[]> vecsValue,
		int bit, int numVecs, Vector<String> queryMetrics) {
		return allMetricVecsSetToForBit(vecsValue, numVecs, bit, queryMetrics);
	}

	public static final boolean noMetricVecsSetForBit(Hashtable<String, BitSet[]> vecsValue,
		int bit, Vector<String> queryMetrics) {
		return allMetricVecsSetToForBit(vecsValue, 0, bit, queryMetrics);
	}

	public static final boolean allMetricVecsSetToMoreThanForBit(
		Hashtable<String, BitSet[]> vecsValue, int threshold, int bit, Vector<String> queryMetrics) {
		BitSet tmpVecsSet = new BitSet();

		for (int metric = 0; metric < queryMetrics.size(); metric++) {
			if (vecsValue.containsKey(queryMetrics.get(metric)) == false)
				continue;
			if (vecsValue.get(queryMetrics.get(metric))[bit].cardinality() >= threshold)
				tmpVecsSet.set(metric);
		}
		return (tmpVecsSet.cardinality() == queryMetrics.size());
	}

	public static final boolean allMetricVecsSetToMoreThanForBit(
		Hashtable<String, BitSet[]> vecsValue, int threshold, int bit, int upperBit,
		Vector<String> queryMetrics) {
		BitSet tmpVecsSet = new BitSet();

		for (int metric = 0; metric < queryMetrics.size(); metric++) {
			if (vecsValue.containsKey(queryMetrics.get(metric)) == false)
				continue;
			for (int curBit = bit; curBit < upperBit; curBit++)
				if (vecsValue.get(queryMetrics.get(metric))[curBit].cardinality() >= threshold)
					tmpVecsSet.set(metric);
		}
		return (tmpVecsSet.cardinality() == queryMetrics.size());
	}
}
