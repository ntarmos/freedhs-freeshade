/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Vector;

import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.util.QuickMath;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSMetricsProbeRequestMessage extends DHSRequestMessage {

	public enum BinarySearchProbeMode {
		SEQUENTIAL, BS_UPPER, BS_LOWER
	}

	private static final long					serialVersionUID	= 1480577984149196416L;
	private int									currentBit			= 0;
	private int									lowBit				= 0;
	private int									highBit				= 0;
	private int									lowestNotSetBit		= 0;
	private int									highestAllSetBit	= 0;
	private Hashtable<String, HashSketchImpl>	result				= null;
	private LinkedList<NodeHandle>				path				= null;
	private BitSet								bitsProbed			= null;
	private BitSet								bitsActuallyProbed	= null;
	private BinarySearchProbeMode				mode				= BinarySearchProbeMode.SEQUENTIAL;

	/**
	 * @param data
	 * @param sourceNode
	 * @param storeHops
	 */
	public DHSMetricsProbeRequestMessage(Vector<String> data, NodeHandle sourceNode,
			int currentBit, boolean storeHops) {
		this(data, sourceNode, currentBit, null, null, storeHops);
	}

	public DHSMetricsProbeRequestMessage(Vector<String> data, NodeHandle sourceNode,
			int currentBit, NodeHandle retryNodeCCW, NodeHandle retryNodeCW, boolean storeHops) {
		super(data, sourceNode, storeHops, retryNodeCCW, retryNodeCW);
		this.currentBit = currentBit;
		this.path = new LinkedList<NodeHandle>();
		this.bitsProbed = new BitSet();
		this.bitsActuallyProbed = new BitSet();

		// Create an empty hashtable big enough to hold the results for all
		// metrics without expansion.
		this.result = new Hashtable<String, HashSketchImpl>(data.size() + 1, (float)1.0);
	}

	public final int getCurrentBit() {
		return currentBit;
	}

	public final void setCurrentBit(int currentBit) {
		this.currentBit = currentBit;
	}

	public final Hashtable<String, HashSketchImpl> getResult() {
		return result;
	}

	public final void setResult(Hashtable<String, HashSketchImpl> result) {
		this.result = result;
	}

	public final void addToResult(Hashtable<String, HashSketchImpl> input) {
		if (input == null || input.size() == 0)
			return;
		Enumeration<String> keys = input.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			HashSketchImpl oldHS = result.get(key);
			HashSketchImpl newHS = input.get(key);
			if (oldHS == null) {
				result.put(key, newHS);
			} else {
				oldHS.merge(newHS);
				result.put(key, oldHS);
			}
		}
	}

	public final void addNodeToPath(NodeHandle nh) {
		if (path.size() == 0 || path.contains(nh) == false)
			path.addLast(nh);
	}

	final LinkedList<NodeHandle> getPath() {
		return path;
	}

	public final int getHighBit() {
		return this.highBit;
	}

	public final void setHighBit(int highBit) {
		this.highBit = highBit;
	}

	public final int getLowBit() {
		return this.lowBit;
	}

	public final void setLowBit(int lowBit) {
		this.lowBit = lowBit;
	}

	public final BinarySearchProbeMode getMode() {
		return this.mode;
	}

	public final void setMode(BinarySearchProbeMode mode) {
		this.mode = mode;
	}

	public final int getHighestAllSetBit() {
		return this.highestAllSetBit;
	}

	public final void setHighestAllSetBit(int highestAllSetBit) {
		this.highestAllSetBit = highestAllSetBit;
	}

	public final int getLowestNotSetBit() {
		return this.lowestNotSetBit;
	}

	public final void setLowestNotSetBit(int lowestNotSetBit) {
		this.lowestNotSetBit = lowestNotSetBit;
	}

	public final void addBitProbed(int bit) {
		bitsProbed.set(bit);
	}

	public final boolean hasBitProbed(int bit) {
		return (bitsProbed.get(bit));
	}

	public final int getNumBitsActuallyProbed() {
		return bitsActuallyProbed.cardinality();
	}

	public final void addBitActuallyProbed(int bit) {
		bitsActuallyProbed.set(bit);
	}

	public final boolean allVecsSetForBit(int bit) {
		for (String metric : ((Vector<String>)getDataAsObject())) {
			HashSketchImpl hs = result.get(metric);
			if (hs == null || hs.getNumVecsSetForBit(bit) < hs.getNumVecs()) {
				return false;
			}
		}
		return true;
	}

	public final boolean allVecsSetForBit(int bit, int threshold) {
		for (String metric : ((Vector<String>)getDataAsObject())) {
			HashSketchImpl hs = result.get(metric);
			if (hs == null || hs.getNumVecsSetForBit(bit) < threshold) {
				return false;
			}
		}
		return true;
	}

	public final boolean noVecsSetForBit(int bit) {
		for (String metric : ((Vector<String>)getDataAsObject())) {
			HashSketchImpl hs = result.get(metric);
			if (hs != null && hs.getNumVecsSetForBit(bit) > 0)
				return false;
		}
		return true;
	}

	public final void fixProbedBits(int lowBit, int highBit) {
		for (int bit = lowBit; bit <= highBit; bit++) {
			if (hasBitProbed(bit) == false && allVecsSetForBit(bit) == true) {
				addBitProbed(bit);
			}
		}
	}

	public final int nextBinarySearchBit(int lowerBit, int upperBit) {
		int nextBit = (upperBit < lowerBit ? -1 : lowerBit
				+ (int)QuickMath.floor((double)(upperBit - lowerBit) / 2.0)), iBit = nextBit;
		if (nextBit < 0)
			return -1;

		for (; (nextBit <= upperBit) && hasBitProbed(nextBit); nextBit++) {
		}

		if (nextBit > upperBit) {
			for (nextBit = iBit - 1; (nextBit >= lowerBit) && hasBitProbed(nextBit); nextBit--) {
			}
		}

		return (nextBit >= lowerBit ? nextBit : -1);
	}

	public final int nextSequentialBit(int curBit, int upperBit) {
		int nextBit = curBit, iBit = nextBit;

		for (; (nextBit <= upperBit) && hasBitProbed(nextBit); nextBit++) {
		}

		return (nextBit <= upperBit ? nextBit : -1);
	}
}
