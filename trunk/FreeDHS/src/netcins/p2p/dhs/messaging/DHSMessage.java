/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import java.util.Random;
import java.util.Vector;

import netcins.p2p.dhs.DHSTuple;
import netcins.util.RandomIdSelector;
import rice.pastry.Id;
import rice.pastry.NodeHandle;
import rice.pastry.messaging.Message;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DHSMessage extends Message {
	public static final boolean	defaultStoreHops	= false;
	private static final int	myAddress			= 0x09630234;

	private boolean				storeHops			= DHSMessage.defaultStoreHops;
	protected Object[]			data				= null;
	protected long				msgId				= -1;
	protected Vector<Id>		hops				= null;
	protected int				hopCount			= 0;
	protected Id				targetId			= null;
	protected boolean			oneHopMsg			= false;
	protected NodeHandle		reqNode				= null;
	protected RandomIdSelector	ranges				= null;

	protected DHSMessage(DHSTuple[] data, NodeHandle sourceNode, boolean storeHops) {
		super(myAddress);
		this.setSender(sourceNode);
		setData(data);
		this.msgId = (new Random()).nextLong();
		this.storeHops = storeHops;
	}

	protected DHSMessage(DHSTuple data, NodeHandle sourceNode, boolean storeHops) {
		super(myAddress);
		this.setSender(sourceNode);
		this.data = new DHSTuple[1];
		this.data[0] = data;
		this.msgId = (new Random()).nextLong();
		this.storeHops = storeHops;
	}

	protected DHSMessage(Object data, NodeHandle sourceNode, boolean storeHops) {
		super(myAddress);
		this.setSender(sourceNode);
		this.data = new Object[1];
		this.data[0] = data;
		this.msgId = (new Random()).nextLong();
		this.storeHops = storeHops;
	}

	public static int getMyAddress() {
		return myAddress;
	}

	public int getHopCount() {
		return (storeHops ? (hops != null ? hops.size() : 0) : hopCount);
	}

	public RandomIdSelector getIdRanges() {
		return ranges;
	}

	public void addHop(Id id) {
		if (storeHops) {
			if (hops == null)
				hops = new Vector<Id>(10, 10);
			hops.add(id);
		}
		hopCount++;
	}

	public void addHops(Vector<Id> hops) {
		if (storeHops) {
			if (hops == null || hops.size() == 0)
				return;
			if (this.hops == null)
				this.hops = new Vector<Id>(10, 10);
			this.hops.addAll(hops);
		}
	}

	public void addHops(DHSMessage msg) {
		addHops(msg.getHops());
		this.hopCount += msg.hopCount;
	}

	public void clearHops() {
		hops = null;
		hopCount = 0;
	}

	public boolean getStoreHops() {
		return storeHops;
	}

	public Vector<Id> getHops() {
		return hops;
	}

	public DHSTuple[] getDataAsTupleArray() {
		if (data == null || data.length == 0)
			return new DHSTuple[0];
		DHSTuple[] ret = new DHSTuple[data.length];
		System.arraycopy(data, 0, ret, 0, data.length);
		return ret;
	}

	public Object getDataAsObject() {
		if (data != null && data.length > 0)
			return data[0];
		return null;
	}

	public void setData(DHSTuple[] data) {
		if (data == null || data.length == 0) {
			this.data = null;
			return;
		}
		this.data = new DHSTuple[data.length];
		System.arraycopy(data, 0, this.data, 0, data.length);
	}

	public Long getMsgId() {
		return msgId;
	}

	protected void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public Id getTargetId() {
		return targetId;
	}

	public void setTargetId(Id targetId) {
		this.targetId = targetId;
	}

	public NodeHandle getReqNode() {
		return reqNode;
	}

	public Id getReqNodeId() {
		return reqNode != null ? reqNode.getNodeId() : null;
	}

	public void setReqNode(NodeHandle reqNode) {
		this.reqNode = reqNode;
	}

	@Override
	public String toString() {
		return "[ " + this.getClass().getSimpleName() + "." + msgId + " :: " + getSenderId()
				+ " => " + getTargetId() + " ]";
	}

	public final boolean isOneHopMsg() {
		return this.oneHopMsg;
	}

	public final void setOneHopMsg(boolean isOneHopMsg) {
		this.oneHopMsg = isOneHopMsg;
	}
}
