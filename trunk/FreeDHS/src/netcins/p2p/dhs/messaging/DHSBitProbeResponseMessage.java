/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import java.util.Arrays;
import java.util.HashSet;

import netcins.p2p.dhs.DHSTuple;
import netcins.util.RandomIdSelector;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSBitProbeResponseMessage extends DHSResponseMessage {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -4592031860756380066L;
	NodeHandle					retryNodeCW			= null;
	NodeHandle					retryNodeCCW		= null;
	private boolean				goingCW				= true;
	private boolean				mustRetry			= false;

	public DHSBitProbeResponseMessage(DHSTuple[] data, NodeHandle sourceNode,
		DHSBitProbeRequestMessage reqMsg) {
		super(data, sourceNode, reqMsg);
		if (reqMsg.getIdRanges() != null)
			this.ranges = reqMsg.getIdRanges().copy();
		else
			this.ranges = new RandomIdSelector();
		this.ranges.add(sourceNode.getLocalNode().getLeafSet().range(sourceNode, 0));
	}

	public DHSBitProbeResponseMessage(Object data, NodeHandle sourceNode,
		DHSBitProbeRequestMessage reqMsg) {
		super(data, sourceNode, reqMsg);
		if (reqMsg.getIdRanges() != null)
			this.ranges = reqMsg.getIdRanges().copy();
		else
			this.ranges = new RandomIdSelector();
		this.ranges.add(sourceNode.getLocalNode().getLeafSet().range(sourceNode, 0));
	}

	public DHSBitProbeResponseMessage(DHSTuple[] data, NodeHandle sourceNode,
		DHSBitProbeRequestMessage reqMsg, NodeHandle retryNodeCW, NodeHandle retryNodeCCW) {
		this(data, sourceNode, reqMsg);
		this.retryNodeCW = retryNodeCW;
		this.retryNodeCCW = retryNodeCCW;
		if (this.retryNodeCW != null && ranges != null && ranges.isCovered(this.retryNodeCW))
			this.retryNodeCW = null;
		if (this.retryNodeCCW != null && ranges != null && ranges.isCovered(this.retryNodeCCW))
			this.retryNodeCCW = null;
		this.goingCW = (retryNodeCW != null);
		this.mustRetry = (this.retryNodeCW != null || this.retryNodeCCW != null);
	}

	public DHSBitProbeResponseMessage(Object data, NodeHandle sourceNode,
		DHSBitProbeRequestMessage reqMsg, NodeHandle retryNodeCW, NodeHandle retryNodeCCW) {
		this(data, sourceNode, reqMsg);
		this.retryNodeCW = retryNodeCW;
		this.retryNodeCCW = retryNodeCCW;
		if (this.retryNodeCW != null && ranges != null && ranges.isCovered(this.retryNodeCW))
			this.retryNodeCW = null;
		if (this.retryNodeCCW != null && ranges != null && ranges.isCovered(this.retryNodeCCW))
			this.retryNodeCCW = null;
		this.goingCW = (retryNodeCW != null);
		this.mustRetry = (this.retryNodeCW != null || this.retryNodeCCW != null);
	}

	public void mergeRetryNodes(DHSBitProbeResponseMessage msg) {
		if (msg.ranges != null)
				this.ranges.add(msg.ranges);
		if (goingCW == true && this.retryNodeCW == null)
			this.retryNodeCW = msg.retryNodeCW;
		if (goingCW == false && this.retryNodeCCW == null)
			this.retryNodeCCW = msg.retryNodeCCW;
		if (this.retryNodeCW != null && ranges != null && ranges.isCovered(this.retryNodeCW))
			this.retryNodeCW = null;
		if (this.retryNodeCCW != null && ranges != null && ranges.isCovered(this.retryNodeCCW))
			this.retryNodeCCW = null;
		this.goingCW = (this.goingCW ? (this.retryNodeCW != null) : (this.retryNodeCCW == null
			? this.goingCW : false));
	}

	public boolean hasRetryNode() {
		return (retryNodeCW != null || retryNodeCCW != null);
	}

	public NodeHandle getRetryNode() {
		NodeHandle ret = (goingCW ? retryNodeCW : retryNodeCCW);

		if (ret == null) {
			goingCW = !goingCW;
			ret = (goingCW ? retryNodeCW : retryNodeCCW);
		}

		if (ret != null) {
			if (goingCW)
				retryNodeCW = null;
			else
				retryNodeCCW = null;
		}

		return ret;
	}

	NodeHandle[] getRetryNodes() {
		NodeHandle[] ret = new NodeHandle[2];
		ret[0] = retryNodeCW;
		ret[1] = retryNodeCCW;
		return ret;
	}

	public boolean mustRetry() {
		return mustRetry;
	}

	public void setMustRetry(boolean mustRetry) {
		this.mustRetry = mustRetry;
	}

	public void mergeData(DHSBitProbeResponseMessage imsg) {
		DHSTuple[] iData = (DHSTuple[])imsg.getDataAsTupleArray();
		HashSet<DHSTuple> merged = new HashSet<DHSTuple>();
		if (data != null)
			merged.addAll(Arrays.asList((DHSTuple[])data));
		if (iData != null)
			merged.addAll(Arrays.asList(iData));

		if (merged.size() > 0) {
			data = new DHSTuple[merged.size()];
			merged.toArray((DHSTuple[])data);
		} else {
			data = null;
		}
	}

	@Override
	public String toString() {
		return super.toString() + "[ GCW: " + goingCW + " , retry: " + mustRetry + " , CCW: "
			+ (retryNodeCCW != null ? retryNodeCCW.getNodeId() : "<null>") + " , CW: "
			+ (retryNodeCW != null ? retryNodeCW.getNodeId() : "<null>") + " ]";
	}
}
