/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DHSRequestMessage extends DHSMessage {
	protected NodeHandle	retryNodeCW		= null;
	protected NodeHandle	retryNodeCCW	= null;
	protected boolean		goingCW			= true;
	protected boolean		mustRetry		= false;
	protected int			numRetries		= 0;

	protected DHSRequestMessage(Object data, NodeHandle sourceNode, boolean storeHops) {
		this(data, sourceNode, storeHops, null, null);
	}

	protected DHSRequestMessage(Object data, NodeHandle sourceNode, boolean storeHops,
		NodeHandle retryNodeCCW, NodeHandle retryNodeCW) {
		super(data, sourceNode, storeHops);
		setReqNode(sourceNode);
		this.retryNodeCCW = retryNodeCCW;
		this.retryNodeCW = retryNodeCW;
		setRetryDirection(retryNodeCW, retryNodeCCW);
	}

	protected DHSRequestMessage(DHSTuple[] data, NodeHandle sourceNode, boolean storeHops) {
		this(data, sourceNode, storeHops, null, null);
	}

	protected DHSRequestMessage(DHSTuple[] data, NodeHandle sourceNode, boolean storeHops,
		NodeHandle retryNodeCCW, NodeHandle retryNodeCW) {
		super(data, sourceNode, storeHops);
		setReqNode(sourceNode);
		this.retryNodeCCW = retryNodeCCW;
		this.retryNodeCW = retryNodeCW;
		setRetryDirection(retryNodeCCW, retryNodeCW);
	}

	private void setRetryDirection(NodeHandle retryNodeCCW, NodeHandle retryNodeCW) {
		this.goingCW = (goingCW == true && retryNodeCW != null)
			|| (goingCW == false && retryNodeCCW == null);
		this.mustRetry = (this.retryNodeCW != null || this.retryNodeCCW != null);
	}

	public final int getNumRetries() {
		return this.numRetries;
	}

	public final int incNumRetries() {
		this.numRetries++;
		return this.numRetries;
	}

	public final void clearNumRetries() {
		this.numRetries = 0;
	}

	public final boolean mustRetry() {
		return this.mustRetry;
	}

	public final void setMustRetry(boolean mustRetry) {
		this.mustRetry = mustRetry;
		if (this.mustRetry == false)
			this.numRetries = 0;
	}

	public final void mergeRetryNodes(DHSRequestMessage msg) {
		if (goingCW == true && this.retryNodeCW == null)
			this.retryNodeCW = msg.retryNodeCW;
		if (goingCW == false && this.retryNodeCCW == null)
			this.retryNodeCCW = msg.retryNodeCCW;
		this.goingCW = (this.goingCW ? (this.retryNodeCW != null) : (this.retryNodeCCW == null
			? this.goingCW : false));
	}

	private final NodeHandle mergeNodeHandle(NodeHandle nh) {
		return (nh == null || nh.equals(this.getSender()) || nh.getNodeId().equals(this
			.getTargetId())) ? null : nh;
	}

	public final void mergeRetryNodes(NodeHandle[] nodes) {
		if (numRetries == 0) {
			this.retryNodeCCW = mergeNodeHandle(nodes[0]);
			this.retryNodeCW = mergeNodeHandle(nodes[1]);
			this.goingCW = (this.retryNodeCW != null);
			return;
		}
		if (goingCW == false && this.retryNodeCCW == null)
			this.retryNodeCCW = mergeNodeHandle(nodes[0]);
		if (goingCW == true && this.retryNodeCW == null)
			this.retryNodeCW = mergeNodeHandle(nodes[1]);
		this.goingCW = (this.goingCW ? (this.retryNodeCW != null) : (this.retryNodeCCW == null
			? this.goingCW : false));
	}

	public final boolean hasRetryNode() {
		return (retryNodeCW != null || retryNodeCCW != null);
	}

	public final NodeHandle getRetryNode() {
		NodeHandle ret = (goingCW ? retryNodeCW : retryNodeCCW);

		if (ret == null) {
			goingCW = !goingCW;
			ret = (goingCW ? retryNodeCW : retryNodeCCW);
		}

		if (ret != null) {
			if (goingCW)
				retryNodeCW = null;
			else
				retryNodeCCW = null;
		}

		//System.err.println("Retrying to node " + (ret != null ? ret.getNodeId().toString() : null));

		return ret;
	}
}
