/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import java.util.Hashtable;
import java.util.LinkedList;

import netcins.dbms.centralized.sketches.HashSketchImpl;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSMetricsProbeResponseMessage extends DHSResponseMessage {

	private static final long		serialVersionUID	= 3435918036326847408L;

	private int						numBitsProbed		= 0;
	private LinkedList<NodeHandle>	path				= null;

	/**
	 * @param data
	 * @param sourceNode
	 * @param reqMsg
	 */
	public DHSMetricsProbeResponseMessage(Hashtable<String, HashSketchImpl> data,
			NodeHandle sourceNode, DHSMetricsProbeRequestMessage reqMsg, int numBitsProbed) {
		super(data, sourceNode, reqMsg);
		this.numBitsProbed = numBitsProbed;
		this.path = new LinkedList<NodeHandle>();
		this.path.addAll(reqMsg.getPath());
	}

	public final int getNumBitsProbed() {
		return this.numBitsProbed;
	}

	public final NodeHandle getNextNodeInPath() {
		if (path != null)
			return path.removeLast();
		return null;
	}

	public final boolean hasMoreNodesInPath() {
		if (path == null)
			return false;
		return (path.size() > 0);
	}
}
