/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import netcins.util.RandomIdSelector;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSBitProbeRequestMessage extends DHSRequestMessage {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3031373155152500132L;

	public DHSBitProbeRequestMessage(Object data, NodeHandle sourceNode, boolean storeHops) {
		super(data, sourceNode, storeHops);
		this.ranges = new RandomIdSelector();
	}

	public DHSBitProbeRequestMessage(DHSTuple[] data, NodeHandle sourceNode, boolean storeHops) {
		super(data, sourceNode, storeHops);
		this.ranges = new RandomIdSelector();
	}
}
