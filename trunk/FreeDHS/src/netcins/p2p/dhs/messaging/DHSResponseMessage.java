/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DHSResponseMessage extends DHSMessage {
	private Long	reqMsgId	= null;

	protected DHSResponseMessage(DHSTuple[] data, NodeHandle sourceNode, DHSRequestMessage reqMsg) {
		super(data, sourceNode, reqMsg.getStoreHops());
		this.reqMsgId = reqMsg.getMsgId();
		this.hopCount = reqMsg.hopCount;
		this.reqNode = reqMsg.reqNode;
		super.addHops(reqMsg.getHops());
	}

	protected DHSResponseMessage(Object data, NodeHandle sourceNode, DHSRequestMessage reqMsg) {
		super(data, sourceNode, reqMsg.getStoreHops());
		this.reqMsgId = reqMsg.getMsgId();
		this.hopCount = reqMsg.hopCount;
		this.reqNode = reqMsg.reqNode;
		super.addHops(reqMsg.getHops());
	}

	public Long getReqMsgId() {
		return reqMsgId;
	}

	@Override
	public String toString() {
		return "[ " + this.getClass().getSimpleName() + "." + /* msgId + "/" + */reqMsgId + " :: "
				+ getSenderId() + " => " + getTargetId() + " ]";
	}
}
