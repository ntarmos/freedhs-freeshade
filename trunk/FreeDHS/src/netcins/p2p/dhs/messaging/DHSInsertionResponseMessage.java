/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSInsertionResponseMessage extends DHSResponseMessage {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -1714809481776585036L;

	public DHSInsertionResponseMessage(DHSTuple[] data, NodeHandle sourceNode,
			DHSInsertionRequestMessage rmsg) {
		super(data, sourceNode, rmsg);
	}
}
