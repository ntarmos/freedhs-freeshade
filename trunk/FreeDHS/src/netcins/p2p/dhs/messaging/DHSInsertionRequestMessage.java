/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.messaging;

import netcins.p2p.dhs.DHSTuple;
import rice.pastry.NodeHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSInsertionRequestMessage extends DHSRequestMessage {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3630456659859343099L;

	public DHSInsertionRequestMessage(Object data, NodeHandle sourceNode, boolean storeHops) {
		super(data, sourceNode, storeHops);
	}

	public DHSInsertionRequestMessage(DHSTuple[] data, NodeHandle sourceNode, boolean storeHops) {
		super(data, sourceNode, storeHops);
	}

	public DHSInsertionRequestMessage(DHSInsertionRequestMessage imsg) {
		super(imsg.getDataAsTupleArray(), imsg.getSender(), imsg.getStoreHops());
		this.setMsgId(imsg.getMsgId());
		this.setReqNode(imsg.getReqNode());
		this.addHops(imsg);
	}

	@Override
	public String toString() {
		return super.toString() + "[ vec: " + ((DHSTuple[])data)[0].getBit() + " , bit: "
				+ ((DHSTuple[])data)[0].getBit() + " ]";
	}
}
