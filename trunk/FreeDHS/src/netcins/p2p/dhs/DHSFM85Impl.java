/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.util.RandomIdSelector;
import rice.environment.Environment;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
final class DHSFM85Impl extends DHSImpl {

	protected DHSFM85Impl(DHSSketchAlgorithm sketchAlgo, int numVecs, String hashAlgo, int L,
		int retries, int replicas, Vector<String> metrics, Environment env)
		throws NoSuchAlgorithmException {
		super(sketchAlgo.setUsingFM85(), numVecs, hashAlgo, L, retries, replicas, metrics, env);
	}

	protected DHSFM85Impl(DHSSketchAlgorithm sketchAlgo, int numVecs, String hashAlgo, int L,
		int retries, int replicas, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, replicas, null, env);
	}

	protected DHSFM85Impl(DHSSketchAlgorithm sketchAlgo, int numVecs, int L, int retries,
		int replicas, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, replicas, env);
	}

	@Override
	protected final void insertBinarySearch(PastryAppl sourceNode, String metric,
		ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	@Override
	protected final void insertBinarySearch(PastryAppl sourceNode, String metric,
		Vector<ItemSerializable> objects) {
		basicInsert(sourceNode, metric, objects);
	}

	@Override
	protected final void insertClassic(PastryAppl sourceNode, String metric, ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	@Override
	protected final void insertClassic(PastryAppl sourceNode, String metric,
		Vector<ItemSerializable> objects) {
		basicInsert(sourceNode, metric, objects);
	}

	@Override
	protected final DistributedEstimationResult[] estimatePartial(PastryAppl sourceNode,
		Vector<String> metrics, int numBits, Hashtable<String, BitSet[]> vecsValue, int hopCount,
		HashSet<Integer> bitsProbed, RandomIdSelector idRanges) throws NoSuchMetricException {

		if (bitsProbed == null) {
			bitsProbed = new HashSet<Integer>();
		}

		int bit;
		for (bit = 0; bit < numBits; bit++) {
			if (bitsProbed.contains(bit)) {
				if (DHSUtils.noMetricVecsSetForBit(vecsValue, bit, metrics) == true) {
					break;
				} else {
					continue;
				}
			}

			DHSBitProbeResult response = probe((DHSPastryAppl)sourceNode, metrics, bit, idRanges);
			bitsProbed.add(bit);
			idRanges.add(response.range);
			hopCount += response.hopCount;

			if (response.resultData != null && response.resultData.length > 0) {
				for (int item = 0; item < response.resultData.length; item++) {
					DHSTuple dhsTuple = response.resultData[item];
					setBitForMetric(dhsTuple.getMetric(), dhsTuple.getVec(), dhsTuple.getBit());
					vecsValue.get(dhsTuple.getMetric())[dhsTuple.getBit()].set(dhsTuple.getVec());
				}
				if (DHSUtils.noMetricVecsSetForBit(vecsValue, bit, metrics) == true) {
					break;
				}
			} else {
				break;
			}

		}
		DistributedEstimationResult[] ret = new DistributedEstimationResult[metrics.size()];
		for (int i = 0; i < ret.length; i++) {
			try {
				ret[i] = new DistributedEstimationResult(metrics.get(i), estimate(metrics.get(i)),
					hopCount, bitsProbed.size());
			} catch (NoSuchMetricException e) {
				e.printStackTrace();
			}
		}

		return ret;
	}
}
