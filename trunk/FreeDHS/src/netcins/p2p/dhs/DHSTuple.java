/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs;

import java.io.IOException;

import netcins.ItemSerializable;
import netcins.util.DataSerializer;
import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSTuple implements ItemSerializable, Comparable<DHSTuple> {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1454641264771208352L;
	private String				metric				= null;
	private int					bit					= 0;
	private int					vec					= 0;
	private long				timestamp			= 0;

	public DHSTuple(String metric, int vec, int bit, long timestamp) {
		this.metric = metric;
		this.bit = bit;
		this.vec = vec;
		this.timestamp = timestamp;
	}

	public DHSTuple(String metric, int vec, int bit) {
		this(metric, vec, bit, System.currentTimeMillis());
	}

	public DHSTuple(InputBuffer buf) throws IOException {
		this.deserialize(buf);
	}

	public void serialize(OutputBuffer buf) throws IOException {
		buf.writeUTF(metric);
		buf.writeInt(vec);
		buf.writeInt(bit);
		buf.writeLong(timestamp);
	}

	public void deserialize(InputBuffer buf) throws IOException {
		metric = buf.readUTF();
		vec = buf.readByte();
		bit = buf.readByte();
		timestamp = buf.readLong();
	}

	public byte[] serialize() {
		int offset = metric.length() * Character.SIZE / 8;
		byte[] ret = new byte[offset + (2 * Integer.SIZE + Long.SIZE) / 8];
		DataSerializer.serializeString(metric, ret, 0);
		DataSerializer.serializeInt(vec, ret, offset);
		offset += Integer.SIZE / 8;
		DataSerializer.serializeInt(bit, ret, offset);
		offset += Integer.SIZE / 8;
		DataSerializer.serializeLong(timestamp, ret, offset);
		return ret;
	}

	public String getMetric() {
		return metric;
	}

	public int getBit() {
		return bit;
	}

	public int getVec() {
		return vec;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "[ " + metric + " :: " + vec + " : " + bit + " ]";
	}

	public int compareTo(DHSTuple o) {
		int metricCompare = (metric != null && o.metric != null ? metric.compareTo(o.metric) : 0);
		if (metricCompare != 0)
			return metricCompare;
		if (vec != o.vec)
			return vec - o.vec;
		if (bit != o.bit)
			return bit - o.bit;
		return 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final DHSTuple other = (DHSTuple)obj;
		if (this.metric == null) {
			if (other.metric != null)
				return false;
		} else if (!this.metric.equals(other.metric))
			return false;
		if (this.bit != other.bit)
			return false;
		if (this.vec != other.vec)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + this.bit;
		result = PRIME * result + ((this.metric == null) ? 0 : this.metric.hashCode());
		result = PRIME * result + this.vec;
		return result;
	}

	/* (non-Javadoc)
	 * @see netcins.ItemSerializable#deserialize(byte[])
	 */
	public ItemSerializable deserialize(byte[] buf) throws IOException {
		throw new IOException("Unsupported operation");
	}

}
