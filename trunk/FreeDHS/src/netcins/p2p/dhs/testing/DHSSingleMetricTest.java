/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs.testing;

import java.security.NoSuchAlgorithmException;
import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.DF03SketchImpl;
import netcins.dbms.centralized.sketches.FM85SketchImpl;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithm;
import netcins.p2p.testing.DHSGenericTest;
import netcins.p2p.testing.DHSGenericTestParams;
import netcins.util.QuickMath;
import netcins.util.stats.ZipfGenerator;
import rice.environment.Environment;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSSingleMetricTest extends DHSGenericTest {
	private static final String	metric	= "Single Metric";
	private HashSketchImpl		centralizedHashSketch;

	public DHSSingleMetricTest(DHSSketchAlgorithm algorithm, int numVecs, int l, int retries,
			int replicas, Environment env) {
		super(algorithm, numVecs, l, retries, replicas, env);
		// Using the dhsImpl object in the parent class, so no more initializers here...
	}

	public static void main(String[] args) {
		DHSGenericTestParams testParams = new DHSGenericTestParams(DHSSingleMetricTest.class.getCanonicalName(), args);

		long startTime = System.currentTimeMillis(), endTime = 0;

		DHSSingleMetricTest test = null;
		int seed = (int)QuickMath.floor(Math.random() * (double)Integer.MAX_VALUE);
		// int seed = 1725674223;

		System.out.println("\n" + " --- Begin Simulation --- ");
		ZipfGenerator zipf = new ZipfGenerator(testParams.numValues, testParams.theta, seed);
		int[] data = new int[testParams.numTuples];
		for (int i = 0; i < testParams.numTuples; i++)
			data[i] = zipf.nextRank();

		DHSSketchAlgorithm testAlgo = testParams.algorithm;

		System.out.println(" --- Seed: " + seed + ", domain: " + testParams.numValues
				+ ", Zipf theta: " + testParams.theta + " ---");
		try {
			Environment env = Environment.directEnvironment(seed);
			test = new DHSSingleMetricTest(testAlgo, testParams.numVecs, testParams.L,
					testParams.retries, testParams.replicas, env);
			test.addNodes(testParams.numNodes);
			System.out.println("\n" + " ---" + "\n");
			test.clear();
			if (testAlgo.isUsingBatchInsertions())
				test.addDataBatch(data);
			else
				test.addData(data);

			System.out.println("\n" + " ---" + "\n");

			for (int iter = 0; iter < testParams.iterations; iter++) {
				test.setQNode(env.getRandomSource().nextInt(testParams.numNodes));
				System.out.println("Testing " + testAlgo + ", iteration: " + iter
						+ ", querying node # " + test.getQNode() + " ...");
				test.clearTransientData();
				test.query();
				System.out.println("\n" + " ---" + "\n");
			}
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: " + (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsBatch(netcins.p2p.dhs.DHSPastryAppl, java.util.Vector)
	 */
	@Override
	protected void addItemsBatch(DHSPastryAppl curAppl, Vector<BasicItem> tuples) {
		for (BasicItem tuple : tuples)
			dhsImpl.batchInsert(curAppl, DHSSingleMetricTest.metric, tuple);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsNormal(netcins.p2p.dhs.DHSPastryAppl, java.util.Vector)
	 */
	@Override
	protected void addItemsNormal(DHSPastryAppl curAppl, Vector<BasicItem> tuples) {
		for (BasicItem tuple : tuples)
			dhsImpl.insert(curAppl, DHSSingleMetricTest.metric, tuple);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsCentralized(java.util.Vector)
	 */
	@Override
	protected void addItemsCentralized(Vector<BasicItem> tuples) {
		for (BasicItem item : tuples)
			centralizedHashSketch.insert(item);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#clearLocalEstimator()
	 */
	@Override
	public void clearLocalEstimator() {
		centralizedHashSketch.clear();
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#estimateCentralizedEstimator()
	 */
	@Override
	protected double estimateCentralizedEstimator() {
		return centralizedHashSketch.estimate();
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#estimateDistributedEstimator()
	 */
	@Override
	protected DistributedEstimationResult estimateDistributedEstimator(DHSPastryAppl curAppl) {
		Vector<String> metrics = new Vector<String>();
		metrics.add(DHSSingleMetricTest.metric);

		DistributedEstimationResult[] distRes = null;
		try {
			distRes = dhsImpl.estimate(curAppl, metrics);
			System.out.println(dhsImpl + ", distributed estimation: " + distRes[0]);
		} catch (NoSuchMetricException e) {
			e.printStackTrace();
		}
		return distRes[0];
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#initLocalEstimator(netcins.p2p.dhs.DHSSketchAlgorithm, int, int)
	 */
	@Override
	protected void initLocalEstimator(DHSSketchAlgorithm algorithm, int numVecs, int L) {
		try {
			centralizedHashSketch = (algorithm.isUsingDF03() ? new DF03SketchImpl(numVecs, L)
					: new FM85SketchImpl(numVecs, L));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
}
