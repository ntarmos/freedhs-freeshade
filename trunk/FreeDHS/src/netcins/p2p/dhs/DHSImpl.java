/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.DF03SketchImpl;
import netcins.dbms.centralized.sketches.FM85SketchImpl;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.DistributedSynopsis;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.p2p.dhs.messaging.DHSBitProbeResponseMessage;
import netcins.util.QuickMath;
import netcins.util.RandomIdSelector;
import rice.environment.Environment;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.IdRange;
import rice.pastry.NodeHandle;
import rice.pastry.NodeSet;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DHSImpl extends DistributedSynopsis {

	/** Default number of bitmap vectors in the DHSImpl implementation */
	public static final int					defaultNumVecs		= 256;
	/** Default number of retries in the DHSImpl implementation */
	public static final int					defaultRetries		= 5;
	/** Default number of replicas in the DHSImpl implementation */
	public static final int					defaultReplicas		= 5;
	/** Default Hash Sketch algorithm used by the DHSImpl instance */
	public static final DHSSketchAlgorithm	defaultSketchAlgo	= new DHSSketchAlgorithm();

	private static final int				intsPerId			= Id.IdBitLength / 32;

	/* DHS parameters */
	private int								L					= -1;
	private int								numVecs				= -1;
	private int								retries				= 0;
	private int								replicas			= 0;
	private String							hashAlgo			= null;
	private DHSSketchAlgorithm				sketchAlgo			= null;
	private Environment						env					= null;

	/* Internal DHS state */
	private HashSketchImpl					baseHashSketchImpl	= null;
	private Vector<HashSketchImpl>			distributedHS		= null;

	// /////////////////////
	// Protected methods //
	// /////////////////////

	protected final int[] getBitsForObject(ItemSerializable object) {
		return (int[])baseHashSketchImpl.tryAddObject(object);
	}

	protected final boolean setBitForMetric(String metric, int vec, int bit)
		throws NoSuchMetricException {
		int index = -1;
		if (distributedHS == null || (index = getIndexForMetric(metric)) < 0)
			throw new NoSuchMetricException();
		distributedHS.get(index).setBit(vec, bit);
		return true;
	}

	protected final boolean getBitForMetric(String metric, int vec, int bit)
		throws NoSuchMetricException {
		int index = -1;
		if (distributedHS == null || (index = getIndexForMetric(metric)) < 0)
			throw new NoSuchMetricException();
		return distributedHS.get(index).getBit(vec, bit);
	}

	protected final DHSMetricProbeResult probeRecursive(DHSPastryAppl sourceNode,
		Vector<String> metrics) {
		long msgId = sourceNode.query(metrics);
		sourceNode.waitForMetricProbeQueryToComplete(msgId);

		int numHops = sourceNode.getQueryHopCount(msgId);
		assert (numHops >= 0);
		int lastBitProbed = sourceNode.getNumBitsProbed(msgId);

		// DEBUG: print intermediate hops, as stored in query packet...
		Logger logger = sourceNode.getLogger();
		if (logger.level <= Logger.FINE) {
			Vector<Id> hops = sourceNode.getQueryHops(msgId);
			StringBuilder sb = new StringBuilder();
			if (hops != null && hops.size() > 0) {
				sb.append("\t" + hops.get(0));
				for (int hop = 1; hop < hops.size(); hop++)
					sb.append(" => " + hops.get(hop));
				sb.append("\n");
			}
			logger.log("Query " + msgId + ": " + numHops + " hops :: " + sb.toString());
		}

		Object resultObject = sourceNode.removeResponseToMetricQuery(msgId);
		Hashtable<String, HashSketchImpl> resultData = (resultObject != null
			? (Hashtable<String, HashSketchImpl>)resultObject : null);
		return new DHSMetricProbeResult(resultData, numHops, lastBitProbed);
	}

	protected final DHSBitProbeResult probe(DHSPastryAppl sourceNode, Vector<String> metrics,
		int bit, RandomIdSelector idRanges) {
		DHSTuple[] queryTuples = new DHSTuple[metrics.size()];
		for (int i = 0; i < queryTuples.length; i++)
			queryTuples[i] = new DHSTuple(metrics.get(i), -1, bit);
		
		Id targetId = mapToNodeId(sourceNode, bit, idRanges);
		if (targetId == null) {
			return new DHSBitProbeResult(null, 0);
		}
		long msgId = sourceNode.query(targetId, queryTuples);
		// sourceNode.waitForQueryToComplete(msgId);
		sourceNode.waitForQueriesToComplete();

		int numHops = sourceNode.getQueryHopCount(msgId);
		assert (numHops >= 0);

		// DEBUG: print intermediate hops, as stored in query packet...
		Logger logger = sourceNode.getLogger();
		if (logger.level <= Logger.FINE) {
			Vector<Id> hops = sourceNode.getQueryHops(msgId);
			StringBuilder sb = new StringBuilder();
			if (hops != null && hops.size() > 0) {
				sb.append("\t" + hops.get(0));
				for (int hop = 1; hop < hops.size(); hop++)
					sb.append(" => " + hops.get(hop));
				sb.append("\n");
			}
			logger.log("Query " + msgId + ": " + numHops + " hops :: " + sb.toString());
		}

		return new DHSBitProbeResult(sourceNode.removeResponseToBitQuery(msgId), numHops);
	}

	protected DHSImpl(DHSSketchAlgorithm sketchAlgo, int numVecs, String hashAlgo, int L,
		int retries, int replicas, Vector<String> dhsMetrics, Environment env)
		throws NoSuchAlgorithmException {
		this.L = L;
		this.hashAlgo = hashAlgo;
		this.sketchAlgo = sketchAlgo;
		this.numVecs = numVecs;
		this.retries = retries;
		this.replicas = replicas;
		this.baseHashSketchImpl = (sketchAlgo.isUsingDF03() ? new DF03SketchImpl(numVecs, L,
			hashAlgo) : new FM85SketchImpl(numVecs, L, hashAlgo));
		this.distributedHS = new Vector<HashSketchImpl>(10, 2);
		addDHSMetrics(dhsMetrics);
		this.env = env;
		if (sketchAlgo.isUsingDF03())
			assert (this instanceof DHSDF03Impl);
		else
			assert (this instanceof DHSFM85Impl);
	}

	protected DHSImpl(DHSSketchAlgorithm sketchAlgo, int numVecs, String hashAlgo, int L,
		int retries, int replicas, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, replicas, null, env);
	}

	protected DHSImpl(DHSSketchAlgorithm sketchAlgo, int numVecs, int L, int retries, int replicas,
		Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, replicas, env);
	}

	protected final double estimate(String metric) throws NoSuchMetricException {
		int index = -1;
		if (distributedHS == null || (index = getIndexForMetric(metric)) < 0)
			throw new NoSuchMetricException();
		return distributedHS.get(index).estimate();
	}

	public final boolean nodeIsInArc(NodeHandle nh, int l) {
		return (!nh.getLocalNode().getLeafSet().range(nh, 0).intersect(getIdRangeForBit(l))
			.isEmpty());
	}

	public final Id mapToNodeId(DHSPastryAppl sourceNode, int l) {
		return mapToNodeId(sourceNode, l, null);
	}

	public final Id mapToNodeId(DHSPastryAppl sourceNode, int l, RandomIdSelector idRanges) {
		NodeHandle nh = mapToNode(sourceNode, l, idRanges);
		//return (nh != null ? nh.getNodeId() : getRandomIdForBit(l, idRanges));
		if (nh != null)
			return nh.getNodeId();
		if (idRanges != null) {
			IdRange idRange = new IdRange(Id.build(thr(l)), Id.build(thr(l - 1)));
			Id ret = idRanges.randomId(idRange, env.getRandomSource());
			//if (ret != null)
			return ret;
		}
		return getRandomIdForBit(l);
	}

	public final HashSketchImpl getBaseHashSketchImpl() {
		return baseHashSketchImpl;
	}

	protected final NodeHandle mapToNode(DHSPastryAppl sourceNode, int l, RandomIdSelector idRanges) {
		if (sketchAlgo.isUsingShortcuts() == false || sourceNode == null)
			return null;

		IdRange idRange = new IdRange(Id.build(thr(l)), Id.build(thr(l - 1)));

		LinkedList<NodeHandle> nhList = new LinkedList<NodeHandle>();

		nhList.add(sourceNode.getNodeHandle());
		NodeSet ns = sourceNode.getLeafSet().neighborSet(Integer.MAX_VALUE);
		for (NodeHandle nh : (List<NodeHandle>)sourceNode.getRoutingTable().asList())
			if (nhList.contains(nh) == false)
				nhList.add(nh);
		for (Iterator<NodeHandle> nsi = ns.getIterator(); nsi.hasNext();) {
			NodeHandle nh = nsi.next();
			if (nhList.contains(nh) == false)
				nhList.add(nh);
		}
		for (int curnh = 0; curnh < nhList.size();) {
			if (!nodeIsInArc(nhList.get(curnh), l))
				nhList.remove(curnh);
			else
				curnh++;
		}
		if (idRanges != null) {
			for (int i = 0; i < nhList.size(); ) {
				IdRange curNodeRange = nhList.get(i).getLocalNode().getLeafSet().range(nhList.get(i), 0);
				if (idRanges.isCovered(curNodeRange))
					nhList.remove(i);
				else
					i++;
			}
		}
		return (nhList.size() > 0 ? nhList.get(env.getRandomSource().nextInt(nhList.size())) : null);
	}

	protected final void basicInsert(PastryAppl sourceNode, String metric, ItemSerializable object) {
		Vector<ItemSerializable> objects = new Vector<ItemSerializable>();
		objects.add(object);
		basicInsert(sourceNode, metric, objects);
	}

	protected final void basicInsert(PastryAppl sourceNode, String metric,
		Vector<ItemSerializable> objects) {
		DHSTuple tupleData[] = new DHSTuple[objects.size()];
		for (int i = 0; i < objects.size(); i++) {
			int[] bits = (int[])baseHashSketchImpl.tryAddObject(objects.get(i));
			tupleData[i] = new DHSTuple(metric, bits[0], bits[1]);
		}
		long msgIds = ((DHSPastryAppl)sourceNode).insert(tupleData);
	}

	// //////////////////
	// Public methods //
	// //////////////////

	public static final HashSketchImpl newBaseSketchInstance(DHSImpl dhsImpl) {
		try {
			if (dhsImpl.getSketchAlgo().isUsingDF03())
				return new DF03SketchImpl(dhsImpl.baseHashSketchImpl);
			else
				return new FM85SketchImpl(dhsImpl.baseHashSketchImpl);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static final DHSImpl newInstance(DHSImpl dhsImpl) {
		try {
			if (dhsImpl.getSketchAlgo().isUsingDF03())
				return new DHSDF03Impl(dhsImpl.sketchAlgo, dhsImpl.numVecs, dhsImpl.hashAlgo,
					dhsImpl.L, dhsImpl.retries, dhsImpl.replicas, dhsImpl.env);
			else
				return new DHSFM85Impl(dhsImpl.sketchAlgo, dhsImpl.numVecs, dhsImpl.hashAlgo,
					dhsImpl.L, dhsImpl.retries, dhsImpl.replicas, dhsImpl.env);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static final DHSImpl newInstance(DHSSketchAlgorithm sketchAlgo, int numVecs,
		String hashAlgo, int L, int retries, int replicas, Environment env)
		throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isUsingDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs, hashAlgo, L, retries, replicas, env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs, hashAlgo, L, retries, replicas, env);
	}

	public static final DHSImpl newInstance(DHSSketchAlgorithm sketchAlgo, int numVecs, int L,
		int retries, int replicas, Environment env) throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isUsingDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs, L, retries, replicas, env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs, L, retries, replicas, env);
	}

	public static final DHSImpl newInstance(DHSSketchAlgorithm sketchAlgo, int numVecs, int L,
		int retries, int replicas, Vector<String> metrics, Environment env)
		throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isUsingDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries,
				replicas, metrics, env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries,
				replicas, metrics, env);
	}

	public static final DHSImpl newInstance(DHSSketchAlgorithm sketchAlgo, int numVecs,
		String hashAlgo, int L, int retries, int replicas, Vector<String> dhsMetrics,
		Environment env) throws NoSuchAlgorithmException {
		if (sketchAlgo == null)
			return null;
		if (sketchAlgo.isUsingDF03())
			return new DHSDF03Impl(sketchAlgo, numVecs, hashAlgo, L, retries, replicas, dhsMetrics,
				env);
		else
			return new DHSFM85Impl(sketchAlgo, numVecs, hashAlgo, L, retries, replicas, dhsMetrics,
				env);
	}

	public final void clearHS() {
		if (distributedHS != null)
			for (HashSketchImpl hs : distributedHS)
				hs.clear();
	}

	public final void clear() {
		clearHS();
	}

	public final int getNumPendingInsertions(DHSPastryAppl sourceNode) {
		return sourceNode.getNumPendingInsertions();
	}

	public final int getNumVecs() {
		return numVecs;
	}

	public final int getNumRetries() {
		return retries;
	}

	public final int getNumReplicas() {
		return replicas;
	}

	public String toString() {
		return "[ DHSImpl :: algo: " + sketchAlgo + " , HS: " + baseHashSketchImpl + " , retries: "
			+ retries + ", replicas: " + replicas + " ]";
	}

	public final String getHashAlgo() {
		return hashAlgo;
	}

	public final int getNumBits() {
		return L;
	}

	public final void addDHSMetrics(Vector<String> dhsMetrics) {
		if (dhsMetrics == null)
			return;
		for (String s : dhsMetrics) {
			int index = -1;
			if ((index = addDSMetric(s)) >= 0) {
				try {
					this.distributedHS.add(index, sketchAlgo.isUsingDF03() ? new DF03SketchImpl(
						baseHashSketchImpl) : new FM85SketchImpl(baseHashSketchImpl));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public final void batchInsert(DHSPastryAppl sourceNode, String metric, ItemSerializable object) {
		Vector<String> metrics = new Vector<String>();
		metrics.add(metric);
		addDHSMetrics(metrics);
		sourceNode.batchInsert(metric, object);
	}

	public final void doBatch(DHSPastryAppl sourceNode) {
		sourceNode.doBatch();
	}

	public final void insert(PastryAppl sourceNode, String metric, Vector<ItemSerializable> objects) {
		Vector<String> metrics = new Vector<String>();
		metrics.add(metric);
		addDHSMetrics(metrics);
		if (sketchAlgo.isUsingBinarySearchBitProbing())
			insertBinarySearch(sourceNode, metric, objects);
		else
			insertClassic(sourceNode, metric, objects);
	}

	public final void insert(PastryAppl sourceNode, String metric, ItemSerializable object) {
		Vector<ItemSerializable> objects = new Vector<ItemSerializable>();
		objects.add(object);
		insert(sourceNode, metric, objects);
	}

	public final DistributedEstimationResult[] estimate(PastryAppl srcNode, Vector<String> metrics)
		throws NoSuchMetricException {
		DHSPastryAppl sourceNode = (DHSPastryAppl)srcNode;
		DistributedEstimationResult[] ret = null;
		Logger logger = sourceNode.getLogger();

		if (sketchAlgo.isUsingIterativeProcessing()) {
			if (sketchAlgo.isUsingBinarySearchBitProbing())
				ret = estimateBinarySearch(sourceNode, metrics);
			else
				ret = estimateClassic(sourceNode, metrics);

			if (logger.level <= Logger.FINE) {
				for (HashSketchImpl hs : distributedHS)
					logger.log(sketchAlgo + " distributed HS :: " + "\n" + hs + "\n"
						+ hs.toStringFull());
			}
		} else {
			ret = estimateRecursive(sourceNode, metrics);
		}

		return ret;
	}

	public final String getSketchAlgoName() {
		return sketchAlgo.toString();
	}

	public final DHSSketchAlgorithm getSketchAlgo() {
		return sketchAlgo;
	}

	// ////////////////////
	// Abstract methods //
	// ////////////////////

	protected abstract void insertClassic(PastryAppl sourceNode, String metric,
		ItemSerializable item);

	protected abstract void insertClassic(PastryAppl sourceNode, String metric,
		Vector<ItemSerializable> items);

	protected abstract void insertBinarySearch(PastryAppl sourceNode, String metric,
		ItemSerializable item);

	protected abstract void insertBinarySearch(PastryAppl sourceNode, String metric,
		Vector<ItemSerializable> item);

	protected abstract DistributedEstimationResult[] estimatePartial(PastryAppl sourceNode,
		Vector<String> metrics, int numBits, Hashtable<String, BitSet[]> vecsValue, int hopCount,
		HashSet<Integer> bitsProbed, RandomIdSelector idRanges) throws NoSuchMetricException;

	// ///////////////////
	// Private methods //
	// ///////////////////

	private DistributedEstimationResult[] estimateBinarySearch(DHSPastryAppl sourceNode,
		Vector<String> metrics) throws NoSuchMetricException {
		Hashtable<String, BitSet[]> vecsValue = new Hashtable<String, BitSet[]>();
		RandomIdSelector idRanges = new RandomIdSelector();
		for (int metric = 0; metric < metrics.size(); metric++) {
			BitSet[] val = new BitSet[L];
			for (int bit = 0; bit < L; bit++)
				val[bit] = new BitSet();
			vecsValue.put(metrics.get(metric), val);
		}
		int hopCount = 0;
		HashSet<Integer> bitsProbed = new HashSet<Integer>();

		for (HashSketchImpl h : distributedHS)
			h.clear();

		// Binary search for the lowest bit position for which all vectors have
		// a 0-bit...
		int bitLow = 0, bitHigh = L;
		// System.out.println("Searching for lowest all-set-to-0 bit starting from bit "
		//		+ sketchAlgo.nextBit(0, bitLow, bitHigh));
		for (int curBit = sketchAlgo.nextBit(0, bitLow, bitHigh); bitLow < bitHigh; curBit = sketchAlgo
			.nextBit(curBit, bitLow, bitHigh)) {
			assert (bitsProbed.contains(curBit) == false);

			if (DHSUtils.allMetricVecsSetForBit(vecsValue, curBit, numVecs, metrics) == true) {
				bitLow = curBit;
				continue;
			}

			DHSBitProbeResult response = probe(sourceNode, metrics, (int)curBit, idRanges);
			bitsProbed.add(curBit);
			idRanges.add(response.range);
			hopCount += response.hopCount;

			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item++) {
					DHSTuple dhsTuple = response.resultData[item];
					int metricIndex = getIndexForMetric(dhsTuple.getMetric());
					distributedHS.get(metricIndex).setBit(dhsTuple.getVec(), dhsTuple.getBit());
					vecsValue.get(dhsTuple.getMetric())[dhsTuple.getBit()].set(dhsTuple.getVec());
				}
				if (DHSUtils.noMetricVecsSetForBit(vecsValue, curBit, metrics) == false)
					bitLow = curBit + 1;
				else
					bitHigh = curBit;
			} else {
				bitHigh = curBit;
			}
		}

		// ... then revert to the standard probing procedure, with the above
		// position as the starting point.
		return estimatePartial(sourceNode,
			metrics,
			bitLow,
			vecsValue,
			hopCount,
			bitsProbed,
			idRanges);
	}

	private DistributedEstimationResult[] estimateRecursive(DHSPastryAppl sourceNode,
		Vector<String> metrics) throws NoSuchMetricException {

		for (HashSketchImpl hs : distributedHS)
			hs.clear();

		DHSMetricProbeResult result = probeRecursive(sourceNode, metrics);
		DistributedEstimationResult ret[] = new DistributedEstimationResult[metrics.size()];

		for (int metric = 0; metric < metrics.size(); metric++) {
			String curMetric = metrics.get(metric);
			HashSketchImpl hs = result.resultData.get(curMetric);
			ret[metric] = new DistributedEstimationResult(curMetric,
				hs != null ? hs.estimate() : 0, result.hopCount, result.lastBitProbed);

			Logger logger = sourceNode.getLogger();
			if (logger.level <= Logger.FINE) {
				logger.log(sketchAlgo + " distributed HS for " + curMetric + " :: " + "\n" + hs
					+ "\n" + hs.toStringFull());
			}
		}

		return ret;
	}

	private DistributedEstimationResult[] estimateClassic(DHSPastryAppl sourceNode,
		Vector<String> metrics) throws NoSuchMetricException {
		for (HashSketchImpl hs : distributedHS)
			hs.clear();
		Hashtable<String, BitSet[]> vecsValue = new Hashtable<String, BitSet[]>();
		RandomIdSelector idRanges = new RandomIdSelector();

		for (int metric = 0; metric < metrics.size(); metric++) {
			BitSet[] val = new BitSet[L];
			for (int bit = 0; bit < L; bit++)
				val[bit] = new BitSet();
			vecsValue.put(metrics.get(metric), val);
		}
		return estimatePartial(sourceNode, metrics, L, vecsValue, 0, null, idRanges);
	}

	private IdRange getIdRangeForBit(int r) {
		return new IdRange(Id.build(thr(r)), Id.build(thr(r - 1)));
	}

	private Id getRandomIdForBit(int r) {
		int index = Id.IdBitLength - r - 1;

		assert (r >= -1 && r < L && index < Id.IdBitLength);

		byte rnd[] = new byte[(int)QuickMath.ceil((double)index / 8.0)];
		int id[] = new int[Id.IdBitLength / 32];

		env.getRandomSource().nextBytes(rnd);

		int mask = (0xff >> ((r + 1) % 8)) & 0xff;

		for (int i = 0; i < rnd.length - 1; i++) {
			int k = rnd[i] & 0xff;
			id[i / 4] |= k << ((i % 4) * 8);
		}

		int lastByte = (rnd[rnd.length - 1] & mask) & 0xff;
		id[(rnd.length - 1) / 4] |= lastByte << (((rnd.length - 1) % 4) * 8);
		id[index / 32] |= (1 << (index % 32));

		return Id.build(id);
	}

	public final int[] thr(int r) {
		assert (r >= -1 && r < L);
		int id[] = new int[intsPerId];
		int index = Id.IdBitLength - r - 1;
		if (index < Id.IdBitLength)
			id[index / 32] = (1 << (index % 32));
		else
			for (int i = 0; i < intsPerId; i++)
				id[i] = 0xffffffff;
		return id;
	}
}

class DHSBitProbeResult {
	DHSTuple[]			resultData	= null;
	int					hopCount	= 0;
	RandomIdSelector	range		= null;

	public DHSBitProbeResult(DHSBitProbeResponseMessage result, int hopCount) {
		if (result != null) {
			this.resultData = result.getDataAsTupleArray();
			this.range = result.getIdRanges();
		}
		this.hopCount = hopCount;
		assert (hopCount >= 0);
	}
}

class DHSMetricProbeResult {
	Hashtable<String, HashSketchImpl>	resultData		= null;
	int									hopCount		= 0;
	int									lastBitProbed	= 0;

	public DHSMetricProbeResult(Hashtable<String, HashSketchImpl> resultData, int hopCount,
		int lastBitProbed) {
		this.resultData = resultData;
		this.hopCount = hopCount;
		this.lastBitProbed = lastBitProbed;
		assert (hopCount >= 0);
	}
}
