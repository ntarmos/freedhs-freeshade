/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.DF03SketchImpl;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.util.QuickMath;
import netcins.util.RandomIdSelector;
import rice.environment.Environment;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
final class DHSDF03Impl extends DHSImpl {

	int	vecsSetUpperLimit	= 0;

	protected DHSDF03Impl(DHSSketchAlgorithm sketchAlgo, int numVecs, String hashAlgo, int L,
		int retries, int replicas, Vector<String> dhsMetrics, Environment env)
		throws NoSuchAlgorithmException {
		super(sketchAlgo.setUsingDF03(), numVecs, hashAlgo, L, retries, replicas, dhsMetrics, env);
		this.vecsSetUpperLimit = (int)QuickMath.ceil((double)getNumVecs()
			* DF03SketchImpl.df03VecsSetULPercentage);
	}

	protected DHSDF03Impl(DHSSketchAlgorithm sketchAlgo, int numVecs, String hashAlgo, int L,
		int retries, int replicas, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, hashAlgo, L, retries, replicas, null, env);
	}

	protected DHSDF03Impl(DHSSketchAlgorithm sketchAlgo, int numVecs, int L, int retries,
		int replicas, Environment env) throws NoSuchAlgorithmException {
		this(sketchAlgo, numVecs, HashSketchImpl.defaultHashAlgo, L, retries, replicas, null, env);
	}

	@Override
	protected final DistributedEstimationResult[] estimatePartial(PastryAppl sourceNode,
		Vector<String> metrics, int numBits, Hashtable<String, BitSet[]> vecsValue, int hopCount,
		HashSet<Integer> bitsProbed, RandomIdSelector idRanges) throws NoSuchMetricException {

		if (getSketchAlgo().isUsingSequentialBitProbing())
			return estimateClassicPartial(sourceNode,
				metrics,
				numBits,
				0,
				vecsValue,
				hopCount,
				bitsProbed,
				idRanges);
		else
			return estimateBinarySearchPartial(sourceNode,
				metrics,
				numBits,
				0,
				vecsValue,
				hopCount,
				bitsProbed,
				idRanges);
	}

	private DistributedEstimationResult[] estimateClassicPartial(PastryAppl sourceNode,
		Vector<String> metrics, int numBits, int endBit, Hashtable<String, BitSet[]> vecsValue,
		int hopCount, HashSet<Integer> bitsProbed, RandomIdSelector idRanges)
		throws NoSuchMetricException {
		if (bitsProbed == null) {
			bitsProbed = new HashSet<Integer>();
		}

		int bit;
		for (bit = numBits - 1; bit >= endBit
			&& DHSUtils.allMetricVecsSetToMoreThanForBit(vecsValue,
				vecsSetUpperLimit,
				bit,
				numBits,
				metrics) == false; bit--) {
			if (bitsProbed.contains(bit))
				continue;

			DHSBitProbeResult response = probe((DHSPastryAppl)sourceNode, metrics, bit, idRanges);
			idRanges.add(response.range);
			bitsProbed.add(bit);

			hopCount += response.hopCount;

			if (response.resultData != null) {
				for (int item = 0; item < response.resultData.length; item++) {
					DHSTuple dhsTuple = response.resultData[item];
					setBitForMetric(dhsTuple.getMetric(), dhsTuple.getVec(), dhsTuple.getBit());
					vecsValue.get(dhsTuple.getMetric())[dhsTuple.getBit()].set(dhsTuple.getVec());
				}
			}
			if (DHSUtils.allMetricVecsSetToMoreThanForBit(vecsValue,
				vecsSetUpperLimit,
				bit,
				numBits,
				metrics) == true) {
				System.out.println("Bit " + bit + " set. Terminating probing...");
				break;
			}
		}
		System.out.println("Probing ended at bit " + (bit >= 0 ? bit : 0));
		DistributedEstimationResult[] ret = new DistributedEstimationResult[metrics.size()];
		for (int i = 0; i < ret.length; i++) {
			try {
				ret[i] = new DistributedEstimationResult(metrics.get(i), estimate(metrics.get(i)),
					hopCount, bitsProbed.size());
			} catch (NoSuchMetricException e) {
				e.printStackTrace();
			}
		}

		return ret;
	}

	private DistributedEstimationResult[] estimateBinarySearchPartial(PastryAppl sourceNode,
		Vector<String> metrics, int numBits, int endBit, Hashtable<String, BitSet[]> vecsValue,
		int hopCount, HashSet<Integer> bitsProbed, RandomIdSelector idRanges)
		throws NoSuchMetricException {

		System.out.println("Reverting to classic from bit " + numBits + " down to bit" + endBit);
		return estimateClassicPartial(sourceNode,
			metrics,
			numBits,
			endBit,
			vecsValue,
			hopCount,
			bitsProbed,
			idRanges);
	}

	@Override
	protected final void insertBinarySearch(PastryAppl sourceNode, String metric,
		ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	@Override
	protected final void insertBinarySearch(PastryAppl sourceNode, String metric,
		Vector<ItemSerializable> objects) {
		basicInsert(sourceNode, metric, objects);
	}

	@Override
	protected final void insertClassic(PastryAppl sourceNode, String metric, ItemSerializable object) {
		basicInsert(sourceNode, metric, object);
	}

	@Override
	protected final void insertClassic(PastryAppl sourceNode, String metric,
		Vector<ItemSerializable> objects) {
		basicInsert(sourceNode, metric, objects);
	}
}
