/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 * 
 * TODO Add time-related info to cache-like structures (e.g. make algos
 * timestamp-aware, make cacheHS use an LRU-like algo, do cache eviction, etc.
 */

package netcins.p2p.dhs;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.centralized.sketches.DF03SketchImpl;
import netcins.dbms.centralized.sketches.FM85SketchImpl;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.p2p.dhs.messaging.DHSBitProbeRequestMessage;
import netcins.p2p.dhs.messaging.DHSBitProbeResponseMessage;
import netcins.p2p.dhs.messaging.DHSInsertionRequestMessage;
import netcins.p2p.dhs.messaging.DHSInsertionResponseMessage;
import netcins.p2p.dhs.messaging.DHSMessage;
import netcins.p2p.dhs.messaging.DHSMetricsProbeRequestMessage;
import netcins.p2p.dhs.messaging.DHSMetricsProbeResponseMessage;
import netcins.p2p.dhs.messaging.DHSRequestMessage;
import netcins.p2p.dhs.messaging.DHSResponseMessage;
import netcins.p2p.dhs.messaging.DHSMetricsProbeRequestMessage.BinarySearchProbeMode;
import netcins.util.QuickMath;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.NodeHandle;
import rice.pastry.PastryNode;
import rice.pastry.client.CommonAPIAppl;
import rice.pastry.messaging.Message;
import rice.pastry.routing.RouteMessage;

/**
 * A PastryAppl implementing the functionality required by DHSImpl.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSPastryAppl extends CommonAPIAppl {

	private class CacheEntry {
		private HashSketchImpl	hs			= null;
		private long			timestamp	= -1;

		public CacheEntry(HashSketchImpl hs) {
			this.hs = hs;
			this.timestamp = System.currentTimeMillis();
		}

		/**
		 * @return the hs
		 */
		public final HashSketchImpl getHs() {
			return this.hs;
		}

		/**
		 * @return the timestamp
		 */
		public final long getTimestamp() {
			return this.timestamp;
		}
	}

	/* Private class variables */
	private static final int								myAddress				= DHSMessage
																						.getMyAddress();

	/* DHS/Pastry node internal state */
	private DHSImpl											dhsImpl					= null;
	private Vector<DHSTuple>								bitData					= null;
	private Hashtable<String, CacheEntry>					cacheHS					= null;
	private Hashtable<String, HashSketchImpl>				localHS					= null;

	/* Hit/hop counters */
	private Integer											insertionHits			= 0;
	private Integer											queryHits				= 0;
	private Integer											insertionsHopCount		= 0;

	/* Request/response queues */
	private Hashtable<Long, DHSInsertionRequestMessage>		pendingInsertions		= null;
	private Hashtable<Long, DHSInsertionResponseMessage>	insertionResponses		= null;
	private Hashtable<Long, DHSBitProbeRequestMessage>		pendingQueries			= null;
	private Hashtable<Long, DHSBitProbeResponseMessage>		queryResponses			= null;
	private Hashtable<Long, DHSMetricsProbeRequestMessage>	pendingMetricQueries	= null;
	private Hashtable<Long, DHSMetricsProbeResponseMessage>	metricQueryResponses	= null;

	/*
	 * For debugging/reporting purposes -- destined to hold the position of the current
	 * DHSPastryAppl instance in the global array of PastryAppl's.
	 */
	public int												index					= 0;

	/**
	 * Constructs a new DHSPastryAppl instance.
	 * 
	 * @param pn The PastryNode on which the application is to be executed.
	 * @param dhsImpl An instance of DHSImpl, to drive DHSImpl data insertion
	 *            and retrieval.
	 */
	public DHSPastryAppl(PastryNode pn, DHSImpl dhsImpl) {
		super(pn);
		this.address = myAddress;
		setDHSImpl(dhsImpl);
		reinitData();
	}

	/**
	 * Returns the RMI address of this application.
	 * 
	 * @return The RMI address of the DHSPastryAppl at the local node.
	 * @see rice.pastry.client.PastryAppl#getAddress()
	 */
	@Override
	public int getAddress() {
		return myAddress;
	}

	/**
	 * Called by pastry when a message is enroute and is passing through this
	 * node.
	 * 
	 * @param rm The message that is passing through.
	 * @see rice.pastry.client.CommonAPIAppl#forward(RouteMessage)
	 */
	@Override
	@SuppressWarnings("deprecation")
	public void forward(RouteMessage rm) {
		Object o = rm.unwrap();
		if (o instanceof DHSMessage && rm.getNextHop() != null && rm.getSender() != null
			&& this.getNodeId().equals(rm.getNextHop().getNodeId()) == false) {
			NodeHandle sender = rm.getSender();
			Id targetId = rm.getTarget();

			if (o instanceof DHSRequestMessage
				&& dhsImpl.getSketchAlgo().isUsingShortcuts() == true
				&& ((DHSMessage)o).isOneHopMsg() == false) {
				NodeHandle newTarget = null;

				if (o instanceof DHSBitProbeRequestMessage
					|| o instanceof DHSInsertionRequestMessage) {
					DHSTuple[] data = ((DHSMessage)o).getDataAsTupleArray();
					if (data != null) {
						BitSet bs = new BitSet(dhsImpl.getNumBits());
						for (int i = 0; i < data.length; i++) {
							bs.set(data[i].getBit());
						}
						for (int curBit = bs.nextSetBit(0); curBit >= 0 && newTarget == null; curBit = bs
							.nextSetBit(curBit + 1)) {
							if (dhsImpl.nodeIsInArc(this.getNodeHandle(), curBit))
								newTarget = this.getNodeHandle();
							else if (dhsImpl.nodeIsInArc(rm.getNextHop(), curBit))
								newTarget = rm.getNextHop();
							if (newTarget == null)
								newTarget = dhsImpl.mapToNode(this,
									bs.nextSetBit(0),
									(o instanceof DHSBitProbeRequestMessage
										? ((DHSBitProbeRequestMessage)o).getIdRanges() : null));
						}
					}
				}

				if (o instanceof DHSMetricsProbeRequestMessage) {
					if (dhsImpl.nodeIsInArc(this.getNodeHandle(),
						((DHSMetricsProbeRequestMessage)o).getCurrentBit()))
						newTarget = this.getNodeHandle();
					else if (dhsImpl.nodeIsInArc(rm.getNextHop(),
						((DHSMetricsProbeRequestMessage)o).getCurrentBit()))
						newTarget = rm.getNextHop();
					if (newTarget == null)
						newTarget = dhsImpl.mapToNode(this, ((DHSMetricsProbeRequestMessage)o)
							.getCurrentBit(), null);
				}

				if (newTarget != null) {
					targetId = newTarget.getNodeId();
					rm.setDestinationId(targetId);
					rm.setNextHop(newTarget);
					((DHSMessage)o).setOneHopMsg(true);
					((DHSMessage)o).setTargetId(targetId);
				}
			}

			if (this.getNodeId().equals(rm.getNextHop().getNodeId()) == false
				&& this.getNodeId().equals(rm.getSenderId()) == false)
				((DHSMessage)o).addHop(this.getNodeId());
			//System.out.println("Forward: Node " + index + " (" + this.getNodeId()
			//	+ ")  adding hop: " + sender + " => " + this.getNodeId() + " => "
			//	+ rm.getNextHop().getNodeId() + " for msg no " + ((DHSMessage)o).getMsgId());

			rm = new RouteMessage(targetId, (DHSMessage)o, rm.getNextHop(), rm.getOptions(),
				(byte)1);
			rm.setSender(this.getNodeHandle());
		}
	}

	/**
	 * Called by pastry when the neighbor set changes.
	 * 
	 * @param nh The handle of the node that was added or removed.
	 * @param joined true if a node was added, false otherwise.
	 */
	@Override
	public void update(NodeHandle nh, boolean joined) {
		/*
		 * We could/should transfer bit data from/to neighbor nodes if the
		 * current node is on the edge of a bit arc. However, we choose a
		 * soft-state approach wrt DHS data maintenance; nodes periodically
		 * reinsert/update the bit data stored in the DHS and things eventually
		 * go their way. Actually, this tactic could result in critical data
		 * loss only in the "single-node" arcs, so this remains as a ToDo item.
		 */
	}

	/**
	 * Executed when a message is destined to the current node.
	 * 
	 * @param key The Id of the message's destination.
	 * @param msg The (unwrapped) message.
	 */
	@Override
	public void deliver(rice.pastry.Id key, Message msg) {
		if (msg == null) {
			System.err.println("Null message ?!");
			return;
		}

		if (msg instanceof DHSMessage) {
			DHSMessage dmsg = (DHSMessage)msg;
			dmsg.setOneHopMsg(false);
			if (this.getNodeId().equals(dmsg.getReqNodeId()) == false) {
				dmsg.addHop(this.getNodeId());
				//System.out.println("Deliver: Node " + index + " (" + this.getNodeId()
				//	+ ") adding hop: " + dmsg.getSenderId() + " => " + this.getNodeId()
				//	+ " for msg no " + dmsg.getMsgId());
			}
		}

		// DEBUG: Print extra information...
		if (logger.level <= Logger.FINE) {
			if (msg instanceof DHSMessage)
				logger.log("AP::  " + (DHSMessage)msg + " => " + getNodeId() + ".");
		}

		if (msg instanceof DHSMetricsProbeRequestMessage) {
			processDHSMetricsProbeRequestMessage((DHSMetricsProbeRequestMessage)msg);
		} else if (msg instanceof DHSMetricsProbeResponseMessage) {
			processDHSMetricsProbeResponseMessage((DHSMetricsProbeResponseMessage)msg);
		} else if (msg instanceof DHSBitProbeRequestMessage) {
			processDHSBitProbeRequestMessage((DHSBitProbeRequestMessage)msg);
		} else if (msg instanceof DHSBitProbeResponseMessage) {
			processDHSBitProbeResponseMessage((DHSBitProbeResponseMessage)msg);
		} else if (msg instanceof DHSInsertionRequestMessage) {
			processDHSInsertionRequestMessage((DHSInsertionRequestMessage)msg);
		} else if (msg instanceof DHSInsertionResponseMessage) {
			processDHSInsertionResponseMessage((DHSInsertionResponseMessage)msg);
		} else {
			System.err.println("The thing that shouldn't be!");
		}
	}

	/**
	 * Returns a String representation of the current DHSPastryAppl object.
	 * 
	 * @return A String representation of the current DHSPastryAppl object.
	 */
	@Override
	public String toString() {
		return "[ DHSPastryAppl " + thePastryNode + " :: " + dhsImpl + " ]";
	}

	/**
	 * Clears all hit counters for the local node.
	 */
	public void clearHits() {
		synchronized (insertionHits) {
			insertionHits = 0;
		}
		synchronized (queryHits) {
			queryHits = 0;
		}
		synchronized (insertionsHopCount) {
			insertionsHopCount = 0;
		}
	}

	/**
	 * Returns the logger instance for the local node.
	 * 
	 * @return The logger instance for the local node.
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Initializes the local DHSImpl instance.
	 * 
	 * @param dhsImpl The DHSImpl instance.
	 */
	public void setDHSImpl(DHSImpl dhsImpl) {
		synchronized (this) {
			this.dhsImpl = dhsImpl;
			if (this.dhsImpl != null) {
			}
		}
	}

	/**
	 * Reinitializes the local data store and all response/query queues.
	 */
	public void reinitData() {
		synchronized (this) {
			this.bitData = new Vector<DHSTuple>(100, 2);
			this.cacheHS = new Hashtable<String, CacheEntry>();
			this.localHS = new Hashtable<String, HashSketchImpl>();
		}
		clearTransientData();
		clearQueues();
	}

	public void clearTransientData() {
		clearSketches();
		clearQueues();
	}

	/**
	 * Waits until the local PastryNode is in a ready state.
	 * 
	 * @see rice.pastry.PastryNode#isReady()
	 */
	public void waitForNodeToBeReady() {
		synchronized (thePastryNode) {
			if (thePastryNode.getLocalHandle().isAlive()) {
				while (!thePastryNode.isReady() && !thePastryNode.joinFailed()) {
					try {
						thePastryNode.wait(100);
						if (thePastryNode.joinFailed()) {
							throw new RuntimeException(
								"Could not join the FreePastry ring.  Reason:"
									+ thePastryNode.joinFailedReason());
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/* *************************** */
	/* Insertion-related methods */
	/* *************************** */

	/**
	 * Inserts a new DHSTuple object in the overlay using the designated ID.
	 * 
	 * @param data The DHSTuple to insert in the overlay.
	 * 
	 * @return The identifier of the insertion request. This can be used later
	 *         on to query for the status of the insertion.
	 * @see #hasResponseToInsertion(long)
	 * @see #getResponseToInsertion(long)
	 * @see #getInsertionHopCount(long)
	 */
	public long insert(DHSTuple[] data) {
		DHSInsertionRequestMessage omsg = null;
		int minBit = dhsImpl.getNumBits();
		int maxBit = 0;
		for (int i = 0; i < data.length; i++) {
			if (minBit > data[i].getBit())
				minBit = data[i].getBit();
			if (maxBit < data[i].getBit())
				maxBit = data[i].getBit();
		}
		Id targetId = dhsImpl.mapToNodeId(this, minBit);
		if (logger.level <= Logger.FINE)
			omsg = new DHSInsertionRequestMessage(data, this.getNodeHandle(), true);
		else
			omsg = new DHSInsertionRequestMessage(data, this.getNodeHandle(), false);
		omsg.setTargetId(targetId);

		synchronized (pendingInsertions) {
			assert (pendingInsertions.containsKey(omsg.getMsgId()) == false);
			pendingInsertions.put(omsg.getMsgId(), omsg);
		}
		//System.err.println("Node " + this.index + "(" + this.getNodeId()
		//	+ ") sending insertion msg no " + omsg.getMsgId() + " to " + targetId + " (bits: "
		//	+ minBit + " - " + maxBit + ")");

		omsg.setSender(this.getNodeHandle());
		route(targetId, omsg, null);
		return omsg.getMsgId();
	}

	public long insert(DHSTuple data) {
		DHSTuple[] dataArray = new DHSTuple[1];
		dataArray[0] = data;
		return insert(dataArray);
	}

	/**
	 * Adds an item to the local batch of insertions. This method is a local
	 * operation on every node, populating a local hash sketch for the given
	 * metric. In order to actually update the data stored in the overlay one
	 * has to call {@link #doBatch()} to execute all pending batch inserts.
	 * 
	 * @param metric The metric under which to add the given item.
	 * @param data The data item to insert.
	 */
	public void batchInsert(String metric, ItemSerializable data) {
		synchronized (localHS) {
			HashSketchImpl hs = localHS.get(metric);
			if (hs == null) {
				try {
					hs = (dhsImpl.getSketchAlgo().isUsingDF03() ? new DF03SketchImpl(dhsImpl
						.getNumVecs(), dhsImpl.getNumBits(), dhsImpl.getHashAlgo())
						: new FM85SketchImpl(dhsImpl.getNumVecs(), dhsImpl.getNumBits(), dhsImpl
							.getHashAlgo()));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
					return;
				}
				localHS.put(metric, hs);
			}
			hs.insert(data);
		}
	}

	/**
	 * Executes all pending batch inserts. New items are added to the local
	 * synopsis by calling {@link #batchInsert(String, ItemSerializable)}.
	 * 
	 * @return The number of messages sent during this batch run.
	 */
	public int doBatch() {
		int ret = 0;
		synchronized (localHS) {
			if (localHS.size() > 0) {
				Vector<ItemSerializable> hstuples = new Vector<ItemSerializable>();
				Hashtable<Integer, Vector<DHSTuple>> tuples = new Hashtable<Integer, Vector<DHSTuple>>();
				for (Enumeration<String> metrics = localHS.keys(); metrics.hasMoreElements();) {
					String curMetric = metrics.nextElement();
					HashSketchImpl hs = localHS.remove(curMetric);
					if (hs.isEmpty())
						continue;

					for (int bit = hs.getNumBits() - 1; bit >= 0; bit--) {
						for (int vec = 0; vec < hs.getNumVecs(); vec++) {
							if (hs.getBit(vec, bit) == true) {
								hstuples.add(new DHSTuple(curMetric, vec, bit));
							}
						}
					}
					hs.clear();
				}
				if (hstuples.size() > 0) {
					DHSTuple[] tupleArray = new DHSTuple[hstuples.size()];
					hstuples.copyInto(tupleArray);
					insert(tupleArray);
					ret++;
				}
			}
		}
		return ret;
	}

	/**
	 * Removes all insertion responses from the response list.
	 * 
	 * @return The number of responses removed from the response list.
	 */
	public int clearInsertionResponses() {
		int ret;
		synchronized (insertionResponses) {
			ret = insertionResponses.size();
			insertionResponses.clear();
		}
		return ret;
	}

	/**
	 * Returns the number of times the current node was the target of an
	 * insertion request.
	 * 
	 * @return The number of times the current node was the target of an
	 *         insertion request.
	 */
	public int getInsertionHits() {
		synchronized (insertionHits) {
			return insertionHits;
		}
	}

	/**
	 * Gets the number of hops it took the insertion message to reach its
	 * destination. The caller must first have checked for the status of the
	 * request, using hasResponseToInsertion(msgId).
	 * 
	 * @param msgId The ID of the insertion request whose hop count is
	 *            requested.
	 * @return The number of hops it took the insertion message to reach its
	 *         destination. -1 if there is no response for the designated
	 *         insertion request ID.
	 * @see #insert(DHSTuple)
	 * @see #hasResponseToInsertion(long)
	 */
	public int getInsertionHopCount(long msgId) {
		synchronized (insertionResponses) {
			DHSInsertionResponseMessage rmsg = insertionResponses.get(msgId);
			if (rmsg == null)
				return -1;
			return rmsg.getHopCount();
		}
	}

	public int getInsertionsHopCount() {
		synchronized (insertionsHopCount) {
			return insertionsHopCount;
		}
	}

	/**
	 * Retruns the number of pending insertions.
	 * 
	 * @return The number of pending insertions.
	 */
	public int getNumPendingInsertions() {
		synchronized (pendingInsertions) {
			return pendingInsertions.size();
		}
	}

	/**
	 * Gets the response to the designated insertion request. The caller must
	 * first have checked for the status of the request, using
	 * hasResponseToInsertion(msgId). This method does not remove the response
	 * from the list of responses; the caller must use
	 * removeResponseToInsertion(msgId) to accomplish this.
	 * 
	 * @param msgId The ID of the insertion request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated insertion request. NULL if there is no response for
	 *         the designated request ID.
	 * @see #insert(DHSTuple)
	 * @see #hasResponseToInsertion(long)
	 * @see #removeResponseToInsertion(long)
	 */
	public DHSTuple[] getResponseToInsertion(long msgId) {
		DHSInsertionResponseMessage rmsg = null;
		synchronized (insertionResponses) {
			rmsg = insertionResponses.get(msgId);
		}
		return (rmsg != null ? (DHSTuple[])rmsg.getDataAsTupleArray() : new DHSTuple[0]);
	}

	/**
	 * Checks if there are any pending insertions.
	 * 
	 * @return true if there are pending insertions, false otherwise.
	 */
	public boolean hasPendingInsertions() {
		synchronized (pendingInsertions) {
			return (pendingInsertions.isEmpty() == false);
		}
	}

	/**
	 * Checks if there is a response for the designated insertion request.
	 * 
	 * @param msgId The ID of the insertion request to check
	 * @return true if there is a response for the given insertion request,
	 *         false otherwise.
	 */
	public boolean hasResponseToInsertion(long msgId) {
		synchronized (insertionResponses) {
			return (insertionResponses != null ? insertionResponses.containsKey(msgId) : false);
		}
	}

	/**
	 * Removes and returns the response to the designated insertion request. The
	 * caller must first have checked for the status of the request, using
	 * hasResponseToInsertion(msgId). This method removes the response from the
	 * list of responses; the caller must use getResponseToInsertion(msgId) if
	 * she just wants to check the response.
	 * 
	 * @param msgId The ID of the insertion request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated insertion request. NULL if there is no response for
	 *         the designated request ID.
	 * @see #insert(DHSTuple)
	 * @see #hasResponseToInsertion(long)
	 * @see #getResponseToInsertion(long)
	 */
	public DHSTuple[] removeResponseToInsertion(long msgId) {
		DHSInsertionResponseMessage rmsg = null;
		synchronized (insertionResponses) {
			rmsg = insertionResponses.remove(msgId);
		}
		return (rmsg != null ? (DHSTuple[])rmsg.getDataAsTupleArray() : new DHSTuple[0]);
	}

	/**
	 * Waits until the designated insertion request has finished.
	 * 
	 * @param msgId The ID of the insertion request to wait for.
	 */
	public void waitForInsertionToComplete(long msgId) {
		synchronized (pendingInsertions) {
			while (pendingInsertions.containsKey(msgId) == true) {
				try {
					pendingInsertions.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Waits until all insertion requests have finished.
	 */
	public void waitForInsertionsToComplete() {
		synchronized (pendingInsertions) {
			while (pendingInsertions.isEmpty() == false) {
				try {
					pendingInsertions.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/* *********************** */
	/* Query-related methods */
	/* *********************** */

	public long query(Vector<String> queryMetrics) {
		DHSMetricsProbeRequestMessage omsg = null;
		Vector<String> metrics = new Vector<String>(queryMetrics);
		Collections.sort(metrics);

		omsg = new DHSMetricsProbeRequestMessage(metrics, this.getNodeHandle(), dhsImpl
			.getSketchAlgo().firstBit(0, dhsImpl.getNumBits()), false);
		omsg.addNodeToPath(this.getNodeHandle());
		omsg.setLowBit(0);
		omsg.setHighBit(dhsImpl.getNumBits() - 1);

		if (dhsImpl.getSketchAlgo().isUsingBinarySearchBitProbing() == true) {
			omsg.setMode(BinarySearchProbeMode.BS_UPPER);
			// System.out.println("Searching for lowest all-set-to-0 bit starting from bit "
			//		+ omsg.getCurrentBit());
		} else {
			omsg.setMode(BinarySearchProbeMode.SEQUENTIAL);
		}

		NodeHandle targetHandle = dhsImpl.mapToNode(this, omsg.getCurrentBit(), null);
		Id targetId = null;

		if (targetHandle != null) {
			targetId = targetHandle.getNodeId();
			omsg.setOneHopMsg(true);
		} else {
			targetId = dhsImpl.mapToNodeId(this, omsg.getCurrentBit());
		}
		omsg.setTargetId(targetId);

		synchronized (pendingMetricQueries) {
			assert (pendingMetricQueries.containsKey(omsg.getMsgId()) == false);
			pendingMetricQueries.put(omsg.getMsgId(), omsg);
		}

		omsg.setSender(this.getNodeHandle());
		if (dhsImpl.nodeIsInArc(this.getNodeHandle(), omsg.getCurrentBit()) == true) {
			deliver(omsg.getTargetId(), omsg);
		} else {
			route(omsg.getTargetId(), omsg, targetHandle);
		}
		return omsg.getMsgId();
	}

	/**
	 * Sends a probe to the node responsible for the designated ID, for the
	 * metrics and bits designated by the data in the second argument. Query
	 * processing will proceed from there, so for better performance tuples in
	 * data should all refer to the same bit position.
	 * 
	 * @param targetId The target ID.
	 * @param data An array of DHSTuple's denoting the metrics and bits to
	 *            probe.
	 * @return The identifier of the probe request. This can be used later on to
	 *         query for the status of the query.
	 * @see #hasResponseToQuery(long)
	 * @see #getResponseToBitQuery(long)
	 * @see #getQueryHops(long)
	 * @see #getQueryHopCount(long)
	 */
	public long query(Id targetId, DHSTuple[] data) {
		DHSBitProbeRequestMessage omsg = null;

		if (logger.level <= Logger.FINE)
			omsg = new DHSBitProbeRequestMessage(data, this.getNodeHandle(), true);
		else
			omsg = new DHSBitProbeRequestMessage(data, this.getNodeHandle(), false);
		omsg.setTargetId(targetId);

		synchronized (pendingQueries) {
			assert (pendingQueries.containsKey(omsg.getMsgId()) == false);
			pendingQueries.put(omsg.getMsgId(), omsg);
		}

		BitSet bs = new BitSet(dhsImpl.getNumBits());
		for (int i = 0; i < data.length; i++)
			bs.set(data[i].getBit());
		int minBit = bs.nextSetBit(0);
		if (minBit == -1)
			return -1;

		omsg.setSender(this.getNodeHandle());
		if (dhsImpl.nodeIsInArc(this.getNodeHandle(), minBit) == true) {
			if (logger.level <= Logger.FINER)
				logger.log("Client request remote query to id " + targetId + " for bit #" + minBit
					+ " but I (" + this.getNodeId()
					+ ") am in that arc too. Silently forcing target to local node...");
			deliver(targetId, omsg);
		} else {
			route(targetId, omsg, null);
		}
		return omsg.getMsgId();
	}

	/**
	 * Retruns the number of pending queries.
	 * 
	 * @return The number of pending queries.
	 */
	public int getNumPendingQueries() {
		int ret = 0;
		synchronized (pendingQueries) {
			ret = pendingQueries.size();
		}
		synchronized (pendingMetricQueries) {
			ret += pendingMetricQueries.size();
		}
		return ret;
	}

	/**
	 * Gets the number of hops it took the query message to reach its
	 * destination. The caller must first have checked for the status of the
	 * request, using hasResponseToQuery(msgId).
	 * 
	 * @param msgId The ID of the query request whose hop count is requested.
	 * @return The number of hops it took the query message to reach its
	 *         destination. -1 if there is no response for the designated query
	 *         request ID.
	 * @see #query(Id, DHSTuple[])
	 * @see #query(Vector)
	 * @see #hasResponseToQuery(long)
	 */
	public int getQueryHopCount(long msgId) {
		DHSResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.get(msgId);
		}
		if (rmsg != null)
			return rmsg.getHopCount();

		synchronized (metricQueryResponses) {
			rmsg = metricQueryResponses.get(msgId);
		}
		if (rmsg != null)
			return rmsg.getHopCount();

		return -1;
	}

	public int getNumBitsProbed(long msgId) {
		DHSMetricsProbeResponseMessage rmsg = null;
		synchronized (metricQueryResponses) {
			rmsg = metricQueryResponses.get(msgId);
		}
		if (rmsg != null)
			return rmsg.getNumBitsProbed();

		return -1;
	}

	/**
	 * Gets the IDs of all nodes the designated query request went through en
	 * route to its destination.
	 * 
	 * @param msgId The ID of the query request whose hops are requested.
	 * @return A Vector containing the IDs of all nodes the query message went
	 *         through, or null if there is no information for the requested
	 *         query ID.
	 * @see #hasResponseToQuery(long)
	 */
	public Vector<Id> getQueryHops(long msgId) {
		DHSResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.get(msgId);
		}
		if (rmsg != null)
			return rmsg.getHops();

		synchronized (metricQueryResponses) {
			rmsg = metricQueryResponses.get(msgId);
		}
		if (rmsg != null)
			return rmsg.getHops();

		return null;
	}

	/**
	 * Gets the response to the designated query request. The caller must first
	 * have checked for the status of the request, using
	 * hasResponseToQuery(msgId). This method does not remove the response from
	 * the list of responses; the caller must use removeResponseToQuery(msgId)
	 * to accomplish this.
	 * 
	 * @param msgId The ID of the query request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated query request. NULL if there is no response for the
	 *         designated request ID.
	 * @see #query(Id, DHSTuple[])
	 * @see #hasResponseToQuery(long)
	 * @see #removeResponseToBitQuery(long)
	 */
	public DHSBitProbeResponseMessage getResponseToBitQuery(long msgId) {
		DHSBitProbeResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.get(msgId);
		}
		return rmsg;
	}

	/**
	 * Gets the response to the designated query request. The caller must first
	 * have checked for the status of the request, using
	 * hasResponseToQuery(msgId). This method does not remove the response from
	 * the list of responses; the caller must use removeResponseToQuery(msgId)
	 * to accomplish this.
	 * 
	 * @param msgId The ID of the query request whose response is requested.
	 * @return The data payload of the response, as an Object instance.
	 * @see #query(Id, DHSTuple[])
	 * @see #hasResponseToQuery(long)
	 * @see #removeResponseToBitQuery(long)
	 */
	public Object getResponseToMetricQuery(long msgId) {
		DHSMetricsProbeResponseMessage rmsg = null;
		synchronized (metricQueryResponses) {
			rmsg = metricQueryResponses.get(msgId);
		}
		return (rmsg != null ? rmsg.getDataAsObject() : null);
	}

	/**
	 * Returns the number of times the current node was the target of a query
	 * request.
	 * 
	 * @return The number of times the current node was the target of a query
	 *         request.
	 */
	public int getQueryHits() {
		synchronized (queryHits) {
			return queryHits;
		}
	}

	/**
	 * Checks if there is a response for the designated query request.
	 * 
	 * @param msgId The ID of the query request to check
	 * @return true if there is a response for the given query request, false
	 *         otherwise.
	 */
	public boolean hasResponseToQuery(long msgId) {
		synchronized (queryResponses) {
			if (queryResponses.containsKey(msgId) == true)
				return (!queryResponses.get(msgId).mustRetry());
		}
		synchronized (metricQueryResponses) {
			if (metricQueryResponses.containsKey(msgId) == true)
				return true;
		}
		return false;
	}

	/**
	 * Checks if there are any pending queries.
	 * 
	 * @return true if there are pending queries, false otherwise.
	 */
	public boolean hasPendingQueries() {
		boolean ret = false;
		synchronized (pendingQueries) {
			ret = pendingQueries.isEmpty();
		}
		if (ret == false) {
			synchronized (pendingMetricQueries) {
				ret = pendingMetricQueries.isEmpty();
			}
		}
		return ret;
	}

	public void waitForBitProbeQueryToComplete(long id) {
		synchronized (pendingQueries) {
			while (pendingQueries.containsKey(id) == true) {
				try {
					pendingQueries.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void waitForMetricProbeQueryToComplete(long id) {
		synchronized (pendingMetricQueries) {
			while (pendingMetricQueries.containsKey(id) == true) {
				try {
					pendingMetricQueries.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Waits until the all query requests have finished.
	 */
	public void waitForQueriesToComplete() {
		synchronized (pendingQueries) {
			while (pendingQueries.isEmpty() == false) {
				try {
					pendingQueries.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		synchronized (pendingMetricQueries) {
			while (pendingMetricQueries.isEmpty() == false) {
				try {
					pendingMetricQueries.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Removes and returns the response to the designated query request. The
	 * caller must first have checked for the status of the request, using
	 * hasResponseToQuery(msgId). This method removes the response from the list
	 * of responses; the caller must use getResponseToQuery(msgId) if she just
	 * wants to check the response.
	 * 
	 * @param msgId The ID of the query request whose response is requested.
	 * @return An array of DHSTuple objects returned as the response to the
	 *         designated query request. NULL if there is no response for the
	 *         designated request ID.
	 * @see #query(Id, DHSTuple[])
	 * @see #hasResponseToQuery(long)
	 * @see #getResponseToBitQuery(long)
	 */
	public DHSBitProbeResponseMessage removeResponseToBitQuery(long msgId) {
		DHSBitProbeResponseMessage rmsg = null;
		synchronized (queryResponses) {
			rmsg = queryResponses.remove(msgId);
		}
		return rmsg;
	}

	/**
	 * Removes and returns the response to the designated query request. The
	 * caller must first have checked for the status of the request, using
	 * hasResponseToQuery(msgId). This method removes the response from the list
	 * of responses; the caller must use getResponseToMetricQuery(msgId) if she
	 * just wants to check the response.
	 * 
	 * @param msgId The ID of the query request whose response is requested.
	 * @return The data payload of the response.
	 * @see #query(Id, DHSTuple[])
	 * @see #hasResponseToQuery(long)
	 * @see #getResponseToMetricQuery(long)
	 */
	public Object removeResponseToMetricQuery(long msgId) {
		DHSResponseMessage rmsg = null;
		synchronized (metricQueryResponses) {
			rmsg = metricQueryResponses.remove(msgId);
		}
		return rmsg.getDataAsObject();
	}

	// ///////////////////
	// Private methods //
	// ///////////////////

	private void clearQueues() {
		synchronized (this) {
			pendingInsertions = new Hashtable<Long, DHSInsertionRequestMessage>();
			insertionResponses = new Hashtable<Long, DHSInsertionResponseMessage>();
			pendingQueries = new Hashtable<Long, DHSBitProbeRequestMessage>();
			queryResponses = new Hashtable<Long, DHSBitProbeResponseMessage>();
			pendingMetricQueries = new Hashtable<Long, DHSMetricsProbeRequestMessage>();
			metricQueryResponses = new Hashtable<Long, DHSMetricsProbeResponseMessage>();
		}
	}

	public void clearCache() {
		if (cacheHS != null) {
			synchronized (cacheHS) {
				cacheHS.clear();
			}
		}
	}

	private void clearSketches() {
		if (dhsImpl != null) {
			synchronized (dhsImpl) {
				dhsImpl.clearHS();
			}
		}

		if (localHS != null) {
			synchronized (localHS) {
				localHS.clear();
			}
		}
	}

	private void processDHSMetricsProbeRequestMessage(DHSMetricsProbeRequestMessage imsg) {
		Hashtable<String, HashSketchImpl> result = imsg.getResult();
		Vector<String> queryMetrics = (Vector<String>)imsg.getDataAsObject();
		int numMetrics = queryMetrics.size();

		if (imsg.getSenderId().equals(getNodeId()) == false) {
			synchronized (queryHits) {
				//System.err.println(getNodeId() + " adding query hit for q.no " + imsg.getMsgId()
				//	+ " from " + imsg.getReqNodeId() + " through " + imsg.getSenderId());
				queryHits++;
			}
		}

		boolean isBitProbed = imsg.hasBitProbed(imsg.getCurrentBit());
		boolean terminate = false;
		boolean allVecsSet = true;
		boolean noVecsSet = false;
		int fromCache = 0;

		imsg.addBitActuallyProbed(imsg.getCurrentBit());

		Collections.sort(queryMetrics);

		int lowerSetBit = dhsImpl.getNumBits() - 1, higherSetBit = 0;

		if (queryMetrics.size() > 0) {
			synchronized (bitData) {
				for (DHSTuple tuple : bitData) {
					if (Collections.binarySearch(queryMetrics, tuple.getMetric()) >= 0) {
						HashSketchImpl r = result.get(tuple.getMetric());
						if (r == null) {
							r = DHSImpl.newBaseSketchInstance(dhsImpl);
							result.put(tuple.getMetric(), r);
						}
						if (dhsImpl.getSketchAlgo().isUsingFM85()) {
							r.setBit(tuple.getVec(), tuple.getBit());
						} else {
							for (int bit = tuple.getBit(); bit >= 0
								&& r.getBit(tuple.getVec(), bit) == false; bit--)
								r.setBit(tuple.getVec(), bit);
						}
						if (tuple.getBit() > higherSetBit)
							higherSetBit = tuple.getBit();
						if (tuple.getBit() < lowerSetBit)
							lowerSetBit = tuple.getBit();
					}
				}
			}
		}

		imsg.fixProbedBits(lowerSetBit, higherSetBit);

		allVecsSet = (dhsImpl.getSketchAlgo().isUsingDF03() ? imsg.allVecsSetForBit(imsg
			.getCurrentBit(), (int)QuickMath.ceil((double)dhsImpl.getNumVecs()
			* DF03SketchImpl.df03VecsSetULPercentage)) : imsg
			.allVecsSetForBit(imsg.getCurrentBit()));
		noVecsSet = imsg.noVecsSetForBit(imsg.getCurrentBit());

		NodeHandle targetHandle = null;
		Id targetId = null;
		boolean oneHop = false;
		DHSMessage omsg = null;

		imsg.setMustRetry(!allVecsSet);

		if (imsg.mustRetry() == true) {
			imsg.mergeRetryNodes(findNextHops(imsg.getCurrentBit()));
			if (imsg.incNumRetries() <= dhsImpl.getNumRetries() && imsg.hasRetryNode() == true) {
				targetHandle = imsg.getRetryNode();
				targetId = targetHandle.getNodeId();
				//System.err.println(this.getNodeId() + " retrying to " + targetId);
			} else {
				imsg.setMustRetry(false);
				terminate = noVecsSet;
			}
		}

		synchronized (dhsImpl) {
			if (dhsImpl.getSketchAlgo().isUsingResultCaching() == true) {
				synchronized (cacheHS) {
					for (int i = 0; i < queryMetrics.size(); i++) {
						String metric = queryMetrics.get(i);
						if (cacheHS.containsKey(metric)) {
							result.put(metric, cacheHS.get(metric).getHs());
							queryMetrics.remove(i);
							fromCache++;
						}
					}
				}
			}
		}

		if (fromCache < numMetrics && imsg.mustRetry() == false) {
			imsg.addBitProbed(imsg.getCurrentBit());

			int nextBit = 0;
			int lowerBit = imsg.getLowBit();
			int upperBit = imsg.getHighBit();

			if (dhsImpl.getSketchAlgo().isUsingBinarySearchBitProbing() == true) {
				terminate = false;
			}

			if (imsg.getMode() == BinarySearchProbeMode.BS_UPPER) {
				if (noVecsSet == true) {
					upperBit = imsg.getCurrentBit() - 1;
				} else {
					lowerBit = imsg.getCurrentBit() + 1;
				}
				nextBit = imsg.nextBinarySearchBit(lowerBit, upperBit);

				if (nextBit < 0) {
					imsg.setLowestNotSetBit(imsg.getCurrentBit());
					upperBit = imsg.getCurrentBit() - 1;
					lowerBit = 0;
					if (dhsImpl.getSketchAlgo().isUsingDF03()) {
						imsg.setMode(BinarySearchProbeMode.BS_LOWER);
						nextBit = imsg.nextBinarySearchBit(lowerBit, upperBit);
						// System.out.println("Searching for highest all-set-to-1 bit down from bit "
						//		+ (upperBit + 1));
					} else {
						imsg.setMode(BinarySearchProbeMode.SEQUENTIAL);
						imsg.setLowBit(0);
						imsg.setHighBit(imsg.getCurrentBit());
						nextBit = imsg.nextSequentialBit(lowerBit, upperBit);
						// System.out.println("Reverting to classic from bit " + lowerBit
						//		+ " up to bit " + (upperBit + 1));
					}
				}
			} else if (imsg.getMode() == BinarySearchProbeMode.BS_LOWER) {
				if (allVecsSet == true) {
					lowerBit = imsg.getCurrentBit() + 1;
				} else {
					upperBit = imsg.getCurrentBit() - 1;
				}

				nextBit = imsg.nextBinarySearchBit(lowerBit, upperBit);

				if (nextBit < 0) {
					imsg.setMode(BinarySearchProbeMode.SEQUENTIAL);
					imsg.setHighestAllSetBit(imsg.getCurrentBit());
					upperBit = imsg.getLowestNotSetBit() - 1;
					lowerBit = imsg.nextSequentialBit(imsg.getCurrentBit() + 1, upperBit);
					nextBit = lowerBit;
					// System.out.println("Reverting to classic from bit " + lowerBit + " up to bit "
					//		+ upperBit);
				}
			} else if (terminate == false) {
				nextBit = lowerBit = imsg.nextSequentialBit(imsg.getCurrentBit() + 1, upperBit);
			}

			if (nextBit < 0 || nextBit > upperBit)
				terminate = true;

			if (terminate == false) {
				imsg.setCurrentBit(nextBit);
				imsg.setLowBit(lowerBit);
				imsg.setHighBit(upperBit);
				if ((targetHandle = dhsImpl.mapToNode(this, imsg.getCurrentBit(), null)) == null) {
					targetId = dhsImpl.mapToNodeId(this, imsg.getCurrentBit());
				} else {
					targetId = targetHandle.getNodeId();
				}
			}
		}

		if (fromCache == numMetrics || terminate == true) {
			imsg.addHop(this.getNodeId());
			omsg = new DHSMetricsProbeResponseMessage(result, this.getNodeHandle(), imsg, imsg
				.getNumBitsActuallyProbed());

			do
				targetHandle = ((DHSMetricsProbeResponseMessage)omsg).getNextNodeInPath();
			while (((DHSMetricsProbeResponseMessage)omsg).hasMoreNodesInPath()
				&& targetHandle.getNodeId().equals(this.getNodeId()));

			if (targetHandle != null)
				targetId = targetHandle.getNodeId();

			//System.out.println("MetricsProbeResponse: Node " + index + " (" + this.getNodeId()
			//	+ ") adding hop: " + this.getNodeId() + " => " + targetHandle
			//	+ " for msg no " + imsg.getMsgId());
		} else {
			omsg = imsg;
			synchronized (pendingMetricQueries) {
				if (pendingMetricQueries.containsKey(imsg.getMsgId()) == false)
					pendingMetricQueries.put(imsg.getMsgId(), imsg);
			}

			if (targetHandle == null && dhsImpl.getSketchAlgo().isUsingRecursiveProcessing())
				((DHSMetricsProbeRequestMessage)omsg).addNodeToPath(this.getNodeHandle());
		}

		if (targetHandle != null)
			omsg.setOneHopMsg(true);
		omsg.setSender(this.getNodeHandle());
		route(targetId, omsg, targetHandle);
	}

	private void processDHSMetricsProbeResponseMessage(DHSMetricsProbeResponseMessage imsg) {
		DHSMetricsProbeRequestMessage rmsg = null;

		synchronized (pendingMetricQueries) {
			rmsg = pendingMetricQueries.get(imsg.getReqMsgId());
		}
		assert (rmsg != null);

		NodeHandle prevNode = this.getNodeHandle();
		while (prevNode.getNodeId().equals(this.getNodeId()) && imsg.hasMoreNodesInPath())
			prevNode = imsg.getNextNodeInPath();

		if (prevNode.getNodeId().equals(this.getNodeId()) == false) {
			imsg.setTargetId(prevNode.getNodeId());
			imsg.setSender(this.getNodeHandle());
			route(prevNode.getNodeId(), imsg, prevNode);
			synchronized (dhsImpl) {
				if (dhsImpl.getSketchAlgo().isUsingResultCaching() == true) {
					synchronized (cacheHS) {
						Hashtable<String, HashSketchImpl> result = (Hashtable<String, HashSketchImpl>)imsg
							.getDataAsObject();
						for (String metric : result.keySet()) {
							cacheHS.put(metric, new CacheEntry(result.get(metric)));
						}
					}
				}
			}
		} else {
			synchronized (metricQueryResponses) {
				metricQueryResponses.put(imsg.getReqMsgId(), imsg);
			}
		}
		synchronized (pendingMetricQueries) {
			pendingMetricQueries.remove(imsg.getReqMsgId());
			if (pendingMetricQueries.isEmpty() == true)
				pendingMetricQueries.notifyAll();
		}
	}

	private NodeHandle[] findNextHops(int bit) {
		NodeHandle neighborCW = this.getLeafSet().get(1);
		NodeHandle neighborCCW = this.getLeafSet().get(-1);

		NodeHandle[] ret = new NodeHandle[2];
		ret[0] = (dhsImpl.nodeIsInArc(neighborCCW, bit) == false ? null : neighborCCW);
		ret[1] = (dhsImpl.nodeIsInArc(neighborCW, bit) == false ? null : neighborCW);
		return ret;
	}

	/**
	 * Called by deliver(...) to process a DHSBitProbeRequestMessage destined to
	 * the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSBitProbeRequestMessage(DHSBitProbeRequestMessage imsg) {
		DHSTuple[] dhsTuples = (DHSTuple[])imsg.getDataAsTupleArray();
		int queryBit = dhsTuples[0].getBit();
		Hashtable<String, Integer> metrics = new Hashtable<String, Integer>();
		int numMetrics = 0;

		if (imsg.getSenderId().equals(getNodeId()) == false) {
			synchronized (queryHits) {
				queryHits++;
			}
		}

		for (int i = 0; i < dhsTuples.length; i++) {
			metrics.put(dhsTuples[i].getMetric(), i);
		}
		numMetrics = metrics.size();
		assert (numMetrics == dhsTuples.length);

		Vector<DHSTuple> response = new Vector<DHSTuple>(10, 100);
		int relevantResults[] = new int[metrics.size()];
		boolean resultVecs[][] = new boolean[metrics.size()][dhsImpl.getNumVecs()];

		synchronized (bitData) {
			for (DHSTuple tuple : bitData) {
				if (metrics.containsKey(tuple.getMetric())) {
					response.add(tuple);
					if (tuple.getBit() == queryBit) {
						int metricIndex = metrics.get(tuple.getMetric());
						if (resultVecs[metricIndex][tuple.getVec()] == false) {
							resultVecs[metricIndex][tuple.getVec()] = true;
							relevantResults[metricIndex]++;
						}
					}
				}
			}
		}

		BitSet needMore = new BitSet();
		for (int i = 0; i < relevantResults.length; i++) {
			if (relevantResults[i] < dhsImpl.getNumVecs()) {
				needMore.set(i);
			}
		}

		DHSTuple[] ret = null;
		DHSBitProbeResponseMessage omsg = null;

		if (response.size() > 0) {
			ret = new DHSTuple[response.size()];
			response.copyInto(ret);
		}

		//System.out.println("BitProbeResponse: Node " + index + " (" + this.getNodeId()
		//	+ ") adding hop: " + this.getNodeId() + " => " + imsg.getSenderId()
		//	+ " for msg no " + imsg.getMsgId());
		imsg.addHop(this.getNodeId());
		if (ret != null && needMore.isEmpty()) {
			omsg = new DHSBitProbeResponseMessage(ret, this.getNodeHandle(), imsg);
		} else {
			NodeHandle neighborCW = this.getLeafSet().get(1);
			NodeHandle neighborCCW = this.getLeafSet().get(-1);

			int numFalse = 0;
			if (imsg.getIdRanges().isCovered(neighborCCW))
				neighborCCW = null;
			else {
				for (int i = 0; i < numMetrics; i++)
					if (needMore.get(i) == true
						&& dhsImpl.nodeIsInArc(neighborCCW, dhsTuples[i].getBit()) == false)
						numFalse++;
				if (numFalse == needMore.cardinality())
					neighborCCW = null;
			}

			numFalse = 0;
			if (imsg.getIdRanges().isCovered(neighborCW))
				neighborCW = null;
			else {
				for (int i = 0; i < numMetrics; i++)
					if (needMore.get(i) == true
						&& dhsImpl.nodeIsInArc(neighborCW, dhsTuples[i].getBit()) == false)
						numFalse++;
				if (numFalse == needMore.cardinality())
					neighborCW = null;
			}

			omsg = new DHSBitProbeResponseMessage(ret, this.getNodeHandle(), imsg, neighborCW,
				neighborCCW);
		}
		omsg.setTargetId(imsg.getReqNodeId());

		omsg.setSender(this.getNodeHandle());
		route(omsg.getTargetId(), omsg, imsg.getReqNode());
	}

	/**
	 * Called by deliver(...) to process a DHSBitProbeResponseMessage destined
	 * to the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSBitProbeResponseMessage(DHSBitProbeResponseMessage imsg) {
		synchronized (pendingQueries) {
			assert (pendingQueries.containsKey(imsg.getReqMsgId()) == true);
			DHSBitProbeRequestMessage reqMsg = pendingQueries.remove(imsg.getReqMsgId());
			DHSBitProbeResponseMessage resp = null;

			synchronized (queryResponses) {
				if (queryResponses.containsKey(imsg.getReqMsgId())) {
					resp = queryResponses.remove(imsg.getReqMsgId());
					resp.mergeRetryNodes(imsg);
					resp.mergeData(imsg);
					resp.addHops(imsg);
					DHSTuple[] respData = (DHSTuple[])resp.getDataAsTupleArray();
					if (respData != null) {
						int relevantResultsCount = 0;
						for (int i = 0; i < respData.length; i++) {
							DHSTuple qTuple = reqMsg.getDataAsTupleArray()[0];
							if (respData[i].getMetric().equals(qTuple.getMetric())
								&& respData[i].getBit() == qTuple.getBit())
								relevantResultsCount++;
						}
						if (relevantResultsCount == dhsImpl.getNumVecs())
							resp.setMustRetry(false);
					}
				} else {
					resp = imsg;
				}
			}

			if (resp.mustRetry() == true && reqMsg.incNumRetries() > dhsImpl.getNumRetries()) {
				// Bailing out...
				resp.setMustRetry(false);
			}

			if (resp.mustRetry() == true && resp.hasRetryNode() == false) {
				// Also bailing out...
				resp.setMustRetry(false);
			}

			synchronized (queryResponses) {
				assert (queryResponses.containsKey(resp.getReqMsgId()) == false);
				queryResponses.put(resp.getReqMsgId(), resp);
			}

			if (resp.mustRetry() == true) {
				// Retrying through successor probing...
				NodeHandle targetHandle = resp.getRetryNode();
				reqMsg.setTargetId(targetHandle.getNodeId());
				reqMsg.clearHops();
				assert (pendingQueries.containsKey(reqMsg.getMsgId()) == false);
				pendingQueries.put(reqMsg.getMsgId(), reqMsg);
				reqMsg.setSender(this.getNodeHandle());
				route(targetHandle.getNodeId(), reqMsg, targetHandle);
			} else if (pendingQueries.isEmpty() == true) {
				synchronized (queryResponses) {
					queryResponses.notifyAll();
				}
			}
		}
	}

	/**
	 * Called by deliver(...) to process a DHSInsertionRequestMessage destined
	 * to the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSInsertionRequestMessage(DHSInsertionRequestMessage imsg) {
		DHSTuple[] data = (DHSTuple[])imsg.getDataAsTupleArray();
		Vector<DHSTuple> foreignTuples = new Vector<DHSTuple>();
		int minBit = dhsImpl.getNumBits(), curBit = dhsImpl.getNumBits();
		synchronized (bitData) {
			for (int i = 0; i < data.length; i++) {
				if (dhsImpl.nodeIsInArc(this.getNodeHandle(), data[i].getBit()) == false) {
					foreignTuples.add(data[i]);
					if (minBit > data[i].getBit())
						minBit = data[i].getBit();
					continue;
				}
				if (curBit > data[i].getBit())
					curBit = data[i].getBit();
				int index = Collections.binarySearch(bitData, data[i]);
				if (index < 0) {
					bitData.add(-index - 1, data[i]);
				} else if (bitData.get(index).getTimestamp() < data[i].getTimestamp()) {
					bitData.set(index, data[i]);
				}
			}
		}
		synchronized (insertionHits) {
			insertionHits++;
		}

		DHSMessage omsg = null;
		NodeHandle targetHandle = null;
		Id targetId = null;

		if (curBit != dhsImpl.getNumBits())
			imsg.mergeRetryNodes(findNextHops(curBit));
		if (imsg.incNumRetries() <= dhsImpl.getNumReplicas() && imsg.hasRetryNode()) {
			omsg = imsg;
			targetHandle = imsg.getRetryNode();
			targetId = targetHandle.getNodeId();
		} else if (foreignTuples.size() == 0) {
			//System.out.println("InsertionResponse: Node " + index + " (" + this.getNodeId()
			//	+ ") adding hop: " + this.getNodeId() + " => " + imsg.getReqNodeId()
			//	+ " for msg no " + imsg.getMsgId());
			imsg.addHop(this.getNodeId());
			omsg = new DHSInsertionResponseMessage(null, this.getNodeHandle(), imsg);
			omsg.setTargetId(imsg.getReqNodeId());
			targetHandle = imsg.getReqNode();
			targetId = omsg.getTargetId();
		} else {
			targetId = dhsImpl.mapToNodeId(this, minBit);
			DHSTuple[] tmpData = new DHSTuple[foreignTuples.size()];
			foreignTuples.copyInto(tmpData);
			omsg = new DHSInsertionRequestMessage(imsg);
			omsg.setData(tmpData);
			omsg.setTargetId(targetId);
		}

		//System.err.println("Node " + this.index + "(" + this.getNodeId()
		//	+ ") sending msg no " + omsg.getMsgId() + " to " + targetId);
		omsg.setSender(this.getNodeHandle());
		route(targetId, omsg, targetHandle);
	}

	/**
	 * Called by deliver(...) to process a DHSInsertionResponseMessage destined
	 * to the local node.
	 * 
	 * @param imsg The message to process.
	 */
	private void processDHSInsertionResponseMessage(DHSInsertionResponseMessage imsg) {
		synchronized (pendingInsertions) {
			if (pendingInsertions.containsKey(imsg.getReqMsgId()) == false) {
				System.err.println("Non-existent MsgId " + imsg.getReqMsgId() + " on node " + index
					+ "(" + this.getNodeId() + ")");
				assert (pendingInsertions.containsKey(imsg.getReqMsgId()) == true);
			}
			DHSInsertionRequestMessage rmsg = pendingInsertions.remove(imsg.getReqMsgId());
			if (pendingInsertions.isEmpty())
				pendingInsertions.notifyAll();
		}
		synchronized (insertionsHopCount) {
			insertionsHopCount += imsg.getHopCount();
		}

		// XXX: Commented out for memory space conservation reasons. However, a
		// full-fledged implementation might need this.

		// insertionResponses.put(rmsg.getMsgId(), imsg);
	}
}
