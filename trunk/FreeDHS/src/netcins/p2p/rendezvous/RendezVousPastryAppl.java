/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.rendezvous;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import rice.Continuation;
import rice.environment.Environment;
import rice.pastry.Id;
import rice.pastry.PastryNode;
import rice.pastry.client.CommonAPIAppl;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.messaging.Message;
import rice.persistence.LRUCache;
import rice.persistence.MemoryStorage;
import rice.persistence.PersistentStorage;
import rice.persistence.Storage;
import rice.persistence.StorageManagerImpl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class RendezVousPastryAppl extends CommonAPIAppl {
	private RendezVousPastImpl			localPastImpl		= null;
	private PastryIdFactory				idf					= null;
	private Environment					env					= null;
	private Hashtable<String, Double>	estimates			= null;
	private Hashtable<String, Integer>	estimateHops		= null;
	private Set<Long>					pendingInsertions	= null;
	public int							index				= 0;

	/**
	 * @param pn
	 */
	public RendezVousPastryAppl(PastryNode pn) {
		this(pn, null);
	}

	/**
	 * Waits until the local PastryNode is in a ready state.
	 * 
	 * @see rice.pastry.PastryNode#isReady()
	 */
	public void waitForNodeToBeReady() {
		synchronized (thePastryNode) {
			while (thePastryNode.isReady() == false && thePastryNode.joinFailed() == false) {
				try {
					thePastryNode.wait(100);

					if (thePastryNode.joinFailed()) {
						throw new RuntimeException("Could not join the FreePastry ring.  Reason:"
								+ thePastryNode.joinFailedReason());
					}

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.sketches.DistributedSynopsis#estimate(Vector<String>)
	 */
	public DistributedEstimationResult[] estimate(Vector<String> metrics) {
		DistributedEstimationResult ret[] = new DistributedEstimationResult[metrics.size()];
		int curIndex = 0;
		for (String metric : metrics) {
			final String estMetric = metric;
			localPastImpl.lookup((Id)idf.buildId(metric), new Continuation() {
				public void receiveResult(Object result) {
					RendezVousPastContent pc = (RendezVousPastContent)result;
					if (pc == null) {
						System.err.println("Got a null response for " + estMetric);
					} else {
						synchronized (estimates) {
							estimateHops.put(estMetric, pc.getHops());
							estimates.put(estMetric, (double)pc.getContent().size());
							estimates.notifyAll();
						}
					}
				}

				public void receiveException(Exception result) {
					result.printStackTrace();
				}
			});

			synchronized (estimates) {
				while (estimates.containsKey(metric) == false) {
					try {
						estimates.wait(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				double est = estimates.remove(metric);
				int hops = estimateHops.remove(metric);

				ret[curIndex++] = new DistributedEstimationResult(metric, est, hops, 1);
			}
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.sketches.DistributedSynopsis#insert(String, ItemSerializable)
	 */
	public void insert(String metric, ItemSerializable object) {
		RendezVousPastContent pc = new RendezVousPastContent(env, metric, object);
		final Long reqId = pc.getRequesetId();
		synchronized (pendingInsertions) {
			pendingInsertions.add(reqId);
		}
		localPastImpl.insert(pc, new Continuation() {
			public void receiveResult(Object result) {
				Boolean[] results = ((Boolean[])result);
				boolean ok = true;
				for (int ctr = 0; ok == true && ctr < results.length; ctr++) {
					if (results[ctr].booleanValue() == false) {
						System.err.println("Insertion failed!");
						ok = false;
					}
				}
				if (ok == true) {
					synchronized (pendingInsertions) {
						pendingInsertions.remove(reqId);
						if (pendingInsertions.isEmpty() == true)
							pendingInsertions.notify();
					}
				}
			}

			public void receiveException(Exception result) {
				result.printStackTrace();
			}
		});
		//waitForInsertionsToComplete();
	}

	/**
	 * @param pn
	 * @param instance
	 */
	public RendezVousPastryAppl(PastryNode pn, String instance) {
		super(pn, instance);
		this.estimates = new Hashtable<String, Double>(10, (float)0.9);
		this.estimateHops = new Hashtable<String, Integer>(10, (float)0.9);
		this.pendingInsertions = Collections.synchronizedSet(new HashSet<Long>(10, 0.9F));
		this.env = thePastryNode.getEnvironment();
		// used for generating PastContent object Ids.
		// this implements the "hash function" for our DHT
		this.idf = new rice.pastry.commonapi.PastryIdFactory(env);

		// create a different storage root for each node
		String storageDirectory = "./FreePastry-Storage-Root/storage"
				+ thePastryNode.getId().hashCode();

		// create the persistent part
		// Storage stor = new MemoryStorage(idf);
		// Preferring on-disk storage to save on memory requirements.
		Storage stor = null;
		try {
			stor = new PersistentStorage(idf, storageDirectory, 4 * 1024 * 1024, env);
		} catch (IOException e) {
			e.printStackTrace();
		}
		waitForNodeToBeReady();
		this.localPastImpl = new RendezVousPastImpl(thePastryNode, new StorageManagerImpl(idf,
				stor, new LRUCache(new MemoryStorage(idf), 0, env)), 0, "");
	}

	/* (non-Javadoc)
	 * @see rice.pastry.client.CommonAPIAppl#deliver(rice.pastry.Id, rice.pastry.messaging.Message)
	 */
	@Override
	public void deliver(Id key, Message msg) {
	}

	public long getInsertionHits() {
		return localPastImpl.getInsertionHits();
	}

	public long getQueryHits() {
		return localPastImpl.getQueryHits();
	}

	public long getInsertionsHopCount() {
		return 0;
		// TODO Skeleton implementation
	}

	public void waitForInsertionsToComplete() {
		synchronized (pendingInsertions) {
			while (pendingInsertions.isEmpty() == false) {
				try {
					pendingInsertions.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
