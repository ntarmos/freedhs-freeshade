/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.rendezvous;

import java.util.Vector;

import rice.pastry.client.PastryAppl;

import netcins.ItemSerializable;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.DistributedSynopsis;
import netcins.dbms.distributed.sketches.NoSuchMetricException;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class RendezVousImpl extends DistributedSynopsis {

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.sketches.DistributedSynopsis#clear()
	 */
	public void clear() {
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.sketches.DistributedSynopsis#estimate(netcins.p2p.dhs.DHSPastryAppl, java.util.Vector)
	 */
	public DistributedEstimationResult[] estimate(PastryAppl sourceNode, Vector<String> metrics)
			throws NoSuchMetricException {
		RendezVousPastryAppl srcNode = (RendezVousPastryAppl)sourceNode;
		DistributedEstimationResult res[] = srcNode.estimate(metrics);
		return res;
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.sketches.DistributedSynopsis#insert(netcins.p2p.dhs.DHSPastryAppl, java.lang.String, netcins.ItemSerializable)
	 */
	public void insert(PastryAppl sourceNode, String metric, ItemSerializable object) {
		RendezVousPastryAppl srcNode = (RendezVousPastryAppl)sourceNode;
		srcNode.insert(metric, object);
	}

	public String toString() {
		return "[ RendezVousImpl ]";
	}
}
