/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.rendezvous.testing;

import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.DF03SketchImpl;
import netcins.dbms.centralized.sketches.FM85SketchImpl;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSSketchAlgorithm;
import netcins.p2p.rendezvous.RendezVousImpl;
import netcins.p2p.rendezvous.RendezVousPastryAppl;
import netcins.util.QuickMath;
import netcins.util.stats.ZipfGenerator;
import rice.environment.Environment;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.direct.NetworkSimulatorImpl;
import rice.pastry.standard.RandomNodeIdFactory;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DistributedSynopsisSingleMetricTest {
	//private DHSImpl							dhsImpl					= null;
	private RendezVousImpl					rvImpl					= null;
	private PastryIdFactory					idFactory				= null;
	private PastryNodeFactory				factory					= null;
	private Environment						env						= null;
	//private Vector<DHSPastryAppl>			dhsPastryAppls			= null;
	private Vector<RendezVousPastryAppl>	rvPastryAppls			= null;
	private DHSSketchAlgorithm				sketchAlgo				= DHSImpl.defaultSketchAlgo;
	private HashSketchImpl					centralizedHashSketch	= null;
	private static final String				metric					= "Single Metric";

	private int								numItems				= 0;
	private int								qNode					= -1;

	private boolean							isInited				= false;
	private Logger							logger					= null;
	private NetworkSimulatorImpl			simulator				= null;

	public DistributedSynopsisSingleMetricTest(DHSSketchAlgorithm sketchAlgo, int numVecs, int L,
			int retries, Environment env) {
		init(sketchAlgo, numVecs, L, retries, env);
	}

	public DistributedSynopsisSingleMetricTest(int numVecs, int L, int retries, Environment env) {
		init(DHSImpl.defaultSketchAlgo, numVecs, L, retries, env);
	}

	public DistributedSynopsisSingleMetricTest() {
		this(-1, -1, -1, null);
	}

	public void init(DHSSketchAlgorithm algorithm, int numVecs, int L, int retries, Environment env) {
		if (isInited)
			destroy();
		this.env = env;
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null) {
			destroy();
			return;
		}
		//this.dhsPastryAppls = new Vector<DHSPastryAppl>();
		this.rvPastryAppls = new Vector<RendezVousPastryAppl>();
		this.idFactory = new rice.pastry.commonapi.PastryIdFactory(env);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(new RandomNodeIdFactory(env), simulator, env);
		try {
			this.centralizedHashSketch = (algorithm.isUsingDF03() ? new DF03SketchImpl(numVecs, L)
					: new FM85SketchImpl(numVecs, L));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		initDSImpl(algorithm, numVecs, L, retries);
		logger = env.getLogManager().getLogger(this.getClass(), "");
		isInited = true;
	}

	public void initDSImpl(DHSSketchAlgorithm algorithm, int numVecs, int L, int retries) {
		Vector<String> metrics = new Vector<String>();
		metrics.add(DistributedSynopsisSingleMetricTest.metric);
		/*
		 this.sketchAlgo = algorithm;
		 try {
		 this.dhsImpl = DHSImpl.newInstance(algorithm, numVecs, L, retries, metrics, env);
		 } catch (NoSuchAlgorithmException e) {
		 e.printStackTrace();
		 }
		 */
		this.rvImpl = new RendezVousImpl();
		/*
		 System.out.println("Initializing DHSSingleMetricTest with " + this.dhsImpl);
		 for (DHSPastryAppl app : dhsPastryAppls)
		 app.setDHSImpl(this.dhsImpl);
		 */
	}

	private void clearHitsAndCaches() {
		/*
		 for (DHSPastryAppl app : dhsPastryAppls) {
		 app.clearHits();
		 app.clearCache();
		 }
		 */
	}

	private void clearTransientData() {
		/*
		 if (isInited == false)
		 return;
		 for (DHSPastryAppl app : dhsPastryAppls) {
		 app.clearTransientData();
		 }
		 */
	}

	public boolean isInited() {
		return isInited;
	}

	public void destroy() {
		if (isInited == false)
			return;
		isInited = false;
		env.destroy();
		//dhsPastryAppls = null;
		rvPastryAppls = null;
		env = null;
	}

	public void clearData() {
		if (isInited == false)
			return;
		/*
		 for (DHSPastryAppl appl : dhsPastryAppls) {
		 appl.reinitData();
		 }
		 */
		centralizedHashSketch.clear();
		clearHitsAndCaches();
		numItems = 0;
	}

	public void addNodes(int numNodes) {

		System.runFinalization();
		System.gc();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		// this.numNodes = numNodes;
		System.out.println("Populating the overlay with new nodes...");

		PastryNode bootstrapNode = null;
		for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0)
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle)null);
			else
				node = factory.newNode(bootstrapNode.getLocalHandle());

			if (logger.level <= Logger.FINER)
				logger.log("Finished creating new node " + node);

			//DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);
			//dhsAppl.index = curNodeIndex;
			RendezVousPastryAppl rvAppl = new RendezVousPastryAppl(node);
			rvAppl.index = curNodeIndex;

			//dhsPastryAppls.add(dhsAppl);
			rvPastryAppls.add(rvAppl);

			if ((curNodeIndex % 100) == 0)
				System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
			else if ((curNodeIndex % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + numNodes + "/" + numNodes + " done.");

		/*
		 Collections.sort(dhsPastryAppls, new Comparator<DHSPastryAppl>() {
		 public int compare(DHSPastryAppl o1, DHSPastryAppl o2) {
		 return o1.getNodeId().compareTo(o2.getNodeId());
		 }
		 });
		 for (int curNodeIndex = 0; curNodeIndex < dhsPastryAppls.size(); curNodeIndex++) {
		 dhsPastryAppls.get(curNodeIndex).index = curNodeIndex;
		 }
		 */

		Collections.sort(rvPastryAppls, new Comparator<RendezVousPastryAppl>() {
			public int compare(RendezVousPastryAppl o1, RendezVousPastryAppl o2) {
				return o1.getNodeId().compareTo(o2.getNodeId());
			}
		});
		for (int curNodeIndex = 0; curNodeIndex < rvPastryAppls.size(); curNodeIndex++) {
			rvPastryAppls.get(curNodeIndex).index = curNodeIndex;
		}

		/*
		 System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize (pass 1)");
		 for (int i = 0; i < dhsPastryAppls.size(); i++) {
		 dhsPastryAppls.get(i).waitForNodeToBeReady();
		 if ((i % 100) == 0)
		 System.out.print(" " + i + "/" + numNodes + " ");
		 else if ((i % 10) == 0)
		 System.out.print(".");
		 }
		 System.out.println(" " + dhsPastryAppls.size() + "/" + numNodes + " done.");
		 */
		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize (pass 2)");
		for (int i = 0; i < rvPastryAppls.size(); i++) {
			rvPastryAppls.get(i).waitForNodeToBeReady();
			if ((i % 100) == 0)
				System.out.print(" " + i + "/" + numNodes + " ");
			else if ((i % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + rvPastryAppls.size() + "/" + numNodes + " done.");

		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl node generation took " + (endTime - startTime) / 1000 + "\".");
	}

	/*
	 private void doBatchInsertions() {
	 int items = 0;
	 int stopStep = 100000;
	 int nextStop = stopStep;
	 System.out.print(" " + 0 + "/" + numItems + " ");
	 for (int i = 0; i < dhsPastryAppls.size(); i++) {
	 items += dhsPastryAppls.get(i).doBatch();
	 if (items > nextStop) {
	 nextStop += stopStep;
	 waitForInsertionsToComplete();
	 System.out.print(" " + items + "/" + numItems + " ");
	 }
	 }
	 waitForInsertionsToComplete();
	 System.out.println(" " + numItems + "/" + numItems + " done.");
	 }
	 */

	private void waitForInsertionsToComplete() {
		/*
		 for (int i = 0; i < dhsPastryAppls.size(); i++) {
		 DHSPastryAppl curAppl = dhsPastryAppls.get(i);
		 curAppl.waitForInsertionsToComplete();
		 curAppl.clearInsertionResponses();
		 }
		 */
		for (int i = 0; i < rvPastryAppls.size(); i++) {
			RendezVousPastryAppl curAppl = rvPastryAppls.get(i);
			curAppl.waitForInsertionsToComplete();
		}
	}

	/*
	 public void addDataBatch(int[] tuples) {
	 System.runFinalization();
	 System.gc();
	 clearHitsAndCaches();
	 long startTime = System.currentTimeMillis(), endTime = 0;

	 if (isInited == false)
	 throw new RuntimeException("Uninitialized test instance!");
	 this.numItems = tuples.length;

	 System.out.println("Injecting " + numItems + " new data tuples...");
	 for (int data = 0; data < numItems; data++) {
	 BasicItem item = new BasicItem((Id)idFactory.buildRandomId(env.getRandomSource()),
	 tuples[data]);
	 dhsImpl.batchInsert(dhsPastryAppls.get(data % dhsPastryAppls.size()),
	 DistributedSynopsisSingleMetricTest.metric, item);
	 centralizedHashSketch.insert(item);
	 }
	 doBatchInsertions();

	 endTime = System.currentTimeMillis();
	 System.out.println("DHSImpl data generation took " + (endTime - startTime) / 1000 + "\".");
	 System.runFinalization();
	 System.gc();
	 long numHits = 0;
	 long numInsertionsHops = 0;
	 System.out.print("Insertion Hits: ");
	 for (DHSPastryAppl curAppl : dhsPastryAppls) {
	 System.out.print(" " + curAppl.getInsertionHits());
	 numHits += curAppl.getInsertionHits();
	 numInsertionsHops += curAppl.getInsertionsHopCount();
	 }
	 System.out.println();
	 System.out.println("Node insertion hits using batch insertions: " + numHits
	 + ", hop count: " + numInsertionsHops);
	 }
	 */

	public void addData(int[] tuples) {
		System.runFinalization();
		System.gc();
		clearHitsAndCaches();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems = tuples.length;
		int stepBig = 100;
		int stepSmall = stepBig / 10;

		System.out.println("Injecting " + numItems + " new data tuples...");
		for (int data = 0; data < numItems; data++) {
			BasicItem item = new BasicItem((Id)idFactory.buildRandomId(env.getRandomSource()),
					tuples[data]);
			//dhsImpl.insert(dhsPastryAppls.get(data % dhsPastryAppls.size()),
			//		DistributedSynopsisSingleMetricTest.metric, item);
			rvImpl.insert(rvPastryAppls.get(data % rvPastryAppls.size()),
					DistributedSynopsisSingleMetricTest.metric, item);
			centralizedHashSketch.insert(item);

			if ((data % stepBig) == 0) {
				if (data > 0)
					waitForInsertionsToComplete();
				System.out.print(" " + data + "/" + numItems + " ");
			} else if ((data % stepSmall) == 0) {
				System.out.print(".");
			}

		}
		waitForInsertionsToComplete();

		System.out.println(" done. ");

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl data generation took " + (endTime - startTime) / 1000 + "\".");
		System.runFinalization();
		System.gc();
		long numHits = 0;
		long numInsertionsHops = 0;
		/*
		 System.out.print("DHS Insertion Hits: ");
		 for (DHSPastryAppl curAppl : dhsPastryAppls) {
		 System.out.print(" " + curAppl.getInsertionHits());
		 numHits += curAppl.getInsertionHits();
		 numInsertionsHops += curAppl.getInsertionsHopCount();
		 }
		 System.out.println();
		 System.out.println("DHS Node insertion hits: " + numHits + ", hop count: " + numInsertionsHops);
		 numHits = 0;
		 numInsertionsHops = 0;
		 */
		System.out.print("Rendez-Vous Insertion Hits: ");
		for (RendezVousPastryAppl curAppl : rvPastryAppls) {
			System.out.print(" " + curAppl.getInsertionHits());
			numHits += curAppl.getInsertionHits();
			numInsertionsHops += curAppl.getInsertionsHopCount();
		}
		System.out.println();
		System.out.println("Rendez-Vous Node insertion hits: " + numHits + ", hop count: "
				+ numInsertionsHops);
	}

	public void query() {
		System.runFinalization();
		System.gc();
		this.clearTransientData();
		long startTime = System.currentTimeMillis(), endTime;
		//System.out.println("DHS Querying node: " + dhsPastryAppls.get(qNode).getNodeId() + ", node #"
		//		+ qNode);
		System.out.println("Rendez-Vous Querying node: " + rvPastryAppls.get(qNode).getNodeId()
				+ ", node #" + qNode);
		Vector<String> metrics = new Vector<String>();
		metrics.add(DistributedSynopsisSingleMetricTest.metric);
		System.out.println(sketchAlgo + " centralized estimation: "
				+ QuickMath.doubleToString(centralizedHashSketch.estimate()));
		if (logger.level <= Logger.FINE) {
			logger.log(sketchAlgo + " centralized HS :: " + "\n" + centralizedHashSketch + "\n"
					+ centralizedHashSketch.toStringFull());
		}
		DistributedEstimationResult[] distRes = null;
		try {
			//distRes = dhsImpl.estimate(dhsPastryAppls.get(qNode), metrics);
			//System.out.println(dhsImpl + ", distributed estimation: " + distRes[0]);
			distRes = rvImpl.estimate(rvPastryAppls.get(qNode), metrics);
			System.out.println(rvImpl + ", distributed estimation: " + distRes[0]);
		} catch (NoSuchMetricException e) {
			e.printStackTrace();
		}

		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl query took " + (endTime - startTime) / 1000 + "\".");
		long numHits = 0;
		/*
		 System.out.print("DHS Query hits: ");
		 for (DHSPastryAppl curAppl : dhsPastryAppls) {
		 long curHits = curAppl.getQueryHits();
		 System.out.print(" " + curHits);
		 numHits += curHits;
		 }
		 System.out.println();
		 System.out.println("DHS Node query hits: " + numHits);
		 numHits = 0;
		 */
		System.out.print("Rendez-Vous Query hits: ");
		for (RendezVousPastryAppl curAppl : rvPastryAppls) {
			long curHits = curAppl.getQueryHits();
			System.out.print(" " + curHits);
			numHits += curHits;
		}
		System.out.println();
		System.out.println("Rendez-Vous Node query hits: " + numHits);
	}

	public static void main(String[] args) {

		int numNodes = 0, numTuples = 0, numVecs = 0, L = 0, retries = 0, numValues = 0, iterations = 1;
		double theta = 0;

		try {
			numNodes = Integer.parseInt(args[0]);
			numTuples = Integer.parseInt(args[1]);
			numVecs = Integer.parseInt(args[2]);
			L = Integer.parseInt(args[3]);
			retries = Integer.parseInt(args[4]);
			numValues = Integer.parseInt(args[5]);
			theta = Double.parseDouble(args[6]);
			iterations = Integer.parseInt(args[7]);
		} catch (NumberFormatException e) {
			System.out.println("Usage: java [-cp FreePastry-<version>.jar]");
			System.out.println("\tnetcins.p2p.dhs.testing.DHSSingleMetricTest <numNodes> <numItems> <numVecs> <L> <retries> <numValues> <zipf theta> <iterations>");
			System.exit(0);
		}

		long startTime = System.currentTimeMillis(), endTime = 0;

		DistributedSynopsisSingleMetricTest test = null;
		int seed = (int)QuickMath.floor(Math.random() * (double)Integer.MAX_VALUE);
		// int seed = 1725674223;

		System.out.println("\n" + " --- Begin Simulation --- ");
		ZipfGenerator zipf = new ZipfGenerator(numValues, theta, seed);
		int[] data = new int[numTuples];
		for (int i = 0; i < numTuples; i++)
			data[i] = zipf.nextRank();

		//DHSSketchAlgorithm testAlgos[] = DHSSketchAlgorithm.values();
		DHSSketchAlgorithm testAlgos[] = new DHSSketchAlgorithm[1];
		testAlgos[0] = new DHSSketchAlgorithm();

		System.out.println(" --- Seed: " + seed + ", domain: " + numValues + ", Zipf theta: "
				+ theta + " ---");
		try {
			Environment env = Environment.directEnvironment(seed);
			test = new DistributedSynopsisSingleMetricTest(new DHSSketchAlgorithm(), numVecs, L,
					retries, env);
			test.addNodes(numNodes);
			System.out.println("\n" + " ---" + "\n");
			int nVecs = numVecs;
			// for (int nVecs = numVecs; nVecs >= 32; nVecs /= 2) {
			test.initDSImpl(testAlgos[0], nVecs, L, retries);
			test.clearData();
			test.addData(data);
			env.getTimeSource().sleep(5000);

			System.out.println("\n" + " ---" + "\n");

			for (DHSSketchAlgorithm algo : testAlgos) {
				test.initDSImpl(algo, nVecs, L, retries);
				for (int iter = 0; iter < iterations; iter++) {
					test.qNode = env.getRandomSource().nextInt(numNodes);
					System.out.println("Testing " + algo + ", iteration: " + iter
							+ ", querying node # " + test.qNode + " ...");
					test.query();
					System.out.println("\n" + " ---" + "\n");
				}
			}
			// }
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: " + (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}
}
