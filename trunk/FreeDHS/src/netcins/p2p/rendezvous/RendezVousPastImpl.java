/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.rendezvous;

import java.io.IOException;

import rice.p2p.commonapi.Application;
import rice.p2p.commonapi.Id;
import rice.p2p.commonapi.Message;
import rice.p2p.commonapi.Node;
import rice.p2p.past.PastImpl;
import rice.p2p.past.PastPolicy;
import rice.p2p.past.messaging.InsertMessage;
import rice.p2p.past.messaging.LookupMessage;
import rice.p2p.past.messaging.PastMessage;
import rice.p2p.past.rawserialization.SocketStrategy;
import rice.pastry.NodeHandle;
import rice.pastry.routing.RouteMessage;
import rice.persistence.Cache;
import rice.persistence.StorageManager;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
class RendezVousPastImpl extends PastImpl implements Application {

	private long	insertionHits	= 0;
	private long	queryHits		= 0;

	public RendezVousPastImpl(Node node, StorageManager manager, int replicas, String instance) {
		super(node, manager, replicas, instance);
	}

	public RendezVousPastImpl(Node node, StorageManager manager, int replicas, String instance,
			PastPolicy policy) {
		super(node, manager, replicas, instance, policy);
	}

	public RendezVousPastImpl(Node node, StorageManager manager, Cache backup, int replicas,
			String instance, PastPolicy policy, StorageManager trash) {
		super(node, manager, backup, replicas, instance, policy, trash);
	}

	public RendezVousPastImpl(Node node, StorageManager manager, Cache backup, int replicas,
			String instance, PastPolicy policy, StorageManager trash, boolean useOwnSocket) {
		super(node, manager, backup, replicas, instance, policy, trash, useOwnSocket);
	}

	public RendezVousPastImpl(Node node, StorageManager manager, Cache backup, int replicas,
			String instance, PastPolicy policy, StorageManager trash, SocketStrategy strategy) {
		super(node, manager, backup, replicas, instance, policy, trash, strategy);
	}

	public void deliver(Id id, Message message) {
		final PastMessage msg = (PastMessage)message;
		if (msg.isResponse() == false) {
			if (msg instanceof InsertMessage
					&& this.getLocalNodeHandle().getId().equals(msg.getSource().getId()) == false) {
				InsertMessage imsg = (InsertMessage)msg;
				RendezVousPastContent rvpc = (RendezVousPastContent)imsg.getContent();
				if (rvpc != null)
					rvpc.incHops();
				insertionHits++;
			} else if (msg instanceof LookupMessage)
				queryHits++;
		}
		super.deliver(id, message);
	}

	public long getInsertionHits() {
		return insertionHits;
	}

	public long getQueryHits() {
		return queryHits;
	}

	public boolean forward(rice.p2p.commonapi.RouteMessage message) {
		RouteMessage rm = (RouteMessage)message;
		Message internal = null;
		try {
			internal = rm.getMessage(endpoint.getDeserializer());
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}

		if (internal instanceof InsertMessage) {
			if (rm.getNextHop() != null
					&& this.getLocalNodeHandle().getId().equals(rm.getNextHop().getNodeId()) == false) {
				InsertMessage imsg = (InsertMessage)internal;
				RendezVousPastContent rvpc = (RendezVousPastContent)imsg.getContent();
				if (rvpc != null)
					rvpc.incHops();
				NodeHandle sender = rm.getSender();
				rm.setMessage(internal);
				//message = new RouteMessage((rice.pastry.Id)rm.getTarget(), (rice.pastry.messaging.Message)imsg, rm.nextHop, rm.getOptions(), (byte)1);
				//((RouteMessage)message).setSender(sender);
			}
		} else if (internal instanceof LookupMessage) {
		}
		return super.forward(message);
	}
}

/*
 class RendezVousInsertMessage extends InsertMessage implements rice.p2p.commonapi.Message {
 private static final long	serialVersionUID	= 7289296387205940926L;
 private long hops = 0;

 public RendezVousInsertMessage(int uid, PastContent content, rice.p2p.commonapi.NodeHandle source, Id dest) {
 super(uid, content, source, dest);
 }
 
 public RendezVousInsertMessage(InsertMessage imsg) {
 this(imsg.getUID(), imsg.getContent(), imsg.getSource(), imsg.getDestination());
 }
 
 @Override
 public void addHop(rice.p2p.commonapi.NodeHandle handle) {
 RendezVousPastContent pc = (RendezVousPastContent)(((JavaSerializedPastContent)this.content).content);
 pc.incHops();
 }
 
 public long getNumHops() {
 RendezVousPastContent pc = (RendezVousPastContent)(((JavaSerializedPastContent)this.content).content);
 return pc.getHops();
 }
 }
 */

/*
 class RendezVousLookupMessage extends LookupMessage implements rice.p2p.commonapi.Message {
 private static final long	serialVersionUID	= 6410427933186981486L;
 private long hops = 0;

 public RendezVousLookupMessage(int uid, Id id, rice.p2p.commonapi.NodeHandle source, Id dest) {
 super(uid, id, source, dest);
 }

 
 public RendezVousLookupMessage(LookupMessage imsg) {
 this(imsg.getUID(), imsg.getId(), imsg.getSource(), imsg.getDestination());
 }
 
 @Override
 public void addHop(rice.p2p.commonapi.NodeHandle handle) {
 hops ++;
 }
 
 public long getNumHops() {
 return hops;
 }
 }
 */
