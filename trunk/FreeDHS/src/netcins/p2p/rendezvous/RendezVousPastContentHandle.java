/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.rendezvous;

import java.io.IOException;

import rice.p2p.commonapi.Endpoint;
import rice.p2p.commonapi.Id;
import rice.p2p.commonapi.NodeHandle;
import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.past.ContentHashPastContentHandle;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class RendezVousPastContentHandle extends ContentHashPastContentHandle {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 2238520611778835487L;

	/**
	 * @param nh
	 * @param id
	 */
	public RendezVousPastContentHandle(NodeHandle nh, Id id) {
		super(nh, id);
	}

	/**
	 * @param buf
	 * @param endpoint
	 * @throws IOException
	 */
	public RendezVousPastContentHandle(InputBuffer buf, Endpoint endpoint) throws IOException {
		super(buf, endpoint);
	}

}
