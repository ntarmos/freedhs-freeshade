/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.rendezvous;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import netcins.ItemSerializable;
import netcins.dbms.BasicItem;
import rice.environment.Environment;
import rice.p2p.commonapi.Id;
import rice.p2p.past.ContentHashPastContent;
import rice.p2p.past.Past;
import rice.p2p.past.PastContent;
import rice.p2p.past.PastContentHandle;
import rice.p2p.past.PastException;
import rice.pastry.commonapi.PastryIdFactory;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
class RendezVousPastContent extends ContentHashPastContent {
	private static final long	serialVersionUID	= -1839747787894631455L;
	private Set<BasicItem>		content				= null;
	private Environment			env					= null;
	private long				timestamp			= 0;
	private long				requestId			= 0;
	private int					hops				= 0;

	public RendezVousPastContent(Environment env, String metric, ItemSerializable item) {
		super((Id)(new PastryIdFactory(env)).buildId(metric));
		this.env = env;
		this.content = Collections.synchronizedSet(new HashSet<BasicItem>(1, 0.9F));
		this.content.add((BasicItem)item);
		this.timestamp = env.getTimeSource().currentTimeMillis();
		this.requestId = env.getRandomSource().nextLong();
	}

	public RendezVousPastContent(RendezVousPastContent other) {
		super(other.myId);
		this.content = Collections.synchronizedSet(new HashSet<BasicItem>(1, 0.9F));
		if (other != null)
			this.content.addAll(other.content);
		this.timestamp = other.timestamp;
		this.requestId = other.requestId;
		this.env = other.env;
		this.hops = other.hops;
	}

	public long getRequesetId() {
		return requestId;
	}

	public Set<BasicItem> getContent() {
		return content;
	}

	public long getTimestamp() {
		return timestamp;
	}

	private void merge(RendezVousPastContent other) {
		synchronized (content) {
			this.content.addAll(other.content);
		}
		if (this.hops < other.hops)
			this.hops = other.hops;
	}

	@Override
	public PastContent checkInsert(rice.p2p.commonapi.Id id, PastContent existingContent)
			throws PastException {
		// only allow correct content hash key
		if (!id.equals(getId())) {
			throw new PastException("DHSPastContent: can't insert, content hash incorrect");
		}

		if (existingContent == null)
			return this;

		if ((existingContent instanceof RendezVousPastContent) == false) {
			throw new PastException(
					"RendezVousPastContent: can't insert, content class incorrect: "
							+ existingContent.getClass().getSimpleName());
		}

		RendezVousPastContent ret = new RendezVousPastContent(this);
		ret.merge((RendezVousPastContent)existingContent);

		return ret;
	}

	public int getHops() {
		return hops;
	}

	public void setHops(int hops) {
		this.hops = hops;
	}

	public void incHops() {
		this.hops++;
	}

	public PastContentHandle getHandle(Past local) {
		return new RendezVousPastContentHandle(local.getLocalNodeHandle(), getId());
	}
}
