/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.testing;

import netcins.p2p.dhs.DHSSketchAlgorithm;

public class DHSGenericTestParams {
	public String				curClass	= null;
	public int					numNodes	= 0;
	public int					numTuples	= 0;
	public int					numVecs		= 0;
	public int					L			= 0;
	public int					retries		= 0;
	public int					replicas	= 0;
	public int					numValues	= 0;
	public int					iterations	= 1;
	public double				theta		= 0;
	public DHSSketchAlgorithm	algorithm	= new DHSSketchAlgorithm().setUsingDF03()
												.setUsingSequentialBitProbing()
												.setUsingRecursiveProcessing()
												.setNotUsingResultCaching().setNotUsingShortcuts();

	private void usage() {
		System.err.println("Usage: java [-cp FreePastry-<version>.jar] " + curClass + "\n"
			+ "\t<numNodes> <numItems> <numVecs> <L> <retries> "
			+ "<replicas> <numValues> <zipf theta> <iterations>\n"
			+ "\t[df03|fm85] [recursive|iterative] [sequential|bsearch] "
			+ "[caching|nocaching] [batching|nobatching] [shortcuts|noshortcuts]");
		System.exit(0);
	}

	public DHSGenericTestParams(String curClass, String[] args) {
		this.curClass = curClass;
		if (args.length < 9)
			usage();
		try {
			numNodes = Integer.parseInt(args[0]);
			numTuples = Integer.parseInt(args[1]);
			numVecs = Integer.parseInt(args[2]);
			L = Integer.parseInt(args[3]);
			retries = Integer.parseInt(args[4]);
			replicas = Integer.parseInt(args[5]);
			numValues = Integer.parseInt(args[6]);
			theta = Double.parseDouble(args[7]);
			iterations = Integer.parseInt(args[8]);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			usage();
		}

		for (int i = 9; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("df03"))
				algorithm.setUsingDF03();
			else if (args[i].equalsIgnoreCase("fm85"))
				algorithm.setUsingFM85();
			else if (args[i].equalsIgnoreCase("recursive"))
				algorithm.setUsingRecursiveProcessing();
			else if (args[i].equalsIgnoreCase("iterative"))
				algorithm.setUsingIterativeProcessing();
			else if (args[i].equalsIgnoreCase("sequential"))
				algorithm.setUsingSequentialBitProbing();
			else if (args[i].equalsIgnoreCase("bsearch"))
				algorithm.setUsingBinarySearchBitProbing();
			else if (args[i].equalsIgnoreCase("caching"))
				algorithm.setUsingResultCaching();
			else if (args[i].equalsIgnoreCase("nocaching"))
				algorithm.setNotUsingResultCaching();
			else if (args[i].equalsIgnoreCase("batching"))
				algorithm.setUsingBatchInsertions();
			else if (args[i].equalsIgnoreCase("nobatching"))
				algorithm.setNotUsingBatchInsertions();
			else if (args[i].equalsIgnoreCase("shortcuts"))
				algorithm.setUsingShortcuts();
			else if (args[i].equalsIgnoreCase("noshortcuts"))
				algorithm.setNotUsingShortcuts();
			else {
				System.err.println("Unknown keyword: " + args[i]);
				usage();
			}
		}
	}
}
