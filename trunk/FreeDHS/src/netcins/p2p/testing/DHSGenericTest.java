/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.p2p.testing;

import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithm;
import netcins.util.QuickMath;
import rice.environment.Environment;
import rice.environment.logging.Logger;
import rice.pastry.Id;
import rice.pastry.NodeHandle;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.direct.NetworkSimulatorImpl;
import rice.pastry.standard.RandomNodeIdFactory;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DHSGenericTest {
	protected NetworkSimulatorImpl	simulator	= null;
	protected PastryIdFactory		idFactory	= null;
	protected PastryNodeFactory		factory		= null;
	protected Environment			env			= null;
	protected Logger				logger		= null;

	protected DHSImpl				dhsImpl		= null;
	protected Vector<DHSPastryAppl>	pastryAppls	= null;
	protected DHSSketchAlgorithm	sketchAlgo	= DHSImpl.defaultSketchAlgo;

	protected int					numItems	= 0;
	private int						qNode		= -1;
	protected boolean				isInited	= false;

	/**
	 * Clears hits counters and hash sketch caches.
	 */
	public void clearHitsAndCaches() {
		for (DHSPastryAppl app : pastryAppls) {
			app.clearHits();
			app.clearCache();
		}
	}

	public void setQNode(int qNode) {
		if (qNode < 0 || pastryAppls == null || pastryAppls.size() <= qNode)
			throw new RuntimeException("Invalid query node value");
		this.qNode = qNode;
	}

	public int getQNode() {
		return qNode;
	}

	/**
	 * Clears all node transient data (local hash sketch caches, etc.)
	 */
	public void clearTransientData() {
		if (isInited == false)
			return;
		for (DHSPastryAppl app : pastryAppls) {
			app.clearTransientData();
		}
	}

	/**
	 * Checks if the current instance has been properly initialized.
	 * @return true if properly initialized, false otherwise
	 */
	public boolean isInited() {
		return isInited;
	}

	/**
	 * Destroys the current test instance and stops the simulator.
	 */
	public void destroy() {
		if (isInited == false)
			return;
		isInited = false;
		env.destroy();
		pastryAppls = null;
		env = null;
	}

	public abstract void clearLocalEstimator();

	/**
	 * Clears all data stored on nodes (DHS data, data caches, hit counters, etc.)
	 */
	public void clear() {
		if (isInited == false)
			return;
		for (DHSPastryAppl appl : pastryAppls) {
			appl.reinitData();
		}
		clearLocalEstimator();
		clearHitsAndCaches();
		numItems = 0;
	}

	/**
	 * Adds nodes to the overlay
	 * @param numNodes number of nodes to add
	 */
	public void addNodes(int numNodes) {

		if (numNodes == 0)
			return;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		System.out.println("Populating the overlay with new nodes...");
		long startTime = System.currentTimeMillis(), endTime = 0;

		// Lower the simulation speed, since node joins/leaves are computationally intensive tasks
		simulator.setMaxSpeed(50.0f);

		if (numNodes < 0) { // If negative, remove this many nodes.
			for (int i = 0; i < -numNodes && pastryAppls.size() > 0; i++) {
				int curNodeIndex = env.getRandomSource().nextInt(pastryAppls.size());
				DHSPastryAppl curAppl = pastryAppls.remove(curNodeIndex);
				curAppl.getNodeHandle().getLocalNode().destroy();
			}
		} else { // If positive, add this many nodes.
			NodeHandle bootstrapHandle = (pastryAppls.size() > 0
					? pastryAppls.get(0).getNodeHandle() : null);
			for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
				PastryNode node = null;
				if (bootstrapHandle == null) {
					node = factory.newNode((rice.pastry.NodeHandle)null);
					bootstrapHandle = node.getLocalHandle();
				} else {
					node = factory.newNode(bootstrapHandle);
				}

				if (logger.level <= Logger.FINER)
					logger.log("Finished creating new node " + node);

				DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);
				dhsAppl.index = curNodeIndex;

				pastryAppls.add(dhsAppl);

				if ((curNodeIndex % 100) == 0)
					System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
				else if ((curNodeIndex % 10) == 0)
					System.out.print(".");
			}
			System.out.println(" " + numNodes + "/" + numNodes + " done.");

			Collections.sort(pastryAppls, new Comparator<DHSPastryAppl>() {
				public int compare(DHSPastryAppl o1, DHSPastryAppl o2) {
					return o1.getNodeId().compareTo(o2.getNodeId());
				}
			});

			System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
			for (int i = 0; i < pastryAppls.size(); i++) {
				pastryAppls.get(i).waitForNodeToBeReady();
				if ((i % 100) == 0)
					System.out.print(" " + i + "/" + numNodes + " ");
				else if ((i % 10) == 0)
					System.out.print(".");
			}
			System.out.println(" " + pastryAppls.size() + "/" + numNodes + " done.");
		}

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl node generation took " + (endTime - startTime) / 1000 + "\".");

		Collections.sort(pastryAppls, new Comparator<DHSPastryAppl>() {
			public int compare(DHSPastryAppl o1, DHSPastryAppl o2) {
				return o1.getNodeId().compareTo(o2.getNodeId());
			}
		});

		for (int curNodeIndex = 0; curNodeIndex < pastryAppls.size(); curNodeIndex++) {
			pastryAppls.get(curNodeIndex).index = curNodeIndex;
		}

		simulator.setFullSpeed();
	}

	/**
	 * Waits for all insertions to complete and clears any related status data.
	 */
	protected void waitForInsertionsToComplete() {
		for (int i = 0; i < pastryAppls.size(); i++) {
			DHSPastryAppl curAppl = pastryAppls.get(i);
			curAppl.waitForInsertionsToComplete();
			curAppl.clearInsertionResponses();
		}
	}

	public DHSGenericTest(DHSSketchAlgorithm sketchAlgo, int numVecs, int L, int retries,
			int replicas, Environment env) {
		init(sketchAlgo, numVecs, L, retries, replicas, env);
	}

	/*
	 public DHSGenericTest(int numVecs, int L, int retries, int replicas, Environment env) {
	 init(DHSImpl.defaultSketchAlgo, numVecs, L, retries, replicas, env);
	 }
	 
	 public DHSGenericTest() {
	 this(-1, -1, -1, 0, null);
	 }
	 */

	protected abstract void initLocalEstimator(DHSSketchAlgorithm algorithm, int numVecs, int L);

	public void init(DHSSketchAlgorithm algorithm, int numVecs, int L, int retries, int replicas,
			Environment env) {
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null)
			return;
		if (isInited)
			this.destroy();

		this.env = env;
		this.pastryAppls = new Vector<DHSPastryAppl>();
		this.idFactory = new rice.pastry.commonapi.PastryIdFactory(env);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(new RandomNodeIdFactory(env), simulator, env);
		this.numItems = 0;
		this.logger = env.getLogManager().getLogger(this.getClass(), "");
		this.sketchAlgo = algorithm;
		try {
			this.dhsImpl = DHSImpl.newInstance(algorithm, numVecs, L, retries, replicas, null, env);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		System.out.println("Initializing " + this.getClass().getSimpleName() + " with "
				+ this.dhsImpl);
		for (DHSPastryAppl app : pastryAppls)
			app.setDHSImpl(this.dhsImpl);

		this.initLocalEstimator(algorithm, numVecs, L);
		this.isInited = true;
	}

	/**
	 * Complete any remaining batch insertions.
	 */
	protected void doBatchInsertions() {
		int stopStep = 100000;
		int nextStop = stopStep;
		System.out.print(" 0/" + numItems + " ");
		for (int i = 0, items = 0; i < pastryAppls.size(); i++) {
			items += pastryAppls.get(i).doBatch();
			if (items > nextStop) {
				nextStop += stopStep;
				waitForInsertionsToComplete();
				System.out.print(" " + items + "/" + numItems + " ");
			}
		}
		waitForInsertionsToComplete();
		System.out.println(" " + numItems + "/" + numItems + " done.");
	}

	protected abstract void addItemsBatch(DHSPastryAppl curAppl, Vector<BasicItem> tuples);

	protected abstract void addItemsCentralized(Vector<BasicItem> tuples);

	/**
	 * Adds the given data items in batch mode.
	 * @param tuples the tuples to add
	 */
	public void addDataBatch(int[] tuples) {

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems += tuples.length;

		System.out.println("Injecting " + numItems + " new data tuples...");

		long startTime = System.currentTimeMillis(), endTime = 0;
		int breaks = 1;
		int itemsPerIteration = (int)QuickMath.ceil((double)numItems / (double)pastryAppls.size()
				/ (double)breaks);

		for (int node = 0, data = 0; node < (breaks * pastryAppls.size()) && data < numItems; node++, data += itemsPerIteration) {
			DHSPastryAppl curNode = pastryAppls.get(node / breaks);
			Vector<BasicItem> curNodeItems = new Vector<BasicItem>();
			for (int cnData = 0; cnData < itemsPerIteration && (cnData + data) < numItems; cnData++)
				curNodeItems.add(new BasicItem(curNode.getNodeId(),
						(Id)idFactory.buildRandomId(env.getRandomSource()), tuples[data]));
			addItemsBatch(curNode, curNodeItems);
			addItemsCentralized(curNodeItems);
			curNode.doBatch();

			if (node > 0 && node % (100 * breaks) == 0) {
				waitForInsertionsToComplete();
				System.out.print(" " + (data + itemsPerIteration > numItems ? numItems : data)
						+ "/" + numItems + " ");
			} else if (node % (10 * breaks) == 0)
				System.out.print(".");
		}
		waitForInsertionsToComplete();
		System.out.println(" done. ");

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl data generation took " + (endTime - startTime) / 1000 + "\".");

		printInsertionCounters();
	}

	protected abstract void addItemsNormal(DHSPastryAppl curAppl, Vector<BasicItem> tuples);

	/**
	 * Adds the given data items in normal mode.
	 * @param tuples the tuples to add
	 */
	public void addData(int[] tuples) {

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems += tuples.length;

		System.out.println("Injecting " + numItems + " new data tuples...");
		long startTime = System.currentTimeMillis(), endTime = 0;
		for (int data = 0; data < numItems; data++) {
			DHSPastryAppl curNode = pastryAppls.get(data % pastryAppls.size());
			Vector<BasicItem> items = new Vector<BasicItem>();
			items.add(new BasicItem(curNode.getNodeId(),
					(Id)idFactory.buildRandomId(env.getRandomSource()), tuples[data]));
			addItemsNormal(curNode, items);
			addItemsCentralized(items);

			if ((data % 100000) == 0) {
				if (data > 0)
					waitForInsertionsToComplete();
				System.out.print(" " + data + "/" + numItems + " ");
			} else if ((data % 10000) == 0) {
				System.out.print(".");
			}
		}
		waitForInsertionsToComplete();

		System.out.println(" done. ");

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl data generation took " + (endTime - startTime) / 1000 + "\".");

		printInsertionCounters();
	}

	public void printInsertionCounters() {
		long numHits = 0;
		long numInsertionsHops = 0;
		System.out.print("Insertion Hits: ");
		for (DHSPastryAppl curAppl : pastryAppls) {
			System.out.print(" " + curAppl.getInsertionHits());
			numHits += curAppl.getInsertionHits();
			numInsertionsHops += curAppl.getInsertionsHopCount();
		}
		System.out.println();
		System.out.println("Node insertion hits: " + numHits + ", hop count: " + numInsertionsHops);
	}

	protected abstract double estimateCentralizedEstimator();

	protected abstract DistributedEstimationResult estimateDistributedEstimator(
			DHSPastryAppl curAppl);

	public void query() {
		System.out.println("Querying node: " + pastryAppls.get(qNode).getNodeId() + ", node #"
				+ qNode);

		long startTime = System.currentTimeMillis(), endTime;
		DistributedEstimationResult distEst = estimateDistributedEstimator(pastryAppls.get(qNode));
		double centEst = estimateCentralizedEstimator();
		endTime = System.currentTimeMillis();

		System.out.println("DHSImpl query took " + (endTime - startTime) / 1000 + "\".");
		System.out.println(sketchAlgo + " centralized estimation: "
				+ QuickMath.doubleToString(centEst));
		System.out.println(sketchAlgo + ", distributed estimation: " + distEst);
		System.out.println("Estimation error: "
				+ (100.0 * QuickMath.abs(distEst.getDistributedEstimation() - centEst) / centEst)
				+ " %");

		printQueryCounters();
	}

	public void printQueryCounters() {
		long numHits = 0;
		System.out.print("Query hits: ");
		for (DHSPastryAppl curAppl : pastryAppls) {
			long curHits = curAppl.getQueryHits();
			System.out.print(" " + curHits);
			numHits += curHits;
		}
		System.out.println();
		System.out.println("Node query hits: " + numHits);
	}
}
