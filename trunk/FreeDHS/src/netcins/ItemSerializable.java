/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins;

import java.io.IOException;
import java.io.Serializable;

import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;

/**
 * Interface defining a set of methods to be used when an item is to be sent
 * from a node over the wire to a remote node.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public interface ItemSerializable extends Serializable {
	/**
	 * Returns a byte array representation of the current object. Implementors
	 * should also provide a constructor with a byte array as its sole argument,
	 * reconstructing the item from its serialized form.
	 * 
	 * @return a byte array representation of the current object
	 */
	public byte[] serialize();

	/**
	 * Serializes the object to the supplied
	 * {@link rice.p2p.commonapi.rawserialization.OutputBuffer}.
	 * 
	 * @param buf the OutputBuffer
	 * @throws IOException
	 */
	public void serialize(OutputBuffer buf) throws IOException;

	/**
	 * Deserializes the object from the supplied byte array.
	 * @param buf the input byte array
	 * @return an ItemSerializable instance
	 * @throws IOException
	 */
	public ItemSerializable deserialize(byte[] buf) throws IOException;

	/**
	 * Deserializes the object from the supplied
	 * {@link rice.p2p.commonapi.rawserialization.InputBuffer}.
	 * 
	 * @param buf the InputBuffer
	 * @throws IOException
	 */
	public void deserialize(InputBuffer buf) throws IOException;
}
