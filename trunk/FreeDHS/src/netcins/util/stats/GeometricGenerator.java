/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$ 
 */

package netcins.util.stats;

import java.util.Random;

import netcins.util.QuickMath;

/**
 * A random number generator, following the Geometric distribution. This
 * implementation needs to keep an array of the CDF of G(1-p) for all possible
 * ranks, amounting to a memory requirement of 8 times the number of ranks in
 * bytes (i.e. 8Mbytes for 1M ranks).
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class GeometricGenerator {
	private double allValues[] = null;

	private double p = 0.0;

	private Random rng = null;

	/**
	 * Constructor for ZipfGenerator.
	 * 
	 * @param domain
	 *            The number of values in the domain.
	 * @param p
	 *            The parameter of the Geometric distribution.
	 */
	public GeometricGenerator(int domain, double p) {
		double norm = 0;
		this.p = p;
		this.allValues = new double[domain];

		for (int i = 0; i < allValues.length; i++) {
			allValues[i] = 1.0 - Math.pow(1 - p, i + 1);
			//System.out.println(allValues[i]);
		}

		rng = new Random();
	}

	/**
	 * Constructor for GeometricGenerator.
	 * 
	 * @param domain
	 *            The number of values in the domain.
	 * @param p
	 *            The parameter of the Geometric distribution.
	 * @param seed
	 *            The seed for the internal random number generator.
	 */
	public GeometricGenerator(int domain, double p, long seed) {
		this(domain, p);
		rng = new Random(seed);
	}

	/**
	 * Returns the 'p' parameter of the current Geometric distribution.
	 * 
	 * @return The 'p' parameter of the current Geometric distribution.
	 */
	public double getP() {
		return p;
	}

	/**
	 * Returns the cardinality of the domain of ranks.
	 * 
	 * @return The cardinality of the domain of ranks.
	 */
	public int getDomain() {
		return allValues.length;
	}

	/**
	 * Computes the rank corresponding to a given probability.
	 * 
	 * @param p
	 *            The probability to compute the rank for. As a probability
	 *            value, p must be in the interval [0, 1].
	 * @return The rank corresponding to the given probability.
	 */
	public int getRank(double p) {
		assert (p >= 0 && p <= 1);
		int low = 0, high = allValues.length - 1;
		while (low < high) {
			int cur = (low + high) / 2;
			if (QuickMath.abs(allValues[cur] - p) < .000000001)
				break;
			if (allValues[cur] < p) {
				low = cur + 1;
			} else {
				high = cur;
			}
		}
		return low + 1;
	}

	/**
	 * Computes a random rank, following the Geometric distribution.
	 * 
	 * @return The next random rank, following the Geometric distribution.
	 */
	public int nextRank() {
		return getRank(rng.nextDouble());
	}
	
	public int nextRank(Random rng) {
		if (rng != null)
			return getRank(rng.nextDouble());
		else
			return getRank(this.rng.nextDouble());
	}
	
	public static void main(String args[]) {
		GeometricGenerator bg = new GeometricGenerator(20, 0.5);
		int count[] = new int[40];
		
		for (int i = 0; i < 10000; i ++) {
			int rank = bg.nextRank();
			count[rank] ++;
		}
		
		//for (int i = 0; i < 40; i ++)
		//	System.out.println(count[i]);
	}
}
