/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$ 
 */

package netcins.util.stats;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import netcins.util.QuickMath;

/**
 * A random number generator, following the Zipf distribution. This
 * implementation needs to keep an array of the CDF of Zipf for all possible
 * ranks, amounting to a memory requirement of 8 times the number of ranks in
 * bytes (i.e. 8Mbytes for 1M ranks). For a somewhat slower implementation for
 * theta = 1.0 but requiring O(1) space, see
 * {@link netcins.util.QuickMath#quickZipf(double, long)}.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 * @see netcins.util.QuickMath#quickZipf(double, long)
 */
public class ZipfGenerator {
	private double allValues[] = null;

	private double theta = 0.0;

	private Random rng = null;

	/**
	 * Constructor for ZipfGenerator.
	 * 
	 * @param domain
	 *            The number of values in the domain.
	 * @param theta
	 *            The parameter of the Zipf distribution.
	 */
	public ZipfGenerator(int domain, double theta) {
		double norm = 0;
		this.theta = theta;
		this.allValues = new double[domain];

		for (int i = 0; i < allValues.length; i++) {
			allValues[i] = 1.0 / Math.pow(1 + i, theta);
			norm += allValues[i];
		}

		allValues[0] /= norm;
		for (int i = 1; i < allValues.length; i++) {
			allValues[i] = (allValues[i] / norm) + allValues[i - 1];
		}

		rng = new Random();
	}

	/**
	 * Constructor for ZipfGenerator.
	 * 
	 * @param domain
	 *            The number of values in the domain.
	 * @param theta
	 *            The parameter of the Zipf distribution.
	 * @param seed
	 *            The seed for the internal random number generator.
	 */
	public ZipfGenerator(int domain, double theta, long seed) {
		this(domain, theta);
		rng = new Random(seed);
	}

	/**
	 * Returns the 'theta' parameter of the current Zipf distribution.
	 * 
	 * @return The 'theta' parameter of the current Zipf distribution.
	 */
	public double getTheta() {
		return theta;
	}

	/**
	 * Returns the cardinality of the domain of ranks.
	 * 
	 * @return The cardinality of the domain of ranks.
	 */
	public int getDomain() {
		return allValues.length;
	}

	/**
	 * Computes the rank corresponding to a given probability.
	 * 
	 * @param p
	 *            The probability to compute the rank for. As a probability
	 *            value, p must be in the interval [0, 1].
	 * @return The rank corresponding to the given probability.
	 */
	public int getRank(double p) {
		assert (p >= 0 && p <= 1);
		int low = 0, high = allValues.length - 1;
		while (low < high) {
			int cur = (low + high) / 2;
			if (QuickMath.abs(allValues[cur] - p) < .000000001)
				break;
			if (allValues[cur] < p) {
				low = cur + 1;
			} else {
				high = cur;
			}
		}
		return low;
	}

	/**
	 * Computes a random rank, following the Zipf distribution.
	 * 
	 * @return The next random rank, following the Zipf distribution.
	 */
	public int nextRank() {
		return getRank(rng.nextDouble());
	}
	
	public static void main(String[] args) {
		double theta = 0.5;
		int domain = 20;
		int ntries = 10000;
		
		Comparator<Integer> invIntComp = new Comparator<Integer>() {

			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
			
		};
		
		Integer hits[] = new Integer[domain];
		for (int i = 0; i < hits.length; i ++)
			hits[i] = new Integer(0);
		ZipfGenerator zg = new ZipfGenerator(domain, theta);
		for (int i = 0; i < ntries; i ++)
			hits[zg.nextRank()] ++;
		Collections.sort(Arrays.asList(hits), invIntComp);
		for (int i = 0; i < hits.length; i ++)
			System.out.println(hits[i]);
	}
}
