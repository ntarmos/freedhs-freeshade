/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$ 
 */

package netcins.util.stats;

import java.util.Random;

import netcins.dbms.BasicItem;
import netcins.util.QuickMath;

/**
 * A random number generator, following the Geometric distribution. This
 * implementation needs to keep an array of the CDF of G(1-p) for all possible
 * ranks, amounting to a memory requirement of 8 times the number of ranks in
 * bytes (i.e. 8Mbytes for 1M ranks).
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class BinomialGenerator {
	private GeometricGenerator geomGen = null;
	private int domain = 0;
	private double p = 0.0;

	private Random rng = null;

	/**
	 * Constructor for ZipfGenerator.
	 * 
	 * @param domain
	 *            The number of values in the domain.
	 * @param p
	 *            The parameter of the Geometric distribution.
	 */
	public BinomialGenerator(int domain, double p) {
		this.p = p;
		this.domain = domain;
		//this.geomGen = new GeometricGenerator((int)QuickMath.ceil(1/p) + 1, p);
		this.geomGen = new GeometricGenerator(domain, p);
		rng = new Random();
	}

	/**
	 * Constructor for GeometricGenerator.
	 * 
	 * @param domain
	 *            The number of values in the domain.
	 * @param p
	 *            The parameter of the Geometric distribution.
	 * @param seed
	 *            The seed for the internal random number generator.
	 */
	public BinomialGenerator(int domain, double p, long seed) {
		this.p = p;
		this.domain = domain;
		//this.geomGen = new GeometricGenerator((int)QuickMath.ceil(1/p) + 1, p, seed);
		this.geomGen = new GeometricGenerator(domain, p, seed);
		rng = new Random(seed);
	}
	
	public BinomialGenerator(int domain, double p, BasicItem seed) {
		this(domain, p, seed.hashCode());
	}

	/**
	 * Returns the 'p' parameter of the current Geometric distribution.
	 * 
	 * @return The 'p' parameter of the current Geometric distribution.
	 */
	public double getP() {
		return p;
	}

	/**
	 * Returns the cardinality of the domain of ranks.
	 * 
	 * @return The cardinality of the domain of ranks.
	 */
	public long getDomain() {
		return domain;
	}

	/**
	 * Computes the rank corresponding to a given probability.
	 * 
	 * @param p
	 *            The probability to compute the rank for. As a probability
	 *            value, p must be in the interval [0, 1].
	 * @return The rank corresponding to the given probability.
	 */
	public int getRank(long n, Random rng) {
		int d = 0;
		//long x = (long)QuickMath.ceil(1.0 / p);
		for (long sum = 0; sum <= n; d++) {
			int rank = geomGen.nextRank(rng);
			//if (rank == x + 1)
			//	sum += x + 1;
			//else
				sum += rank;
		}
		return d - 1;
	}

	/**
	 * Computes a random rank, following the Geometric distribution.
	 * 
	 * @return The next random rank, following the Geometric distribution.
	 */
	public int nextRank() {
		return getRank(domain, null);
	}
	
	public int nextRank(Random rng) {
		return getRank(domain, rng);
	}
	
	public static void main(String args[]) {
		int n = 6;
		int tries = 100000;
		double d = QuickMath.log2(n) - 2*QuickMath.log2(Math.log(n));
		double p = Math.pow(2.0, -d);

		System.err.println("Drawing from B(" + n + ", " + p + ")...");
		long time = System.nanoTime();
		BinomialGenerator bg = new BinomialGenerator(n, p);
		int count[] = new int[n + 1];

		time = System.nanoTime() - time;
		System.err.println("Generation took " + ((double)time)/1000000.0 + "ms");
		
		time = System.nanoTime();
		for (int i = 0; i < tries; i ++) {
			int rank = bg.nextRank();
			count[rank] ++;
		}

		time = System.nanoTime() - time;
		System.err.println("Drawing took " + ((double)time)/1000000.0/(double)tries + "ms per draw");
		double sumP = 0;
		for (int i = 0; i < count.length; i ++) {
			double prob = ((double)count[i] / (double)tries);
			sumP += prob;
			System.out.println(i + " " + prob);
		}
		System.out.println("SumP: " + sumP);
	}
}
