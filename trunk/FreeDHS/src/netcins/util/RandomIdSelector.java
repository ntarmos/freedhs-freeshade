/**
 * $Id$
 */

package netcins.util;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Vector;

import rice.environment.Environment;
import rice.environment.random.RandomSource;
import rice.pastry.Id;
import rice.pastry.IdRange;
import rice.pastry.NodeHandle;
import rice.pastry.Id.Distance;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class RandomIdSelector {
	private Vector<IdRange>			idRanges	= new Vector<IdRange>();
	private RandomSource			rng			= null;
	private static final BigInteger	idMax		= BigInteger.ONE.shiftLeft(Id.IdBitLength)
													.subtract(BigInteger.ONE);

	public RandomIdSelector(RandomSource rng) {
		this.rng = rng;
	}

	public RandomIdSelector() {
	}

	public void add(IdRange range) {
		boolean changed = false;
		for (int i = 0; i < idRanges.size(); i++) {
			IdRange merged = idRanges.get(i).merge(range);
			if (merged != idRanges.get(i)) {
				idRanges.set(i, merged);
				changed = true;
			}
		}
		if (changed)
			consolidate();
		else
			idRanges.add(new IdRange(range.getCCW(), range.getCW()));
	}

	public void add(Id id) {
		add(new IdRange(id, id));
	}

	public void add(RandomIdSelector ris) {
		if (ris != null)
			for (int i = 0; i < ris.idRanges.size(); i++)
				add(new IdRange(ris.idRanges.get(i).getCCW(), ris.idRanges.get(i).getCW()));
	}

	public void subtract(IdRange range) {
		int numRanges = idRanges.size();
		boolean delete[] = new boolean[numRanges];
		for (int i = 0; i < numRanges; i++) {
			boolean intersects = !idRanges.get(i).intersect(range).isEmpty();
			if (!intersects) continue;
			IdRange diffcw = idRanges.get(i).subtract(range, true);
			IdRange diffccw = idRanges.get(i).subtract(range, false);
			if (!diffcw.isEmpty()) {
				idRanges.add(diffcw);
				delete[i] = true;
			}
			if (!diffccw.isEmpty() && !diffccw.equals(diffcw)) {
				idRanges.add(diffccw);
				delete[i] = true;
			}
			if (delete[i] == false && intersects)
				delete[i] = true;
		}
		for (int i = numRanges - 1; i >= 0; i--) {
			if (delete[i])
				idRanges.remove(i);
		}
		consolidate();
	}

	public void subtract(RandomIdSelector ris) {
		for (IdRange range : ris.idRanges)
			subtract(range);
	}

	public RandomIdSelector copy() {
		RandomIdSelector ret = new RandomIdSelector();
		Collections.copy(ret.idRanges, idRanges);
		return ret;
	}

	private void consolidate() {
		boolean changed = true;
		while (changed) {
			changed = false;
			for (int i = 0; i < idRanges.size(); i++) {
				for (int j = i + 1; j < idRanges.size();) {
					IdRange merged = idRanges.get(i).merge(idRanges.get(j));
					if (merged != idRanges.get(i)) {
						idRanges.set(i, merged);
						idRanges.remove(j);
						changed = true;
					} else
						j++;
				}
			}
		}
	}

	public int numSegments() {
		return idRanges.size();
	}

	public BigInteger numIDs() {
		BigInteger numIds = BigInteger.ZERO;
		for (int i = 0; i < idRanges.size(); i++) {
			IdRange range = idRanges.get(i);
			BigInteger ccw = new BigInteger(range.getCCW().toByteArray());
			BigInteger cw = new BigInteger(range.getCW().toByteArray());
			if (ccw.compareTo(cw) < 0) {
				numIds = numIds.add(cw.subtract(ccw).add(BigInteger.ONE));
			} else {
				numIds = numIds.add(idMax.subtract(ccw).add(cw).add(BigInteger.ONE));
			}
		}
		return numIds;
	}

	public Id buildRandomId(IdRange range, RandomSource rng) {
		Id offset = Id.makeRandomId(rng);
		byte[] rnd = offset.copy();
		byte[] ccw = range.getCCW().copy();
		byte[] cw = range.getCW().copy();
		int msdb = range.getCCW().indexOfMSDB(range.getCW());
		byte[] ret = new byte[cw.length];
		System.arraycopy(ccw, 0, ret, 0, cw.length);
		for (int i = msdb - 1; i >= 0; i--)
			ret[i / 8] |= rnd[i / 8] & (0x01 << (i % 8));
		return Id.build(ret);
	}

	public Id randomId(IdRange range, RandomSource rng) {
		RandomIdSelector ret = new RandomIdSelector();
		ret.add(range);
		for (IdRange idRange : idRanges)
			ret.subtract(idRange);
		return ret.selectRandomId(rng);
	}

	private Id selectRandomId(RandomSource rng) {
		RandomSource tmpRng = (rng != null ? rng : this.rng);
		if (idRanges.size() == 0)
			return null;
		return buildRandomId(idRanges.get(rng.nextInt(idRanges.size())), rng);
	}

	public boolean isCovered(IdRange range) {
		RandomIdSelector tmp = new RandomIdSelector();
		tmp.add(range);
		tmp.subtract(this);
		return (tmp.idRanges.size() == 0);
	}

	public boolean isCovered(NodeHandle nh) {
		return isCovered(nh.getLocalNode().getLeafSet().range(nh, 0));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Environment env = Environment.directEnvironment();
		RandomSource rng = env.getRandomSource();
		RandomIdSelector ris = new RandomIdSelector(rng);
		int one[] = new int[Id.One.length];

		Id low = Id.build(Id.Null);
		System.arraycopy(Id.One, 0, one, 0, Id.One.length);
		Distance d = new Distance(one);
		d.shift(-159, 0);
		IdRange targetRange = new IdRange(low.add(d), low);

		for (int i = 0; i < 10; i++) {
			Id ccw = Id.makeRandomId(rng);
			System.arraycopy(Id.One, 0, one, 0, Id.One.length);
			Distance width = new Distance(one);
			width.shift(-158, 0);
			IdRange curRange = new IdRange(ccw, ccw.add(width));
			System.out.println("Adding " + curRange.getCCW().toStringFull() + " to "
				+ curRange.getCW().toStringFull() + " dist: " + width);
			ris.add(curRange);
		}
		for (int i = 0; i < ris.idRanges.size(); i++)
			System.out.println(ris.idRanges.get(i).getCCW().toStringFull() + " to "
				+ ris.idRanges.get(i).getCW().toStringFull());
		Id rndId = ris.randomId(targetRange, rng);
		System.out.println(targetRange.getCCW().toStringFull() + " to "
			+ targetRange.getCW().toStringFull() + " => "
			+ (rndId != null ? rndId.toStringFull() : null));
		env.destroy();
	}
}
