/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.ItemSerializable;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class HashAlgorithm {
	private MessageDigest	hash		= null;
	private String			hashAlgo	= null;
	private int				numBits		= -1;
	
	public HashAlgorithm(String hashAlgo) throws NoSuchAlgorithmException {
		try {
			hash = MessageDigest.getInstance(hashAlgo);
		} catch (NoSuchAlgorithmException e) {
			Logger log = Logger.getLogger(this.getClass().getName());
			log.log(Level.SEVERE, "Can't find an implementation for " + hashAlgo
					+ ". Bailing out...");
			throw e;
		}
		this.hashAlgo = hashAlgo;
		numBits = hash.getDigestLength() * 8;
	}
	
	public String getHashAlgo() {
		return hashAlgo;
	}
	
	public int getNumBits() {
		return numBits;
	}
	
	public synchronized byte[] hash(ItemSerializable data) {
		hash.reset();
		hash.update(data.serialize());
		return hash.digest();
	}
	
	public static boolean getBitFromDigest(int bit, byte[] digest) {
		return ((digest[bit / 8] & (1 << (bit % 8))) != 0);
	}
}
