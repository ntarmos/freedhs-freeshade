/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.util;

import java.lang.reflect.Field;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DataSerializer {

	// No instances of this class allowed out there.
	private DataSerializer() {
	}

	/**
	 * @param i
	 * @return
	 */
	public static final byte[] serializeInt(final Integer i) {
		if (i == null)
			return new byte[0];

		byte[] ret = new byte[Integer.SIZE / 8];
		DataSerializer.serializeInt(i, ret, 0);
		return ret;
	}

	/**
	 * @param i
	 * @param ret
	 * @param offset
	 */
	public static final void serializeInt(final Integer i, byte[] ret, int offset) {
		if (i == null)
			return;

		for (int ind = 0; ind < (Integer.SIZE / 8); ind++) {
			ret[offset + ind] = (byte)(((i & (0xffL << (ind * 8))) >> (ind * 8)) & 0xff);
		}
	}

	/**
	 * @param s
	 * @return
	 */
	public static final byte[] serializeShort(final Short s) {
		if (s == null)
			return new byte[0];

		byte[] ret = new byte[Short.SIZE / 8];
		DataSerializer.serializeShort(s, ret, 0);
		return ret;
	}

	/**
	 * @param s
	 * @param ret
	 * @param offset
	 */
	public static final void serializeShort(final Short s, byte[] ret, int offset) {
		if (s == null)
			return;

		for (int ind = 0; ind < Short.SIZE / 8; ind++) {
			ret[offset + ind] = (byte)(((s & (0xffL << (ind * 8))) >> (ind * 8)) & 0xff);
		}
	}

	/**
	 * @param l
	 * @return
	 */
	public static final byte[] serializeLong(final Long l) {
		if (l == null)
			return new byte[0];

		byte[] ret = new byte[Long.SIZE / 8];
		DataSerializer.serializeLong(l, ret, 0);
		return ret;
	}

	/**
	 * @param l
	 * @param ret
	 * @param offset
	 */
	public static final void serializeLong(final Long l, byte[] ret, int offset) {
		if (l == null)
			return;
		for (int ind = 0; ind < Long.SIZE / 8; ind++) {
			ret[offset + ind] = (byte)(((l & (0xffL << (ind * 8))) >> (ind * 8)) & 0xff);
		}
	}

	/**
	 * @param d
	 * @return
	 */
	public static final byte[] serializeDouble(final Double d) {
		if (d == null)
			return new byte[0];

		long rawBits = Double.doubleToRawLongBits(d);
		return DataSerializer.serializeLong(rawBits);
	}

	/**
	 * @param d
	 * @param ret
	 * @param offset
	 */
	public static final void serializeDouble(final Double d, byte[] ret, int offset) {
		if (d == null)
			return;

		long rawBits = Double.doubleToRawLongBits(d);
		DataSerializer.serializeLong(rawBits, ret, offset);
	}

	/**
	 * @param n
	 * @param ret
	 * @param offset
	 */
	public static final void serializeNumber(final Number n, byte[] ret, int offset) {
		if (n == null)
			return;
		if (n instanceof Short)
			serializeShort((Short)n, ret, offset);
		else if (n instanceof Integer)
			serializeInt((Integer)n, ret, offset);
		else if (n instanceof Long)
			serializeLong((Long)n, ret, offset);
		else if (n instanceof Double)
			serializeDouble((Double)n, ret, offset);
	}

	/**
	 * @param n
	 * @return
	 */
	public static final byte[] serializeNumber(final Number n) {
		if (n == null)
			return new byte[0];

		Field size;
		try {
			size = n.getClass().getField("SIZE");
		} catch (SecurityException e) {
			return null;
		} catch (NoSuchFieldException e) {
			return null;
		}

		byte[] ret = null;
		try {
			ret = new byte[size.getInt(n) / 8];
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
		serializeNumber(n, ret, 0);
		return ret;
	}

	/**
	 * @param s
	 * @param ret
	 * @param offset
	 */
	public static final void serializeString(final String s, byte[] ret, int offset) {
		if (s == null)
			return;

		byte[] sBytes = DataSerializer.serializeString(s);
		DataSerializer.serializeInt(sBytes.length, ret, offset);
		System.arraycopy(sBytes, 0, ret, offset + Integer.SIZE / 8, sBytes.length);
	}

	/**
	 * @param s
	 * @return
	 */
	public static final byte[] serializeString(String s) {
		if (s == null)
			return new byte[0];

		int sLen = s.length() * Character.SIZE / 8;
		byte[] sBytes = new byte[sLen + Integer.SIZE / 8];
		DataSerializer.serializeInt(sLen, sBytes, 0);
		System.arraycopy(s.getBytes(), 0, sBytes, Integer.SIZE / 8, sLen);
		return sBytes;
	}

	/**
	 * @param material
	 * @param offset
	 * @return
	 */
	public static final Integer deserializeInt(final byte[] material, int offset) {
		if (material == null || material.length < (Integer.SIZE / 8 + offset))
			return null;
		int res = 0;
		for (int i = 0; i < (Integer.SIZE / 8); i++)
			res |= (((long)(material[offset + i] & 0xff)) << (i * 8));
		return res;
	}

	/**
	 * @param material
	 * @return
	 */
	public static final int deserializeInt(final byte[] material) {
		return DataSerializer.deserializeInt(material, 0);
	}

	/**
	 * @param material
	 * @param offset
	 * @return
	 */
	public static final Long deserializeLong(final byte[] material, int offset) {
		if (material == null || material.length < (Long.SIZE / 8 + offset))
			return null;
		long res = 0;
		for (int i = 0; i < (Long.SIZE / 8); i++)
			res |= (((long)(material[offset + i] & 0xff)) << (i * 8));
		return res;
	}

	/**
	 * @param material
	 * @return
	 */
	public static final long deserializeLong(final byte[] material) {
		return DataSerializer.deserializeLong(material, 0);
	}

	/**
	 * @param material
	 * @param offset
	 * @return
	 */
	public static final Short deserializeShort(final byte[] material, int offset) {
		if (material == null || material.length < (Short.SIZE / 8 + offset))
			return null;
		short res = 0;
		for (int i = 0; i < (Short.SIZE / 8); i++)
			res |= (((long)(material[offset + i] & 0xff)) << (i * 8));
		return res;
	}

	/**
	 * @param material
	 * @return
	 */
	public static final short deserializeShort(final byte[] material) {
		return DataSerializer.deserializeShort(material, 0);
	}

	/**
	 * @param material
	 * @param offset
	 * @return
	 */
	public static final Double deserializeDouble(final byte[] material, int offset) {
		if (material == null || material.length < (Double.SIZE / 8 + offset))
			return null;
		long l = DataSerializer.deserializeLong(material, offset);
		return Double.longBitsToDouble(l);
	}

	/**
	 * @param material
	 * @return
	 */
	public static final Double deserializeDouble(final byte[] material) {
		return DataSerializer.deserializeDouble(material, 0);
	}

	/**
	 * @param material
	 * @param offset
	 * @return
	 */
	public static final String deserializeString(final byte[] material, int offset) {
		if (material == null || material.length < (Integer.SIZE / 8 + offset + 1))
			return null;
		int sLen = DataSerializer.deserializeInt(material, offset);
		if (material.length < (Integer.SIZE / 8 + offset + sLen))
			return null;
		return new String(material, offset + Integer.SIZE / 8, sLen);
	}
}
