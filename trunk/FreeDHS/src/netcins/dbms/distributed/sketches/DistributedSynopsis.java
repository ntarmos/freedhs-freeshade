/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.sketches;

import java.util.Collections;
import java.util.Vector;

import netcins.ItemSerializable;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DistributedSynopsis {
	Vector<String>	dsMetrics	= new Vector<String>(10, 2);

	public abstract void insert(PastryAppl sourceNode, String metric, ItemSerializable object);

	public abstract DistributedEstimationResult[] estimate(PastryAppl sourceNode,
			Vector<String> metrics) throws NoSuchMetricException;

	public final void clearDSMetrics() {
		if (dsMetrics != null)
			dsMetrics.clear();
	}

	public final void addDSMetrics(Vector<String> dsMetrics) {
		if (dsMetrics == null)
			return;
		this.dsMetrics.addAll(dsMetrics);
	}

	public final int getIndexForMetric(String dsMetric) {
		return Collections.binarySearch(this.dsMetrics, dsMetric);
	}

	public final int addDSMetric(String dsMetric) {
		int index = -1;
		if ((index = getIndexForMetric(dsMetric)) < 0) {
			this.dsMetrics.add(-index - 1, dsMetric);
			return -index - 1;
		}
		return -1;
	}
}
