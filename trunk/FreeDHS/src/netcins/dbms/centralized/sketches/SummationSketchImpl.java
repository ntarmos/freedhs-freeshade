/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.Random;

import netcins.ItemSerializable;
import netcins.dbms.BasicItem;
import netcins.util.DataSerializer;
import netcins.util.HashAlgorithm;
import netcins.util.QuickMath;
import netcins.util.stats.BinomialGenerator;
import rice.p2p.commonapi.rawserialization.InputBuffer;
import rice.p2p.commonapi.rawserialization.OutputBuffer;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class SummationSketchImpl implements Sketch {
	private HashSketchImpl									hsImpl		= null;
	private static Hashtable<Integer, BinomialGenerator>	binomials	= null;

	private class BasicItemInstance implements ItemSerializable {
		/**
		 * 
		 */
		private static final long	serialVersionUID	= -1357415647557846220L;
		private BasicItem			item;
		private int					instance;
		public static final int		SIZE				= BasicItem.SIZE + Integer.SIZE;

		BasicItemInstance(BasicItem item, int instance) {
			this.item = item;
			this.instance = instance;
		}

		BasicItemInstance(BasicItem item) {
			this(item, 0);
		}

		/* (non-Javadoc)
		 * @see netcins.ItemSerializable#deserialize(byte[])
		 */
		public ItemSerializable deserialize(byte[] buf) throws IOException {
			return new BasicItemInstance(new BasicItem(buf), DataSerializer.deserializeInt(buf,
					BasicItem.SIZE));
		}

		/* (non-Javadoc)
		 * @see netcins.ItemSerializable#deserialize(rice.p2p.commonapi.rawserialization.InputBuffer)
		 */
		public void deserialize(InputBuffer buf) throws IOException {
			byte data[] = new byte[BasicItemInstance.SIZE];
			buf.read(data);
			BasicItem localItem = new BasicItem(0);
			item = (BasicItem)localItem.deserialize(data);
			this.instance = DataSerializer.deserializeInt(data, BasicItem.SIZE);
		}

		/* (non-Javadoc)
		 * @see netcins.ItemSerializable#serialize()
		 */
		public byte[] serialize() {
			byte[] data = new byte[BasicItemInstance.SIZE];
			byte[] itemBytes = item.serialize();
			for (int i = 0; i < itemBytes.length; i++)
				data[i] = itemBytes[i];
			DataSerializer.serializeInt(instance, data, BasicItem.SIZE);
			return data;
		}

		/* (non-Javadoc)
		 * @see netcins.ItemSerializable#serialize(rice.p2p.commonapi.rawserialization.OutputBuffer)
		 */
		public void serialize(OutputBuffer buf) throws IOException {
			byte ret[] = serialize();
			buf.write(ret, 0, ret.length);
		}

		public int hashCode() {
			final int PRIME = 31;
			int result = 1;
			result = PRIME * result + item.hashCode();
			result = PRIME * result + Integer.valueOf(instance).hashCode();
			return result;
		}
	}

	public SummationSketchImpl(HashSketchImpl hsImpl) {
		if (hsImpl == null)
			throw new RuntimeException(
					"Null HashSkethImpl instance passed to SummationSkethImpl constructor");
		try {
			this.hsImpl = HashSketchUtils.newInstance(hsImpl);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if (binomials == null)
			binomials = new Hashtable<Integer, BinomialGenerator>();
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.centralized.sketches.Sketch#clear()
	 */
	public void clear() {
		if (hsImpl != null)
			hsImpl.clear();
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.centralized.sketches.Sketch#estimate()
	 */
	public double estimate() {
		return hsImpl.estimate();
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.centralized.sketches.Sketch#insert(netcins.ItemSerializable)
	 */
	public void insert(ItemSerializable itemSer) {
		// XXX : this should be revisited
		BasicItem tmpItem = new BasicItem((BasicItem)itemSer);
		int itemValue = tmpItem.intValue();

		int m = hsImpl.getNumVecs();
		int q_i = itemValue / m, r_i = itemValue % m;

		for (int i = 0; i < r_i; i++) {
			tmpItem.setValue(i);
			hsImpl.insert(tmpItem);
		}

		if (q_i > 0) {
			int binomial_n = q_i;
			int delta_i = delta(binomial_n);

			BinomialGenerator bg = getBinomialGenerator(binomial_n, delta_i);
			tmpItem.setValue(q_i);
			BasicItemInstance itemInstance = new BasicItemInstance(tmpItem);
			HashAlgorithm hash = hsImpl.getHash();
			int instanceCount = 0;

			for (int vec = 0; vec < m; vec++) {
				for (int bit = 0; bit < delta_i && bit < hsImpl.getNumBits(); bit++) {
					hsImpl.setBit(vec, bit);
				}

				int a = bg.nextRank(new Random(tmpItem.hashCode() + vec));
				for (int rank = 0; rank < a; rank++) {
					itemInstance.instance = instanceCount++;
					byte[] digest = hash.hash(itemInstance); // XXX Is this what hash() stands for in the paper???
					int bit = delta_i;
					while (bit < (hsImpl.getNumBits() - 1)
							&& HashAlgorithm.getBitFromDigest(bit, digest) == false) {
						bit++;
					}

					hsImpl.setBit(vec, bit);
				}
			}
		}
	}

	private static final BinomialGenerator getBinomialGenerator(int binomial_n) {
		return getBinomialGenerator(binomial_n, delta(binomial_n));
	}

	private static final BinomialGenerator getBinomialGenerator(int binomial_n, double delta) {
		BinomialGenerator bg = null;
		synchronized (binomials) {
			bg = binomials.get(new Integer(binomial_n));
			if (bg == null) {
				bg = new BinomialGenerator(binomial_n, Math.pow(2.0, -delta));
				binomials.put(new Integer(binomial_n), bg);
			}
		}
		return bg;
	}

	private static final int delta(long q) {
		return (q > 2 ? (int)QuickMath.floor(QuickMath.log2(q) - 2
				* QuickMath.log2(QuickMath.log2(q))) : 0);
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.centralized.sketches.Sketch#tryAddObject(netcins.ItemSerializable)
	 */
	public Object tryAddObject(ItemSerializable data) {
		// XXX : Unsupported operation
		return null;
	}

	public int getNumBits() {
		return hsImpl.getNumBits();
	}

	public int getNumVecs() {
		return hsImpl.getNumVecs();
	}

	public boolean getBit(int nVec, int nBit) {
		return hsImpl.getBit(nVec, nBit);
	}

	public String toString() {
		return hsImpl.toString();
	}

	public String toStringFull() {
		return hsImpl.toStringFull();
	}
}
