/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import java.security.NoSuchAlgorithmException;
import java.util.BitSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.ItemSerializable;
import netcins.p2p.dhs.DHSTuple;
import netcins.util.HashAlgorithm;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class HashSketchImpl implements Sketch {

	protected BitSet[]			sketch		= null;
	private HashAlgorithm		hash		= null;
	private int					numBits		= 0;
	private boolean				isEmpty		= true;
	private int[]				numVecsSet	= null;

	private static final int	bitMasks[]	= { 0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f,
			0xff							};
	private static int			numVecBits	= 0, numVecBytes = 0;

	protected HashSketchImpl(HashSketchImpl other) throws NoSuchAlgorithmException {
		this(other.getNumVecs(), other.getNumBits(), other.getHashAlgo());
	}

	protected HashSketchImpl(int numVecs, String hashAlgo) throws NoSuchAlgorithmException {
		try {
			hash = new HashAlgorithm(hashAlgo);
		} catch (NoSuchAlgorithmException e) {
			Logger log = Logger.getLogger(this.getClass().getName());
			log.log(Level.SEVERE, "Can't find an implementation for " + hashAlgo
					+ ". Bailing out...");
			throw e;
		}
		numBits = hash.getNumBits();
		sketch = new BitSet[numVecs];
		numVecBits = Integer.SIZE - Integer.numberOfLeadingZeros(sketch.length) - 1;
		numVecBytes = numVecBits / 8;
		numVecsSet = new int[numBits];
		for (int i = 0; i < numVecs; i++)
			sketch[i] = new BitSet();
	}

	protected HashSketchImpl(int numVecs) throws NoSuchAlgorithmException {
		this(numVecs, defaultHashAlgo);
	}

	protected HashSketchImpl(int numVecs, int numBits, String hashAlgo)
			throws NoSuchAlgorithmException {
		this(numVecs, hashAlgo);
		assert (numBits <= (hash.getNumBits()));
		this.numBits = numBits;
		this.numVecsSet = new int[numBits];
	}

	protected HashSketchImpl(int numVecs, int numBits) throws NoSuchAlgorithmException {
		this(numVecs, numBits, defaultHashAlgo);
	}

	public int getNumVecs() {
		return (sketch != null ? sketch.length : 0);
	}

	public int getNumBits() {
		return numBits;
	}

	public String getHashAlgo() {
		return hash.getHashAlgo();
	}

	public HashAlgorithm getHash() {
		return hash;
	}

	public BitSet getVec(int vec) {
		return sketch[vec];
	}

	public void merge(HashSketchImpl hs) {
		if (hs.getNumBits() != getNumBits() || hs.getNumVecs() != getNumVecs()
				|| hs.getHashAlgo().equals(getHashAlgo()) == false)
			throw new RuntimeException("Trying to merge different hash sketches.");
		for (int i = 0; i < sketch.length; i++) {
			sketch[i].or(hs.sketch[i]);
		}

		numVecsSet = new int[numBits];
		for (int vec = 0; vec < sketch.length; vec++) {
			for (int bit = 0; bit < numBits; bit++) {
				if (sketch[vec].get(bit) == true)
					numVecsSet[bit]++;
			}
		}
	}

	public int getNumVecsSetForBit(int bit) {
		return numVecsSet[bit];
	}

	/**
	 * Adds an object to the hash sketch.
	 * 
	 * @param data Input object
	 * @return An int[] containing: (i) in position 0, the index of the vector
	 *         to be updated by the input object, and (ii) in position 1, the
	 *         index of the bit to be set in the selected bitmap vector.
	 */
	public Object tryAddObject(ItemSerializable data) {
		if (sketch == null || sketch.length == 0)
			throw new RuntimeException("HashSketchImpl: Uninitialized instance.");

		int[] ret = new int[2];

		if (data instanceof DHSTuple) {
			DHSTuple tuple = (DHSTuple)data;
			if (tuple.getBit() < getNumBits() && tuple.getVec() < getNumVecs()) {
				ret[0] = tuple.getVec();
				ret[1] = tuple.getBit();
				return ret;
			} else
				throw new RuntimeException("Huh?!");
		}

		byte[] digest = hash.hash(data);

		int vec = 0;
		for (int i = 0; i < numVecBytes; i++)
			vec |= (((int)(digest[i] & 0xff)) << (i * 8));
		if ((numVecBits % 8) != 0)
			vec |= ((int)((digest[numVecBytes] & bitMasks[numVecBits % 8]) & 0xff)) << (numVecBytes * 8);

		int rho = 0;
		for (int i = numVecBits + 1; i < digest.length * 8; i++) {
			if (HashAlgorithm.getBitFromDigest(i, digest) == true)
				rho++;
			else
				break;
		}
		if (rho == digest.length * 8 - numVecBits - 1)
			rho = 0;

		ret[0] = vec;
		ret[1] = rho;

		return ret;
	}

	public void insert(ItemSerializable data) {
		int[] bits = (int[])tryAddObject(data);
		setBit(bits[0], bits[1]);
	}

	public String diff(HashSketchImpl hs) {
		if (this.sketch == null || hs.sketch == null || this.sketch.length != hs.sketch.length
				|| hs.numBits != this.numBits)
			return "Uninitialized or different type sketches";
		BitSet[] ret = new BitSet[this.sketch.length];
		for (int i = 0; i < this.sketch.length; i++) {
			ret[i] = this.sketch[i];
			ret[i].xor(hs.sketch[i]);
		}
		return toStringFull(ret, this.numBits);
	}

	public boolean setBit(int nVec, int nBit) {
		if (nVec < 0 || nVec >= sketch.length)
			throw new RuntimeException("Can't set vec #" + nVec);
		if (nBit < 0 || nBit > numBits)
			throw new RuntimeException("Can't set bit #" + nBit);
		boolean oldValue = sketch[nVec].get(nBit);
		if (oldValue == false)
			numVecsSet[nBit]++;
		sketch[nVec].set(nBit);
		isEmpty = false;
		return oldValue;
	}

	public boolean getBit(int nVec, int nBit) {
		if (nVec < 0 || nVec >= sketch.length)
			throw new RuntimeException("Can't set vec #" + nVec);
		if (nBit < 0 || nBit > numBits)
			throw new RuntimeException("Can't set bit #" + nBit);
		return sketch[nVec].get(nBit);
	}

	public void clear() {
		if (isEmpty)
			return;
		for (int i = 0; i < sketch.length; i++)
			sketch[i] = new BitSet();
		numVecsSet = new int[numBits];
		isEmpty = true;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	/*
	 * private int rho(BitSet input) { return (input.cardinality() == 0 ? 0 :
	 * input.nextSetBit(0)); }
	 */

	protected String toStringInternal() {
		return "hash: " + hash.getHashAlgo() + " , numVecs: " + sketch.length + " , numBits: "
				+ numBits;
	}

	public String toString() {
		return "[ " + toStringInternal() + " ]";
	}

	public String toStringFull() {
		return HashSketchImpl.toStringFull(this);
	}

	public static String toStringFull(HashSketchImpl hs) {
		return HashSketchImpl.toStringFull(hs.sketch, hs.numBits);
	}

	private static String toStringFull(BitSet[] hs, int nBits) {
		if (hs == null)
			return "[HS :: Uninitialized instance]";
		StringBuilder ret = new StringBuilder();
		int numVecDigits = (hs.length > 1000 ? 4 : (hs.length > 100 ? 3 : 2));
		for (int i = 0; i < hs.length; i++) {
			StringBuilder builder = new StringBuilder();
			for (int j = 0; j < nBits; j++)
				builder.append(hs[i].get(nBits - j - 1) ? "1" : "0");
			ret.append("[ ");
			for (int p = 0; p < (numVecDigits - (i < 10 ? 1 : (i < 100 ? 2 : 3))); p++)
				ret.append(" ");

			ret.append(i + " : " + builder.toString() + " ]" + "\n");
		}
		return ret.toString();
	}
}
