/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import java.security.NoSuchAlgorithmException;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class HashSketchUtils {

	private HashSketchUtils() {
	}

	public static final HashSketchImpl newHashSketchInstance(SketchAlgorithm algorithm,
			HashSketchImpl hsImpl) {
		if (hsImpl == null || algorithm == null)
			return null;
		try {
			if (hsImpl instanceof HashSketchImpl) {
				if (algorithm.isUsingDF03())
					return new DF03SketchImpl(hsImpl);
				else
					return new FM85SketchImpl(hsImpl);
			}
			return newInstance(hsImpl);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static final HashSketchImpl newInstance(HashSketchImpl hsImpl)
			throws NoSuchAlgorithmException {
		if (hsImpl == null)
			return null;
		if (hsImpl instanceof FM85SketchImpl)
			return (HashSketchImpl)(new FM85SketchImpl(hsImpl));
		if (hsImpl instanceof DF03SketchImpl)
			return (HashSketchImpl)(new DF03SketchImpl(hsImpl));
		throw new NoSuchAlgorithmException("Trying to copy a sketch of type"
				+ hsImpl.getClass().getSimpleName());
	}
}
