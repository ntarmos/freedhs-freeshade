/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import java.security.NoSuchAlgorithmException;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class FM85SketchImpl extends HashSketchImpl {

	/**
	 * @param other
	 * @throws NoSuchAlgorithmException
	 */
	public FM85SketchImpl(HashSketchImpl other) throws NoSuchAlgorithmException {
		super(other);
	}

	/**
	 * @param numVecs
	 * @param hashAlgo
	 * @throws NoSuchAlgorithmException
	 */
	public FM85SketchImpl(int numVecs, String hashAlgo) throws NoSuchAlgorithmException {
		super(numVecs, hashAlgo);
	}

	/**
	 * @param numVecs
	 * @throws NoSuchAlgorithmException
	 */
	public FM85SketchImpl(int numVecs) throws NoSuchAlgorithmException {
		super(numVecs);
	}

	/**
	 * @param numVecs
	 * @param numBits
	 * @param hashAlgo
	 * @throws NoSuchAlgorithmException
	 */
	public FM85SketchImpl(int numVecs, int numBits, String hashAlgo)
			throws NoSuchAlgorithmException {
		super(numVecs, numBits, hashAlgo);
	}

	/**
	 * @param numVecs
	 * @param numBits
	 * @throws NoSuchAlgorithmException
	 */
	public FM85SketchImpl(int numVecs, int numBits) throws NoSuchAlgorithmException {
		super(numVecs, numBits);
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.centralized.sketches.Sketch#estimate()
	 */
	public double estimate() {
		double meanR = 0;
		for (int i = 0; i < sketch.length; i++) {
			for (int j = 0; j <= sketch[i].length(); j++) {
				if (sketch[i].get(j) == false) {
					meanR += j;
					break;
				}
			}
		}
		meanR /= sketch.length;
		return 1.29281 * Math.pow(2.0, meanR) * sketch.length;
	}
	
	public String toString() {
		return "[ FM85 :: " + toStringInternal() + " ]";
	}
}
