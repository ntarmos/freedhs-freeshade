/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;


/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class SketchAlgorithm {

	/*
	 * Default algorithm is DF03.
	 */
	private boolean	usingFM85	= false;

	/**
	 * @param usingFM85 True to use an FM85 estimator, false for a DF03 estimator
	 */
	public SketchAlgorithm(boolean usingFM85) {
		this.usingFM85 = usingFM85;
	}
	
	public SketchAlgorithm() {
	}
	
	/**
	 * @return true if the current algorithm is using a FM85 estimator, false
	 *         otherwise
	 */
	public final boolean isUsingFM85() {
		return this.usingFM85;
	}

	/**
	 * @param usingFM85 true if the current algorithm is using a FM85 estimator,
	 *            false otherwise
	 */
	public final SketchAlgorithm setUsingFM85() {
		this.usingFM85 = true;
		return this;
	}

	/**
	 * @return true if the current algorithm is using a DF03 estimator, false
	 *         otherwise
	 */
	public final boolean isUsingDF03() {
		return !isUsingFM85();
	}

	/**
	 * Ask the algorithm to use a DF03 estimator
	 */
	public final SketchAlgorithm setUsingDF03() {
		this.usingFM85 = false;
		return this;
	}
	
	public String toString() {
		return (usingFM85 ? "FM85" : "DF03");
	}
}
