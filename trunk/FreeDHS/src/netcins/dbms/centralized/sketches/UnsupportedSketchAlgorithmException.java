/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 * @deprecated Deprecating usage of SketchAlgorithms and
 *             UnsupportedSketchAlgorithmException in favor of
 *             {FM85,DF03,AMS}SketchImpl instances.
 */
public class UnsupportedSketchAlgorithmException extends Exception {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3888787148579751554L;

	public UnsupportedSketchAlgorithmException(String msg) {
		super(msg);
	}

	public UnsupportedSketchAlgorithmException() {
		super("Unsupported sketch algorithm");
	}
}
