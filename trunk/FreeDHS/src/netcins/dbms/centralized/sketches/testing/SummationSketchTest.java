/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches.testing;

import java.security.NoSuchAlgorithmException;
import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.DF03SketchImpl;
import netcins.dbms.centralized.sketches.FM85SketchImpl;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SketchAlgorithm;
import netcins.dbms.centralized.sketches.SummationSketchImpl;
import netcins.util.QuickMath;
import netcins.util.stats.ZipfGenerator;
import rice.environment.Environment;
import rice.p2p.commonapi.IdFactory;
import rice.pastry.Id;
import rice.pastry.commonapi.PastryIdFactory;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class SummationSketchTest {
	private HashSketchImpl		centralizedHashSketch	= null;
	private int					centralizedSum			= 0;
	private static final String	metric					= "Single Metric";
	private SketchAlgorithm		algorithm				= null;
	private SummationSketchImpl	centralizedSumSketch	= null;
	private IdFactory			idFactory				= null;
	private Environment			env						= null;

	private int					numItems				= 0;

	private boolean				isInited				= false;

	public SummationSketchTest(int numVecs, int L, SketchAlgorithm algorithm, Environment env) {
		if (isInited)
			destroy();
		if (numVecs <= 0 || L <= 0 || algorithm == null) {
			destroy();
			return;
		}
		this.env = env;
		this.idFactory = new PastryIdFactory(env);
		initHSImpl(algorithm, numVecs, L);
		isInited = true;
	}

	public void initHSImpl(SketchAlgorithm algorithm, int numVecs, int L) {
		Vector<String> metrics = new Vector<String>();
		metrics.add(SummationSketchTest.metric);
		this.algorithm = algorithm;
		try {
			this.centralizedHashSketch = (algorithm.isUsingDF03() ? new DF03SketchImpl(numVecs, L)
					: new FM85SketchImpl(numVecs, L));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		System.out.println("Initializing SummationSketchTest with " + this.centralizedHashSketch);
	}

	public void setSummationSketchImpl(SummationSketchImpl centralizedSumSketch) {
		this.centralizedSumSketch = centralizedSumSketch;
	}

	public boolean isInited() {
		return isInited;
	}

	public void destroy() {
		if (isInited == false)
			return;
		clearData();
		isInited = false;
	}

	public void clearData() {
		if (isInited == false)
			return;
		centralizedHashSketch.clear();
		centralizedSumSketch.clear();
		centralizedSum = 0;
		numItems = 0;
	}

	public void addData(int[] tuples) {
		System.runFinalization();
		System.gc();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems = tuples.length;

		System.out.println("Injecting " + numItems + " new data tuples...");
		for (int data = 0; data < numItems; data++) {
			BasicItem item = new BasicItem((Id)idFactory.buildRandomId(env.getRandomSource()),
					tuples[data]);
			centralizedSum += item.intValue();
			centralizedSumSketch.insert(item);

			if ((data % 100000) == 0) {
				System.out.print(" " + data + "/" + numItems + " ");
			} else if ((data % 10000) == 0) {
				System.out.print(".");
			}
		}

		System.out.println(" done. ");

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl data generation took " + (endTime - startTime) / 1000 + "\".");
		System.runFinalization();
		System.gc();
	}

	public void query() {
		System.runFinalization();
		System.gc();
		long startTime = System.currentTimeMillis(), endTime;
		Vector<String> metrics = new Vector<String>();
		metrics.add(SummationSketchTest.metric);
		System.out.println(algorithm + " centralized estimation: " + centralizedSum);
		double distRes = 0;

		distRes = centralizedSumSketch.estimate();
		System.out.println(centralizedSumSketch + ", estimation: " + QuickMath.doubleToString(distRes));
		System.out.println("Estimation error: "
				+ (100.0 * (distRes - centralizedSum) / centralizedSum) + " %");
		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("SummationSketchImpl query took " + (endTime - startTime) / 1000 + "\".");
	}

	public static void main(String[] args) {

		int numTuples = 0, numVecs = 0, L = 0, numValues = 0, iterations = 1;
		double theta = 0;

		try {
			numTuples = Integer.parseInt(args[0]);
			numVecs = Integer.parseInt(args[1]);
			L = Integer.parseInt(args[2]);
			numValues = Integer.parseInt(args[3]);
			theta = Double.parseDouble(args[4]);
			iterations = Integer.parseInt(args[5]);
		} catch (NumberFormatException e) {
			System.out.println("Usage: java [-cp FreePastry-<version>.jar]");
			System.out.println("\tnetcins.dbms.centralized.sketches.testing.SummationSketchTest <numItems> <numVecs> <L> <numValues> <zipf theta> <iterations>");

			System.exit(0);
		}

		long startTime = System.currentTimeMillis(), endTime = 0;

		SummationSketchTest test = null;
		int seed = (int)QuickMath.floor(Math.random() * (double)Integer.MAX_VALUE);
		//seed = 429092866;

		System.out.println("\n" + " --- Begin Simulation --- ");
		SketchAlgorithm testAlgos[] = new SketchAlgorithm[1];
		testAlgos[0] = new SketchAlgorithm().setUsingDF03();
		ZipfGenerator zipf = new ZipfGenerator(numValues, theta, seed);

		System.out.println(" --- Seed: " + seed + ", domain: " + numValues + ", Zipf theta: "
				+ theta + " ---");
		try {
			Environment env = Environment.directEnvironment(seed);
			test = new SummationSketchTest(numVecs, L, new SketchAlgorithm(), env);
			System.out.println("\n" + " ---" + "\n");
			int nVecs = numVecs;

			for (SketchAlgorithm algo : testAlgos) {
				int[] data = new int[numTuples];
				int sum = 0;
				for (int i = 0; i < numTuples; i++) {
					data[i] = zipf.nextRank();
					sum += data[i];
				}
				System.out.println("Data sum: " + sum);

				test.initHSImpl(algo, nVecs, L);
				SummationSketchImpl testSummationSketch = new SummationSketchImpl(
						test.centralizedHashSketch);
				test.setSummationSketchImpl(testSummationSketch);
				for (int iter = 0; iter < iterations; iter++) {
					test.clearData();
					test.addData(data);
					System.out.println("Testing " + algo + ", iteration: " + iter + " ...");
					test.query();
					System.out.println("\n" + " ---" + "\n");
				}
			}
			// }
			endTime = System.currentTimeMillis();
			test.destroy();
			env.destroy();
			System.out.println(" Total Simulation Time: " + (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}
}
