/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DF03SketchImpl extends HashSketchImpl {

	public static final double	df03VecsSetULPercentage	= 1.0;

	/**
	 * @param other
	 * @throws NoSuchAlgorithmException 
	 */
	public DF03SketchImpl(HashSketchImpl other) throws NoSuchAlgorithmException {
		super(other);
	}

	/**
	 * @param numVecs
	 * @param hashAlgo
	 * @throws NoSuchAlgorithmException
	 */
	public DF03SketchImpl(int numVecs, String hashAlgo) throws NoSuchAlgorithmException {
		super(numVecs, hashAlgo);
	}

	/**
	 * @param numVecs
	 * @throws NoSuchAlgorithmException
	 */
	public DF03SketchImpl(int numVecs) throws NoSuchAlgorithmException {
		super(numVecs);
	}

	/**
	 * @param numVecs
	 * @param numBits
	 * @param hashAlgo
	 * @throws NoSuchAlgorithmException
	 */
	public DF03SketchImpl(int numVecs, int numBits, String hashAlgo)
			throws NoSuchAlgorithmException {
		super(numVecs, numBits, hashAlgo);
	}

	/**
	 * @param numVecs
	 * @param numBits
	 * @throws NoSuchAlgorithmException
	 */
	public DF03SketchImpl(int numVecs, int numBits) throws NoSuchAlgorithmException {
		super(numVecs, numBits);
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.centralized.sketches.Sketch#estimate()
	 */
	public double estimate() {
		double meanR = 0;
		int[] allR = new int[sketch.length];
		for (int i = 0; i < sketch.length; i++) {
			allR[i] = (sketch[i].length() > 0 ? sketch[i].length() : 0);
		}
		Arrays.sort(allR);
		int low = (int)QuickMath.floor((double)sketch.length
				* (1.0 - DF03SketchImpl.df03VecsSetULPercentage));

		for (int i = low; i < sketch.length; i++)
			meanR += allR[i];

		meanR /= (sketch.length - low);
		return 0.39701 * Math.pow(2, meanR) * (sketch.length - low);
	}
	
	public String toString() {
		return "[ DF03 :: " + toStringInternal() + " ]";
	}
}
