/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.sketches;

import netcins.ItemSerializable;

/**
 * Basic interface to be implemented by all sketch implementations.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public interface Sketch {

	/** Default hash algorithm used to compute the hash value for item insertion */
	public static final String	defaultHashAlgo	= "MD5";

	/**
	 * Inserts a single item in the sketch.
	 * 
	 * @param object The item to be added.
	 */
	public void insert(ItemSerializable object);

	/**
	 * Clears the internal state of the current sketch instance.
	 */
	public void clear();

	/**
	 * Computes the estimate of the current sketch instance.
	 * 
	 * @return The estimate for the current sketch instance.
	 */
	public double estimate();
}
