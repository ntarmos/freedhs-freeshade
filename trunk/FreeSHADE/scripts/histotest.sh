#!/bin/sh

JVM_ARGS='-Xms128m -Xmx900m -Xrs -ea'
PASTRY_ROOT=/home/ntarmos/workspace/pastry-svn

DHS_ROOT=`echo \`dirname $0\`/../..`

DHS_CLASSPATH=$DHS_ROOT/DHS/classes:$DHS_ROOT/P2PStats/classes:$PASTRY_ROOT/classes
for jar in $PASTRY_ROOT/lib/*.jar; do
	DHS_CLASSPATH=$DHS_CLASSPATH:$jar
done
echo "Using CLASSPATH: $DHS_CLASSPATH"

java $JVM_ARGS -classpath $DHS_CLASSPATH \
	netcins.dbms.distributed.histograms.testing.DHSHistogramsTest \
	$*

