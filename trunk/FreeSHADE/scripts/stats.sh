#!/bin/sh

CONFFILE=$1

if [ -z "$CONFFILE" -o ! -f $CONFFILE ]; then
	echo "No conf file defined. Bailing out..."
	exit 0
fi

. $CONFFILE

for theta in $THETA; do
	PRTH=0
	for bmp in $BITMAPS; do
		LOGFILE="logs/$LOGSUBDIR/console.log-$theta-$bmp"
		if [ ! -f $LOGFILE ]; then
			continue;
		fi;
		NRES=`grep 'Estimation error' $LOGFILE | wc -l | awk '{print $1}'`
		if [ $NRES -gt 0 ]; then
			TAIL=$(( $NRES - $NRES * $OUTLIERS / 200 )) 
			HEAD=$(( $NRES - $NRES * $OUTLIERS / 100 )) 

			if [ $PRTH -eq 0 ]; then
				echo "Theta: $theta";
				PRTH=1
			fi

			echo -n "       $bmp bitmaps: ";
			awk '/Estimation error/{print $3}' $LOGFILE 2>/dev/null | \
				sort -g | tail `echo "-$TAIL"` | head `echo "-$HEAD"` | \
				awk '{sum+=$1; count++;}END{print "\t"sum/(count > 0 ? count : 1)" "count}';
		fi
	done;
done

