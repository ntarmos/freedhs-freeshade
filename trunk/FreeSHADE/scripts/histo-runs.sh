#!/bin/bash

THETA=(0.0 0.7 1.0 1.2)
MAXV=(1000 10000)
CACHE=(cache nocache)

for CUR_CACHE in $CACHE; do
	for CUR_MAXV in $MAXV; do
		for CUR_THETA in $THETA; do
			echo "Executing $CUR_THETA theta - $CUR_MAXV values - $CUR_CACHE case"
			./scripts/histotest.sh \
				1000 1500000 128 32 7 0 0 $CUR_MAXV 10 $CUR_THETA $CUR_CACHE 1000 \
				&>logs/console.log-$CUR_THETA-$CUR_MAXV-$CUR_CACHE;
			./scripts/genplots.sh logs/console.log-$CUR_THETA-$CUR_MAXV-$CUR_CACHE;
		done;
	done;
done;

