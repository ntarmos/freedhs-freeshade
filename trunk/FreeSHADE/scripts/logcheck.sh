#!/bin/sh


TOPDIR="`dirname $0`/..";
LOGDIR="$TOPDIR/logs";
PLOT='';

if [ -n "$1" -a -d "$TOPDIR/logs/$1" ]; then
	LOGDIR="$LOGDIR/$1";
	PLOT="$1"
fi

if [ -n "$2" ]; then
	SLEEP="$2";
else
	SLEEP=300;
fi

log() {
	echo -n "$(date '+%Y%m%d.%H%M%S') [DHS logcheck"
	[ -n "$PLOT" ] && echo -n " ($PLOT)";
	echo "]: $*";
}

if which gfind >/dev/null; then
	FIND=$(which gfind);
elif which find >/dev/null; then
	FIND=$(which find);
else
	log "No known 'find' executable in path. Bailing out...";
	exit 1;
fi

lastfile() {
	$FIND $LOGDIR -type f -printf '%C@ %p\n' | sort -g | tail -1;
}

PREVF="$(lastfile)";

clear;
log "Starting up...";
while true; do
	sleep 300;
	LASTF="$(lastfile)";
	DATE=`date '+%a, %b %d %H:%M'`;
	if [ -n "$PREVF" -a -n "$LASTF" -a "$PREVF" = "$LASTF" ]; then
		PID=$(lsof $(echo "$LASTF" | awk '{print $2}') 2>/dev/null)
		if [ $? -eq 0 ]; then
			PID=$(echo "$PID" | tail -1 | awk '{print $1}')
		else
			PID='';
		fi
		if [ -n "$PID" ]; then
			log "Simulation seems stuck. Killing off java processes..."
			kill -9 $PID;
		else
			log "Simulation seems terminated. Bailing out..."
			exit 0;
		fi
	else
		log "Simulation up and running.";
	fi
	PREVF="$LASTF";
done

