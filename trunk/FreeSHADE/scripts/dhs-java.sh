#!/bin/sh

JVM_ARGS='-Xms128m -Xmx900m -Xrs -ea'
PASTRY_ROOT=/home/ntarmos/workspace/pastry-svn

DHS_ROOT=`echo \`dirname $0\`/../..`

DHS_CLASSPATH=$DHS_ROOT/FreeDHS/classes:$DHS_ROOT/FreeSHADE/classes:$PASTRY_ROOT/classes
for jar in $PASTRY_ROOT/lib/*.jar; do
	DHS_CLASSPATH=$DHS_CLASSPATH:$jar
done

java $JVM_ARGS -classpath $DHS_CLASSPATH \
	$*

