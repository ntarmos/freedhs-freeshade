#!/bin/sh

TOPDIR=`dirname $0`/..
CONFFILE=$1

if [ -z "$CONFFILE" -o ! -f $CONFFILE ]; then
	echo "No conf file defined. Bailing out..."
	exit 0
fi

. $CONFFILE

if [ -f $TOPDIR/logs/$LOGSUBDIR ]; then
	echo "$TOPDIR/logs/$LOGSUBDIR is a file! Bailing out..."
	exit 0
fi

if [ ! -d $TOPDIR/logs/$LOGSUBDIR ]; then
	mkdir -p $TOPDIR/logs/$LOGSUBDIR
fi

if [ -z "$NODES" ]; then
	NODES=1000;
fi

if [ -z "$ITEMMULT" ]; then
	ITEMMULT=300;
fi

if [ -z "$REPLICAS" ]; then
	REPLICAS=0
fi

if [ -z "$RETRIES" ]; then
	RETRIES=5;
fi

for CUR_NODES in $NODES; do
	for CUR_MULT in $ITEMMULT; do
		CUR_ITEMS=$(( $CUR_NODES*$CUR_MULT ));
		for CUR_THETA in $THETA; do
			for CUR_BITMAPS in $BITMAPS; do
				for CUR_ALGO in $ALGORITHMS; do
					for CUR_CACHE in $CACHE; do
						for CUR_ROUTING in $ROUTING; do
							for CUR_PROBE in $PROBING; do
								for CUR_REPL in $REPLICAS; do
									for CUR_RETR in $RETRIES; do
										for CUR_BATCH in $BATCHING; do
											for CUR_SHORT in $SHORTCUTS; do
												while true; do
	echo "Executing $CUR_THETA theta - $CUR_BITMAPS bitmaps -- $CUR_NODES nodes -- \
$CUR_ITEMS items -- $CUR_RETR retries -- $CUR_REPL replicas -- $CUR_ALGO $CUR_CACHE \
$CUR_ROUTING $CUR_PROBE $CUR_BATCH $CUR_SHORT case"
	$TOPDIR/scripts/dhs-java.sh $TESTCLASS \
		$CUR_NODES $CUR_ITEMS $CUR_BITMAPS 32 $CUR_RETR $CUR_REPL 1000 $CUR_THETA \
		$ITERATIONS $CUR_ALGO $CUR_CACHE $CUR_ROUTING $CUR_PROBE $CUR_BATCH $CUR_SHORT \
		>> $TOPDIR/logs/$LOGSUBDIR/console.log-$CUR_NODES-$CUR_ITEMS-$CUR_THETA-$CUR_BITMAPS-$CUR_ALGO-$CUR_CACHE-$CUR_ROUTING-$CUR_PROBE-$CUR_RETR-$CUR_REPL-$CUR_BATCH-$CUR_SHORT 2>&1;
	if [ $? -eq 0 ]; then
		break;
	fi
												done;
											done;
										done;
									done;
								done;
							done;
						done;
					done;
				done;
			done;
		done;
	done;
done;

