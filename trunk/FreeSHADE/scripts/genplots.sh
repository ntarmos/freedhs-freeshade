# should be run using /bin/bash

NORMALFONTFACE="Times"
NORMALFONTSIZE="11"
SMALLFONTFACE="Times"
SMALLFONTSIZE="9"

TOPDIR=`dirname $0`
PLOTSDIR=$TOPDIR/../plots
LOGSDIR=$TOPDIR/../logs
if [ $# -ge 1 ]; then
	LOGFILE=$1
else
	LOGFILE=$LOGSDIR/console.log
fi
if [ $# -eq 2 ]; then
	RV_LOGFILE=$2
fi
RV_GEN=$TOPDIR/gen_rv_hits
RV_STATS=$PLOTSDIR/rv-stats
SNAPSHOTS=(250 500 750 1000)


TMPDIR=/tmp
function tmpfile {
	TMPF=`mktemp /tmp/statsplot.XXXXXXXX || (echo "Can't create temp file!"; exit 1)`
	test -f $TMPF || (echo "WTF?!"; exit 1)
	echo $TMPF
}

test -f $LOGFILE || (echo "Can't open $LOGFILE!"; exit 1)
test -x $RV_GEN || (echo "Can't find $RV_GEN!"; exit 1)

echo "Using log file $LOGFILE"

PLOTCMDS=`tmpfile`
SETUP_LINE=`cat $LOGFILE | grep 'Initializing test' | tail -1`
NBITS=`echo $SETUP_LINE | awk '{print $34}'`
NCELLS=`echo $SETUP_LINE | awk '{print $10}'`
MINV=`echo $SETUP_LINE | awk '{print $14}' | colrm 1 1 | sed 's/\.[0-9]*$//g'`
MAXV=`echo $SETUP_LINE | awk '{print $16}' | sed -e 's/)],//g' -e 's/\.[0-9]*$//g'`
NUMV=$((MAXV - MINV))
ZIPF=`grep 'Seed:' $LOGFILE | tail -1 | awk '{print $10}'`
NITEMS=`grep 'items/sec' $LOGFILE | tail -1 | sed -e 's#/.*$##g' -e 's/ //g'`
NNODES=`grep ' done.$' $LOGFILE | awk '/^ 0\//{print $1}' | tail -1 | colrm 1 2`
CACHING=`grep ' iteration ' $LOGFILE | tail -1 | awk '{print $2}' | sed -e 's/.*\///g' -e 's/,//g'`

RV_GEN="$RV_GEN -b $NBITS -c $NCELLS -m $MINV -M $MAXV -z $ZIPF -Z $ZIPF -i $NITEMS -n $NNODES "
if [ ! -z "$RV_LOGFILE" ]; then
	RV_GEN="$RV_GEN -I $RV_LOGFILE"
fi

RV_LOG=`tmpfile`
RV_LOG_SINGLE=`tmpfile`
RV_STATS_TMP=`tmpfile`
RV_STATS_TMP_SINGLE=`tmpfile`
DHS_I_HITS=`tmpfile`
RV_I_HITS=`tmpfile`
RV_I_HITS_SINGLE=`tmpfile`
DHS_Q_HITS=()
RV_Q_HITS=()
RV_Q_HITS_SINGLE=()

$RV_GEN > $RV_LOG 2>$RV_STATS_TMP
$RV_GEN -c1 > ${RV_LOG_SINGLE} 2>${RV_STATS_TMP_SINGLE}

grep 'Insertion hits per node' $LOGFILE | \
	colrm 1 25 | tr ' ' '\n' | sort -g -r > $DHS_I_HITS
grep 'Insertion hits per node' $RV_LOG | \
	colrm 1 25 | tr ' ' '\n' | sort -g -r > $RV_I_HITS
grep 'Insertion hits per node' ${RV_LOG_SINGLE} | \
	colrm 1 25 | tr ' ' '\n' | sort -g -r > $RV_I_HITS_SINGLE

for (( snap=0; $snap < ${#SNAPSHOTS[@]}; $((snap++)) )); do
	DHS_Q_HITS[$snap]=`tmpfile`;
	grep 'Query hits per node' $LOGFILE | \
		head -${SNAPSHOTS[$snap]} | tail -1 | colrm 1 21 | \
		tr ' ' '\n' | sort -g -r > ${DHS_Q_HITS[$snap]}

	RV_Q_HITS[$snap]=`tmpfile`;
	$RV_GEN -q ${SNAPSHOTS[$snap]} 2>>$RV_STATS_TMP | \
		grep 'Query hits per node' | colrm 1 21 | \
		tr ' ' '\n' | sort -g -r > ${RV_Q_HITS[$snap]}

	RV_Q_HITS_SINGLE[$snap]=`tmpfile`;
	$RV_GEN -q ${SNAPSHOTS[$snap]} -c1 2>>${RV_STATS_TMP_SINGLE} | \
		grep 'Query hits per node' | colrm 1 21 | \
		tr ' ' '\n' | sort -g -r > ${RV_Q_HITS_SINGLE[$snap]}
done;

NORMALFONT=`echo "font \"$NORMALFONTFACE,$NORMALFONTSIZE\""`
SMALLFONT=`echo "font \"$SMALLFONTFACE,$SMALLFONTSIZE\""`

(
echo "set terminal pdf colour enhanced fname \"$SMALLFONTFACE\" fsize $SMALLFONTSIZE"
echo "set style fill pattern 6"
echo "unset y2label"
echo "unset y2tics"
echo "unset ytics"
#echo "set xtics autofreq $SMALLFONT"
#echo "set ytics autofreq $SMALLFONT"
#echo "set key on autotitles"

echo "set xlabel \"Nodes\" $NORMALFONT"
echo "set xtics out nomirror 0,100,1000 $SMALLFONT"
echo "set ytics out nomirror autofreq $SMALLFONT"

#echo "set ytics mirror 0,1000,14000 $NORMALFONT"
echo "set ylabel \"Insertion Load (Node Hits)\" $NORMALFONT"
echo "set output '$PLOTSDIR/insertion-hits-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}.pdf"
echo "plot '$DHS_I_HITS' t 'DHS' w lp axis x1y1, '$RV_I_HITS_SINGLE' t 'RendezVous -- 1 Node per Histogram' w lp axis x1y1, '$RV_I_HITS' t 'RendezVous -- 1 Node per Bucket' w lp axis x1y1"
echo "unset y2tics"

echo "set ylabel \"Query Load (Node Hits)\" $NORMALFONT"
#echo "set ytics mirror 0,10,100 $SMALLFONT"
echo "set ytics out nomirror autofreq $SMALLFONT"

echo "set grid"
echo "set output '$PLOTSDIR/rv-query-hits-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}.pdf"
echo "plot [-50:1000] [0:"$(( `sort -g ${RV_Q_HITS[3]} | tail -1` + 500 ))"] '${RV_Q_HITS[3]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- 1000 queries' w lp, '${RV_Q_HITS[2]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- &{2}750 queries' w lp, '${RV_Q_HITS[1]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- &{2}500 queries' w lp, '${RV_Q_HITS[0]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- &{2}250 queries' w lp"

echo "set output '$PLOTSDIR/rv-query-hits-single-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}.pdf"
echo "plot [-50:1000] [0:"$(( `sort -g ${RV_Q_HITS_SINGLE[3]} | tail -1` + 500 ))"] '${RV_Q_HITS_SINGLE[3]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- 1000 queries' w lp, '${RV_Q_HITS_SINGLE[2]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- &{2}750 queries' w lp, '${RV_Q_HITS_SINGLE[1]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- &{2}500 queries' w lp, '${RV_Q_HITS_SINGLE[0]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'RendezVous -- &{2}250 queries' w lp"

echo "set output '$PLOTSDIR/dhs-query-hits-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}.pdf'"
echo "plot [-50:1000] [0:"$(( `sort -g ${DHS_Q_HITS[3]} | tail -1` + 10 ))"] '${DHS_Q_HITS[3]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'DHS -- 1000 queries' w lp, '${DHS_Q_HITS[2]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'DHS -- &{2}750 queries' w lp, '${DHS_Q_HITS[1]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'DHS -- &{2}500 queries' w lp, '${DHS_Q_HITS[0]}' u (("'$0'" / 5 != int("'$0'" / 5)) ? 1/0 : "'$1'") t 'DHS -- &{2}250 queries' w lp"


echo "set ylabel \"DHS -- Query Load (Node Hits)\" $NORMALFONT"
echo "set y2label \"RendezVous -- Query Load (Node Hits)\" $NORMALFONT"
echo "unset grid"

STEPS=8;
for (( snap=0; $snap < ${#SNAPSHOTS[@]}; $((snap++)) )); do
	CRQHITS=${RV_Q_HITS[$snap]}
	CRQHITS_SINGLE=${RV_Q_HITS_SINGLE[$snap]}
	CDQHITS=${DHS_Q_HITS[$snap]}
	CSNAP=${SNAPSHOTS[$snap]}

	LIM_RV=$(( `head -1 ${CRQHITS}` / 100 * 100 ));
	LIM_RV_SINGLE=$(( `head -1 ${CRQHITS_SINGLE}` / 100 * 100 ));
	STP_RV=$(( ($LIM_RV / $STEPS / 100 + 1) * 100 ));
	if [ $LIM_RV -le `head -1 ${CRQHITS}` ]; then
		LIM_RV=$(( ($LIM_RV / $STP_RV + 1) * $STP_RV ));
	fi
	if [ $LIM_RV_SINGLE -le `head -1 ${CRQHITS_SINGLE}` ]; then
		LIM_RV_SINGLE=$(( ($LIM_RV / $STP_RV + 1) * $STP_RV ));
	fi

	LIM_DHS=$(( `head -1 ${CDQHITS}` / 10 * 10 ));
	STP_DHS=$(( ($LIM_DHS / $STEPS / 10 + 1) * 10 ));
	if [ $LIM_DHS -le `head -1 ${CDQHITS}` ]; then
		LIM_DHS=$(( ($LIM_DHS / $STP_DHS + 1) * $STP_DHS ));
	fi

#	echo "set ytics nomirror 0,$STP_DHS,$LIM_DHS $SMALLFONT"
	echo "set ytics nomirror out $SMALLFONT"

	echo "unset y2tics"
	echo "set output '$PLOTSDIR/${CSNAP}qs-query-hits-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}.pdf'"
	echo "plot [-50:999] [0:${LIM_RV}] '$CDQHITS' t 'DHS -- $CSNAP queries' w lp, '$CRQHITS_SINGLE' t 'RendezVous (1 Node per Histogram) -- $CSNAP queries' w lp, '$CRQHITS' t 'RendezVous (1 Node per Bucket) -- $CSNAP queries' w lp"

#	echo "set y2tics nomirror 0,$STP_RV,$LIM_RV $SMALLFONT"
	echo "set y2tics nomirror out $SMALLFONT"
	echo "set output '$PLOTSDIR/${CSNAP}qs-query-hits-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}-scaled.pdf'"
	echo "plot [-50:999] [0:${LIM_DHS}] [0:999] [0:${LIM_RV}] '$CDQHITS' t 'DHS -- $CSNAP queries' w lp axis x1y1, '$CRQHITS_SINGLE' t 'RendezVous (1 Node per Histogram) -- $CSNAP queries' w lp axis x1y2, '$CRQHITS' t 'RendezVous (1 Node per Bucket) -- $CSNAP queries' w lp axis x1y2"
done
) >> $PLOTCMDS
#
cat $PLOTCMDS | gnuplot

mv $RV_STATS_TMP ${RV_STATS}-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}
mv ${RV_STATS_TMP_SINGLE} ${RV_STATS}-single-${ZIPF}zipf-${NUMV}values-${NNODES}nodes-${NITEMS}items-${NCELLS}buckets-${CACHING}

for tmpfile in $PLOTCMDS $RV_LOG ${RV_LOG_SINGLE} $DHS_I_HITS $RV_I_HITS $RV_I_HITS_SINGLE ${DHS_Q_HITS[@]} ${RV_Q_HITS[@]} ${RV_Q_HITS_SINGLE[@]}; do
	test -f $tmpfile && rm -f $tmpfile
done

