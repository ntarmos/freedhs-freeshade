/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import netcins.dbms.BasicItem;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class AverageShiftedEquiWidthHistogramImpl extends BasicHistogramImpl {
	public static final int				defaultNumShifts	= 10;

	private BasicItem					shift				= null;
	private int							numShifts;
	private EquiWidthHistogramImpl[]	histograms			= null;
	private double						bucketSpread		= 0;
	private int							numBuckets;

	public AverageShiftedEquiWidthHistogramImpl(BasicItem lowest, BasicItem highest,
			Integer numBuckets) {
		this(lowest, highest, numBuckets, AverageShiftedEquiWidthHistogramImpl.defaultNumShifts);
	}

	public AverageShiftedEquiWidthHistogramImpl(BasicItem lowest, BasicItem highest,
			Integer numBuckets, Integer numShifts) {
		super(lowest, highest, numBuckets);
		buckets = null; // We don't need to maintain buckets here.
		assert (numShifts > 0);
		this.numShifts = numShifts;
		this.numBuckets = numBuckets;
		this.bucketSpread = highest.subtract(lowest).doubleValue() / (double)(numBuckets);
		this.shift = new BasicItem(bucketSpread / (double)numShifts);
		this.histograms = new EquiWidthHistogramImpl[numShifts];
		for (int i = 0; i < histograms.length; i++) {
			histograms[i] = new EquiWidthHistogramImpl(lowest.add(shift.multiply(i)),
					highest.add(shift.multiply(i)), numBuckets);
		}
	}

	public AverageShiftedEquiWidthHistogramImpl(BasicItem lowest, BasicItem highest,
			int numBuckets, EquiWidthHistogramImpl[] histograms) {
		this(lowest, highest, numBuckets, histograms.length);
		this.histograms = new EquiWidthHistogramImpl[histograms.length];
		System.arraycopy(histograms, 0, this.histograms, 0, histograms.length);
	}

	public void insert(BasicItem item, double freq) throws BucketIndexOutOfBoundsException {
		for (int i = 0; i < histograms.length; i++) {
			try {
				histograms[i].insert(item, freq);
			} catch (BucketIndexOutOfBoundsException e) {
			}
		}
	}

	public double getNumItemsInRange(BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException {
		double ret = 0;

		if (low.compareTo(lowest) < 0 || high.compareTo(lowest) < 0 || low.compareTo(highest) > 0
				|| high.compareTo(highest) > 0)
			throw new BucketIndexOutOfBoundsException();

		for (int i = 0; i < histograms.length; i++) {
			BasicItem l = (low.compareTo(histograms[i].getLowest()) < 0 ? histograms[i].getLowest()
					: low);
			BasicItem h = (high.compareTo(histograms[i].getHighest()) > 0
					? histograms[i].getHighest() : high);
			ret += histograms[i].getNumItemsInRange(l, h);
			/*
			 * int startBucket; try { startBucket =
			 * histograms[i].getBucketForItem(low); } catch
			 * (BucketIndexOutOfBoundsException e) { startBucket = 0; } int
			 * endBucket; try { endBucket =
			 * histograms[i].getBucketForItem(high); } catch
			 * (BucketIndexOutOfBoundsException e) { endBucket =
			 * histograms[i].getNumBuckets() - 1; } ret +=
			 * histograms[i].getNumItemsInRange(
			 * histograms[i].getBucket(startBucket).getLowest(),
			 * histograms[i].getBucket(endBucket).getHighest());
			 */
		}
		return (ret / numShifts);
	}

	public double getNumDistinctItemsInRange(BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException {
		double ret = 0;

		if (low.compareTo(lowest) < 0 || high.compareTo(lowest) < 0 || low.compareTo(highest) > 0
				|| high.compareTo(highest) > 0)
			throw new BucketIndexOutOfBoundsException();

		for (int i = 0; i < histograms.length; i++) {
			BasicItem l = (low.compareTo(histograms[i].getLowest()) < 0 ? histograms[i].getLowest()
					: low);
			BasicItem h = (high.compareTo(histograms[i].getHighest()) > 0
					? histograms[i].getHighest() : high);
			ret += histograms[i].getNumDistinctItemsInRange(l, h);
			/*
			 * int startBucket; try { startBucket =
			 * histograms[i].getBucketForItem(low); } catch
			 * (BucketIndexOutOfBoundsException e) { startBucket = 0; } int
			 * endBucket; try { endBucket =
			 * histograms[i].getBucketForItem(high); } catch
			 * (BucketIndexOutOfBoundsException e) { endBucket =
			 * histograms[i].getNumBuckets() - 1; } for (int bucket =
			 * startBucket; bucket <= endBucket; bucket ++) ret +=
			 * histograms[i].getNumDistinctItemsInBucket(bucket);
			 */
		}
		return (ret / (double)histograms.length);
	}

	public int getBucketForItem(BasicItem item) {
		throw new UnsupportedOperationException();
	}

	public void initBuckets() {
		areBucketsInited = true;
	}

	public int getNumBuckets() {
		return numBuckets;
	}

	public void clear() {
		for (int i = 0; i < histograms.length; i++)
			histograms[i].clear();
	}

	public String toString() {
		return "[ AS :: " + super.toString() + ", shifts: " + histograms.length + " ]";
	}

	public String toStringFull() {
		StringBuilder builder = new StringBuilder();
		builder.append(toString() + "\n");
		for (int i = 0; i < histograms.length; i++)
			builder.append(histograms[i].toStringFull() + "\n");
		return builder.toString();
	}

	public double getNumItemsInBucket(int bucket) throws BucketIndexOutOfBoundsException {
		double ret = 0;
		for (int i = 0; i < histograms.length; i++)
			ret += histograms[i].getNumItemsInBucket(bucket);
		return (ret / (double)histograms.length);
	}

	public Bucket getBucket(int bucket) throws BucketIndexOutOfBoundsException {
		return new Bucket(lowest.add(bucketSpread * bucket),
				lowest.add(bucketSpread * (bucket + 1)), getNumItemsInBucket(bucket),
				getNumDistinctItemsInBucket(bucket));
	}
}
