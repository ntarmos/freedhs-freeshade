/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms.testing;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.AdvancedHistogramImpl;
import netcins.dbms.centralized.histograms.AverageShiftedEquiWidthHistogramImpl;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.centralized.histograms.CompressedHistogramImpl;
import netcins.dbms.centralized.histograms.EquiDepthHistogramImpl;
import netcins.dbms.centralized.histograms.EquiWidthHistogramImpl;
import netcins.dbms.centralized.histograms.Histogram;
import netcins.dbms.centralized.histograms.MaxDiffHistogramImpl;
import netcins.util.QuickMath;
import netcins.util.stats.ZipfGenerator;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class CentralizedHistogramsTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BasicItem lowest = new BasicItem(0), highest = new BasicItem(1000000);
		int numBuckets = 10, numItems = 1000000, domain = 0;
		double theta = 0.7;

		Histogram[] histograms = new Histogram[4];

		histograms[0] = new EquiWidthHistogramImpl(lowest, highest, numBuckets);
		histograms[1] = new CompressedHistogramImpl(lowest, highest, numBuckets);
		histograms[2] = new EquiDepthHistogramImpl(lowest, highest, numBuckets);
		histograms[3] = new MaxDiffHistogramImpl(lowest, highest, numBuckets);
		histograms[4] = new AverageShiftedEquiWidthHistogramImpl(lowest, highest, numBuckets, 10);

		long time = System.currentTimeMillis(), prevTime, curTime;
		System.out.print("Generating items... ");
		domain = highest.subtract(lowest).intValue();
		ZipfGenerator zipf = new ZipfGenerator(domain, theta);

		long items[] = new long[domain];

		for (int i = 0; i < domain; i++)
			items[i] = lowest.add(i).intValue();
		for (int i = items.length - 1; i > 1; i--) {
			int index = (int)QuickMath.floor(Math.random() * i);
			long temp = items[i];
			items[i] = items[index];
			items[index] = temp;
		}

		Vector<BasicItem> data = new Vector<BasicItem>();

		for (int i = 0; i < numItems; i++)
			data.add(new BasicItem(items[zipf.nextRank()]));
		items = null;
		zipf = null;
		System.gc();
		System.out.println(" done.");

		System.out.print("Inserting items... ");
		prevTime = System.currentTimeMillis();
		for (Histogram histo : histograms) {
			for (int i = 0; i < numItems; i++) {
				try {
					histo.insert(data.get(i));
				} catch (BucketIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
			if (histo instanceof AdvancedHistogramImpl)
				((AdvancedHistogramImpl)histo).initData();
			/*
			 * if (i > 0 && (i % 100000) == 0) { curTime =
			 * System.currentTimeMillis(); System.out.println(" " + i + "/" +
			 * numItems + " items, " + QuickMath.doubleToString(100000000.0 /
			 * (double)(curTime - prevTime)) + " items/sec "); prevTime =
			 * curTime; } else if (i > 0 && (i % 10000) == 0) {
			 * System.out.print("."); }
			 */
		}
		curTime = System.currentTimeMillis();
		System.out.print(" " + ((curTime - prevTime) / 1000) + "'' ");
		System.out.println(" done.");

		System.out.println("Querying... ");
		for (Histogram histo : histograms) {
			System.out.println(histo.toString());
			double sum = 0;
			for (int i = 0; i < numBuckets; i++) {
				double bItems = 0;
				try {
					bItems = histo.getNumItemsInBucket(i);
				} catch (BucketIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
				System.out.print(QuickMath.doubleToString(bItems) + " ");
				sum += bItems;
			}
			System.out.println();
			System.out.println("Total number of items: " + sum);
			System.out.println(histo.toStringFull());
			System.out.println(" --- ");
			System.out.println();
		}

		for (Histogram histo : histograms)
			histo.clear();

		System.out.print("Inserting items... ");
		prevTime = System.currentTimeMillis();
		for (Histogram histo : histograms) {
			try {
				histo.insertAll(data);
			} catch (BucketIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}
		curTime = System.currentTimeMillis();
		System.out.print(" " + ((curTime - prevTime) / 1000) + "'' ");
		System.out.println(" done.");

		System.out.println("Querying... ");
		for (Histogram histo : histograms) {
			System.out.println(histo.toString());
			double sum = 0;
			for (int i = 0; i < numBuckets; i++) {
				double bItems = 0;
				try {
					bItems = histo.getNumItemsInBucket(i);
				} catch (BucketIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
				System.out.print(QuickMath.doubleToString(bItems) + " ");
				sum += bItems;
			}
			System.out.println();
			System.out.println("Total number of items: " + sum);
			System.out.println(histo.toStringFull());
			System.out.println(" --- ");
			System.out.println();
		}
		time = System.currentTimeMillis() - time;
		System.out.println("Total time: " + QuickMath.doubleToString((double)time / 1000.0) + "''");
	}
}
