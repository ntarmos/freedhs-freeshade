/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import java.util.Iterator;

import netcins.dbms.BasicItem;
import netcins.util.QuickMath;

/**
 * Implementation class for equi-depth histograms. This is a rather naive
 * implementation, useable mainly for experimental evaluation puproses. All
 * inserted items are kept internally in a Vector object, and the actual
 * histogram is dynamically generated (or re-generated, if necessary) on query
 * time.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class EquiDepthHistogramImpl extends AdvancedHistogramImpl {

	private int	itemsPerBucket	= 0;

	/**
	 * Constructs a new EquiDepthHistogramImpl object.
	 * 
	 * @param lowest The lowest value to be inserted in the histogram.
	 * @param highest The highest value to be inserted in the histogram.
	 * @param numBuckets The number of buckets of the histogram.
	 */
	public EquiDepthHistogramImpl(BasicItem lowest, BasicItem highest, Integer numBuckets) {
		super(lowest, highest, numBuckets);
	}

	public EquiDepthHistogramImpl(BasicItem lowest, BasicItem highest, Bucket[] buckets) {
		super(lowest, highest, buckets);
	}

	/**
	 * Returns the bucket corresponding to a given item. The return value is
	 * actually the bucket index in which the given item would have been put,
	 * were it inserted in the histogram earlier. That is, if one inserts this
	 * item in the historgam, there is no guarantee it will actually reside in
	 * the returned bucket (since bucket boundaries are not constant).
	 * 
	 * @param item The item to check for.
	 * @return The index of the bucket corresponding to the given item.
	 * @throws BucketIndexOutOfBoundsException
	 * @see netcins.dbms.centralized.histograms.Histogram#getBucketForItem(BasicItem)
	 */
	public int getBucketForItem(BasicItem item) throws BucketIndexOutOfBoundsException {
		if (item.compareTo(lowest) < 0 || item.compareTo(highest) > 0)
			throw new BucketIndexOutOfBoundsException();
		if (areBucketsInited == false)
			initBuckets();

		for (int i = 0; i < buckets.length; i++) {
			if (item.compareTo(buckets[i].getHighest()) < 0)
				return i;
		}
		return buckets.length - 1; // This should never happen...
	}

	/**
	 * Computes the number of items in the given range, using the underlying
	 * equi-depth histogram.
	 * 
	 * @throws BucketIndexOutOfBoundsException
	 * @see netcins.dbms.centralized.histograms.Histogram#getNumItemsInRange(BasicItem,
	 *      BasicItem)
	 */
	public double getNumItemsInRange(BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException {
		initBuckets();
		int startBucket = getBucketForItem(low);
		int endBucket = getBucketForItem(high);
		double ret = 0;
		for (int i = startBucket; i <= endBucket; i++)
			ret += buckets[i].getItemCountInRange_USA(low, high);
		return ret;
	}

	/**
	 * Computes the number of distinct items in the given range, using the
	 * underlying equi-depth histogram.
	 * 
	 * @throws BucketIndexOutOfBoundsException
	 * @see netcins.dbms.centralized.histograms.Histogram#getNumDistinctItemsInRange(BasicItem,
	 *      BasicItem)
	 */
	public double getNumDistinctItemsInRange(BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException {
		initBuckets();
		int startBucket = getBucketForItem(low);
		int endBucket = getBucketForItem(high);
		double ret = 0;
		for (int i = startBucket; i <= endBucket; i++)
			ret += buckets[i].getDistinctItemCountInRange_USA(low, high);
		return ret;
	}

	/**
	 * Initializes the internal bucket structure.
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#initBuckets()
	 */
	public void initBuckets() {
		if (initData() == false)
			return;

		buckets = new Bucket[buckets.length];

		double totalFrequency = 0;
		for (ItemFreq itemFreq : data) {
			totalFrequency += itemFreq.getFreq();
		}

		itemsPerBucket = (int)QuickMath.floor(totalFrequency / (double)buckets.length);

		int curBucket = 0;
		double targetFrequency = itemsPerBucket;
		Iterator<ItemFreq> iterator = data.iterator();
		ItemFreq prevItem = iterator.next(), preCurItem = prevItem;
		double curFrequency = prevItem.getFreq();
		int curDistFrequency = 1;

		for (; iterator.hasNext();) {
			ItemFreq curItem = iterator.next();
			if ((curFrequency + curItem.getFreq()) > targetFrequency) { // Bucket
				// limit
				// exceeded.
				double diffNotAdding = targetFrequency - curFrequency;
				double diffAdding = curFrequency + curItem.getFreq() - targetFrequency;
				if (diffNotAdding < diffAdding) { // Don't add the last item
					buckets[curBucket] = new Bucket(prevItem.getItem(), preCurItem.getItem(),
							curFrequency, curDistFrequency);
					targetFrequency = itemsPerBucket + diffNotAdding;
					prevItem = preCurItem = curItem;
				} else { // Add the last item
					curDistFrequency++;
					curFrequency += curItem.getFreq();
					buckets[curBucket] = new Bucket(prevItem.getItem(), curItem.getItem(),
							curFrequency, curDistFrequency);
					targetFrequency = itemsPerBucket - diffAdding;
					if (iterator.hasNext()) {
						prevItem = preCurItem = iterator.next();
					}
				}
				curFrequency = prevItem.getFreq();
				curDistFrequency = 1;
				curBucket++;
			} else { // There's room for more
				curDistFrequency++;
				curFrequency += curItem.getFreq();
				preCurItem = curItem;
			}
		}
		if (iterator.hasNext() == false && buckets[buckets.length - 1] == null) {
			buckets[buckets.length - 1] = new Bucket(prevItem.getItem(),
					data.get(data.size() - 1).getItem(), curFrequency, curDistFrequency);
		}
		areBucketsInited = true;
	}

	public String toString() {
		initBuckets();
		return "[ ED :: " + super.toString() + ", depth: " + itemsPerBucket + " ]";
	}
}
