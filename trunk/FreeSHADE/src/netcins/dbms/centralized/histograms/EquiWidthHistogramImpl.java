/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import netcins.dbms.BasicItem;
import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class EquiWidthHistogramImpl extends BasicHistogramImpl {
	private double	bucketSpread	= 0;

	public EquiWidthHistogramImpl(BasicItem lowest, BasicItem highest, Integer numBuckets) {
		super(lowest, highest, numBuckets);
		assert (lowest != null && highest != null && numBuckets > 0);
		bucketSpread = highest.subtract(lowest).doubleValue() / (double)(numBuckets);
		initBuckets();
	}

	public EquiWidthHistogramImpl(BasicItem lowest, BasicItem highest, Bucket[] buckets) {
		super(lowest, highest, buckets);
		bucketSpread = highest.subtract(lowest).doubleValue() / (double)(buckets.length);
	}

	public double getNumItemsInRange(BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException {
		int startBucket = getBucketForItem(low);
		int endBucket = getBucketForItem(high);
		startBucket = (startBucket < 0 ? 0 : (startBucket >= buckets.length ? buckets.length - 1
				: startBucket));
		endBucket = (endBucket < 0 ? 0 : (endBucket >= buckets.length ? buckets.length - 1
				: endBucket));
		double ret = 0;
		for (int i = startBucket; i <= endBucket; i++)
			ret += buckets[i].getItemCountInRange_USA(low, high);
		return ret;
	}

	public double getNumDistinctItemsInRange(BasicItem low, BasicItem high)
			throws BucketIndexOutOfBoundsException {
		int startBucket = getBucketForItem(low);
		int endBucket = getBucketForItem(high);
		startBucket = (startBucket < 0 ? 0 : (startBucket >= buckets.length ? buckets.length - 1
				: startBucket));
		endBucket = (endBucket < 0 ? 0 : (endBucket >= buckets.length ? buckets.length - 1
				: endBucket));
		double ret = 0;
		for (int i = startBucket; i <= endBucket; i++)
			ret += buckets[i].getDistinctItemCountInRange_USA(low, high);
		return ret;
	}

	public int getBucketForItem(BasicItem item) throws BucketIndexOutOfBoundsException {
		//if (item.compareTo(lowest) < 0 || item.compareTo(highest) > 0)
		//	throw new BucketIndexOutOfBoundsException();
		if (item.compareTo(lowest) < 0)
			return 0;
		if (item.compareTo(highest) > 0)
			return buckets.length - 1;
		int ret = (int)QuickMath.floor((double)(item.longValue() - lowest.longValue())
				/ bucketSpread);

		return (ret < buckets.length ? ret : ret - 1);
	}

	public void initBuckets() {
		if (areBucketsInited == true)
			return;
		for (int i = 0; i < buckets.length; i++)
			buckets[i] = new Bucket(lowest.add(new BasicItem(i * bucketSpread)),
					lowest.add(new BasicItem((i + 1) * bucketSpread)));
		areBucketsInited = true;
	}

	public void insert(BasicItem i, double freq) throws BucketIndexOutOfBoundsException {
		int bucket = getBucketForItem(i);
		if (bucket < 0 || bucket >= buckets.length)
			return;
		buckets[bucket].addItem(i, freq);
	}

	public String toString() {
		return "[ EW :: " + super.toString() + ", spread: " + bucketSpread + " ]";
	}

	public void clear() {
		super.clear();
		initBuckets();
	}
}
