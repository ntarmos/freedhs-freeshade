/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import netcins.dbms.BasicItem;
import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class MaxDiffHistogramImpl extends AdvancedHistogramImpl {

	private static class DiffPositionValueComparator implements Comparator<DiffPosition>, Serializable {
		/**
		 * 
		 */
		private static final long	serialVersionUID	= 3191879408365298329L;

		public int compare(DiffPosition o1, DiffPosition o2) {
			return o1.pos - o2.pos;
		}
	}

	public MaxDiffHistogramImpl(BasicItem lowest, BasicItem highest, Integer numBuckets) {
		super(lowest, highest, numBuckets);
	}

	public MaxDiffHistogramImpl(BasicItem lowest, BasicItem highest, Bucket[] buckets) {
		super(lowest, highest, buckets);
	}

	public int getBucketForItem(BasicItem item) {
		initBuckets();
		if (item.compareTo(lowest) >= 0 && item.compareTo(highest) < 0) {
			for (int i = 0; i < buckets.length; i++) {
				if (item.compareTo(buckets[i].getHighest()) < 0)
					return i;
			}
		}
		return buckets.length - 1;
	}

	public double getNumItemsInRange(BasicItem low, BasicItem high) {
		int startBucket = getBucketForItem(low);
		int endBucket = getBucketForItem(high);
		double ret = 0;
		for (int i = startBucket; i <= endBucket; i++)
			ret += buckets[i].getItemCountInRange_USA(low, high);
		return ret;
	}

	public double getNumDistinctItemsInRange(BasicItem low, BasicItem high) {
		int startBucket = getBucketForItem(low);
		int endBucket = getBucketForItem(high);
		double ret = 0;
		for (int i = startBucket; i <= endBucket; i++)
			ret += buckets[i].getDistinctItemCountInRange_USA(low, high);
		return ret;
	}

	public void initBuckets() {
		if (initData() == false)
			return;

		PriorityQueue<DiffPosition> q = new PriorityQueue<DiffPosition>();
		Iterator<ItemFreq> iterator = data.iterator();
		ItemFreq prevItem = iterator.next();
		ItemFreq curItem = null;

		for (int i = 1; iterator.hasNext(); i++) {
			curItem = iterator.next();
			q.add(new DiffPosition(QuickMath.abs(curItem.getFreq() - prevItem.getFreq()), i - 1));
			prevItem = curItem;
		}

		DiffPosition sep[] = new DiffPosition[buckets.length - 1];
		for (int i = 0; i < sep.length; i++)
			sep[i] = q.poll();
		DiffPositionValueComparator comparator = new DiffPositionValueComparator();
		Arrays.sort(sep, comparator);

		DiffPosition prevPosition = sep[0];
		DiffPosition nextPosition = null;
		buckets[0] = new Bucket(data.get(0).getItem(), data.get(sep[0].getPos()).getItem(),
				sumFrequencies(0, sep[0].pos), sep[0].pos + 1);
		for (int i = 1; i < buckets.length - 1; i++) {
			nextPosition = sep[i];
			buckets[i] = new Bucket(data.get(prevPosition.pos + 1).getItem(), data.get(
					nextPosition.pos).getItem(), sumFrequencies(prevPosition.pos + 1,
					nextPosition.pos), nextPosition.pos - prevPosition.pos);
			prevPosition = nextPosition;
		}
		buckets[buckets.length - 1] = new Bucket(data.get(prevPosition.pos + 1).getItem(),
				data.get(data.size() - 1).getItem(), sumFrequencies(prevPosition.pos + 1,
						data.size() - 1), data.size() - 1 - prevPosition.pos);

		areBucketsInited = true;
	}

	private double sumFrequencies(int start, int end) {
		double ret = 0;
		Iterator<ItemFreq> iterator = data.subList(start, end + 1).iterator();
		for (; iterator.hasNext();)
			ret += iterator.next().getFreq();
		return ret;
	}

	public String toString() {
		return "[ MD :: " + super.toString() + " ]";
	}
}

class DiffPosition implements Comparable<DiffPosition> {
	double	diff;
	int		pos;

	DiffPosition(double diff, int pos) {
		this.diff = diff;
		this.pos = pos;
	}

	/**
	 * Sort in descending order.
	 */
	public int compareTo(DiffPosition o) {
		if (diff > o.diff)
			return -1;
		if (diff < o.diff)
			return 1;
		return 0;
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (this == o)
			return true;

		if (o instanceof DiffPosition) {
			DiffPosition dp = (DiffPosition)o;
			return (diff == dp.diff && pos == dp.pos);
		}
		return false;
	}

	public double getDiff() {
		return diff;
	}

	public int getPos() {
		return pos;
	}

	public String toString() {
		return "[ Diff: " + diff + ", pos: " + pos + " ]";
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(diff);
		result = PRIME * result + (int)(temp ^ (temp >>> 32));
		result = PRIME * result + pos;
		return result;
	}
}
