/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import java.util.Collection;
import java.util.Vector;

import netcins.dbms.BasicItem;

/**
 * Implementation of common methods in the
 * {@link netcins.dbms.centralized.histograms.Histogram} interface. This class
 * cannot be instantiated directly; rather use one of its subclasses.
 * 
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 * @see AverageShiftedEquiWidthHistogramImpl
 * @see CompressedHistogramImpl
 * @see EquiDepthHistogramImpl
 * @see EquiWidthHistogramImpl
 * @see MaxDiffHistogramImpl
 */
public abstract class BasicHistogramImpl implements Histogram {

	/** The lowest value supported by the current histogram instance. */
	protected BasicItem	lowest				= null;
	/** The highest value supported by the current histogram instance. */
	protected BasicItem	highest				= null;
	/** The buckets of the current histogram instance. */
	protected Bucket[]	buckets				= null;
	/** True if buckets have been initialized, false otherwise. */
	protected boolean	areBucketsInited	= false;

	/**
	 * Initializes common histogram variables.
	 * 
	 * @param lowest The lowest value supported by the current histogram
	 *            instance.
	 * @param highest The highest value supported by the current histogram
	 *            instance.
	 * @param numBuckets The number of buckets of the current histogram
	 *            instance.
	 */
	protected BasicHistogramImpl(BasicItem lowest, BasicItem highest, Integer numBuckets) {
		this(lowest, highest, new Bucket[numBuckets]);
		this.areBucketsInited = false;
	}

	/**
	 * Initializes common histogram variables.
	 * 
	 * @param lowest The lowest value supported by the current histogram
	 *            instance.
	 * @param highest The highest value supported by the current histogram
	 *            instance.
	 * @param buckets The buckets of the current histogram instance.
	 */
	protected BasicHistogramImpl(BasicItem lowest, BasicItem highest, Bucket[] buckets) {
		assert (lowest != null && highest != null);
		this.lowest = new BasicItem(lowest.compareTo(highest) <= 0 ? lowest.doubleValue()
				: highest.doubleValue());
		this.highest = new BasicItem(highest.compareTo(lowest) > 0 ? highest.doubleValue()
				: lowest.doubleValue());
		this.buckets = buckets;
		this.areBucketsInited = true;
	}

	/**
	 * Set the lowest value supported by the current histogram instance.
	 * 
	 * @param lowest The lowest value supported by the current histogram
	 *            instance.
	 */
	public final void setLowest(BasicItem lowest) {
		if (this.lowest.equals(lowest) == false) {
			this.lowest = lowest;
			areBucketsInited = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#getLowest()
	 */
	public final BasicItem getLowest() {
		return lowest;
	}

	/**
	 * Set the highest value supported by the current histogram instance.
	 * 
	 * @param highest The highest value supported by the current histogram
	 *            instance.
	 */
	public final void setHighest(BasicItem highest) {
		if (this.highest.equals(highest) == false) {
			this.highest = highest;
			areBucketsInited = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#getHighest()
	 */
	public final BasicItem getHighest() {
		return highest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#getNumItemsInBucket(int)
	 */
	public double getNumItemsInBucket(int bucket) throws BucketIndexOutOfBoundsException {
		if (bucket < 0 || bucket >= buckets.length)
			throw new BucketIndexOutOfBoundsException();
		if (areBucketsInited == false)
			initBuckets();
		return buckets[bucket].getItemCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#getNumDistinctItemsInBucket(int)
	 */
	public double getNumDistinctItemsInBucket(int bucket) throws BucketIndexOutOfBoundsException {
		if (bucket < 0 || bucket >= buckets.length)
			throw new BucketIndexOutOfBoundsException();
		if (areBucketsInited == false)
			initBuckets();
		return buckets[bucket].getDistinctItemCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#getNumBuckets()
	 */
	public int getNumBuckets() {
		return buckets.length;
	}

	public void setNumBuckets(int numBuckets) {
		if (buckets.length != numBuckets) {
			buckets = new Bucket[numBuckets];
			areBucketsInited = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#getBucket(int)
	 */
	public Bucket getBucket(int bucket) throws BucketIndexOutOfBoundsException {
		if (bucket < 0 || bucket >= buckets.length)
			throw new BucketIndexOutOfBoundsException();
		if (areBucketsInited == false)
			initBuckets();
		return buckets[bucket];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#clear()
	 */
	public void clear() {
		this.buckets = new Bucket[buckets.length];
		areBucketsInited = false;
	}

	/**
	 * Returns a String representation of the current histogram instance.
	 * 
	 * @return A String representation of the current histogram instance.
	 */
	public String toString() {
		return "[ " + getNumBuckets() + " buckets, values in [" + lowest + ", " + highest + ") ]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.centralized.histograms.Histogram#toStringFull()
	 */
	public String toStringFull() {
		if (areBucketsInited == false)
			initBuckets();
		StringBuilder builder = new StringBuilder();
		builder.append(toString() + "\n");
		for (int i = 0; i < buckets.length; i++)
			builder.append("\t[ " + i + " : " + buckets[i].toString() + " ]" + "\n");
		return builder.toString();
	}

	public void insertAll(Collection<? extends BasicItem> items)
			throws BucketIndexOutOfBoundsException {
		for (BasicItem item : items)
			insert(item);
	}

	public void insertAll(Vector<ItemFreq> items) throws BucketIndexOutOfBoundsException {
		for (ItemFreq item : items)
			insert(item.getItem(), item.getFreq());
	}

	public void insert(BasicItem item) throws BucketIndexOutOfBoundsException {
		insert(item, 1);
	}
}
