/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class BucketIndexOutOfBoundsException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -5126085237603297586L;

	/**
	 * 
	 */
	public BucketIndexOutOfBoundsException() {
		this("Bucket index out of bounds");
	}

	/**
	 * @param message
	 */
	public BucketIndexOutOfBoundsException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public BucketIndexOutOfBoundsException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public BucketIndexOutOfBoundsException(String message, Throwable cause) {
		super(message, cause);
	}
}
