/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.centralized.histograms;

import netcins.dbms.BasicItem;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class ItemFreq implements Comparable<ItemFreq> {
	private BasicItem	item;
	private double		freq;

	public ItemFreq(BasicItem item, double freq) {
		this.item = item;
		this.freq = freq;
	}

	public ItemFreq(BasicItem item) {
		this(item, 1);
	}

	public ItemFreq(Number n) {
		this(new BasicItem(n));
	}

	public ItemFreq(Number n, double freq) {
		this(new BasicItem(n), freq);
	}

	/**
	 * @return The item's frequency
	 */
	public double getFreq() {
		return freq;
	}

	/**
	 * @param freq The item's frequency
	 */
	public void setFreq(double freq) {
		this.freq = freq;
	}

	/**
	 * @param freq The frequency to add to the current item's frequency
	 */
	public void addFreq(double freq) {
		this.freq += freq;
	}

	/**
	 * @return The item
	 */
	public BasicItem getItem() {
		return item;
	}

	/**
	 * @param item The item to set
	 */
	public void setItem(BasicItem item) {
		this.item = item;
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (this == o)
			return true;
		if (o instanceof ItemFreq)
			return item.equals(((ItemFreq)o).item);
		if (o instanceof BasicItem)
			return item.equals((BasicItem)o);
		return false;
	}

	public String toString() {
		return "[ " + item.toString() + ", freq: " + freq + " ]";
	}

	public int compareTo(ItemFreq arg0) {
		return item.compareTo(arg0.item);
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(freq);
		result = PRIME * result + (int)(temp ^ (temp >>> 32));
		result = PRIME * result + ((item == null) ? 0 : item.hashCode());
		return result;
	}
}
