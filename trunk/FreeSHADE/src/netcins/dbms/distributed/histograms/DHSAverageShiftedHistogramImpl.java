/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.AverageShiftedEquiWidthHistogramImpl;
import netcins.dbms.centralized.histograms.EquiWidthHistogramImpl;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSAverageShiftedHistogramImpl extends DHSBasicHistogramImpl {

	DHSEquiWidthHistogramImpl[]	histograms		= null;
	private BasicItem			shift			= null;
	private int					numShifts		= 0;
	private double				bucketSpread	= 0;

	public DHSAverageShiftedHistogramImpl(String instance, BasicItem lowest, BasicItem highest,
			Integer numBuckets, DHSImpl dhsImpl) {
		this(instance, lowest, highest, numBuckets, dhsImpl,
				AverageShiftedEquiWidthHistogramImpl.defaultNumShifts);
	}

	public DHSAverageShiftedHistogramImpl(String instance, BasicItem lowest, BasicItem highest,
			Integer numBuckets, DHSImpl dhsImpl, int numShifts) {
		super(instance, lowest, highest, numBuckets, dhsImpl);
		bucketMetrics = null;
		distinctValuesBucketMetrics = null;
		this.numShifts = numShifts;
		this.bucketSpread = highest.subtract(lowest).doubleValue() / (double)(numBuckets);
		this.shift = new BasicItem(bucketSpread / (double)numShifts);
		this.instance = instance;
		this.dhsImpl = dhsImpl;
		this.histograms = new DHSEquiWidthHistogramImpl[numShifts];
		for (int i = 0; i < numShifts; i++) {
			histograms[i] = new DHSEquiWidthHistogramImpl(instance + " shift #" + i,
					lowest.subtract(bucketSpread).add(shift.multiply(i)),
					highest.add(shift.multiply(i)), numBuckets + 1, dhsImpl);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#clear()
	 */
	public void clear() {
		dhsImpl.clear();
		for (DHSEquiWidthHistogramImpl ewHisto : histograms)
			ewHisto.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#initBuckets()
	 */
	public void initBuckets() {
		for (DHSEquiWidthHistogramImpl histo : histograms) {
			histo.initBuckets();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#insert(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem)
	 */
	public void insert(DHSPastryAppl sourceNode, BasicItem item) {
		for (DHSEquiWidthHistogramImpl histo : histograms) {
			histo.insert(sourceNode, item);
		}
	}

	public void batchInsert(DHSPastryAppl sourceNode, BasicItem item) {
		for (DHSEquiWidthHistogramImpl histo : histograms) {
			histo.batchInsert(sourceNode, item);
		}
	}

	public void doBatch(DHSPastryAppl sourceNode) {
		for (DHSEquiWidthHistogramImpl histo : histograms) {
			histo.doBatch(sourceNode);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#reconstruct(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem, netcins.dbms.BasicItem)
	 */
	// TODO : This should be done in a more clever and elaborate way, to avoid
	// reconstructing each and every DHS/EW histo separately.
	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode,
			BasicItem low, BasicItem high) {
		EquiWidthHistogramImpl[] internalHistos = new EquiWidthHistogramImpl[numShifts];
		int hopCount = 0;
		int numBitsProbed = 0;
		for (int i = 0; i < histograms.length; i++) {
			DistributedHistogramReconstructionResult rcHisto = histograms[i].reconstruct(sourceNode);
			internalHistos[i] = (EquiWidthHistogramImpl)rcHisto.getHistogram();
			hopCount += rcHisto.getHopCount();
			numBitsProbed += rcHisto.getNumBitsProbed();
		}
		return new DistributedHistogramReconstructionResult(
				new AverageShiftedEquiWidthHistogramImpl(low, high, numBuckets, internalHistos),
				hopCount, numBitsProbed);
	}

	public String toString() {
		return "[ ASH/DHS " + super.toString() + " ]";
	}
}
