/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.Bucket;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.centralized.histograms.Histogram;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.util.QuickMath;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DHSInferredHistogramImpl extends DHSBasicHistogramImpl {

	private static final String			instanceEWHAddOn		= ".DIH";
	public static final int				ewhNumBucketsMultiplier	= 10;

	protected DHSEquiWidthHistogramImpl	dhsEWHisto				= null;

	/**
	 * @param instance
	 * @param highest
	 * @param lowest
	 * @param numBuckets
	 * @param dhsImpl
	 */
	public DHSInferredHistogramImpl(String instance, BasicItem highest, BasicItem lowest,
			Integer numBuckets, DHSImpl dhsImpl) {
		super(instance, highest, lowest, numBuckets, dhsImpl);
		this.dhsEWHisto = new DHSEquiWidthHistogramImpl(instance + instanceEWHAddOn, highest,
				lowest, numBuckets * ewhNumBucketsMultiplier, dhsImpl);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#insert(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem)
	 */
	public void insert(DHSPastryAppl sourceNode, BasicItem item) {
		dhsEWHisto.insert(sourceNode, item);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#batchInsert(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem)
	 */
	public void batchInsert(DHSPastryAppl sourceNode, BasicItem item) {
		dhsEWHisto.batchInsert(sourceNode, item);
	}
	
	public void doBatch(DHSPastryAppl sourceNode) {
		dhsEWHisto.doBatch(sourceNode);
	}
	
	/**
	 * @param sourceNode
	 * @param low
	 * @param high
	 * @param histoClass
	 * @return
	 */
	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode,
			BasicItem low, BasicItem high, Class histoClass) {
		DistributedHistogramReconstructionResult rcHisto = dhsEWHisto.reconstruct(sourceNode, low,
				high);
		return DHSInferredHistogramImpl.reconstruct(rcHisto, low, high, histoClass);
	}

	/**
	 * @param rcHisto
	 * @param low
	 * @param high
	 * @param histoClass
	 * @return
	 */
	public static DistributedHistogramReconstructionResult reconstruct(
			DistributedHistogramReconstructionResult rcHisto, BasicItem low, BasicItem high,
			Class histoClass) {
		Histogram histo = rcHisto.getHistogram();
		int bucketLow = 0, bucketHigh;

		try {
			bucketLow = histo.getBucketForItem(low);
		} catch (BucketIndexOutOfBoundsException e) {
			bucketLow = 0;
		}
		try {
			bucketHigh = histo.getBucketForItem(high);
		} catch (BucketIndexOutOfBoundsException e) {
			bucketHigh = histo.getNumBuckets() - 1;
		}

		int targetNumBuckets = (int)QuickMath.floor((double)(bucketHigh - bucketLow + 1)
				/ (double)ewhNumBucketsMultiplier);

		Constructor histoConstructor = null;
		Class[] parameterTypes = new Class[3];
		parameterTypes[0] = parameterTypes[1] = BasicItem.class;
		parameterTypes[2] = Integer.class;
		try {
			histoConstructor = histoClass.getConstructor(parameterTypes);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		if (histoConstructor == null)
			return null;

		Histogram internalHisto = null;

		Object[] initArgs = new Object[3];
		initArgs[0] = low;
		initArgs[1] = high;
		initArgs[2] = targetNumBuckets;
		try {
			internalHisto = (Histogram)histoConstructor.newInstance(initArgs);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		if (internalHisto == null)
			return null;

		// TODO : Make sure this corresponds to a USA-based estimation
		for (int internalBucket = bucketLow; internalBucket <= bucketHigh; internalBucket++) {
			Bucket curBucket = null;
			try {
				curBucket = histo.getBucket(internalBucket);
			} catch (BucketIndexOutOfBoundsException e) {
				e.printStackTrace();
				return null;
			}
			double numDItems = curBucket.getDistinctItemCount();
			if (numDItems == 0)
				numDItems = 1;
			double step = curBucket.getHighest().subtract(curBucket.getLowest()).doubleValue()
					/ numDItems;
			double freq = curBucket.getItemCount() / numDItems;
			BasicItem startItem = new BasicItem(curBucket.getLowest().add(0.5 * step));
			for (int item = 0; item < numDItems; item++) {
				try {
					internalHisto.insert(startItem.add(item * step), freq);
				} catch (BucketIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}

		return new DistributedHistogramReconstructionResult(internalHisto, rcHisto.getHopCount(),
				rcHisto.getNumBitsProbed());
	}
}
