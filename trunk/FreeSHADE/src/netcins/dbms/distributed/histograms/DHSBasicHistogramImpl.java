/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DHSBasicHistogramImpl implements DistributedHistogram {
	protected Vector<String>	bucketMetrics				= null;
	protected Vector<String>	distinctValuesBucketMetrics	= null;
	protected DHSImpl			dhsImpl						= null;
	protected String			instance					= null;
	protected BasicItem			lowest						= null, highest = null;
	protected int				numBuckets					= 0;

	/**
	 * @param instance
	 * @param highest
	 * @param lowest
	 * @param numBuckets
	 * @param dhsImpl
	 */
	public DHSBasicHistogramImpl(String instance, BasicItem highest, BasicItem lowest,
			Integer numBuckets, DHSImpl dhsImpl) {
		this.instance = instance;
		this.lowest = new BasicItem(lowest.compareTo(highest) <= 0 ? lowest.longValue()
				: highest.longValue());
		this.highest = new BasicItem(highest.compareTo(lowest) > 0 ? highest.longValue()
				: lowest.longValue());
		this.dhsImpl = dhsImpl;
		this.numBuckets = numBuckets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#clear()
	 */
	public void clear() {
		dhsImpl.clear();
		this.bucketMetrics = null;
		this.distinctValuesBucketMetrics = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#getHighest()
	 */
	public BasicItem getHighest() {
		return highest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#getLowest()
	 */
	public BasicItem getLowest() {
		return lowest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#getNumBuckets()
	 */
	public int getNumBuckets() {
		return numBuckets;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[ " + instance + " :: [ " + getNumBuckets() + " buckets, values in [" + lowest
				+ " , " + highest + ")], " + dhsImpl.toString() + " ]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#reconstruct(rice.pastry.client.PastryAppl)
	 */
	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode) {
		try {
			return reconstruct(sourceNode, lowest, highest);
		} catch (BucketIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param instance
	 * @param numBuckets
	 * @return
	 */
	public static Vector<String> generateBucketMetrics(String instance, int numBuckets) {
		Vector<String> ret = new Vector<String>(numBuckets);
		for (int i = 0; i < numBuckets; i++)
			ret.add(i, instance + ".B" + i);
		return ret;
	}

	/**
	 * @param instance
	 * @param numBuckets
	 * @return
	 */
	public static Vector<String> generateDistinctValuesBucketMetrics(String instance, int numBuckets) {
		Vector<String> ret = new Vector<String>(numBuckets);
		for (int i = 0; i < numBuckets; i++)
			ret.add(instance + ".DB" + i);
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#getNumItemsInRange(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem, netcins.dbms.BasicItem)
	 */
	public DistributedEstimationResult getNumItemsInRange(DHSPastryAppl sourceNode, BasicItem low,
			BasicItem high) throws BucketIndexOutOfBoundsException {
		DistributedHistogramReconstructionResult recon = reconstruct(sourceNode, low, high);
		return new DistributedEstimationResult(instance, recon.getHistogram().getNumItemsInRange(
				low, high), recon.getHopCount(), recon.getNumBitsProbed());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#getNumDistinctItemsInRange(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem, netcins.dbms.BasicItem)
	 */
	public DistributedEstimationResult getNumDistinctItemsInRange(DHSPastryAppl sourceNode,
			BasicItem low, BasicItem high) throws BucketIndexOutOfBoundsException {
		DistributedHistogramReconstructionResult recon = reconstruct(sourceNode, low, high);
		return new DistributedEstimationResult(instance,
				recon.getHistogram().getNumDistinctItemsInRange(low, high), recon.getHopCount(),
				recon.getNumBitsProbed());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#initBuckets()
	 */
	public void initBuckets() {
		if (bucketMetrics == null) {
			bucketMetrics = generateBucketMetrics(instance, numBuckets);
			dhsImpl.addDHSMetrics(bucketMetrics);
		}
		if (distinctValuesBucketMetrics == null) {
			distinctValuesBucketMetrics = generateDistinctValuesBucketMetrics(instance, numBuckets);
			dhsImpl.addDHSMetrics(distinctValuesBucketMetrics);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#getBucketForItem(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem)
	 */
	public int getBucketForItem(DHSPastryAppl sourceNode, BasicItem item)
			throws BucketIndexOutOfBoundsException {
		return reconstruct(sourceNode).getHistogram().getBucketForItem(item);
	}
}
