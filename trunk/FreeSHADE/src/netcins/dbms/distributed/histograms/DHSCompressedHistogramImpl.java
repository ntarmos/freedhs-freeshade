/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.centralized.histograms.CompressedHistogramImpl;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSCompressedHistogramImpl extends DHSInferredHistogramImpl {

	/**
	 * @param instance
	 * @param lowest
	 * @param highest
	 * @param numBuckets
	 * @param dhsImpl
	 */
	public DHSCompressedHistogramImpl(String instance, BasicItem lowest, BasicItem highest,
			Integer numBuckets, DHSImpl dhsImpl) {
		super(instance, lowest, highest, numBuckets, dhsImpl);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DistributedHistogram#reconstruct(rice.pastry.client.PastryAppl,
	 *      netcins.dbms.BasicItem, netcins.dbms.BasicItem)
	 */
	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode,
			BasicItem low, BasicItem high) throws BucketIndexOutOfBoundsException {
		return super.reconstruct(sourceNode, low, high, CompressedHistogramImpl.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see netcins.dbms.distributed.histograms.DHSBasicHistogramImpl#toString()
	 */
	@Override
	public String toString() {
		return "[ CPH/DHS " + super.toString() + " ]";
	}
}
