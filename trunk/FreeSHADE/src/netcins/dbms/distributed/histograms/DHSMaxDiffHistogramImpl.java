/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.histograms;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.centralized.histograms.MaxDiffHistogramImpl;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSMaxDiffHistogramImpl extends DHSInferredHistogramImpl {

	public DHSMaxDiffHistogramImpl(String instance, BasicItem lowest, BasicItem highest,
			Integer numBuckets, DHSImpl dhsImpl) {
		super(instance, lowest, highest, numBuckets, dhsImpl);
	}

	public DistributedHistogramReconstructionResult reconstruct(DHSPastryAppl sourceNode,
			BasicItem low, BasicItem high) throws BucketIndexOutOfBoundsException {
		return super.reconstruct(sourceNode, low, high, MaxDiffHistogramImpl.class);
	}

	public String toString() {
		return "[ MDH/DHS " + super.toString() + " ]";
	}
}
