/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.histograms.testing;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.histograms.AverageShiftedEquiWidthHistogramImpl;
import netcins.dbms.centralized.histograms.BucketIndexOutOfBoundsException;
import netcins.dbms.centralized.histograms.CompressedHistogramImpl;
import netcins.dbms.centralized.histograms.EquiDepthHistogramImpl;
import netcins.dbms.centralized.histograms.EquiWidthHistogramImpl;
import netcins.dbms.centralized.histograms.Histogram;
import netcins.dbms.centralized.histograms.ItemFreq;
import netcins.dbms.centralized.histograms.MaxDiffHistogramImpl;
import netcins.dbms.distributed.histograms.DHSEquiWidthHistogramImpl;
import netcins.dbms.distributed.histograms.DHSInferredHistogramImpl;
import netcins.dbms.distributed.histograms.DistributedHistogram;
import netcins.dbms.distributed.histograms.DistributedHistogramReconstructionResult;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithm;
import netcins.util.QuickMath;
import netcins.util.stats.ZipfGenerator;
import rice.environment.Environment;
import rice.pastry.Id;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.client.PastryAppl;
import rice.pastry.direct.DirectPastryNodeFactory;
import rice.pastry.direct.EuclideanNetwork;
import rice.pastry.direct.NetworkSimulatorImpl;
import rice.pastry.standard.RandomNodeIdFactory;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSHistogramsTest {
	private DHSImpl					dhsImpl				= null;
	private PastryNodeFactory		factory				= null;
	private Environment				env					= null;
	private Vector<DHSPastryAppl>	pastryAppls			= null;
	private DHSSketchAlgorithm		sketchAlgo			= DHSImpl.defaultSketchAlgo;
	private DistributedHistogram	dhsHisto			= null;
	private Histogram				centralizedHistos[]	= new Histogram[5];
	private Vector<ItemFreq>		items				= null;
	private int						numItems			= 0;
	private int						numNodes			= -1;
	private int						qNode				= -1;
	private boolean					isInited			= false;
	private final Logger			logger				= Logger.getLogger("netcins.p2p.dhs.testing.DHSSingleMetricTest");
	private NetworkSimulatorImpl	simulator			= null;

	public DHSHistogramsTest(BasicItem lowest, BasicItem highest, int numBuckets,
			DHSSketchAlgorithm sketchAlgo, int numVecs, int L, int retries, int replicas,
			Environment env) {
		init(lowest, highest, numBuckets, sketchAlgo, numVecs, L, retries, replicas, env);
	}

	public DHSHistogramsTest(BasicItem lowest, BasicItem highest, int numBuckets, int numVecs,
			int L, int retries, int replicas, Environment env) {
		init(lowest, highest, numBuckets, DHSImpl.defaultSketchAlgo, numVecs, L, retries, replicas,
				env);
	}

	public void init(BasicItem lowest, BasicItem highest, int numBuckets,
			DHSSketchAlgorithm algorithm, int numVecs, int L, int retries, int replicas,
			Environment env) {
		if (isInited)
			destroy();
		this.env = env;
		if (numVecs <= 0 || L <= 0 || retries < 0 || env == null) {
			destroy();
			return;
		}
		this.pastryAppls = new Vector<DHSPastryAppl>(10, 10);
		this.simulator = new EuclideanNetwork(env);
		this.factory = new DirectPastryNodeFactory(new RandomNodeIdFactory(env), simulator, env);

		this.centralizedHistos[0] = new EquiWidthHistogramImpl(lowest, highest, numBuckets
				* DHSInferredHistogramImpl.ewhNumBucketsMultiplier);
		this.centralizedHistos[1] = new EquiDepthHistogramImpl(lowest, highest, numBuckets);
		this.centralizedHistos[2] = new CompressedHistogramImpl(lowest, highest, numBuckets);
		this.centralizedHistos[3] = new MaxDiffHistogramImpl(lowest, highest, numBuckets);
		this.centralizedHistos[4] = new AverageShiftedEquiWidthHistogramImpl(lowest, highest,
				numBuckets);

		initDHSHisto(algorithm, numVecs, L, retries, replicas, lowest, highest, numBuckets);
		Logger.getLogger("netcins.dbms.testing.DHSInsertionTesting").setLevel(Level.INFO);
		isInited = true;
	}

	public void initDHSHisto(DHSSketchAlgorithm algorithm, int numVecs, int L, int retries,
			int replicas, BasicItem lowest, BasicItem highest, int numBuckets) {
		this.sketchAlgo = algorithm;
		if (algorithm != null) {
			try {
				this.dhsImpl = DHSImpl.newInstance(algorithm, numVecs, L, retries, replicas, env);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			this.dhsHisto = new DHSEquiWidthHistogramImpl("BI.DIH", lowest, highest, numBuckets
					* DHSInferredHistogramImpl.ewhNumBucketsMultiplier, dhsImpl);
			System.out.println("Initializing test with " + this.dhsHisto);

			for (PastryAppl app : pastryAppls)
				((DHSPastryAppl)app).setDHSImpl(this.dhsImpl);
		}
	}

	private void clearHits() {
		for (PastryAppl app : pastryAppls)
			((DHSPastryAppl)app).clearHits();
	}

	public boolean isInited() {
		return isInited;
	}

	public void destroy() {
		if (isInited == false)
			return;
		isInited = false;
		env.destroy();
		pastryAppls = null;
		env = null;
	}

	public void clearTransientData() {
		if (isInited == false)
			return;
		for (PastryAppl appl : pastryAppls) {
			((DHSPastryAppl)appl).clearTransientData();
		}
	}

	public void addNodes(int numNodes) {

		System.runFinalization();
		System.gc();
		long startTime = System.currentTimeMillis(), endTime = 0;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numNodes = numNodes;
		System.out.println("Populating the overlay with new nodes...");

		PastryNode bootstrapNode = null;
		for (int curNodeIndex = 0; curNodeIndex < numNodes; curNodeIndex++) {
			PastryNode node = null;
			if (curNodeIndex == 0)
				node = bootstrapNode = factory.newNode((rice.pastry.NodeHandle)null);
			else
				node = factory.newNode(bootstrapNode.getLocalHandle());

			logger.log(Level.FINER, "Finished creating new node " + node);

			DHSPastryAppl dhsAppl = new DHSPastryAppl(node, dhsImpl);

			pastryAppls.add(dhsAppl);

			if ((curNodeIndex % 100) == 0)
				System.out.print(" " + curNodeIndex + "/" + numNodes + " ");
			else if ((curNodeIndex % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + numNodes + "/" + numNodes + " done.");

		Collections.sort(pastryAppls, new Comparator<DHSPastryAppl>() {
			public int compare(DHSPastryAppl o1, DHSPastryAppl o2) {
				return o1.getNodeId().compareTo(o2.getNodeId());
			}
		});

		for (int curNodeIndex = 0; curNodeIndex < pastryAppls.size(); curNodeIndex++) {
			pastryAppls.get(curNodeIndex).index = curNodeIndex;
		}
		System.out.println("Done populating the overlay with new nodes. Waiting for the leaf sets to stabilize.");
		for (int i = 0; i < pastryAppls.size(); i++) {
			((DHSPastryAppl)pastryAppls.get(i)).waitForNodeToBeReady();

			if ((i % 100) == 0)
				System.out.print(" " + i + "/" + numNodes + " ");
			else if ((i % 10) == 0)
				System.out.print(".");
		}
		System.out.println(" " + pastryAppls.size() + "/" + numNodes + " done.");

		System.runFinalization();
		System.gc();
		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl node generation took " + (endTime - startTime) / 1000 + "\".");
		qNode = env.getRandomSource().nextInt(pastryAppls.size());
	}

	public void addTuples(Vector<BasicItem> tuples) {
		long startTime = System.currentTimeMillis();
		long step = 100000;

		if (isInited == false)
			throw new RuntimeException("Uninitialized test instance!");
		this.numItems += tuples.size();

		System.runFinalization();
		System.gc();

		// System.out.println("Injecting " + tuples.size() + " new data
		// tuples...");
		long prevTime = System.currentTimeMillis();
		for (int tuple = 0; tuple < tuples.size(); tuple++) {
			BasicItem item = tuples.get(tuple);
			dhsHisto.insert(pastryAppls.get(tuple % pastryAppls.size()), item);
			if (tuple > 0 && (tuple % step) == 0) {
				waitForInsertionsToComplete();
				long curTime = System.currentTimeMillis();
				System.out.print(" " + tuple + "/" + tuples.size() + ", " + step * 1000L
						/ (curTime - prevTime) + " items/sec ");
				prevTime = curTime;
			} else if (tuple > 0 && (tuple % (step / 5)) == 0) {
				System.out.print(".");
			}
		}
		waitForInsertionsToComplete();
		System.out.println(" " + tuples.size() + "/" + tuples.size() + ", " + step * 1000L
				/ (System.currentTimeMillis() - prevTime) + " items/sec done. ");

		System.out.println("DHSImpl data generation took "
				+ (System.currentTimeMillis() - startTime) / 1000 + "\".");

	}

	private void waitForInsertionsToComplete() {
		for (PastryAppl curAppl : pastryAppls) {
			((DHSPastryAppl)curAppl).waitForInsertionsToComplete();
			((DHSPastryAppl)curAppl).clearInsertionResponses();
		}
	}

	public Vector<ItemFreq> generateInputDataBatch(BasicItem lowest, BasicItem highest,
			double theta, int seed, int numTuples) throws IOException {
		if (items != null && items.size() > 0) {
			System.out.println("Data seems to have been already populated. Bailing out...");
			return items;
		}
		items = new Vector<ItemFreq>(highest.subtract(lowest).intValue() + 1, 10);
		ZipfGenerator zipf = new ZipfGenerator(
				(int)QuickMath.floor(highest.subtract(lowest).doubleValue()), theta, seed);
		System.out.println("Adding tuples...");
		Integer[] values = new Integer[(int)QuickMath.floor(highest.subtract(lowest).doubleValue())];
		for (int i = 0; i < values.length; i++)
			values[i] = lowest.intValue() + i;
		Collections.shuffle(Arrays.asList(values));

		int tuplesPerNode = (int)QuickMath.ceil((double)numTuples / (double)pastryAppls.size());
		long startTime = System.currentTimeMillis(), prevTime = startTime;
		long itemsSoFar = 0, itemsSnap = 0;
		for (int node = 0; node < pastryAppls.size(); node++) {
			int itemsThisStep = ((node + 1) * tuplesPerNode > numTuples ? numTuples - node
					* tuplesPerNode : tuplesPerNode);
			DHSPastryAppl curAppl = pastryAppls.get(node);

			for (int curItem = 0; curItem < itemsThisStep; curItem++) {
				BasicItem newItem = new BasicItem(Id.makeRandomId(env.getRandomSource()),
						values[zipf.nextRank()]);
				if (newItem.compareTo(lowest) >= 0 && newItem.compareTo(highest) < 0) {
					ItemFreq newItemFreq = new ItemFreq(newItem);
					int index = Collections.binarySearch(items, newItemFreq);
					if (index < 0)
						items.add(-index - 1, newItemFreq);
					else
						items.get(index).addFreq(1);
				}
				dhsHisto.batchInsert(curAppl, newItem);
			}
			dhsHisto.doBatch(curAppl);
			itemsSoFar += itemsThisStep;
			itemsSnap += itemsThisStep;
			long curTime = System.currentTimeMillis();
			if (node > 0 && node % (pastryAppls.size() / 10) == 0) {
				System.out.println(" " + itemsSoFar + "/" + numTuples + " items, "
						+ ((double)itemsSnap / ((double)(curTime - prevTime) / 1000.0))
						+ " items/sec ");
				prevTime = curTime;
				itemsSnap = 0;
				waitForInsertionsToComplete();
			}
		}
		System.out.println(" " + itemsSoFar + "/" + numTuples + " items, "
				+ ((double)itemsSnap / ((double)(System.currentTimeMillis() - prevTime) / 1000.0))
				+ " items/sec ");
		System.out.println("Waiting for insertions to complete...");
		waitForInsertionsToComplete();
		System.out.println("DHSImpl data generation took "
				+ (System.currentTimeMillis() - startTime) / 1000 + "\".");

		return items;
	}

	public Vector<ItemFreq> generateInputData(BasicItem lowest, BasicItem highest, double theta,
			int seed, int numTuples) throws IOException {
		if (items != null && items.size() > 0) {
			System.out.println("Data seems to have been already populated. Bailing out...");
			return items;
		}
		items = new Vector<ItemFreq>(highest.subtract(lowest).intValue() + 1, 10);
		ZipfGenerator zipf = new ZipfGenerator(
				(int)QuickMath.floor(highest.subtract(lowest).doubleValue()), theta, seed);
		System.out.println("Adding tuples...");
		Integer[] values = new Integer[(int)QuickMath.floor(highest.subtract(lowest).doubleValue())];
		for (int i = 0; i < values.length; i++)
			values[i] = lowest.intValue() + i;
		Collections.shuffle(Arrays.asList(values));

		int tuplesStep = 500000;
		int numSteps = (int)QuickMath.ceil((double)numTuples / (double)tuplesStep);
		for (int step = 0; step < numSteps; step++) {
			Vector<BasicItem> data = new Vector<BasicItem>(numTuples + 1, 10);
			System.out.println("Injecting tuples " + (step * tuplesStep) + " through "
					+ ((step + 1) * tuplesStep > numTuples ? numTuples : (step + 1) * tuplesStep)
					+ " of " + numTuples + " ...");
			for (int i = 0; i < (step != (numSteps - 1) ? tuplesStep : (numTuples - step
					* tuplesStep)); i++) {
				BasicItem newItem = new BasicItem(Id.makeRandomId(env.getRandomSource()),
						values[zipf.nextRank()]);
				data.add(newItem);
				if (newItem.compareTo(lowest) >= 0 && newItem.compareTo(highest) < 0) {
					ItemFreq newItemFreq = new ItemFreq(newItem);
					int index = Collections.binarySearch(items, newItemFreq);
					if (index < 0)
						items.add(-index - 1, newItemFreq);
					else
						items.get(index).addFreq(1);
				}
			}
			this.addTuples(data);
		}
		System.runFinalization();
		System.gc();
		return items;
	}

	public void query(BasicItem lowest, BasicItem highest) {
		if (items == null) {
			System.out.println("Items seems to be uninitialized. Bailing out...");
			return;
		}
		System.runFinalization();
		System.gc();

		System.out.println("Querying node: " + pastryAppls.get(qNode).getNodeId());
		this.clearTransientData();
		long startTime = System.currentTimeMillis(), endTime;

		System.out.print("Inserting into centralized histogram instance...");
		try {
			centralizedHistos[0].clear();
			centralizedHistos[0].insertAll(items);
		} catch (BucketIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		System.out.println(" done");

		DistributedHistogramReconstructionResult distEWHisto = testHisto(dhsHisto,
				centralizedHistos[0], null);
		centralizedHistos[0].clear();

		for (int i = 1; i < centralizedHistos.length; i++) {
			System.out.print("Inserting into centralized histogram instance...");
			try {
				centralizedHistos[i].clear();
				centralizedHistos[i].insertAll(items);
				centralizedHistos[i].initBuckets();
			} catch (BucketIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
			System.out.println(" done.");
			testHisto(null, centralizedHistos[i], distEWHisto);
			centralizedHistos[i].clear();
		}

		endTime = System.currentTimeMillis();
		System.out.println("DHSImpl query took " + (endTime - startTime) / 1000 + "\".");
	}

	private DistributedHistogramReconstructionResult testHisto(DistributedHistogram dTestHisto,
			Histogram cTestHisto, DistributedHistogramReconstructionResult recHisto) {

		System.out.println("Testing " + cTestHisto);
		DistributedHistogramReconstructionResult histo = (recHisto == null
				? dTestHisto.reconstruct(pastryAppls.get(qNode))
				: DHSInferredHistogramImpl.reconstruct(recHisto, cTestHisto.getLowest(),
						cTestHisto.getHighest(), cTestHisto.getClass()));
		Histogram distHisto = histo.getHistogram();
		try {
			System.out.println(sketchAlgo
					+ " centralized estimation: "
					+ cTestHisto.getNumItemsInRange(cTestHisto.getLowest(), cTestHisto.getHighest()));
			System.out.println(sketchAlgo + " distributed estimation: "
					+ distHisto.getNumItemsInRange(cTestHisto.getLowest(), cTestHisto.getHighest())
					+ ", num bits probed: " + histo.getNumBitsProbed() + " , hops: "
					+ histo.getHopCount());
		} catch (BucketIndexOutOfBoundsException e1) {
			e1.printStackTrace();
		}
		/*
		 * System.out.println(sketchAlgo + " centralized histogram: " + "\n" +
		 * cTestHisto.toStringFull()); System.out.println(sketchAlgo + "
		 * distributed histogram: " + "\n" + distHisto.toStringFull());
		 * System.out.println(sketchAlgo + " distributed estimation: " + histo);
		 * double avgError = 0, maxError = Double.MIN_VALUE, minError =
		 * Double.MAX_VALUE; double avgNormError = 0, maxNormError =
		 * Double.MIN_VALUE, minNormError = Double.MAX_VALUE; int maxIndex = 0,
		 * minIndex = 0; int maxNormIndex = 0, minNormIndex = 0; for (int i = 0;
		 * i < distHisto.getNumBuckets(); i++) { double cItems = 0; double
		 * dItems = 0; try { cItems = cTestHisto.getNumItemsInBucket(i); dItems =
		 * distHisto.getNumItemsInBucket(i); } catch
		 * (BucketIndexOutOfBoundsException e) { e.printStackTrace(); } double
		 * rawError = QuickMath.abs(cItems - dItems); double error = rawError /
		 * (cItems > 0 ? cItems : 1); double normError = error * (cItems /
		 * (double)numItems);
		 *
		 * avgError += error; avgNormError += normError;
		 *
		 * if (maxError < error) { maxError = error; maxIndex = i; } if
		 * (minError > error) { minError = error; minIndex = i; } if
		 * (maxNormError < normError) { maxNormError = normError; maxNormIndex =
		 * i; } if (minNormError > normError) { minNormError = normError;
		 * minNormIndex = i; } } avgError /= (double)distHisto.getNumBuckets();
		 *
		 * System.out.println(sketchAlgo + ":" + "\n" + "\tminimum cell error: " +
		 * QuickMath.doubleToString(100 * minError) + "% at cell #" + minIndex +
		 * "\n" + "\tmaximum cell error: " + QuickMath.doubleToString(100 *
		 * maxError) + "% at cell #" + maxIndex + "\n" + "\taverage cell error: " +
		 * QuickMath.doubleToString(100 * avgError) + "%" + "\n" + "\tminimum
		 * normalized cell error: " + QuickMath.doubleToString(100 *
		 * minNormError) + "% at cell #" + minNormIndex + "\n" + "\tmaximum
		 * normalized cell error: " + QuickMath.doubleToString(100 *
		 * maxNormError) + "% at cell #" + maxNormIndex + "\n" + "\ttotal
		 * normalized cell error: " + QuickMath.doubleToString(100 *
		 * avgNormError) + "%");
		 */
		return histo;
	}

	public void printNodeQueryLoads() {
		long totalHits = 0;
		System.out.print("Query hits per node: ");
		for (PastryAppl curAppl : pastryAppls) {
			long curHits = ((DHSPastryAppl)curAppl).getQueryHits();
			System.out.print(curHits + " ");
			totalHits += curHits;
		}
		System.out.println();
		System.out.println("Average query hits per node: "
				+ QuickMath.doubleToString((double)totalHits / (double)numNodes));
	}

	public void printNodeInsertionLoads() {
		long totalHits = 0, totalHops = 0;
		System.out.print("Insertion hits per node: ");
		for (PastryAppl curAppl : pastryAppls) {
			long curHits = ((DHSPastryAppl)curAppl).getInsertionHits();
			System.out.print(curHits + " ");
			totalHits += curHits;
			totalHops += ((DHSPastryAppl)curAppl).getInsertionsHopCount();
		}
		System.out.println();
		System.out.println("Node insertion hits: " + totalHits + ", insertion hops: " + totalHops);
	}

	public void printNodeLoads() {
		printNodeQueryLoads();
		printNodeInsertionLoads();
	}

	public static void main(String[] args) {

		int numNodes = 0, numTuples = 0, numVecs = 0, L = 0, retries = 0, replicas = 0, numBuckets = 0, iterations = 0;
		BasicItem lowest = null, highest = null;
		double theta = 0;
		boolean doCaching = true;
		DHSHistogramsTest test = null;

		try {
			numNodes = Integer.parseInt(args[0]);
			numTuples = Integer.parseInt(args[1]);
			numVecs = Integer.parseInt(args[2]);
			L = Integer.parseInt(args[3]);
			retries = Integer.parseInt(args[4]);
			replicas = Integer.parseInt(args[5]);
			lowest = new BasicItem(Integer.parseInt(args[6]));
			highest = new BasicItem(Integer.parseInt(args[7]));
			numBuckets = Integer.parseInt(args[8]);
			theta = Double.parseDouble(args[9]);
			iterations = Integer.parseInt(args[11]);
			if (args[10].compareToIgnoreCase("cache") == 0)
				doCaching = true;
			else if (args[10].compareToIgnoreCase("nocache") == 0)
				doCaching = false;
			else
				throw new NumberFormatException();
		} catch (NumberFormatException e) {
			System.out.println("Usage: java [-cp lib/<*>.jar:classes]");
			System.out.println("\tnetcins.dbms.distributed.histograms.testing.DHSEquiWidthHistogramTest "
					+ "<numNodes> <numItems> <numVecs> <L> <retries> <replicas> <domain low> <domain high> <numBuckets> <zipf theta> <cache|nocache> <iterations>");
			System.exit(0);
		}

		try {
			long startTime = System.currentTimeMillis(), endTime = 0;
			DHSSketchAlgorithm[] testAlgorithms = new DHSSketchAlgorithm[1];
			testAlgorithms[0] = new DHSSketchAlgorithm().setUsingDF03().setUsingSequentialBitProbing().setUsingRecursiveProcessing();
			if (doCaching == true)
				testAlgorithms[0].setUsingResultCaching();
			else
				testAlgorithms[0].setNotUsingResultCaching();

			test = new DHSHistogramsTest(lowest, highest, numBuckets, null, 0, 0, 0, 0, null);
			int seed = (int)QuickMath.floor(Math.random() * Integer.MAX_VALUE);
			System.out.println(" --- Begin Simulation --- ");
			System.out.println(" --- Seed: " + seed + ", values in [" + lowest.intValue() + ", "
					+ highest.intValue() + "], Zipf theta: " + theta + " --- ");

			Environment env = Environment.directEnvironment(seed);

			// XXX: The SelectorManager goes nuts when this is set to false!
			// env.getSelectorManager().setSelect(false);

			test.init(lowest, highest, numBuckets, null, numVecs, L, retries, replicas, env);
			test.addNodes(numNodes);
			test.initDHSHisto(DHSImpl.defaultSketchAlgo, numVecs, L, retries, replicas, lowest,
					highest, numBuckets);
			test.clearHits();
			test.generateInputDataBatch(lowest, highest, theta, seed, numTuples);
			test.printNodeInsertionLoads();

			for (DHSSketchAlgorithm algo : testAlgorithms) {
				test.initDHSHisto(algo, numVecs, L, retries, replicas, lowest, highest, numBuckets);
				for (int i = 0; i < iterations; i++) {
					test.qNode = env.getRandomSource().nextInt(numNodes);
					System.out.println("Testing " + algo + ", iteration " + i
							+ ", querying node # " + test.qNode + " ...");
					test.query(lowest, highest);
					test.printNodeQueryLoads();
					System.out.println("\n" + " ---" + "\n");
				}
			}
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: " + (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
		} catch (RuntimeException e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}
}
