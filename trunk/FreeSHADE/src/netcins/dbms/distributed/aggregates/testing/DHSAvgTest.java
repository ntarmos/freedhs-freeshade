/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.aggregates.testing;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.centralized.sketches.SummationSketchImpl;
import netcins.dbms.distributed.aggregates.DistributedAverage;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithm;
import netcins.p2p.testing.DHSGenericTest;
import netcins.p2p.testing.DHSGenericTestParams;
import netcins.util.QuickMath;
import netcins.util.stats.ZipfGenerator;
import rice.environment.Environment;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSAvgTest extends DHSGenericTest {
	private static final String	metric			= "Single Metric";
	private DistributedAverage	distAverage;
	private SummationSketchImpl	centralizedSumSketch;
	private HashSketchImpl		centralizedHashSketch;
	private double				centralizedSum	= 0;

	public DHSAvgTest(DHSSketchAlgorithm sketchAlgo, int numVecs, int L, int retries, int replicas,
			Environment env) {
		super(sketchAlgo, numVecs, L, retries, replicas, env);
		this.distAverage = new DistributedAverage(DHSAvgTest.metric, this.dhsImpl);
	}

	public static void main(String[] args) {
		DHSGenericTestParams testParams = new DHSGenericTestParams(DHSAvgTest.class.getCanonicalName(), args);

		long startTime = System.currentTimeMillis(), endTime = 0;

		DHSAvgTest test = null;
		int seed = (int)QuickMath.floor(Math.random() * (double)Integer.MAX_VALUE);
		//int seed = 988569701;

		System.out.println("\n" + " --- Begin Simulation --- ");
		DHSSketchAlgorithm testAlgo = testParams.algorithm;
		double thetas[] = { 0.0, 0.7, 1.0, 1.2 };

		System.out.println(" --- Seed: " + seed + ", domain: " + testParams.numValues
				+ ", Zipf theta: " + testParams.theta + " ---");
		try {
			Environment env = Environment.directEnvironment(seed);
			test = new DHSAvgTest(testAlgo, testParams.numVecs, testParams.L, testParams.retries,
					testParams.replicas, env);
			test.addNodes(testParams.numNodes);
			System.out.println("\n" + " ---" + "\n");
			env.getTimeSource().sleep(5000);

			DistributedAverage testAggregates = new DistributedAverage(DHSAvgTest.metric,
					test.dhsImpl);
			for (int iter = 0; iter < testParams.iterations; iter++) {
				System.out.println("Iteration #" + iter + ". Using Zipf theta: " + testParams.theta);
				ZipfGenerator zipf = new ZipfGenerator(testParams.numValues, testParams.theta,
						env.getRandomSource().nextLong());
				int[] data = new int[testParams.numTuples];
				for (int i = 0; i < testParams.numTuples; i++)
					data[i] = zipf.nextRank();
				test.clear();
				if (testAlgo.isUsingBatchInsertions())
					test.addData(data);
				else
					test.addData(data);

				test.setQNode(env.getRandomSource().nextInt(testParams.numNodes));
				System.out.println("Testing " + testAlgo + ", iteration: " + iter
						+ ", querying node # " + test.getQNode() + " ...");
				test.query();
				System.out.println("\n" + " ---" + "\n");
			}
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: " + (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
		} catch (Exception e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsBatch(netcins.p2p.dhs.DHSPastryAppl, java.util.Vector)
	 */
	@Override
	protected void addItemsBatch(DHSPastryAppl curAppl, Vector<BasicItem> tuples) {
		distAverage.insert(curAppl, tuples);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsCentralized(java.util.Vector)
	 */
	@Override
	protected void addItemsCentralized(Vector<BasicItem> tuples) {
		for (BasicItem tuple : tuples) {
			centralizedSum += tuple.doubleValue();
			centralizedHashSketch.insert(tuple);
			centralizedSumSketch.insert(tuple);
		}
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsNormal(netcins.p2p.dhs.DHSPastryAppl, java.util.Vector)
	 */
	@Override
	protected void addItemsNormal(DHSPastryAppl curAppl, Vector<BasicItem> tuples) {
		for (BasicItem tuple : tuples)
			distAverage.insert(curAppl, tuples);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#clearLocalEstimator()
	 */
	@Override
	public void clearLocalEstimator() {
		this.centralizedHashSketch.clear();
		this.centralizedSumSketch.clear();
		this.centralizedSum = 0;
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#estimateCentralizedEstimator()
	 */
	@Override
	protected double estimateCentralizedEstimator() {
		return centralizedSum / (double)numItems;
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#estimateDistributedEstimator()
	 */
	@Override
	protected DistributedEstimationResult estimateDistributedEstimator(DHSPastryAppl curAppl) {
		distAverage.clearEstimate();
		return distAverage.estimate(curAppl);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#initLocalEstimator(netcins.p2p.dhs.DHSSketchAlgorithm, int, int)
	 */
	@Override
	protected void initLocalEstimator(DHSSketchAlgorithm algorithm, int numVecs, int L) {
		this.centralizedHashSketch = DHSImpl.newBaseSketchInstance(dhsImpl);
		this.centralizedSumSketch = new SummationSketchImpl(this.centralizedHashSketch);
		this.centralizedSum = 0;
	}
}
