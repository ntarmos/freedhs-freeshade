/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.aggregates.testing;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.aggregates.DistributedCount;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSSketchAlgorithm;
import netcins.p2p.testing.DHSGenericTest;
import netcins.p2p.testing.DHSGenericTestParams;
import netcins.util.QuickMath;
import netcins.util.stats.ZipfGenerator;
import rice.environment.Environment;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DHSCountTest extends DHSGenericTest {
	private static final String	metric	= "Single Metric";
	private DistributedCount	distCount;
	private HashSketchImpl		centralizedHashSketch;

	public DHSCountTest(DHSSketchAlgorithm sketchAlgo, int numVecs, int L, int retries,
			int replicas, Environment env) {
		super(sketchAlgo, numVecs, L, retries, replicas, env);
		distCount = new DistributedCount(DHSCountTest.metric, dhsImpl);
	}

	public static void main(String[] args) {
		DHSGenericTestParams testParams = new DHSGenericTestParams(DHSCountTest.class.getCanonicalName(), args);

		long startTime = System.currentTimeMillis(), endTime = 0;

		DHSCountTest test = null;
		int seed = (int)QuickMath.floor(Math.random() * (double)Integer.MAX_VALUE);
		//seed = 1725674223;
		//seed = 1109098675;

		System.out.println("\n" + " --- Begin Simulation --- ");
		ZipfGenerator zipf = new ZipfGenerator(testParams.numValues, testParams.theta, seed);
		int[] data = new int[testParams.numTuples];
		for (int i = 0; i < testParams.numTuples; i++)
			data[i] = zipf.nextRank();

		DHSSketchAlgorithm testAlgo = testParams.algorithm;

		System.out.println(" --- Seed: " + seed + ", domain: " + testParams.numValues
				+ ", Zipf theta: " + testParams.theta + " ---");
		try {
			Environment env = Environment.directEnvironment(seed);
			test = new DHSCountTest(testAlgo, testParams.numVecs, testParams.L, testParams.retries,
					testParams.replicas, env);
			test.addNodes(testParams.numNodes);
			System.out.println("\n" + " ---" + "\n");
			test.clear();
			if (testAlgo.isUsingBatchInsertions())
				test.addDataBatch(data);
			else
				test.addData(data);

			System.out.println("\n" + " ---" + "\n");

			for (int iter = 0; iter < testParams.iterations; iter++) {
				test.setQNode(env.getRandomSource().nextInt(testParams.numNodes));
				System.out.println("Testing " + testAlgo + ", iteration: " + iter
						+ ", querying node # " + test.getQNode() + " ...");
				test.query();
				System.out.println("\n" + " ---" + "\n");
			}
			test.destroy();
			endTime = System.currentTimeMillis();
			System.out.println(" Total Simulation Time: " + (endTime - startTime) / 1000.0 + "\".");
			System.out.println(" --- End Simulation --- ");
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
			if (test != null)
				test.destroy();
			System.exit(-1);
		}
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsBatch(netcins.p2p.dhs.DHSPastryAppl, java.util.Vector)
	 */
	@Override
	protected void addItemsBatch(DHSPastryAppl curAppl, Vector<BasicItem> tuples) {
		distCount.insert(curAppl, tuples);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsCentralized(java.util.Vector)
	 */
	@Override
	protected void addItemsCentralized(Vector<BasicItem> tuples) {
		for (BasicItem tuple : tuples)
			centralizedHashSketch.insert(tuple);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#addItemsNormal(netcins.p2p.dhs.DHSPastryAppl, java.util.Vector)
	 */
	@Override
	protected void addItemsNormal(DHSPastryAppl curAppl, Vector<BasicItem> tuples) {
		for (BasicItem tuple : tuples)
			distCount.insert(curAppl, tuple);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#clearLocalEstimator()
	 */
	@Override
	public void clearLocalEstimator() {
		centralizedHashSketch.clear();
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#estimateCentralizedEstimator()
	 */
	@Override
	protected double estimateCentralizedEstimator() {
		return centralizedHashSketch.estimate();
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#estimateDistributedEstimator()
	 */
	@Override
	protected DistributedEstimationResult estimateDistributedEstimator(DHSPastryAppl curAppl) {
		distCount.clearEstimate();
		return distCount.estimate(curAppl);
	}

	/* (non-Javadoc)
	 * @see netcins.p2p.testing.DHSGenericTest#initLocalEstimator(netcins.p2p.dhs.DHSSketchAlgorithm, int, int)
	 */
	@Override
	protected void initLocalEstimator(DHSSketchAlgorithm algorithm, int numVecs, int L) {
		this.centralizedHashSketch = DHSImpl.newBaseSketchInstance(dhsImpl);
	}
}
