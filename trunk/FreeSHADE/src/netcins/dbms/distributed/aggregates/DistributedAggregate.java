/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.aggregates;

import java.util.Vector;

import netcins.ItemSerializable;
import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.HashSketchImpl;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.dbms.distributed.sketches.NoSuchMetricException;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSTuple;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public abstract class DistributedAggregate {
	protected String					metric		= null;
	protected DHSImpl					dhsImpl		= null;
	protected String					name		= null;
	private DistributedEstimationResult	estimate	= null;
	private HashSketchImpl				hsImpl		= null;

	public String toString() {
		return "[" + name + " of " + metric + " , " + dhsImpl + "]";
	}

	protected DistributedAggregate(String metric, DHSImpl dhsImpl) {
		this(null, metric, dhsImpl);
	}

	protected DistributedAggregate(String name, String metric, DHSImpl dhsImpl) {
		this.name = name;
		this.metric = metric;
		this.dhsImpl = dhsImpl;
		if (dhsImpl != null)
			this.hsImpl = DHSImpl.newBaseSketchInstance(dhsImpl);
	}

	public void setDHSImpl(DHSImpl dhsImpl) {
		this.dhsImpl = dhsImpl;
		this.hsImpl = DHSImpl.newBaseSketchInstance(dhsImpl);
	}

	public void clear() {
		this.dhsImpl.clear();
		this.estimate = null;
		this.hsImpl.clear();
	}

	/**
	 * Computes the estimate of the requested aggregate.
	 * @param sourceNode The querying node
	 * @return The estimated value of the requested aggregate
	 */
	public DistributedEstimationResult estimate(PastryAppl sourceNode) {
		if (estimate != null)
			return estimate;

		Vector<String> metrics = new Vector<String>();
		DistributedEstimationResult result[] = null;
		metrics.add(metric);
		try {
			result = dhsImpl.estimate(sourceNode, metrics);
		} catch (NoSuchMetricException e) {
			e.printStackTrace();
		}
		estimate = result[0];
		return estimate;
	}

	/**
	 * Inserts a BasicItem instance into the aggregate.
	 * @param sourceNode
	 * @param item
	 */
	public void insert(PastryAppl sourceNode, BasicItem item) {
		clearEstimate();
		dhsImpl.insert(sourceNode, metric, transform(sourceNode, item));
	}

	protected abstract BasicItem transform(PastryAppl sourceNode, BasicItem item);

	public void clearEstimate() {
		this.estimate = null;
	}

	protected void setEstimate(double estimate) {
		this.estimate.setDistributedEstimation(estimate);
	}

	/**
	 * Inserts a Vector of BasicItem instances into the aggregate.
	 * @param sourceNode
	 * @param item
	 */
	public void insert(PastryAppl sourceNode, Vector<BasicItem> items) {
		if (items != null && items.size() > 0)
			clearEstimate();

		hsImpl.clear();
		for (BasicItem item : items)
			hsImpl.insert(item);

		for (int bit = hsImpl.getNumBits() - 1; bit >= 0; bit--) {
			Vector<ItemSerializable> tuples = new Vector<ItemSerializable>();
			for (int vec = 0; vec < hsImpl.getNumVecs(); vec++) {
				if (hsImpl.getBit(vec, bit) == true) {
					dhsImpl.batchInsert((DHSPastryAppl)sourceNode, metric, new DHSTuple(metric, vec, bit));
					//tuples.add(new DHSTuple(metric, vec, bit));
				}
			}
			//if (tuples.size() > 0)
			//	dhsImpl.insert(sourceNode, metric, tuples);
		}
		
		dhsImpl.doBatch((DHSPastryAppl)sourceNode);
	}
}
