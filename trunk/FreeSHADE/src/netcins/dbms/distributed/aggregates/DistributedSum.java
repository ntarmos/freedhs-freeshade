/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.aggregates;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.centralized.sketches.SummationSketchImpl;
import netcins.p2p.dhs.DHSImpl;
import netcins.p2p.dhs.DHSPastryAppl;
import netcins.p2p.dhs.DHSTuple;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DistributedSum extends DistributedAggregate {
	private SummationSketchImpl	hsImpl	= null;

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#DistributedAggregate(String, DHSImpl)
	 */
	public DistributedSum(String metric, DHSImpl dhsImpl) {
		super("DistributedSum", "DSUM" + metric, dhsImpl);
		this.hsImpl = new SummationSketchImpl(DHSImpl.newBaseSketchInstance(dhsImpl));
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#clear()
	 */
	@Override
	public void clear() {
		super.clear();
		hsImpl.clear();
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#insert(rice.pastry.client.PastryAppl, netcins.dbms.BasicItem)
	 */
	@Override
	public void insert(PastryAppl sourceNode, BasicItem item) {
		if (item == null)
			return;
		Vector<BasicItem> items = new Vector<BasicItem>();
		items.add(item);
		this.insert(sourceNode, items);
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#insert(PastryAppl, Vector)
	 */
	@Override
	public void insert(PastryAppl sourceNode, Vector<BasicItem> items) {
		if (items == null || items.size() == 0)
			return;

		super.clearEstimate();
		hsImpl.clear();
		for (BasicItem item : items)
			hsImpl.insert(item);

		for (int bit = hsImpl.getNumBits() - 1; bit >= 0; bit--) {
			for (int vec = 0; vec < hsImpl.getNumVecs(); vec++) {
				if (hsImpl.getBit(vec, bit) == true) {
					dhsImpl.batchInsert((DHSPastryAppl)sourceNode, metric, new DHSTuple(metric, vec, bit));
				}
			}
		}
		dhsImpl.doBatch((DHSPastryAppl)sourceNode);
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#transform(rice.pastry.client.PastryAppl, netcins.dbms.BasicItem)
	 */
	@Override
	protected BasicItem transform(PastryAppl sourceNode, BasicItem item) {
		return null;
	}

}
