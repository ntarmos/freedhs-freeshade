/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.aggregates;

import java.util.Vector;

import netcins.dbms.BasicItem;
import netcins.dbms.distributed.sketches.DistributedEstimationResult;
import netcins.p2p.dhs.DHSImpl;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DistributedAverage extends DistributedAggregate {
	DistributedSum		ds	= null;
	DistributedCount	dc	= null;

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#DistributedAggregate(String, DHSImpl)
	 */
	public DistributedAverage(String metric, DHSImpl dhsImpl) {
		super("DistributedAverage", metric, dhsImpl);
		ds = new DistributedSum(metric, dhsImpl);
		dc = new DistributedCount(metric, dhsImpl);
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#insert(rice.pastry.client.PastryAppl, netcins.dbms.BasicItem)
	 */
	@Override
	public void insert(PastryAppl sourceNode, BasicItem item) {
		if (item == null)
			return;
		Vector<BasicItem> items = new Vector<BasicItem>();
		items.add(item);
		this.insert(sourceNode, items);
	}

	protected BasicItem transform(PastryAppl sourceNode, BasicItem item) {
		return null;
	}

	public void insert(PastryAppl sourceNode, Vector<BasicItem> items) {
		if (items != null && items.size() > 0) {
			ds.clear();
			dc.clear();
			ds.insert(sourceNode, items);
			dc.insert(sourceNode, items);
		}
	}

	@Override
	public DistributedEstimationResult estimate(PastryAppl sourceNode) {
		DistributedEstimationResult dsEst = ds.estimate(sourceNode);
		DistributedEstimationResult dcEst = dc.estimate(sourceNode);
		//System.err.println("DistSum: " + dsEst + ", DistCount: " + dcEst);
		dsEst.setDistributedEstimation(dsEst.getDistributedEstimation() / dcEst.getDistributedEstimation());
		dsEst.setHopCount(dsEst.getHopCount() + dcEst.getHopCount());
		dsEst.setNumBitsProbed(dsEst.getNumBitsProbed() + dcEst.getNumBitsProbed());
		dsEst.setMetric(this.metric);
		return dsEst;
	}

}
