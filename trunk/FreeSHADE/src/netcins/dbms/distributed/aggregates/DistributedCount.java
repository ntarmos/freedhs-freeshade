/**
 * (C) 2007-2008, Nikos Ntarmos <ntarmos@ceid.upatras.gr>
 * $Id$
 */

package netcins.dbms.distributed.aggregates;

import netcins.dbms.BasicItem;
import netcins.p2p.dhs.DHSImpl;
import rice.pastry.client.PastryAppl;

/**
 * @author Nikos Ntarmos &lt;ntarmos@ceid.upatras.gr&gt;
 * @version $Id$
 */
public class DistributedCount extends DistributedAggregate {

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#DistributedAggregate(String, DHSImpl)
	 */
	public DistributedCount(String metric, DHSImpl dhsImpl) {
		super("DistributedCount", "DCOUNT" + metric, dhsImpl);
	}

	/* (non-Javadoc)
	 * @see netcins.dbms.distributed.aggregates.DistributedAggregate#transform(netcins.dbms.BasicItem)
	 */
	@Override
	protected BasicItem transform(PastryAppl sourceNode, BasicItem item) {
		return new BasicItem(sourceNode.getNodeId(), item.getKey(), item.getValue());
	}

}
